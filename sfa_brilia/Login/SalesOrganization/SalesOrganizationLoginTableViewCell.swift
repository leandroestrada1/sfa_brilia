//
//  SalesOrganizationLoginTableViewCell.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 16/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class SalesOrganizationLoginTableViewCell: UITableViewCell {
    
    @IBOutlet var salesOrganizationName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
