//
//  LoginSalesOrganizationVC.swift
//  sfa_brilia
//
//  Created by Eduardo Tarallo on 11/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class SalesOrganizationLoginVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var partners: [XITSPDSFAxAPartnersType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadData()
    }
}

extension SalesOrganizationLoginVC {
    
    fileprivate func loadData() {
        OfflineODataAccess.shared.querySalesOrganization { partnersResult in
            OfflineODataAccess.shared.selectedPartner = partnersResult.first
            self.partners = partnersResult
            self.tableView.reloadData()
        }
    }
}

extension SalesOrganizationLoginVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.partners.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "salesOrganizationCell", for: indexPath) as! SalesOrganizationLoginTableViewCell
        
        cell.salesOrganizationName.text = "\(self.partners[indexPath.row].salesOrganizationName ?? "") - \(self.partners[indexPath.row].salesOrganization ?? "")"
        
        return cell
    }
}

extension SalesOrganizationLoginVC: UITableViewDelegate{
    
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        OfflineODataAccess.shared.selectedPartner = self.partners[indexPath.row]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.performOfflineRefresh()
        appDelegate.salesOrganizationSetRootViewController()
    }
}
