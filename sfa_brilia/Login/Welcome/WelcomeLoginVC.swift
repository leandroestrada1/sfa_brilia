//
//  LoginWelcomeViewController.swift
//  sfa_brilia
//
//  Created by Eduardo Tarallo on 11/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPFiori
import SAPFioriFlows
import SAPFoundation

class WelcomeLoginVC: UIViewController {

    
    
    
    weak var fioriScreen: FUIWelcomeScreen?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.largeTitleTextAttributes
        self.title = "Voltar"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    @IBAction func enterPress(_ sender: Any) {
        self.fioriScreen?.primaryActionButton.sendActions(for: .touchUpInside)
    }
}


