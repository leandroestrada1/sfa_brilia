//
//  LoginPartnerVC.swift
//  sfa_brilia
//
//  Created by Eduardo Tarallo on 11/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class PartnerLoginVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrayTuple: [(String, String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension PartnerLoginVC: UITableViewDelegate {
    
}

extension PartnerLoginVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTuple.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "partnerCell", for: indexPath) as? LoginPartnerTableViewCell
        
        cell!.fullNameLb.text = self.arrayTuple[indexPath.row].0
        cell!.partnerNumberLb.text = self.arrayTuple[indexPath.row].1
        
        return cell!
    }
}
