//
//  LoginPartnerTableViewCell.swift
//  sfa_produto
//
//  Created by ITS on 05/04/2019.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class LoginPartnerTableViewCell: UITableViewCell {
    
    @IBOutlet var fullNameLb: UILabel!
    @IBOutlet var partnerNumberLb: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected == true {
            OfflineODataAccess.shared.partnerId = self.partnerNumberLb.text
        }
    }
}
