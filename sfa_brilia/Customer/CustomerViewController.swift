//
//  ViewController.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 17/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class CustomerViewController: UIViewController {

    @IBOutlet weak var tablaView: UITableView!
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var mapsButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuWidthConstraint: NSLayoutConstraint!
    
    fileprivate var customerViewSelected: UIView!
    fileprivate var indexSelected: Int = -1
    fileprivate var customers: [XITSPDSFAxACustomerSalesAreaType] = []
    
    fileprivate var itensMenu = [(image: UIImage(named: "form"), name: "Detalhe do Cliente"),
                                     (image: UIImage(named: "ebook"), name: "Histórico de Ordens de Venda"),
                                     (image: UIImage(named: "invoice"), name: "Histórico de Parcelas"),
                                     (image: UIImage(named: "writing"), name: "Histórico de Ocorrência")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tablaView.delegate = self
        self.tablaView.dataSource = self
        
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.isScrollEnabled = false
        
        self.tablaView.estimatedRowHeight = 145
        
        loadData()
    }
    
    @IBAction func tapMenu(_ sender: Any) {
        if menuWidthConstraint.constant != 0{
            menuWidthConstraint.constant = 0
            UIView.animate(withDuration: 0.3, animations: {self.view.layoutIfNeeded()})
        } else{
            menuWidthConstraint.constant = 320
            UIView.animate(withDuration: 0.2, animations: {self.view.layoutIfNeeded()})
        }
    }
}

//MARK UITABLEDELEGATE
extension CustomerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tablaView {
            if customerViewSelected == nil {
                self.addSubview(row: indexPath.row, customer: customers[indexPath.row])
            } else if indexSelected != indexPath.row {
                self.removeSubview(row: indexPath.row, customer: customers[indexPath.row])
            }
        }
    }
}

//MARK: UITABABLE DATASOURCE
extension CustomerViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tablaView {
            return customers.count
        } else {
            return itensMenu.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tablaView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "customerCell", for: indexPath) as! CustomerTableViewCell
            cell.salesArea = customers[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuTableViewCell
            
            let item = itensMenu[indexPath.row]
            cell.iconMenu.image = item.image
            cell.menuName.text = item.name
            
            return cell
        }
    }
}

//MARK: LOAD DATA
extension CustomerViewController {
    
    fileprivate func loadData() {
        OfflineODataAccess.shared.queryCustomer { salesAreas in
            self.customers = salesAreas.sorted(by: { $0.toXITSPDSFAxACustomerMaster!.organizationBPName1! < $1.toXITSPDSFAxACustomerMaster!.organizationBPName1! })

            self.tablaView.reloadData()
        }
    }
}

//MARK: Control add and remove subview
extension CustomerViewController {
    
    fileprivate func addSubview(row: Int, customer: XITSPDSFAxACustomerSalesAreaType){
        self.indexSelected = row
        let customerDetailView = CustomerDetailView(customer: customer, frame: CGRect(x: 0, y: 0, width: mainView.bounds.size.width, height: mainView.bounds.size.height))
        customerViewSelected = customerDetailView
        mainView.addSubview(customerViewSelected)
    }
    
    fileprivate func removeSubview(row: Int, customer: XITSPDSFAxACustomerSalesAreaType) {
        self.customerViewSelected.removeFromSuperview()
        addSubview(row: row, customer: customer)
    }
}
