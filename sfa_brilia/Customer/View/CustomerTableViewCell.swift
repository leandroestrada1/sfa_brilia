//
//  CustomerTableViewCell.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 17/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {

    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var cnpj: UILabel!
    @IBOutlet private weak var id: UILabel!
    @IBOutlet private weak var address: UILabel!
    @IBOutlet private weak var status: UIView!
    
    var salesArea: XITSPDSFAxACustomerSalesAreaType! {
        didSet {
            
            self.status.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            OfflineODataAccess.shared.queryLastSalesOrder(salesAreaParametro: salesArea) { response in
                if let colorHex = response.colorHex, !(colorHex.isEmpty) {
                    self.status.backgroundColor =  UIColor(hexString: colorHex)
                }
            }
            
            if let customer = salesArea.toXITSPDSFAxACustomerMaster {
                self.name.text = customer.organizationBPName1 ?? ""
                self.address.text = ""
                if let district = customer.district, !district.isEmpty {
                    self.address.text = "\(district), "
                }
                if let cityName = customer.cityName, !cityName.isEmpty {
                    self.address.text?.append(contentsOf: "\(cityName), ")
                }
                self.address.text?.append(contentsOf: "\(customer.region ?? "")")
                self.id.text = Utils().formatRemoveLeadingZeros(salesArea.customer ?? "")
                self.cnpj.text = Utils().formatCnpj(customer.taxNumber1 ?? "")
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected == true {
            self.name.textColor = .white
            self.cnpj.textColor = .white
            self.id.textColor = .white
            self.address.textColor = .white
            self.backgroundColor =  UIColor(named: "SFA_MainColor")
        } else {
            self.name.textColor = .black
            self.cnpj.textColor = .black
            self.id.textColor = .black
            self.address.textColor = .black
            self.backgroundColor = .white
        }
    }
}
