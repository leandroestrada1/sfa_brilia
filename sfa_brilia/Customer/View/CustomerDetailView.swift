//
//  CustomerDetailView.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 18/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class CustomerDetailView: UIView {
    
    @IBOutlet var mainContentView: UIView!
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet var companyNameLabel: UILabel! //
    @IBOutlet var codeLabel: UILabel! //
    @IBOutlet var cnpjLabel: UILabel! //
    @IBOutlet var ieLabel: UILabel!
    @IBOutlet var activityLabel: UILabel! //
    @IBOutlet var paymentConditionLabel: UILabel! //
    @IBOutlet var lastSalesOrderStack: UIStackView!
    @IBOutlet var lastSalesOrderLabel: UILabel!
    @IBOutlet var lastSalesOrderNumberLabel: UILabel!
    @IBOutlet var lastSalesOrderDateLabel: UILabel!
    @IBOutlet var lastSalesOrderPriceLabel: UILabel!
    @IBOutlet var corporateCreditConstraint: NSLayoutConstraint!
    @IBOutlet var contactConstraint: NSLayoutConstraint!
    @IBOutlet var lastInvoiceStack: UIStackView!
    @IBOutlet var lastInvoiceLabel: UILabel!
    @IBOutlet var limitLabel: UILabel!
    @IBOutlet var usedLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel! //
    @IBOutlet var emailLabel: UILabel! //
    @IBOutlet var xmlEmailLabel: UILabel! //
    @IBOutlet var adressLabel: UILabel!
    @IBOutlet var zipCodeLabel: UILabel!
    @IBOutlet var neighborhoodLabel: UILabel!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var prepostoLabel: UILabel!
    @IBOutlet var lastFiscalInvoiceNumberLabel: UILabel!
    @IBOutlet var lastFiscalInvoiceDateLabel: UILabel!
    @IBOutlet var lastFiscalInvoicePriceLabel: UILabel!
    
    var customer: XITSPDSFAxACustomerSalesAreaType!
    let dispatchGroup = DispatchGroup()
    
    init(customer: XITSPDSFAxACustomerSalesAreaType, frame: CGRect) {
        super.init(frame: frame)
        self.customer = customer
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomerDetailView", owner: self, options: nil)
        addSubview(mainContentView)
        mainContentView.frame = self.bounds
        mainContentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        hideLastSalesOrderFields()
        hideLastInvoice()
        getInfoCustomer()
    }
}

//MARK: SET INFO CUSTOMER
extension CustomerDetailView {
    
    fileprivate func showLastSalesOrder(){
        lastSalesOrderLabel.isHidden = true
        lastSalesOrderStack.isHidden = false
        corporateCreditConstraint.constant = 90
    }
    
    fileprivate func hideLastSalesOrderFields(){
        lastSalesOrderLabel.isHidden = false
        lastSalesOrderStack.isHidden = true
        corporateCreditConstraint.constant = 40
    }
    
    fileprivate func showLastInvoice(){
        lastInvoiceLabel.isHidden = true
        lastInvoiceStack.isHidden = false
        contactConstraint.constant = 90
    }
    
    fileprivate func hideLastInvoice(){
        lastSalesOrderLabel.isHidden = false
        lastInvoiceStack.isHidden = true
        contactConstraint.constant = 40
    }
    
    fileprivate func getInfoCustomer() {
        
        let city: String = self.customer.toXITSPDSFAxACustomerMaster?.cityName ?? "No city"
        let state: String = self.customer.toXITSPDSFAxACustomerMaster?.region ?? "No state"
        let street: String = self.customer.toXITSPDSFAxACustomerMaster?.streetName ?? "No street"
        let neighbourhood: String = self.customer.toXITSPDSFAxACustomerMaster?.district ?? "No neighbourhood"
        let companyName: String = self.customer.toXITSPDSFAxACustomerMaster?.organizationBPName1 ?? "No company name"
        let code: String = self.customer.customer ?? "No code"
        let cnpj: String = self.customer.toXITSPDSFAxACustomerMaster?.taxNumber1 ?? "No cnpj"
        let ie: String = self.customer.toXITSPDSFAxACustomerMaster?.taxNumber3 ?? "No ie"
        let phone: String = self.customer.toXITSPDSFAxACustomerMaster?.phoneNumber ?? "No phone"
        let zipCode: String = self.customer.toXITSPDSFAxACustomerMaster?.postalCode ?? "No ZipCode"
        self.colorView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        // Qual a descrição da condição de pagamento do cliente
        self.dispatchGroup.enter()
        OfflineODataAccess.shared.queryForPaymentTermsDisc(id: customer.customerPaymentTerms!) { response in
            self.paymentConditionLabel.text = response
            self.dispatchGroup.leave()
        }
        
        self.dispatchGroup.enter()
        OfflineODataAccess.shared.queryCustomerGroup(id: customer.customerGroup!) { response in
            self.activityLabel.text = response
            self.dispatchGroup.leave()
        }

        self.dispatchGroup.enter()
        OfflineODataAccess.shared.queryLastSalesOrder(salesAreaParametro: customer){ response in
            let lastSalesOrder: XITSPDSFAxACustomerLstSalOrdType = response
            if lastSalesOrder != nil{
                self.showLastSalesOrder()
                self.lastSalesOrderNumberLabel.text = Utils().formatRemoveLeadingZeros(lastSalesOrder.salesOrder!)
                self.lastSalesOrderDateLabel.text = Utils().formatDate(date: (lastSalesOrder.salesOrderDate?.utc())!)
                self.lastSalesOrderPriceLabel.text = Utils().formatCurrency(lastSalesOrder.totalNetAmount!.toString())
            }
            self.dispatchGroup.leave()
        }
        
        //ÚLTIMA NOTA FISCAL
        self.dispatchGroup.enter()
        OfflineODataAccess.shared.queryLastInvoice(lastInvoiceParametro: customer){ response in
            let lastInvoice: XITSPDSFAxACustomerLstInvType = response
            if lastInvoice != nil{
                self.showLastInvoice()
                self.lastFiscalInvoiceNumberLabel.text = Utils().formatRemoveLeadingZeros(lastInvoice.billingDocument!)
                self.lastFiscalInvoiceDateLabel.text = Utils().formatDate(date: (lastInvoice.creationDate?.utc())!)
                self.lastFiscalInvoicePriceLabel.text = Utils().formatCurrency(lastInvoice.brNFTotalAmount!.toString())
            }
            self.dispatchGroup.leave()
        }
        
        self.dispatchGroup.enter()
        OfflineODataAccess.shared.queryLastSalesOrder(salesAreaParametro: customer) { response in
            if let colorHex = response.colorHex, !(colorHex.isEmpty) {
                self.colorView.backgroundColor = UIColor(hexString: colorHex)
            }
            self.dispatchGroup.leave()
        }
        
        //CRÉDITO CORPORATIVO
        let creditLimitAmount = self.customer.toXITSPDSFAxACustomerMaster?.customerCreditLimitAmount
        let creditLimitAmountString = self.customer.toXITSPDSFAxACustomerMaster?.customerCreditLimitAmount?.abs().toString()
        let creditLimitAmount1 =  creditLimitAmount?.abs().doubleValue()
        let creditExposure = self.customer.toXITSPDSFAxACustomerMaster?.creditExposure
        let creditExposure1 = creditExposure?.abs().doubleValue()
        let usedCredit = Int(creditLimitAmount1! - creditExposure1!)
        let usedCreditPercentage = (Double(usedCredit * 100) / (creditLimitAmount1!))
        let formattedUsedCreditPercentage = String(format: "%.2f", usedCreditPercentage)
        let balance = Int(Int(creditLimitAmount1!) - usedCredit)
        let balancePercentage = (Double(creditExposure1! * 100) / creditLimitAmount1!)
        let formattedBalancePercentage = String(format: "%.2f", balancePercentage)
        
        self.limitLabel.text = Utils().formatCurrency(creditLimitAmountString)
        
        if formattedUsedCreditPercentage != "nan"{
            self.usedLabel.text = "\(Utils().formatCurrency(String(usedCredit))) " + "(" + formattedUsedCreditPercentage + "%)"}
        else{
            self.usedLabel.text = "\(Utils().formatCurrency(String(usedCredit))) "
        }
        if formattedBalancePercentage != "nan"{
            self.balanceLabel.text = "\(Utils().formatCurrency(String(balance))) " + "(" + formattedBalancePercentage + "%)"}
        else{
            self.balanceLabel.text = "\(Utils().formatCurrency(String(balance)))"
        }
        
        let email: String = self.customer.toXITSPDSFAxACustomerMaster?.emailAddress ?? "No email"
        let emailXml: String = self.customer.toXITSPDSFAxACustomerMaster?.emailAddress ?? "No email"
        let _: String = self.customer.toXITSPDSFAxACustomerLstSalOrd?.salesOrder ?? "No last sales order"
        
        self.activityLabel.text = self.customer.toXITSPDSFAxACustomerLstSalOrd?.colorHex
        
        self.companyNameLabel.text = companyName
        self.codeLabel.text = Utils().formatRemoveLeadingZeros(code)
        self.cnpjLabel.text = Utils().formatCnpj(cnpj)
        self.ieLabel.text = ie
        self.activityLabel.text = self.customer.toXITSPDSFAxACustomerLstSalOrd?.colorHex
        self.phoneLabel.text = Utils().formatPhone(phone: phone)
        self.emailLabel.text = email
        self.xmlEmailLabel.text = emailXml
        if street != ""{
            self.adressLabel.text = street
        }else{
            self.adressLabel.text = "Sem Logradouro"
        }
        
        self.zipCodeLabel.text = zipCode
        if neighbourhood != ""{
            self.neighborhoodLabel.text = neighbourhood
        }else{
            self.neighborhoodLabel.text = "Sem Bairro"
        }
        self.cityLabel.text = "\(city) / \(state)"
    }
}
