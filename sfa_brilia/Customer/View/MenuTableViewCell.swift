//
//  MenuTableViewCell.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 19/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconMenu: UIImageView!
    @IBOutlet weak var menuName: UILabel!
}
