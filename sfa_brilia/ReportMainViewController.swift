//
//  ReportMainViewController.swift
//  sfa_brilia
//
//  Created by leandro de araujo estrada on 17/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class ReportMainViewController: UIViewController {
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuViewLeading: NSLayoutConstraint!
    @IBOutlet weak var reportDetailView: UIView!
    @IBOutlet weak var reportsDataView: UIView!
    
    
    @IBAction func buttonSyncReport(_ sender: UIButton) {
       reportsDataView?.removeFromSuperview()
      // reportsDataView.addSubview(SyncReportView)
    }

    @IBAction func openMenuButton(_ sender: UIButton) {
        if menuViewLeading.constant != 0{
            menuViewLeading.constant = 0
            UIView.animate(withDuration: 0.3, animations: {self.view.layoutIfNeeded()})
        } else{
            menuViewLeading.constant = -235
            UIView.animate(withDuration: 0.2, animations: {self.view.layoutIfNeeded()})
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        menuView.layer.shadowOpacity = 1
//        menuView.layer.shadowRadius = 6
        // Do any additional setup after loading the view.
    }
}
