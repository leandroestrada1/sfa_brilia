////
//// sfa_produto_brilia
////
//// Created by SAP Cloud Platform SDK for iOS Assistant application on 05/09/19
////
//
//import Foundation
//import SAPCommon
//import SAPFiori
//import SAPFioriFlows
//import SAPFoundation
//import SAPOData
//import SAPOfflineOData
//
//public class OfflineODataController {
//    enum OfflineODataControllerError: Error {
//        case cannotCreateOfflinePath
//        case storeClosed
//    }
//
//    private let logger = Logger.shared(named: "OfflineODataController")
//    var itspdsfaapisrvEntities: ITSPDSFAAPISRVEntities<OfflineODataProvider>!
//    private(set) var isOfflineStoreOpened = false
//
//    public init() {}
//
//    // MARK: - Public methods
//
//    public static func offlineStorePath(for onboardingID: UUID) throws -> URL {
//        guard let documentsFolderURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first else {
//            throw OfflineODataControllerError.cannotCreateOfflinePath
//        }
//        let offlineStoreURL = documentsFolderURL.appendingPathComponent(onboardingID.uuidString)
//        return offlineStoreURL
//    }
//
//    public static func removeStore(for onboardingID: UUID) throws {
//        let offlinePath = try offlineStorePath(for: onboardingID)
//        try OfflineODataProvider.clear(at: offlinePath, withName: nil)
//    }
//
//    // Read more about setting up an application with Offline Store: https://help.sap.com/viewer/fc1a59c210d848babfb3f758a6f55cb1/Latest/en-US/92f0a91d9d3148fd98b86082cf2cb1d5.html
//    public func configureOData(sapURLSession: SAPURLSession, serviceRoot: URL, onboardingID: UUID) throws {
//        var offlineParameters = OfflineODataParameters()
//        offlineParameters.enableRepeatableRequests = true
//
//        // Configure the path of the Offline Store
//        let offlinePath = try OfflineODataController.offlineStorePath(for: onboardingID)
//        try FileManager.default.createDirectory(at: offlinePath, withIntermediateDirectories: true)
//        offlineParameters.storePath = offlinePath
//
//        // Setup an instance of delegate. See sample code below for definition of OfflineODataDelegateSample class.
//        let delegate = OfflineODataDelegateSample()
//        let offlineODataProvider = try! OfflineODataProvider(serviceRoot: serviceRoot, parameters: offlineParameters, sapURLSession: sapURLSession, delegate: delegate)
//        try configureDefiningQueries(on: offlineODataProvider)
//        self.itspdsfaapisrvEntities = ITSPDSFAAPISRVEntities(provider: offlineODataProvider)
//    }
//
//    public func openOfflineStore(synchronize: Bool, completionHandler: @escaping (Error?) -> Void) {
//        if !self.isOfflineStoreOpened {
//            // The OfflineODataProvider needs to be opened before performing any operations.
//            self.itspdsfaapisrvEntities.open { error in
//                if let error = error {
//                    self.logger.error("Could not open offline store.", error: error)
//                    completionHandler(error)
//                    return
//                }
//                self.isOfflineStoreOpened = true
//                self.logger.info("Offline store opened.")
//                if synchronize {
//                    // You might want to consider doing the synchronization based on an explicit user interaction instead of automatically synchronizing during startup
//                    self.synchronize(completionHandler: completionHandler)
//                } else {
//                    completionHandler(nil)
//                }
//            }
//        } else if synchronize {
//            // You might want to consider doing the synchronization based on an explicit user interaction instead of automatically synchronizing during startup
//            self.synchronize(completionHandler: completionHandler)
//        } else {
//            completionHandler(nil)
//        }
//    }
//
//    public func closeOfflineStore() {
//        if self.isOfflineStoreOpened {
//            do {
//                // the Offline store should be closed when it is no longer used.
//                try self.itspdsfaapisrvEntities.close()
//                self.isOfflineStoreOpened = false
//            } catch {
//                self.logger.error("Offline Store closing failed.")
//            }
//        }
//        self.logger.info("Offline Store closed.")
//    }
//
//    // You can read more about data synchnonization: https://help.sap.com/viewer/fc1a59c210d848babfb3f758a6f55cb1/Latest/en-US/59ae11dc4df345bc8073f9da45170706.html
//    public func synchronize(completionHandler: @escaping (Error?) -> Void) {
//        if !self.isOfflineStoreOpened {
//            self.logger.error("Offline Store is still closed")
//            completionHandler(OfflineODataControllerError.storeClosed)
//            return
//        }
//        self.uploadOfflineStore { error in
//            if let error = error {
//                completionHandler(error)
//                return
//            }
//            self.downloadOfflineStore { error in
//                completionHandler(error)
//            }
//        }
//    }
//
//    // MARK: - Private methods
//
//    // Read more about Defining Queries: https://help.sap.com/viewer/fc1a59c210d848babfb3f758a6f55cb1/Latest/en-US/2235da24931b4be69ad0ada82873044e.html
//    private func configureDefiningQueries(on offlineODataProvider: OfflineODataProvider) throws {
//        // Although it is not the best practice, we are defining this query limit as top=20.
//        // If the service supports paging, then paging should be used instead of top!
//        do {
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet).selectAll().top(20), automaticallyRetrievesStreams: true))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH).selectAll().top(20), automaticallyRetrievesStreams: false))
//            try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem).selectAll().top(20), automaticallyRetrievesStreams: false))
//        } catch {
//            self.logger.error("Failed to add defining query for Offline Store initialization", error: error)
//            throw error
//        }
//    }
//
//    private func downloadOfflineStore(completionHandler: @escaping (Error?) -> Void) {
//        // the download function updates the client’s offline store from the backend.
//        self.itspdsfaapisrvEntities.download { error in
//            if let error = error {
//                self.logger.error("Offline Store download failed", error: error)
//            } else {
//                self.logger.info("Offline Store successfully downloaded")
//            }
//            completionHandler(error)
//        }
//    }
//
//    private func uploadOfflineStore(completionHandler: @escaping (Error?) -> Void) {
//        // the upload function updates the backend from the client’s offline store.
//        self.itspdsfaapisrvEntities.upload { error in
//            if let error = error {
//                self.logger.error("Offline Store upload failed.", error: error)
//                completionHandler(error)
//                return
//            }
//            self.logger.info("Offline Store successfully uploaded.")
//            completionHandler(nil)
//        }
//    }
//}
//
//class OfflineODataDelegateSample: OfflineODataDelegate {
//
//    private let logger = Logger.shared(named: "AppDelegateLogger")
//
//    public func offlineODataProvider(_: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
//        self.logger.info("downloadProgress: \(progress.bytesSent)  \(progress.bytesReceived)")
//    }
//
//    public func offlineODataProvider(_: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
//        self.logger.info("downloadProgress: \(progress.bytesReceived)  \(progress.fileSize)")
//    }
//
//    public func offlineODataProvider(_: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
//        self.logger.info("downloadProgress: \(progress.bytesSent)  \(progress.bytesReceived)")
//    }
//
//    public func offlineODataProvider(_: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
//        self.logger.info("requestFailed: \(request.httpStatusCode)")
//    }
//
//    // The OfflineODataStoreState is a Swift OptionSet. Use the set operation to retrieve each setting.
//    private func storeState2String(_ state: OfflineODataStoreState) -> String {
//        var result = ""
//        if state.contains(.opening) {
//            result += ":opening"
//        }
//        if state.contains(.open) {
//            result += ":open"
//        }
//        if state.contains(.closed) {
//            result += ":closed"
//        }
//        if state.contains(.downloading) {
//            result += ":downloading"
//        }
//        if state.contains(.uploading) {
//            result += ":uploading"
//        }
//        if state.contains(.initializing) {
//            result += ":initializing"
//        }
//        if state.contains(.fileDownloading) {
//            result += ":fileDownloading"
//        }
//        if state.contains(.initialCommunication) {
//            result += ":initialCommunication"
//        }
//        return result
//    }
//
//    public func offlineODataProvider(_: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
//        let stateString = storeState2String(newState)
//        self.logger.info("stateChanged: \(stateString)")
//    }
//}
