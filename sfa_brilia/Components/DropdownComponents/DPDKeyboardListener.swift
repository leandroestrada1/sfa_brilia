//
//  KeyboardListener.swift
//  DropDown
//
//  Created by Kevin Hirsch on 30/07/15.
//  Copyright (c) 2015 Kevin Hirsch. All rights reserved.
//

import UIKit

internal final class KeyboardListener {
    static let sharedInstance = KeyboardListener()

    fileprivate(set) var isVisible = false
    fileprivate(set) var keyboardFrame = CGRect.zero
    fileprivate var isListening = false

    deinit {
        stopListeningToKeyboard()
    }
}

// MARK: - Notifications

extension KeyboardListener {
    func startListeningToKeyboard() {
        if self.isListening {
            return
        }

        self.isListening = true

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    func stopListeningToKeyboard() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    fileprivate func keyboardWillShow(_ notification: Notification) {
        self.isVisible = true
        self.keyboardFrame = self.keyboardFrame(fromNotification: notification)
    }

    @objc
    fileprivate func keyboardWillHide(_ notification: Notification) {
        self.isVisible = false
        self.keyboardFrame = self.keyboardFrame(fromNotification: notification)
    }

    fileprivate func keyboardFrame(fromNotification notification: Notification) -> CGRect {
        return ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? CGRect.zero
    }
}
