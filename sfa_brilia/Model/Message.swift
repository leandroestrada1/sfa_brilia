//
//  Message.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 16/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation

class Message {
    var title = "title"
    var id = "id"
    var text = "text"
    var messageDate = "messageDate"
}
