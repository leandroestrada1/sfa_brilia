//
//  URLBase.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 17/05/19.
//  Copyright © 2019 SAP. All rights reserved.
//

struct URLBase {
    
    //: Ambiente Its
//    private static let devUrl = "https://mobile-en1wjg8r0z.hana.ondemand.com/"
//    private static let qasUrl = "https://mobile-dd1vhl2zdl.br1.hana.ondemand.com/"
//    private static let devApplicationId = "ITS_SFA_DEV"
//    private static let qasApplicationId = "ITS_SFA"
    
    
    //: Ambiente Brilia
    private static let devUrl = "https://mobile-bhc4dcg096.br1.hana.ondemand.com/"
    private static let qasUrl = "https://mobile-dd1vhl2zdl.br1.hana.ondemand.com/"
    private static let devApplicationId = "ITS_SFA"
    private static let qasApplicationId = "ITS_SFA"
    
    
    //: Ambiente Herc
//    private static let devUrl = "https://mobile-b2o0ft7xf0.br1.hana.ondemand.com/"
//    private static let qasUrl = "https://mobile-hlq0bkusyf.br1.hana.ondemand.com/"
//    private static let devApplicationId = "ITS_SFA"
//    private static let qasApplicationId = "ITS_SFA"
    

    private static func currentUrl() -> String {
        // NÃO ESQUECER DE ALTERAR ConfigurationProvider.plist
        // Onboardin/ConfigurationProvider.plist
        return URLBase.qasUrl
    }

    static func urlSapSession() -> String {
        return URLBase.currentUrl()
    }

    static func applicationId() -> String {
        if URLBase.currentUrl() == self.devUrl {
            return self.devApplicationId
        } else {
            return self.qasApplicationId
        }
    }

    static func urlSession() -> String {
        return "\(URLBase.currentUrl())\(URLBase.applicationId())"
    }
}
