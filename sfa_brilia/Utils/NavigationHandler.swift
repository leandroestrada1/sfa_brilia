//
//  NavigationHandler.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 29/04/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class NavigationHandler {
    private let appDelegate = UIApplication.shared.delegate as? AppDelegate

    init() {
    }

    func instantiateNavigationController(rootViewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: rootViewController)
        appDelegate?.window?.rootViewController = navigationController
    }

    func setNavHidden(_ hidden: Bool) {
        let nav = self.appDelegate?.window?.rootViewController as? UINavigationController
        nav?.setNavigationBarHidden(hidden, animated: true)
    }

    func pushViewController(_ viewController: UIViewController) {
        let nav = self.appDelegate?.window?.rootViewController as? UINavigationController
        nav?.pushViewController(viewController, animated: true)
    }
}
