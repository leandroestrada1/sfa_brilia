//
//  DateExtension.swift
//  sfa_produto
//
//  Created by Marcio Habigzang Brufatto on 15/08/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation

extension Date {
    func convertToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateStringNew = dateFormatter.string(from: date!)
        
        return dateFormatter.date(from: dateStringNew)!
    }
    
    func convertToString(onFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func dateByAdding(days: Int) -> Date {
        var dateComp = DateComponents()
        dateComp.day = days
        return Calendar.current.date(byAdding: dateComp, to: self)!
    }
    
    
}
