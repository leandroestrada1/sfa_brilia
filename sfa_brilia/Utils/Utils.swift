//
//  Utils.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 31/01/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation

class Utils {
    init() {
    }

    // mask "xx-xx-xx-xx-xx-xx-"
    private func formatMask(numberToFormat: String, mask: String) -> String {
        let number = numberToFormat.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        var result = ""
        var index = number.startIndex
        for ch in mask {
            if index == number.endIndex {
                break
            }
            if ch == "X" {
                result.append(number[index])
                index = number.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }

    func formatCellPhone(phone: String) -> String {
        let mask = "(XX) X XXXX-XXXX"
        return self.formatMask(numberToFormat: phone, mask: mask)
    }

    func formatPhone(phone: String) -> String {
        let mask = "(XX) XXXX-XXXX"
        return self.formatMask(numberToFormat: phone, mask: mask)
    }
    
    func formatCEP(_ cep: String) -> String {
        let mask = "XXXXX-XX"
        return self.formatMask(numberToFormat: cep, mask: mask)
    }

    func formatCpf(cpf: String) -> String {
        let mask = "XXX.XXX.XXX-XX"
        return self.formatMask(numberToFormat: cpf, mask: mask)
    }

    func formatCnpj(_ cnpj: String) -> String {
        let mask = "XX.XXX.XXX/XXXX-XX"
        return self.formatMask(numberToFormat: cnpj, mask: mask)
    }

    func formatRemoveLeadingZeros(_ value: String) -> String {
        return value.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
    }

    func removeTrailingZeros(temp: Double) -> String {
        return String(format: "%g", temp)
    }

    func generateName() -> String {
        let letters = "abcdefghijklmnopqrstuvwxyz"
        let subname = String((0 ... 5).map { _ in letters.randomElement()! })
        return subname
    }

    func generateNumber(size: Int) -> String {
        let numbers = "1234567890"
        let number = String((0 ... size).map { _ in numbers.randomElement()! })
        return number
    }

    func removeMask(_ text: String) -> String {
        return text.replacingOccurrences(of: "[./-]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
    }
    
    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    
    
    func formatCurrency(_ value: String?) -> String {
        guard value != nil else { return "$0,00" }
        let doubleValue = Double(value!) ?? 0.0
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.currencyCode = "BRL"
        formatter.currencySymbol = "R$"
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        let result = formatter.string(from: NSNumber(value: doubleValue)) ?? "R$\(doubleValue)"
        return result
    }
}
