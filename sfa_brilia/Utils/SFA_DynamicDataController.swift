////
////  SFA_DynamicDataController.swift
////  sfa_produto
////
////  Created by ITS on 20/05/2019.
////  Copyright © 2019 SAP. All rights reserved.
////
//
//import Foundation
//import SAPFiori
//import SAPOData
//import SAPOfflineOData
//
//enum Condition: String {
//    case PR00 // Preço
//    case ZACR
//    case ZDES
//    case ZD01 // Desconto e condição de pagamento
//    case IPVA // Rate IPI
//    case IPBS // Base IPI
//    case BCO1 // Rate COFINS
//    case BCO2 // Base COFINS
//    case BPI1 // Rate PIS
//    case BPI2 // Base PIS
//    case ICVA // Rate ICMS
//    case ICBS // Base ICMS
//    case ISTS // Rate ST
//    case ISIC // Base ST
//    case ZU01 // Centros Disponiveis
//    case ZU02 // Tipo de documento
//    case ZU03 // Produto relacionado
//    case ZU04 // Condições de pagamento
//    case ZU05 // Bloqueio de material t
//    case ZU07 // Bloqueio de descontos
//    case ZU08 // Lista de preços
//    case ZU12 // Status cliente
//    case ZU14 // MIX lançamentos
//    case ZU19 // Material da ordem
//    case ZU22 // Mensagem ZBON
//    case ZU23 // Data de remessa
//    case ZBD2
//    case IS_ZU_CONDITION = "isZU"
//    case ZU_COMPARATOR = "ZU"
//}
//
//protocol PropertyNames {
//    func propertyNames(name: String) -> Any?
//}
//
//class SFA_DynamicDataController: PropertyNames {
//    
//    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    private let conditionList: XITSPDSFAxAConditionListType? = nil
//
//    var entities = [CustomersList.shared.selectedSalesArea.customer, CustomersList.shared.selectedSalesArea.customer?.toXITSPDSFAxACustomerMaster, OfflineODataAccess.shared.selectedPartner]
//    
//    var dicEntities = [
//        "xITSPDSFAxA_Partner": "sfa.xITSPDSFAxA_PartnerType",
//        "xITSPDSFAxA_CustomerSalesArea": "sfa.xITSPDSFAxA_CustomerSalesAreaType",
//        "xITSPDSFAxA_CustomerMaster": "sfa.xITSPDSFAxA_CustomerMasterType",
//        "xITSPDSFAxA_ProductMaster": "sfa.xITSPDSFAxA_ProductMasterType",
//    ]
//
//    var dicAtributes = [
//        "SalesOrganization": "SalesOrganization",
//        "DistributionChannel": "DistributionChannel",
//        "CustomerID": "Customer",
//        "Product": "Product",
//        "PriceListType": "PriceListType",
//        "Currency": "Currency",
//    ]
//
//    var outputField: [String?] = []
//
//    init(includeEntities: [EntityValue]) {
//        self.entities = self.entities + includeEntities
//    }
//
//    func conditionLists(conditionType: String, returnValue: @escaping ([XITSPDSFAxAConditionValue], [String?]) -> Void) {
//        self.queryConditionList(conditionType: conditionType) { conditionList in
//            self.getNameSets(conditionType: conditionType, conditionList: conditionList, index: 0) { conditionValues in
//                if !self.outputField.isEmpty {
//                    var values: [String] = []
//                    for conditionValue in conditionValues.enumerated() {
//                        let entityType = conditionValue.element.entityType
//                        for output in self.outputField {
//                            if let property = entityType.propertyMap.value(forKey: output!) {
//                                let dataValue = conditionValue.element.dataValue(for: property)
//                                values.append(dataValue!.toString())
//                            }
//                        }
//                    }
//                    returnValue(conditionValues, values)
//                } else {
//                    returnValue(conditionValues, [])
//                }
//            }
//        }
//    }
//
//    private func valueFor(entity: String, atribute: String) -> String {
//        guard let entityValue = self.dicEntities[entity] else {
//            return ""
//        }
//        guard let atributeValue = self.dicAtributes[atribute] else {
//            return ""
//        }
//        let entitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: entityValue)
//        let entityType = entitySet.entityType
//        guard let property = entityType.propertyMap.value(forKey: atributeValue) else {
//            return ""
//        }
//        var dataValue: DataValue?
//        for entity in self.entities {
//            if entity?.entityType === entityType {
//                dataValue = entity?.dataValue(for: property)
//            }
//        }
//        return dataValue?.toString() ?? ""
//    }
//
//    private func queryConditionList(conditionType: String, _ completion: @escaping ([XITSPDSFAxAConditionListType]) -> Void) {
//        let conditionList = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ConditionListType")
//        let query = DataQuery().from(conditionList).orderBy(XITSPDSFAxAConditionListType.accessSequence).filter(XITSPDSFAxAConditionListType.conditionType.equal(conditionType))
//        appDelegate.sfaEntities?.fetchXITSPDSFAxAConditionList(matching: query) { conditionList, error in
//            if error == nil {
//                completion(conditionList!)
//            } else {
//                Alert(title: "Erro ao buscar lista de condição", message: "\(String(describing: error))").showSimpleAlert()
//            }
//        }
//    }
//
//    private func queryNameSet(conditionType: String, accessSequence: String, _ completion: @escaping ([XITSPDSFAxANameSetType]) -> Void) {
//        let nameSet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_NameSetType")
//        let query = DataQuery().from(nameSet).orderBy(XITSPDSFAxANameSetType.accessSequence)
//            .filter(XITSPDSFAxANameSetType.accessSequence.equal(accessSequence))
//            .filter(XITSPDSFAxANameSetType.conditionType.equal(conditionType))
//        appDelegate.sfaEntities?.fetchXITSPDSFAxANameSet(matching: query) { nameSets, error in
//            if error == nil {
//                completion(nameSets!)
//            } else {
//                Alert(title: "Erro nos Name Sets", message: "\(String(describing: error))").showSimpleAlert()
//            }
//        }
//    }
//
//    private func getNameSets(conditionType: String, conditionList: [XITSPDSFAxAConditionListType], index: Int, _ completion: @escaping ([XITSPDSFAxAConditionValue]) -> Void) {
//        
//        self.queryNameSet(conditionType: conditionType, accessSequence: conditionList[index].accessSequence!, { nameSets in
//            
//            var fields: [String: String] = [:]
//            self.outputField.removeAll()
//            for nameSet in nameSets {
//                let fieldValue = self.valueFor(entity: nameSet.entity!, atribute: nameSet.atribute!)
//                if nameSet.isOutPut != "" {
//                    if let fieldNumber = nameSet.oDataField {
//                        self.outputField.append(fieldNumber)
//                    }
//                    if fieldValue != "" {
//                        fields[nameSet.oDataField!] = fieldValue
//                    }
//                } else {
//                    if fieldValue != "" {
//                        fields[nameSet.oDataField!] = fieldValue
//                    }
//                }
//            }
//
//            self.queryConditionValue(accessSequence: conditionList[index].accessSequence!, conditionType: conditionType, fields: fields) { conditionValues in
//                if (conditionValues?.isEmpty)! {
//                    if conditionList.count > index {
//                        self.getNameSets(conditionType: conditionType, conditionList: conditionList, index: index + 1) { result in
//                            completion(result)
//                        }
//                    }
//                } else {
//                    completion(conditionValues!)
//                }
//            }
//        })
//    }
//
//    func queryConditionValue(accessSequence: String, conditionType: String, fields: [String: String], _ completion: @escaping ([XITSPDSFAxAConditionValue]?) -> Void) {
//        let conditionValue = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ConditionValue")
//        let entityType = conditionValue.entityType
//        var query = DataQuery().from(conditionValue).filter(XITSPDSFAxAConditionValue.conditionType.equal(conditionType).and(XITSPDSFAxAConditionValue.accessSequence.equal(accessSequence)))
//        for (key, value) in fields {
//            let property = entityType.property(withName: key)
//            query = query.filter(property.equal(value))
//        }
//        self.appDelegate.sfaEntities?.fetchXITSPDSFAxAConditionValueSet(matching: query) { conditionValues, error in
//            if error == nil {
//                completion(conditionValues)
//            } else {
//                completion([])
//                Alert(title: "Erro ao buscar condition values", message: "\(String(describing: error))").showSimpleAlert()
//            }
//        }
//    }
//}
//
//extension PropertyNames {
//    func propertyNames(name: String) -> Any? {
//        for mir in Mirror(reflecting: self).children {
//            if mir.label == name {
//                return mir.value
//            }
//        }
//        return nil
//    }
//}
