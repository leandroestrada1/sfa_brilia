//
//  DefiningQueries.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 31/05/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import SAPCommon
import SAPFiori
import SAPFioriFlows
import SAPFoundation
import SAPOData
import SAPOfflineOData
import UserNotifications

extension AppDelegate {
    func configureOData(_ urlSession: SAPURLSession, _ serviceRoot: URL, _ onboarding: Bool) {
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        // Setup an instance of delegate. See sample code below for definition of OfflineODataDelegateSample class.
        
        let delegate = OfflineODataDelegateSample()
        let offlineODataProvider = try! OfflineODataProvider(serviceRoot: serviceRoot, parameters: offlineParameters, sapURLSession: urlSession, delegate: delegate)
        if onboarding {
            do {
                if let partnerId = OfflineODataAccess.shared.partnerId {
                    
                    /* Hierarchy */
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name:
                        ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2), automaticallyRetrievesStreams: false))
                    
                    /* Hierarchy */
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3), automaticallyRetrievesStreams: false))
                    
                    
                    /* Dynamic Controller */
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet), automaticallyRetrievesStreams: false))

                    // Center Supplier
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier.localName, query:
                        DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier), automaticallyRetrievesStreams: false))

                    /* Partners */
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners).filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))

                    /* Customer */
                    // Customer Master
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster), automaticallyRetrievesStreams: false))

                    // Sales Area
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea).filter(XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))
                    
                    // Latitude and Longitude
                   try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet), automaticallyRetrievesStreams: false))

                    /* Customer Documents */
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument).filter(XITSPDSFAxABillingDocumentType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications).filter(XITSPDSFAxANotificationsType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText), automaticallyRetrievesStreams: false))
                    
                    /*Customer Ship To*/
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo), automaticallyRetrievesStreams: false))
                    
                    /* Incoterms */
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms), automaticallyRetrievesStreams: false))
                    
                    /* Sales Order */
                    // Sales Orders Itens
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders).filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd).filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens).filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(partnerId)), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv
                    ), automaticallyRetrievesStreams: false))

                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice
                    ), automaticallyRetrievesStreams: false))
                    
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy
                    ), automaticallyRetrievesStreams: false))
                    
                    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules.localName, query: DataQuery().from(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules
                    ), automaticallyRetrievesStreams: false))
                    
                } else {
                    Alert(title: "Baixando banco sem filtro de parceiros", message: "Não foi possível fazer as defining queries com o parceiro.").showSimpleAlert()
                }
            } catch {
                self.logger.error("Failed to add defining query for Offline Store initialization", error: error)
            }
        }
        self.sfaEntities = ITSPDSFAAPISRVEntities(provider: offlineODataProvider) // SfaEntities(provider: offlineODataProvider)
    }
}
