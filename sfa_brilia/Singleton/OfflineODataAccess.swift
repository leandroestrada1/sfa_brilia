//
//  DashboardDataAccess.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 14/01/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation
import SAPFiori
import SAPOData
import SAPOfflineOData

class OfflineODataAccess {
    
    static let shared = OfflineODataAccess()

    private let appDelegate = UIApplication.shared.delegate as! AppDelegate

    private init() {
    }

    private var isPartnerId = false

    public var partners: [(String, String)] = []

    public var selectedPartner: XITSPDSFAxAPartnersType?

    public var partnerId: String? {
        didSet {
            if self.isPartnerId == false {
                OnboardingManager.shared.continueOnboarding()
                self.isPartnerId = true
            }
        }
    }

    /*
     01 - Banner principal
     02 - Banner de apoio
     03 - Lista de mensagens
    */
    func queryMessagesList(completion _: @escaping ([Message]) -> Void) {
        //        let messages =  SfaEntitiesMetadataParser.parsed.entitySet(withName: "Messages")
        //        let query = DataQuery().from(messages).filter(Message.type_.equal("03")).orderBy(Message.messageDate, SortOrder.descending)
        //        appDelegate.sfaContainer?.fetchMessages(matching: query) { messages, error in
        //            if error == nil {
        //                completion(messages!)
        //            } else {
        //                print("Erro no resultado da query de mensagens")
        //            }
        //        }
    }

    func queryBanner(type _: String, completion _: @escaping (UIImage) -> Void) {
        //        let messages = SfaEntitiesMetadataParser.parsed.entitySet(withName: "Messages")
        //        let query = DataQuery().from(messages).filter(Message.type_.equal(type)).orderBy(Message.messageDate, SortOrder.descending).top(1).expand(Message.documentDetails)
        //        appDelegate.sfaContainer?.fetchMessages(matching: query) { messages, error in
        //            if error == nil {
        //                if let imageData = messages?.first?.documentDetails?.content, let image = UIImage(data: imageData) {
        //                    completion(image)
        //                }
        //            }
        //        }
    }

    func queryMessage(type _: String, completion _: @escaping (Message) -> Void) {
        //        let messages = SfaEntitiesMetadataParser.parsed.entitySet(withName: "Messages")
        //        let query = DataQuery().from(messages).filter(Message.type_.equal(type)).orderBy(Message.messageDate, SortOrder.descending).top(1)
        //        appDelegate.sfaContainer?.fetchMessages(matching: query) { messages, error in
        //            if error == nil {
        //                completion((messages?.first)!)
        //            } else {
        //                print("Erro no resultado da query de mensagens")
        //            }
        //        }
    }

    func queryImage(messageId _: String, completion _: @escaping (UIImage) -> Void) {
        //        let messages = SfaEntitiesMetadataParser.parsed.entitySet(withName: "Messages")
        //        let query = DataQuery().from(messages).filter(Message.id.equal(messageId)).orderBy(Message.messageDate, SortOrder.descending).top(1).expand(Message.documentDetails)
        //        appDelegate.sfaContainer?.fetchMessages(matching: query) { messages, error in
        //            if error == nil {
        //                if let imageData = messages?.first?.documentDetails?.content, let image = UIImage(data: imageData) {
        //                    completion(image)
        //                }
        //            }
        //        }
    }

    //    try offlineODataProvider.add(definingQuery: OfflineODataDefiningQuery(name: SfaEntitiesMetadata.EntitySets.zsfaCustomerMaster.localName, query: DataQuery().from(SfaEntitiesMetadata.EntitySets.zsfaCustomerMaster), automaticallyRetrievesStreams: false))

    func queryCustomersMaster(customerId: String = "", completion: @escaping ([XITSPDSFAxACustomerMasterType]) -> Void) {
        let customersMaster = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerMasterType")
        let query = DataQuery().from(customersMaster)
        if customerId != "" {
            query.filter(XITSPDSFAxACustomerMasterType.customer.equal(customerId))
        }
        
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerMaster(matching: query) { customers, error in
            if error == nil {
                completion(customers!)
            }
        }
    }

    // TODO: CustomerSalesAreaInfo filtrar por SalesOrganization e BPCustomerNumber
    func queryCustomerSalesAreaInfo(completion _: @escaping ([XITSPDSFAxACustomerMasterType]) -> Void) {
        //        let customerSalesAreaInfos = SfaEntitiesMetadataParser.parsed.entitySet(withName: "CustomerSalesAreaInfos")
        //        let query = DataQuery().from(customerSalesAreaInfos)
        //        var customersArray: [CustomerSalesAreaInfo] = []
        //        appDelegate.sfaContainer?.fetchCustomerSalesAreaInfos(matching: query) { customerResult, error in
        //            if error == nil {
        //                if let customers = customerResult {
        //                    for customer in customers {
        //                        if let customerID = customer.customerID {
        //                            let customerMainInfos = SfaContainerMetadataParser.parsed.entitySet(withName: "CustomerMainInfos")
        //                            let query2 = DataQuery().from(customerMainInfos).filter(CustomerMainInfo.customer.equal(customerID))
        //                            self.appDelegate.sfaContainer.fetchCustomerMainInfos(matching: query2) { customerMainInfos, _ in
        //                                let customerToArray = customer
        //                                if customerMainInfos != nil {
        //                                    customerToArray.toCustomerMainInfo = customerMainInfos?.first
        //                                    customersArray.append(customerToArray)
        //                                }
        //                            }
        //                            let zaCustomerLastSalesOrder = SfaContainerMetadataParser.parsed.entitySet(withName: "ZA_CUSTOMER_LAST_SALESORDER")
        //                            let query3 = DataQuery() // .from(zaCustomerLastSalesOrder).filter(ZaCustomerLastSalesorder.customer.equal(customerID))
        ////                            self.appDelegate.sfaContainer.fetchZaCustomerLastSalesorder(matching: query3) { zaCustomerLastSalesOrder, _ in
        ////                                let customerToArray = customer
        ////                                if customerToArray != nil, let zaCustomer = zaCustomerLastSalesOrder {
        ////                                    customerToArray.toZaCustomerLastSalesorder = zaCustomer
        ////                                    customersArray.append(customerToArray)
        ////                                }
        ////                            }
        //                        }
        //                    }
        ////                    completion(customers)
        //                }
        //            }
        //        }
    }

    func queryCustomerMainInfo(customers _: [XITSPDSFAxACustomerMasterType]) {
        //        DispatchQueue.global(qos: .background).async {
        //            for customer in customers {
        //                if let customerID = customer.customerID {
        //                    let customerMainInfos = SfaContainerMetadataParser.parsed.entitySet(withName: "CustomerMainInfos")
        //                    let query = DataQuery().from(customerMainInfos).filter(CustomerMainInfo.customer.equal(customerID))
        //                    self.appDelegate.sfaContainer.fetchCustomerMainInfos(matching: query) { customerMainInfos, _ in
        //                        if customerMainInfos != nil {
        //                            customer.toCustomerMainInfo = customerMainInfos?.first
        //                        }
        //                    }
        //                }
        //            }
        //        }
    }

    func queryZaCustomerLastOrder(customers _: [XITSPDSFAxACustomerMasterType]) {
        //        DispatchQueue.global(qos: .background).async {
        //            for customer in customers {
        //                let zaCustomerLastSalesOrder = SfaContainerMetadataParser.parsed.entitySet(withName: "ZA_CUSTOMER_LAST_SALESORDER")
        //                if let customerID = customer.customerID {
        //                    let query = DataQuery() // .from(zaCustomerLastSalesOrder).filter(ZaCustomerLastSalesorder.customer.equal(customerID))
        //                    self.appDelegate.sfaContainer?.fetchZaCustomerLastSalesorder(matching: query) { zaCustomerLastSalesOrder, _ in
        //                        if let zaCustomer = zaCustomerLastSalesOrder {
        //                            customer.toZaCustomerLastSalesorder = zaCustomer
        //                        }
        //                    }
        //                }
        //            }
        //        }
    }

    //            }

    func queryProductMainInfos(completion: @escaping ([XITSPDSFAxAProductMasterType]) -> Void) {
        let products = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ProductMasterType")
        let query = DataQuery().from(products)
        appDelegate.sfaEntities?.fetchXITSPDSFAxAProductMaster(matching: query) { zsfaProductsTypeResult, error in
            if error == nil {
                completion(zsfaProductsTypeResult!)
            }
        }
    }

    func queryProductDocumentDetails(productId _: String, completion _: @escaping (XITSPDSFAxADocumentsType) -> Void) {
        let products = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_DocumentsType")
        let query = DataQuery().from(products).expand(XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments)
        appDelegate.sfaEntities.fetchXITSPDSFAxAProductMaster(matching: query) { _, _ in
//            if error == nil {
//                if let documentDetails = documents {
//                    //completion(documentDetails)
//                }
//            }
        }
    }

    func queryAllSFASalesOrder(customer: XITSPDSFAxACustomerMasterType, completion: @escaping ([XITSPDSFAxASalesOrdersType]) -> Void) {
        let sfaSalesOrders = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        let query: DataQuery = DataQuery().from(sfaSalesOrders).filter(XITSPDSFAxASalesOrdersType.soldToParty.equal(customer.customer ?? ""))
        appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrders(matching: query) { ordersResult, error in
            if error == nil {
                completion(ordersResult ?? [])
            }
        }
    }

    func queryBillingDocument(customer: XITSPDSFAxACustomerSalesAreaType, completion: @escaping ([XITSPDSFAxABillingDocumentType]) -> Void) {
        let billingDocument = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_BillingDocumentType")
        let query: DataQuery = DataQuery().from(billingDocument)
            .filter(XITSPDSFAxABillingDocumentType.salesOrganization.equal(customer.salesOrganization ?? ""))
            .filter(XITSPDSFAxABillingDocumentType.bpCustomerNumber.equal(customer.bpCustomerNumber ?? ""))
            .filter(XITSPDSFAxABillingDocumentType.soldToParty.equal(customer.customer ?? ""))
            .filter(XITSPDSFAxABillingDocumentType.distributionChannel.equal(customer.distributionChannel ?? ""))
        appDelegate.sfaEntities?.fetchXITSPDSFAxABillingDocument(matching: query) { billingResults, error in
            if error == nil {
                if let result = billingResults {
                    completion(result)
                }
            }
        }
    }

    func queryNotifications(customer: XITSPDSFAxACustomerSalesAreaType, completion: @escaping ([XITSPDSFAxANotificationsType]) -> Void) {
        let notifications = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_NotificationsType")
        let query: DataQuery = DataQuery().from(notifications)
            .filter(XITSPDSFAxANotificationsType.salesOrganization.equal(customer.salesOrganization ?? ""))
            .filter(XITSPDSFAxANotificationsType.distributionChannel.equal(customer.distributionChannel ?? ""))
            .filter(XITSPDSFAxANotificationsType.bpCustomerNumber.equal(customer.bpCustomerNumber ?? ""))
            .filter(XITSPDSFAxANotificationsType.customer.equal(customer.customer ?? ""))
            .expand(XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat)
        appDelegate.sfaEntities?.fetchXITSPDSFAxANotifications(matching: query) { notificationsResult, error in
            if error == nil {
                if let result = notificationsResult {
                    completion(result)
                }
            }
        }
    }
    
    func queryCustomer(customerId: String = "", completion: @escaping ([XITSPDSFAxACustomerSalesAreaType]) -> Void) {
        let sArea = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerSalesAreaType")
        let query = DataQuery().from(sArea)
        if customerId != "" {
            query.filter(XITSPDSFAxACustomerSalesAreaType.customer.equal(customerId))
        }
        query.expand(XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText)
        
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerSalesArea(matching: query) { sareas, error in
            if error == nil {
                var salesAreas: [XITSPDSFAxACustomerSalesAreaType] = []
                var i = 0
                for salesArea in sareas! {
                    i = i + 1
                    let customerMaster = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerMasterType")
                    let query2 = DataQuery().from(customerMaster).filter(XITSPDSFAxACustomerMasterType.customer.equal(salesArea.customer!)).top(1)
                   
                    self.appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerMaster(matching: query2) { customerMaster, error in
                        if error == nil {
                            salesArea.toXITSPDSFAxACustomerMaster = customerMaster?.first!
                            salesAreas.append(salesArea)
                            completion(salesAreas)
                        }
                    }
                }
            }
        }
    }

    func querySalesAreaInfo(customerId: String = "", completion: @escaping ([XITSPDSFAxACustomerSalesAreaType]) -> Void) {
        let sArea = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerSalesAreaType")
        let query = DataQuery().from(sArea)
        if customerId != "" {
            query.filter(XITSPDSFAxACustomerSalesAreaType.customer.equal(customerId))
        }
        query.expand(XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText)
        
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerSalesArea(matching: query) { sareas, error in
            if error == nil {
                var salesAreas: [XITSPDSFAxACustomerSalesAreaType] = []
                var iterator = 0
                for salesArea in (sareas?.enumerated())! {
                    let customerMaster = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerMasterType")
                    let query2 = DataQuery().from(customerMaster).filter(XITSPDSFAxACustomerMasterType.customer.equal(salesArea.element.customer!)).top(1)
//                    let query3 = DataQuery().from(customerMaster).filter(XITSPDSFAxACustomerLstSalOrdType.customer.equal(salesArea.element.customer!)).top(1)
                    self.appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerMaster(matching: query2) { customerMaster, error in
                        if error == nil {
                            salesArea.element.toXITSPDSFAxACustomerMaster = customerMaster?.first!
                            salesAreas.append(salesArea.element)
                            iterator += 1
                            if iterator == (sareas?.count)! - 1 {
                                print("Sales: ==========================================")
                                print(salesAreas)
                                print("Quantidade: \(salesAreas.count)")
                                print("Sales: ==========================================")
                                completion(salesAreas)
                            }
                        }
                    }
                }
            }
        }
    }

    func queryCurrentOrderForCustomer(customer: XITSPDSFAxACustomerSalesAreaType, completion: @escaping ([XITSPDSFAxASalesOrdersType]) -> Void) {
        let orders = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        let query = DataQuery().from(orders)
            .filter(XITSPDSFAxASalesOrdersType.overallSDProcessStatus.equal("99"))
            .filter(XITSPDSFAxASalesOrdersType.soldToParty.equal(customer.customer!))
            .filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(customer.bpCustomerNumber!))
            .filter(XITSPDSFAxASalesOrdersType.distributionChannel.equal(customer.distributionChannel!))
            .filter(XITSPDSFAxASalesOrdersType.salesOrganization.equal(customer.salesOrganization!))
            .filter(XITSPDSFAxASalesOrdersType.organizationDivision.equal(customer.division!))
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms)
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice)
        appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrders(matching: query) { ordersResult, error in
            if error == nil {
                if let order = ordersResult?.first {
                    self.queryExpandOrderItens(itens: (order.toXITSPDSFAxASalesOrdersItens), completion: { (itens) in
                        ordersResult?.first!.toXITSPDSFAxASalesOrdersItens = itens
                        completion(ordersResult!)
                    })
                } else {
                    completion(ordersResult!)
                }
            }
        }
    }
    
    func queryExpandOrderItens(itens: [XITSPDSFAxASalesOrdersItensType], completion: @escaping ([XITSPDSFAxASalesOrdersItensType]) -> Void) {
        let dispatchExpandedOrder = DispatchGroup()
        var itensReturn: [XITSPDSFAxASalesOrdersItensType] = []
        let orderItens = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersItensType")
        for it in itens {
            dispatchExpandedOrder.enter()
            let query = DataQuery().from(orderItens).filter(XITSPDSFAxASalesOrdersItensType.salesOrderItem.equal(it.salesOrderItem!))
                .expand(XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster)
            
            appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrdersItens(matching: query) { itens, error in
                if error == nil {
                    itensReturn.append((itens?.first)!)
                    dispatchExpandedOrder.leave()
                } else {
                    dispatchExpandedOrder.leave()
                }
            }
        }
        dispatchExpandedOrder.notify(queue: .main) {
            completion(itensReturn)
        }
        
    }
    
    
    func querySalesOrdersItens(order: XITSPDSFAxASalesOrdersType, completion: @escaping ([XITSPDSFAxASalesOrdersType]) -> Void) {
        let orders = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        let query = DataQuery().from(orders)
            .filter(XITSPDSFAxASalesOrdersType.salesOrder.equal(order.salesOrder!))
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens)
        appDelegate.sfaEntities.fetchXITSPDSFAxASalesOrders(matching: query) { orders, error in
            if error == nil {
                completion(orders!)
            }
        }
    }
    
    func querySalesOrdersTaxes(order: XITSPDSFAxASalesOrdersType, completion: @escaping ([XITSPDSFAxASalesAmountsType]) -> Void) {
        let orders = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        let query = DataQuery().from(orders)
            .filter(XITSPDSFAxASalesOrdersType.salesOrder.equal(order.salesOrder!))
            .expand(XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts)
        appDelegate.sfaEntities.fetchXITSPDSFAxASalesOrders(matching: query) { taxes, error in
            if error == nil {
                completion((taxes?.first?.toXITSPDSFAxASalesAmounts)!)
            }
        }
    }
    
    func querySalesOrderItemTaxes(orderItem: XITSPDSFAxASalesOrdersItensType, completion: @escaping ([XITSPDSFAxASalesOrdItPriceType]) -> Void) {
        let ordersItens = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersItensType")
        let query = DataQuery().from(ordersItens)
        .filter(XITSPDSFAxASalesOrdersItensType.salesOrderItem.equal(orderItem.salesOrderItem!))
        .expand(XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice)
        appDelegate.sfaEntities.fetchXITSPDSFAxASalesOrdersItens(matching: query) { taxes, error in
            if error == nil {
                completion((taxes?.first?.toXITSPDSFAxASalesOrdItPrice)!)
            }
        }
    }
    
    func queryItemPrice(item: XITSPDSFAxASalesOrdersItensType, completion: @escaping ([XITSPDSFAxASalesOrdItPriceType]) -> Void) {
         let items = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersItensType")
         let query = DataQuery().from(items)
            .filter(XITSPDSFAxASalesOrdersItensType.salesOrderItem.equal(item.salesOrderItem!))
            .expand(XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice)
        appDelegate.sfaEntities.fetchXITSPDSFAxASalesOrdersItens(matching: query) { taxes, error in
            if error == nil {
                completion((taxes?.first?.toXITSPDSFAxASalesOrdItPrice)!)
            }
        }
    }
    
    func queryOrdersforCustomer(customer: XITSPDSFAxACustomerSalesAreaType, completion: @escaping ([XITSPDSFAxASalesOrdersType]) -> Void) {
        let orders = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        let query: DataQuery = DataQuery().from(orders)
            .filter(XITSPDSFAxASalesOrdersType.soldToParty.equal(customer.customer ?? ""))
            .filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(customer.bpCustomerNumber ?? ""))
        appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrders(matching: query) { ordersResult, error in
            if error == nil {
                completion(ordersResult!)
//                if let result = ordersResult?.first {
//                    let orderItems = SfaContainerMetadataParser.parsed.entitySet(withName: "OrderItems")
//                    let query2 = DataQuery().from(orderItems).filter(Order.mobileID.equal(result.mobileID!))
//                    self.appDelegate.sfaContainer?.fetchOrderItems(matching: query2) { itemsResult, error in
//                        if error == nil {
//                            ordersResult?.first?.orderItemDetails = itemsResult!
//                        }
//                        completion(ordersResult!)
//                    }
//                } else {
//                    completion([])
//                }
            }
        }
    }

    // Pegar o sales organization
    func querySalesOrganization(completion: @escaping ([XITSPDSFAxAPartnersType]) -> Void) {
        let partner = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_PartnersType")
        let query: DataQuery = DataQuery().from(partner)
        appDelegate.sfaEntities?.fetchXITSPDSFAxAPartners(matching: query) { zsfaPartnersTypeResult, error in
            if error == nil {
                completion(zsfaPartnersTypeResult!)
            } else {
                Alert(title: "Erro ao buscar Sales Organization", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }

    
    // Pega os centros
    func queryCenterList(valuesOfCenters: [String], completion: @escaping ([XITSPDSFAxACenterSupplierType]) -> Void) {
        let centerSupplier = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CenterSupplierType")
        let query = DataQuery().from(centerSupplier)
        var queryFilter: QueryFilter?
        for valueOfCenter in valuesOfCenters {
            if queryFilter == nil {
                queryFilter = QueryOperator.or(XITSPDSFAxACenterSupplierType.plant.equal(valueOfCenter), XITSPDSFAxACenterSupplierType.plant.equal(valueOfCenter))
            } else {
                queryFilter = queryFilter?.or(XITSPDSFAxACenterSupplierType.plant.equal(valueOfCenter))
            }
        }
        query.filter(queryFilter!)
        self.appDelegate.sfaEntities?.fetchXITSPDSFAxACenterSupplier(matching: query) { value, error in
            if error == nil {
                completion(value!)
            } else {
                Alert(title: "Erro na queryCenterList", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    //:: Pega o plant filtrado pelo centro
    func queryPlantOfCenter(selectedCentro: String, complition: @escaping (String) -> Void) {
        let hierarchy = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CenterSupplierType")
        let query: DataQuery = DataQuery().from(hierarchy).filter(XITSPDSFAxACenterSupplierType.plantName.equal(selectedCentro))
        appDelegate.sfaEntities.fetchXITSPDSFAxACenterSupplier(matching: query) { response, error in
            if error == nil {
                complition(response?.first?.plant ?? "")
            } else {
                Alert(title: "Erro na queryProductHierarchyNode", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    func queryPaymentTerms(paymentTermsIds: [String], completion: @escaping ([XITSPDSFAxAPaymentTermsType]) -> Void) {
        let paymentTerms = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_PaymentTermsType")
        let query = DataQuery().from(paymentTerms)
        var queryFilter: QueryFilter?
        for id in paymentTermsIds {
            if queryFilter == nil {
                queryFilter = QueryOperator.or(XITSPDSFAxAPaymentTermsType.paymentTerms.equal(id), XITSPDSFAxAPaymentTermsType.paymentTerms.equal(id))
            } else {
                queryFilter = queryFilter?.or(XITSPDSFAxAPaymentTermsType.paymentTerms.equal(id))
            }
        }
        query.filter(queryFilter!)
        self.appDelegate.sfaEntities?.fetchXITSPDSFAxAPaymentTerms(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                Alert(title: "Erro nas condições de pagamento", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }

    func querySalesDocType(docTypes: [String], completion: @escaping ([XITSPDSFAxASalesDocTypeType]) -> Void) {
        let salesDocType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesDocTypeType")
        let query = DataQuery().from(salesDocType)
        var queryFilter: QueryFilter?
        for doc in docTypes {
            if queryFilter == nil {
                queryFilter = QueryOperator.or(XITSPDSFAxASalesDocTypeType.salesDocumentType.equal(doc), XITSPDSFAxASalesDocTypeType.salesDocumentType.equal(doc))
            } else {
                queryFilter = queryFilter?.or(XITSPDSFAxASalesDocTypeType.salesDocumentType.equal(doc))
            }
        }
        query.filter(queryFilter!)
        self.appDelegate.sfaEntities?.fetchXITSPDSFAxASalesDocType(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                Alert(title: "Erro ao buscar Sales Doc Type", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    func queryForPaymentTerms() {
        // Analisando a entidade indicada
        let paymentTerms = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerSalesAreaType")
        let query: DataQuery =
            DataQuery().from(paymentTerms).filter(XITSPDSFAxACustomerSalesAreaType.customer.equal("159")).filter(XITSPDSFAxACustomerSalesAreaType.salesOrganization.equal("1000")).filter(XITSPDSFAxACustomerSalesAreaType.distributionChannel.equal("10")).filter(XITSPDSFAxACustomerSalesAreaType.division.equal("10")).filter(XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber.equal("153"))

        appDelegate.sfaEntities.fetchXITSPDSFAxACustomerSalesAreaType(matching: query) { result, error in
            if error == nil {
                if let result1 = result {
                    //print(result1.customerPaymentTerms)
                }
            }
        }
    }
    
    func queryLastSalesOrder(salesAreaParametro: XITSPDSFAxACustomerSalesAreaType, completion: @escaping (XITSPDSFAxACustomerLstSalOrdType) -> Void){
        let customerGroup = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerLstSalOrdType")
        //Tirando de customer group e aplicando filtros
        let queryForLastSalesOrder: DataQuery = DataQuery().from(customerGroup)
            .filter(XITSPDSFAxACustomerSalesAreaType.customer.equal(salesAreaParametro.customer!))
            .filter(XITSPDSFAxACustomerSalesAreaType.salesOrganization.equal(salesAreaParametro.salesOrganization!))          .filter(XITSPDSFAxACustomerSalesAreaType.distributionChannel.equal(salesAreaParametro.distributionChannel!))
            .filter(XITSPDSFAxACustomerSalesAreaType.division.equal(salesAreaParametro.division!))
            .filter(XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber.equal(salesAreaParametro.bpCustomerNumber!))
        
        appDelegate.sfaEntities.fetchXITSPDSFAxACustomerLstSalOrd(matching: queryForLastSalesOrder) { result, error in
            if error == nil {
                if let result1 = result {
                    print(result1.description)
                    if result1.count > 0{
                        completion(result1.first!)
                    }
                }
            }
        }
    }
    
    func queryLastInvoice(lastInvoiceParametro: XITSPDSFAxACustomerSalesAreaType, completion: @escaping (XITSPDSFAxACustomerLstInvType) -> Void){
        let lastInvoice1 = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerLstInvType")
        
        let queryForLastInvoice: DataQuery = DataQuery().from(lastInvoice1)
        .filter(XITSPDSFAxACustomerSalesAreaType.customer.equal(lastInvoiceParametro.customer!))
        .filter(XITSPDSFAxACustomerSalesAreaType.salesOrganization.equal(lastInvoiceParametro.salesOrganization!))
        .filter(XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber.equal(lastInvoiceParametro.bpCustomerNumber!))
        
        appDelegate.sfaEntities.fetchXITSPDSFAxACustomerLstInv(matching: queryForLastInvoice) { result, error in
            if error == nil{
                if let result1 = result {
                    if result1.count > 0{
                        completion(result1.first!)
                    }
                }
            }
        }
    }
    
    // Busca a descrição da condição de pagamento
    // Parâmetro busca a descrição da condição de pagamento cujo paymentTerms é igual ao Id
    func queryForPaymentTermsDisc(id: String, completion: @escaping (String) -> Void) {
            let paymentTermsDisc = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_PaymentTermsType")
        let theQuery: DataQuery = DataQuery().from(paymentTermsDisc).filter(XITSPDSFAxAPaymentTermsType.paymentTerms.equal(id))

        appDelegate.sfaEntities.fetchXITSPDSFAxAPaymentTerms(matching: theQuery) { result, error in
            if error == nil {
                if let result1 = result {
                    for i in result1 {
                        // Quando completar retorna esse valor
                        completion(i.paymentTermsConditionDesc!)
                    }
                }
            }
        }
    }
    
    func queryCustomerGroup(completion: @escaping ([XITSPDSFAxACustomerGrpTextType]) -> Void){
        let customerGroup = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerGrpTextType")
        let queryForCustomerGroup: DataQuery = DataQuery().from(customerGroup)
        
        appDelegate.sfaEntities.fetchXITSPDSFAxACustomerGrpText(matching: queryForCustomerGroup) { result, error in
            if error == nil{
                if let groups = result{
                    completion(groups)
                }
            }
        }
    }
    
    func queryCustomerGroup(id: String, completion: @escaping (String) -> Void){
        let customerGroup = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerGrpTextType")
        let queryForCustomerGroup: DataQuery = DataQuery().from(customerGroup).filter(XITSPDSFAxACustomerGrpTextType.customerGroup.equal(id))
        
        appDelegate.sfaEntities.fetchXITSPDSFAxACustomerGrpText(matching: queryForCustomerGroup) {result, error in
            if error == nil{
                if let result1 = result{
                    for i in result1{
                        completion(i.customerGroupName!)
                    }
                }
            }
        }
    }

    //:: Pega as famílias
    func queryFamilyList(complitionFamily: @escaping ([String]) -> Void) {
        var familyList: [String] = []
        let family = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ProductHieLevel3Type")
        let query: DataQuery = DataQuery().from(family)
        appDelegate.sfaEntities.fetchXITSPDSFAxAProductHieLevel3(matching: query) { result, error in
            if error == nil {
                if let family = result {
                    for name in family {
                        if let family = name.productHierarchyNodeText {
                            familyList.append(family)
                        }
                    }
                } else {
                    Alert(title: "Nenhuma Família encontrada", message: "").showSimpleAlert()
                }
            } else {
                Alert(title: "Erro na queryFamilyList", message: "\(String(describing: error))").showSimpleAlert()
            }
            complitionFamily(familyList)
        }
    }
    
    
    //:: Pega o productHierarchyNode filtrado pela família
    func queryProductHieLevel3(selectedFamily: String, complition: @escaping (String) -> Void) {
        let hierarchy = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ProductHieLevel3Type")
        let query: DataQuery = DataQuery().from(hierarchy).filter(XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText.equal(selectedFamily))
        appDelegate.sfaEntities.fetchXITSPDSFAxAProductHieLevel3(matching: query) { response, error in
            if error == nil {
                complition(response?.first?.productHierarchyNode ?? "")
            } else {
                Alert(title: "Erro na queryProductHierarchyNode", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }

    //:: Pega as categorias
    func queryCategoryList(complitionCategory: @escaping ([String]) -> Void) {
        var categoryList: [String] = []
        let category = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ProductHieLevel2Type")
        let query: DataQuery = DataQuery().from(category)
        appDelegate.sfaEntities.fetchXITSPDSFAxAProductHieLevel2(matching: query) { result, error in
            if error == nil {
                if let category = result {
                    for name in category {
                        if let category = name.productHierarchyNodeText {
                            categoryList.append(category)
                        }
                    }
                } else {
                    Alert(title: "Nenhuma Família encontrada", message: "").showSimpleAlert()
                }
            } else {
                Alert(title: "Erro na queryFamilyList", message: "\(String(describing: error))").showSimpleAlert()
            }
            complitionCategory(categoryList)
        }
    }
    
    //Pega o productHierarchyNode filtrado pela categoria
    func queryProductHieLevel2(selectedCategory: String, complition: @escaping (String) -> Void) {
        let hierarchy = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_ProductHieLevel2Type")
        let query: DataQuery = DataQuery().from(hierarchy).filter(XITSPDSFAxAProductHieLevel2Type.productHierarchyNodeText.equal(selectedCategory))
        appDelegate.sfaEntities.fetchXITSPDSFAxAProductHieLevel2(matching: query) { response, error in
            if error == nil {
                complition(response?.first?.productHierarchyNode ?? "")
            } else {
                Alert(title: "Erro na queryProductHierarchyNode", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    //:: Pega as imagens dos produtos
    func queryProductImages(productReference: String, complitionImage: @escaping (UIImage) -> Void) {
        let images = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_DocumentsType")
        let query: DataQuery = DataQuery().from(images).filter(XITSPDSFAxADocumentsType.linkedSAPObject.equal("MARA")).filter(XITSPDSFAxADocumentsType.linkedSAPObjectKey.equal(productReference))
        appDelegate.sfaEntities?.fetchXITSPDSFAxADocuments(matching: query) { result, error in
            if error == nil {
                if let data = result?.first?.reportContentMedia {
                    if let image = UIImage(data: data) {
                        complitionImage(image)
                    }
                }
            }
        }
    }
    
    func queryLatitudeAndLongitude(customers: [String], completion: @escaping ([XITSPDSFAxACustomerLatLong]) -> Void){
        let long = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerLatLong")
        let query = DataQuery().from(long)
        //mudar o customer para array de customer id
        //let strings = ["123123", "1232123", "1231223"]
//        for customer in customers {
        //let queryFilter = query.filter(XITSPDSFAxACustomerLatLong.customer.equal("0000000159"))
//        }
            //.filter(XITSPDSFAxACustomerLatLong.customer.equal(customer))
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerLatLongSet(matching: query) { result, error in
            if error == nil {
//                if let long = result.first?.longitude{
//
//                }
//                let latitude = result?.first?.latitude //123.3872618;2361.131
//                let latlong = latitude?.split(separator: ";") // [1231.12, 123.123]
//                let lat = latlong![0] //12312.12
//                let long = latlong![1] // 123.123
//                print(lat/*, long*/)
                completion(result!)
            }
        }
        
    }
    
    func queryLatitudeAndLongitudeWhenTouched(customers: [String], completion: @escaping ([XITSPDSFAxACustomerLatLong]) -> Void){
        let long = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerLatLong")
        let query = DataQuery().from(long)
        var queryFilter: QueryFilter?
        for customer in customers{
            if queryFilter == nil {
                queryFilter = QueryOperator.or(XITSPDSFAxACustomerLatLong.customer.equal(customer), XITSPDSFAxACustomerLatLong.customer.equal(customer))
            } else {
                queryFilter = queryFilter?.or(XITSPDSFAxACustomerLatLong.customer.equal(customer))
            }
        }
        query.filter(queryFilter!)
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerLatLongSet(matching: query) { result, error in
            if error == nil {
                completion(result!)
            }else{
                print(error.debugDescription)
            }
        }
    }
    
        //Completion para armazenar e levar para outra classe(Memória)
    func querySalesOrdersHistory(salesOrder: String, salesOrganization: String, distributionChannel: String, organizationDivision: String, soldToParty: String, bpCustomer: String, completion: @escaping([Any]) -> Void) {
        //Referência a entidade que vai ser buscada
        let ordersHistory = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        //Criação de array de tipo Any, que vai ser enviado para outra classe
        var orderHistoryList: [Any] = []
        //Criação da query com filtros chaves, conforme oData
        let query = DataQuery().from(ordersHistory)
                .filter(XITSPDSFAxASalesOrdersType.salesOrder.equal(salesOrder))
                .filter(XITSPDSFAxASalesOrdersType.salesOrganization.equal(salesOrganization))
                .filter(XITSPDSFAxASalesOrdersType.distributionChannel.equal(distributionChannel))
                .filter(XITSPDSFAxASalesOrdersType.organizationDivision.equal(organizationDivision))
                .filter(XITSPDSFAxASalesOrdersType.soldToParty.equal(soldToParty))
                .filter(XITSPDSFAxASalesOrdersType.bpCustomerNumber.equal(bpCustomer))
        //Fetch para trazer da query a resposta, nesse caso result
        appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrders(matching: query) { result, error in
            if error == nil {
                    if let date = result?.first?.salesOrderDate{
                        orderHistoryList.insert(date, at: 0)
                    }
                    if let numeroDocumento = result?.first?.salesOrder{
                        orderHistoryList.insert(numeroDocumento, at: 1)
                    }
                    if let ordemDeCompra = result?.first?.purchaseOrderByCustomer{
                        orderHistoryList.insert(ordemDeCompra, at: 2)
                    }
                    if let totalValue = result?.first?.totalNetAmount{
                        orderHistoryList.insert(totalValue, at: 3)
                    }
                    if let tipoDeDocumento = result?.first?.salesOrderType{
                        orderHistoryList.insert(tipoDeDocumento, at: 4)
                    }
                    if let status = result?.first?.overallTotDelivStatusColorDesc{
                        orderHistoryList.insert(status, at: 5)
                    }
                    if let color = result?.first?.overallTotDelivStatusColorHex{
                        orderHistoryList.insert(color, at: 6)
                    }
                    
                    completion(orderHistoryList)
                
                }
            }
        }
    
    func queryCustomerShipTo(completion: @escaping([XITSPDSFAxACustomerShipToType]) -> Void) {
        let customerShip = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_CustomerShipToType")
        let query = DataQuery().from(customerShip)
        appDelegate.sfaEntities?.fetchXITSPDSFAxACustomerShipTo(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                Alert(title: "Erro ao buscar ship to", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    func queryIncoterms(completion: @escaping([XITSPDSFAxAIncoTermsType]) -> Void) {
        let incoterms = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_IncoTermsType")
        let query = DataQuery().from(incoterms)
        appDelegate.sfaEntities?.fetchXITSPDSFAxAIncoTerms(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                Alert(title: "Erro ao buscar Incoterms", message: "\(String(describing: error))").showSimpleAlert()
            }
        }
    }
    
    func queryPriceLists(completion: @escaping ([XITSPDSFAxAPricelistsType]) -> Void){
        let priceList = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_PricelistsType")
        let query: DataQuery = DataQuery().from(priceList)
        appDelegate.sfaEntities.fetchXITSPDSFAxAPricelists(matching: query) { result, error in
            if error == nil {
                completion(result!)
            }
        }
    }
    
    func queryAnalyticOrders(customer: String = "", startDate: Date, endDate: Date, completion: @escaping ([XITSPDSFAxASalesOrdersType]) -> Void) {
        let query: DataQuery = DataQuery()
        var queryString = "xITSPDSFAxA_SalesOrders?$expand=to_xITSPDSFAxA_SalesDocType&$filter="
        if customer != "" {
            queryString.append("CustomerName eq \'\(customer)\' and")
        }
        
        let startDateForQuery = startDate.convertToString(onFormat: "yyyy-MM-dd'T'hh:mm:ss")
        let endDateForQuery = endDate.convertToString(onFormat: "yyyy-MM-dd'T'hh:mm:ss")
        
        queryString.append(" CreationDate le datetime\'\(endDateForQuery)\' and CreationDate ge datetime\'\(startDateForQuery)\'")
        
        query.queryString = queryString
    
        appDelegate.sfaEntities?.fetchXITSPDSFAxASalesOrders(matching: query){ result, error in
            if error == nil {
                completion(result!)
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    func queryAnalyticInvoice(customer: String = "", paymentStatus: String = "", startDate: Date, endDate: Date, completion: @escaping ([XITSPDSFAxABillingDocumentType]) -> Void){
        
        let query: DataQuery = DataQuery()
        var queryString = "xITSPDSFAxA_BillingDocument?$filter="
        let startingDateForQuery = startDate.convertToString(onFormat: "yyyy-MM-dd'T'hh:mm:ss")
        let endingDateForQuery = endDate.convertToString(onFormat: "yyyy-MM-dd'T'hh:mm:ss")

        queryString.append("NetDueDate le datetime\'\(endingDateForQuery)\' and NetDueDate ge datetime\'\(startingDateForQuery)\'")
        
        if customer != ""{
         queryString.append("and CustomerName eq \'\(customer)\'")
        }
        
        if paymentStatus != "" {
            queryString.append(" and StatusColorDesc eq \'\(paymentStatus)\'")
        }
        
        
        query.queryString = queryString
        
        appDelegate.sfaEntities.fetchXITSPDSFAxABillingDocument(matching: query){ result, error in
            if error == nil {
                completion(result!)
            } else {
                print(error?.localizedDescription)
            }
        }
        
    }
    
    func queryAnalyticFiscalInvoice(completion: @escaping ([XITSPDSFAxAInvoiceType]) -> Void) {
        
        let invoices = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_InvoiceType")
        let query: DataQuery = DataQuery().from(invoices)
        appDelegate.sfaEntities.fetchXITSPDSFAxAInvoice(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    func queryCustomerCenter(completion: @escaping ([XITSPDSFAxAHierarchyType]) -> Void) {
        let centers = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_HierarchyType")
        let query: DataQuery = DataQuery().from(centers)
        appDelegate.sfaEntities.fetchXITSPDSFAxAHierarchy(matching: query) { result, error in
            if error == nil {
                completion(result!)
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    func queryCustomerType(bpCustomerNumber: String, salesOrganization: String, completion: @escaping ([XITSPDSFAxAHierarchyType]) -> Void) {
        let centers = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_HierarchyType")
        let query: DataQuery = DataQuery().from(centers)
            .filter(XITSPDSFAxAHierarchyType.parent.equal(bpCustomerNumber))
            .filter(XITSPDSFAxAHierarchyType.organization.equal(salesOrganization))
        appDelegate.sfaEntities.fetchXITSPDSFAxAHierarchy(matching: query) { centers, error in
            if error == nil {
                completion(centers!)
            } else {
                print("Error ao buscar os centros - \(error?.localizedDescription)")
            }
        }
    }
    
    
    
    func queryBusinessRules(type: String, name: String, completion: @escaping (XITSPDSFAxABusinessRulesType) -> Void) {
    
        let rules = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "sfa.xITSPDSFAxA_BusinessRulesType")
        
        let query = DataQuery().from(rules)
            .filter(XITSPDSFAxABusinessRulesType.tipo.equal(type))
            .filter(XITSPDSFAxABusinessRulesType.nome.equal(name))
            .top(1)
            .select(XITSPDSFAxABusinessRulesType.valor)
        
        appDelegate.sfaEntities.fetchXITSPDSFAxABusinessRules(matching: query) { rule, error in
            if error == nil {
                completion(rule!.count > 0 ? rule![0] : XITSPDSFAxABusinessRulesType())
            } else {
                print(error?.localizedDescription)
            }
        }
    }
}
