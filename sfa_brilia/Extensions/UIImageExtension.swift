//
//  UIImageExtension.swift
//  sfa_brilia
//
//  Created by Marcio Habigzang Brufatto on 17/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func imageResize (sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
}
