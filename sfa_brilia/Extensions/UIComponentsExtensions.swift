//
//  UIComponentsExtensions.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 20/03/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

extension UIView {
    func deselectView() {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func selectView() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 2
    }
    
    func shadowsView() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: -3, height: 3)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 1
        layer.masksToBounds = false
    }
    
    func showPopUp() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = appDelegate.window?.rootViewController
        self.frame = (vc?.view.frame)!
        vc?.view.addSubview(self)
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

class Alert {
    var title: String
    var message: String
    
    init(title: String, message: String) {
        self.title = title
        self.message = message
    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: self.title, message: self.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert(completion: @escaping () -> Void) {
        let alert = UIAlertController(title: self.title, message: self.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion()
        }))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showDatePicker(completion: @escaping (String) -> Void) {
        let alertView = UIAlertController(title: self.title, message: self.message, preferredStyle: .alert)
        
        let pickerView = UIDatePicker(frame:
            CGRect(x: 0, y: 50, width: 260, height: 162))
        pickerView.datePickerMode = .date
        pickerView.locale = Locale(identifier: "pt_BR")
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        alertView.view.addSubview(pickerView)
        let action = UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
            case .default:
                completion(Utils().formatDate(date: pickerView.date))
                break
            case .cancel:
                break
            case .destructive:
                break
                
            } })
        let action2 = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertView.addAction(action)
        alertView.addAction(action2)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
            pickerView.isHidden = false
        })
    }
    
    func showDatePicker(minimumDate: Date, completion: @escaping (String, Date) -> Void) {
        let alertView = UIAlertController(title: self.title, message: self.message, preferredStyle: .alert)
        
        let pickerView = UIDatePicker(frame:
            CGRect(x: 0, y: 50, width: 260, height: 162))
        pickerView.datePickerMode = .date
        pickerView.minimumDate = minimumDate
        pickerView.locale = Locale(identifier: "pt_BR")
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        alertView.view.addSubview(pickerView)
        let action = UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
            case .default:
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                completion(dateFormatter.string(from: pickerView.date), pickerView.date)
                break
            case .cancel:
                break
            case .destructive:
                break
                
            } })
        let action2 = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertView.addAction(action)
        alertView.addAction(action2)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
            pickerView.isHidden = false
        })
    }
}

