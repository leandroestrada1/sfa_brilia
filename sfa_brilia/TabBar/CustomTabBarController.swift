//
//  CustomTabBarController.swift
//  sfa_brilia
//
//  Created by Eduardo Tarallo on 11/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    @IBOutlet var customTabBarView: UIView!
    @IBOutlet weak var clientView: UIView!
    @IBOutlet weak var catalogView: UIView!
    @IBOutlet weak var salesView: UIView!
    @IBOutlet weak var reportsView: UIView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var orderItems: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customTabBarView.frame.size.width = self.view.frame.width
        self.view.addSubview(customTabBarView)
    }
    
    
    @IBAction func changeTab(_ sender: UIButton) {
        if sender.tag != self.selectedIndex {
            let viewSelected = customTabBarView.viewWithTag(sender.tag)
            self.updateView(view: viewSelected!)
            let view = customTabBarView.viewWithTag(self.selectedIndex)
            if view != nil {
                self.deselectedView(view: view!)
            }
            self.selectedIndex = sender.tag
        }
    }
    
    func updateView(view: UIView) {
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 2
        view.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func deselectedView(view: UIView) {
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.clear.cgColor
    }
    

}
