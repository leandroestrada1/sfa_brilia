// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

internal class ITSPDSFAAPISRVEntitiesMetadataParser {
    internal static let options: Int = (CSDLOption.allowCaseConflicts | CSDLOption.disableFacetWarnings | CSDLOption.disableNameValidation | CSDLOption.processMixedVersions | CSDLOption.retainOriginalText | CSDLOption.ignoreUndefinedTerms)

    internal static let parsed: CSDLDocument = ITSPDSFAAPISRVEntitiesMetadataParser.parse()

    static func parse() -> CSDLDocument {
        let parser = CSDLParser()
        parser.logWarnings = false
        parser.csdlOptions = ITSPDSFAAPISRVEntitiesMetadataParser.options
        let metadata = parser.parseInProxy(ITSPDSFAAPISRVEntitiesMetadataText.xml, url: "sfa")
        metadata.proxyVersion = "19.1.2-c24a64-20190318"
        return metadata
    }
}
