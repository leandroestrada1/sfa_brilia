// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

internal class ITSPDSFAAPISRVEntitiesMetadataChanges {
    static func merge(metadata: CSDLDocument) {
        metadata.hasGeneratedProxies = true
        ITSPDSFAAPISRVEntitiesMetadata.document = metadata
        ITSPDSFAAPISRVEntitiesMetadataChanges.merge1(metadata: metadata)
        ITSPDSFAAPISRVEntitiesMetadataChanges.merge2(metadata: metadata)
        ITSPDSFAAPISRVEntitiesMetadataChanges.merge3(metadata: metadata)
        try! ITSPDSFAAPISRVEntitiesFactory.registerAll()
    }

    private static func merge1(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue = metadata.complexType(withName: "sfa.ParameterValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue = metadata.complexType(withName: "sfa.PricingValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue = metadata.complexType(withName: "sfa.TaxValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType = metadata.entityType(withName: "sfa.A_CustSalesPartnerFuncType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent = metadata.entityType(withName: "sfa.AttachmentContent")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType = metadata.entityType(withName: "sfa.I_BillingDocumentBasicStdVHType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType = metadata.entityType(withName: "sfa.I_CompanyCodeStdVHType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType = metadata.entityType(withName: "sfa.I_Customer_VHType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType = metadata.entityType(withName: "sfa.SalesOrderWithoutChargeItemPartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing = metadata.entityType(withName: "sfa.SalesOrderWithoutChargeItemPricing")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType = metadata.entityType(withName: "sfa.SalesOrderWithoutChargeItemType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType = metadata.entityType(withName: "sfa.SalesOrderWithoutChargePartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing = metadata.entityType(withName: "sfa.SalesOrderWithoutChargePricing")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype = metadata.entityType(withName: "sfa.SalesOrderWithoutChargeScheduleLinetype")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType = metadata.entityType(withName: "sfa.SalesOrderWithoutChargeType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType = metadata.entityType(withName: "sfa.SalesQuotationItemPartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType = metadata.entityType(withName: "sfa.SalesQuotationItemPricingType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType = metadata.entityType(withName: "sfa.SalesQuotationItensType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType = metadata.entityType(withName: "sfa.SalesQuotationPartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType = metadata.entityType(withName: "sfa.SalesQuotationPricingType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType = metadata.entityType(withName: "sfa.SalesQuotationsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType = metadata.entityType(withName: "sfa.SyncEventType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType = metadata.entityType(withName: "sfa.SyncOperationStatusType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType = metadata.entityType(withName: "sfa.xITSPDSFAxA_BillingDocumentType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType = metadata.entityType(withName: "sfa.xITSPDSFAxA_BusinessRulesType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CenterSupplierType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType = metadata.entityType(withName: "sfa.xITSPDSFAxA_ConditionListType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType = metadata.entityType(withName: "sfa.xITSPDSFAxA_ConditionScaleType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue = metadata.entityType(withName: "sfa.xITSPDSFAxA_ConditionValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerEmailXMLType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerGrpTextType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerLatLong")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerLstInvType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerLstSalOrdType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerMasterType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerSalesAreaType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType = metadata.entityType(withName: "sfa.xITSPDSFAxA_CustomerShipToType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_DocumentsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType = metadata.entityType(withName: "sfa.xITSPDSFAxA_HierarchyType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_IncoTermsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType = metadata.entityType(withName: "sfa.xITSPDSFAxA_InvoiceType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType = metadata.entityType(withName: "sfa.xITSPDSFAxA_MsgPrtnrType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType = metadata.entityType(withName: "sfa.xITSPDSFAxA_MsgType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType = metadata.entityType(withName: "sfa.xITSPDSFAxA_NameSetType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType = metadata.entityType(withName: "sfa.xITSPDSFAxA_NotificationsStatType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_NotificationsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PartnerLastSalesType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PartnerUserType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PartnersType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PaymentTermsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_PricelistsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel1Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel2Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel3Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel4Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel5Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel6Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel7Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel8Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel9Type")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductMasterType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType = metadata.entityType(withName: "sfa.xITSPDSFAxA_ProductReqMRPType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote = metadata.entityType(withName: "sfa.xITSPDSFAxA_QMNote")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesAmountsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesDocTypeType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesItemAmountsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdItPriceType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrderPartnerType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersItensType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersObs")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersObsN")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersResnsType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType = metadata.entityType(withName: "sfa.xITSPDSFAxA_SoldVolumeType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType = metadata.entityType(withName: "sfa.xITSPDSFAxA_TaxJurisdictionType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType = metadata.entityType(withName: "sfa.xITSPDSFAxA_TaxListType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType = metadata.entityType(withName: "sfa.xITSPDSFAxA_TaxNamesetType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType = metadata.entityType(withName: "sfa.xITSPDSFAxA_TaxValueType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc = metadata.entitySet(withName: "A_CustSalesPartnerFunc")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet = metadata.entitySet(withName: "AttachmentContentSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH = metadata.entitySet(withName: "I_BillingDocumentBasicStdVH")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH = metadata.entitySet(withName: "I_CompanyCodeStdVH")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH = metadata.entitySet(withName: "I_Customer_VH")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge = metadata.entitySet(withName: "SalesOrderWithoutCharge")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem = metadata.entitySet(withName: "SalesOrderWithoutChargeItem")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner = metadata.entitySet(withName: "SalesOrderWithoutChargeItemPartner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings = metadata.entitySet(withName: "SalesOrderWithoutChargeItemPricings")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner = metadata.entitySet(withName: "SalesOrderWithoutChargePartner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings = metadata.entitySet(withName: "SalesOrderWithoutChargePricings")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine = metadata.entitySet(withName: "SalesOrderWithoutChargeScheduleLine")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner = metadata.entitySet(withName: "SalesQuotationItemPartner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing = metadata.entitySet(withName: "SalesQuotationItemPricing")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens = metadata.entitySet(withName: "SalesQuotationItens")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner = metadata.entitySet(withName: "SalesQuotationPartner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing = metadata.entitySet(withName: "SalesQuotationPricing")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations = metadata.entitySet(withName: "SalesQuotations")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent = metadata.entitySet(withName: "SyncEvent")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus = metadata.entitySet(withName: "SyncOperationStatus")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument = metadata.entitySet(withName: "xITSPDSFAxA_BillingDocument")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules = metadata.entitySet(withName: "xITSPDSFAxA_BusinessRules")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier = metadata.entitySet(withName: "xITSPDSFAxA_CenterSupplier")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList = metadata.entitySet(withName: "xITSPDSFAxA_ConditionList")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale = metadata.entitySet(withName: "xITSPDSFAxA_ConditionScale")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet = metadata.entitySet(withName: "xITSPDSFAxA_ConditionValueSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML = metadata.entitySet(withName: "xITSPDSFAxA_CustomerEmailXML")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText = metadata.entitySet(withName: "xITSPDSFAxA_CustomerGrpText")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet = metadata.entitySet(withName: "xITSPDSFAxA_CustomerLatLongSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv = metadata.entitySet(withName: "xITSPDSFAxA_CustomerLstInv")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd = metadata.entitySet(withName: "xITSPDSFAxA_CustomerLstSalOrd")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster = metadata.entitySet(withName: "xITSPDSFAxA_CustomerMaster")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea = metadata.entitySet(withName: "xITSPDSFAxA_CustomerSalesArea")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo = metadata.entitySet(withName: "xITSPDSFAxA_CustomerShipTo")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments = metadata.entitySet(withName: "xITSPDSFAxA_Documents")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy = metadata.entitySet(withName: "xITSPDSFAxA_Hierarchy")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms = metadata.entitySet(withName: "xITSPDSFAxA_IncoTerms")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice = metadata.entitySet(withName: "xITSPDSFAxA_Invoice")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg = metadata.entitySet(withName: "xITSPDSFAxA_Msg")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr = metadata.entitySet(withName: "xITSPDSFAxA_MsgPrtnr")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet = metadata.entitySet(withName: "xITSPDSFAxA_NameSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications = metadata.entitySet(withName: "xITSPDSFAxA_Notifications")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat = metadata.entitySet(withName: "xITSPDSFAxA_NotificationsStat")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner = metadata.entitySet(withName: "xITSPDSFAxA_Partner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales = metadata.entitySet(withName: "xITSPDSFAxA_PartnerLastSales")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser = metadata.entitySet(withName: "xITSPDSFAxA_PartnerUser")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners = metadata.entitySet(withName: "xITSPDSFAxA_Partners")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms = metadata.entitySet(withName: "xITSPDSFAxA_PaymentTerms")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists = metadata.entitySet(withName: "xITSPDSFAxA_Pricelists")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel1")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel2")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel3")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel4")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel5")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel6")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel7")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel8")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9 = metadata.entitySet(withName: "xITSPDSFAxA_ProductHieLevel9")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster = metadata.entitySet(withName: "xITSPDSFAxA_ProductMaster")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet = metadata.entitySet(withName: "xITSPDSFAxA_ProductReqMRPSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet = metadata.entitySet(withName: "xITSPDSFAxA_QMNoteSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts = metadata.entitySet(withName: "xITSPDSFAxA_SalesAmounts")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType = metadata.entitySet(withName: "xITSPDSFAxA_SalesDocType")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts = metadata.entitySet(withName: "xITSPDSFAxA_SalesItemAmounts")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrdItPrice")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrderPartner")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrders")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrdersItens")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrdersObsSet")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrdersObsSetN")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns = metadata.entitySet(withName: "xITSPDSFAxA_SalesOrdersResns")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume = metadata.entitySet(withName: "xITSPDSFAxA_SoldVolume")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList = metadata.entitySet(withName: "xITSPDSFAxA_TaxList")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset = metadata.entitySet(withName: "xITSPDSFAxA_TaxNameset")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue = metadata.entitySet(withName: "xITSPDSFAxA_TaxValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getParameterValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getParameterValue = metadata.dataMethod(withName: "GetParameterValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductPrice.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductPrice = metadata.dataMethod(withName: "GetProductPrice")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductTaxValue.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductTaxValue = metadata.dataMethod(withName: "GetProductTaxValue")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getReadCustomerSalesAreas.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getReadCustomerSalesAreas = metadata.dataMethod(withName: "GetReadCustomerSalesAreas")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getSalesOrderItensHistory.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getSalesOrderItensHistory = metadata.dataMethod(withName: "GetSalesOrderItensHistory")
        }
        if !ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.sumBillingDocumentsNetValues.isRemoved {
            ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.sumBillingDocumentsNetValues = metadata.dataMethod(withName: "SumBillingDocumentsNetValues")
        }
        if !ParameterValue.conditionRecord.isRemoved {
            ParameterValue.conditionRecord = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "ConditionRecord")
        }
        if !ParameterValue.conditionRateValue.isRemoved {
            ParameterValue.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "ConditionRateValue")
        }
        if !ParameterValue.value01.isRemoved {
            ParameterValue.value01 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value01")
        }
        if !ParameterValue.value02.isRemoved {
            ParameterValue.value02 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value02")
        }
        if !ParameterValue.value03.isRemoved {
            ParameterValue.value03 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value03")
        }
        if !ParameterValue.value04.isRemoved {
            ParameterValue.value04 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value04")
        }
        if !ParameterValue.value05.isRemoved {
            ParameterValue.value05 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value05")
        }
        if !ParameterValue.value06.isRemoved {
            ParameterValue.value06 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value06")
        }
        if !ParameterValue.value07.isRemoved {
            ParameterValue.value07 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value07")
        }
        if !ParameterValue.value08.isRemoved {
            ParameterValue.value08 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value08")
        }
        if !ParameterValue.value09.isRemoved {
            ParameterValue.value09 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value09")
        }
        if !ParameterValue.value10.isRemoved {
            ParameterValue.value10 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value10")
        }
        if !ParameterValue.value11.isRemoved {
            ParameterValue.value11 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value11")
        }
        if !ParameterValue.value12.isRemoved {
            ParameterValue.value12 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value12")
        }
        if !ParameterValue.value13.isRemoved {
            ParameterValue.value13 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value13")
        }
        if !ParameterValue.value14.isRemoved {
            ParameterValue.value14 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value14")
        }
        if !ParameterValue.value15.isRemoved {
            ParameterValue.value15 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value15")
        }
        if !ParameterValue.value16.isRemoved {
            ParameterValue.value16 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value16")
        }
        if !ParameterValue.value17.isRemoved {
            ParameterValue.value17 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value17")
        }
        if !ParameterValue.value18.isRemoved {
            ParameterValue.value18 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value18")
        }
        if !ParameterValue.value19.isRemoved {
            ParameterValue.value19 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value19")
        }
        if !ParameterValue.value20.isRemoved {
            ParameterValue.value20 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value20")
        }
        if !PricingValue.accessSequence.isRemoved {
            PricingValue.accessSequence = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue.property(withName: "AccessSequence")
        }
        if !PricingValue.price.isRemoved {
            PricingValue.price = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue.property(withName: "Price")
        }
        if !TaxValue.taxType.isRemoved {
            TaxValue.taxType = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "TaxType")
        }
        if !TaxValue.groupPriority.isRemoved {
            TaxValue.groupPriority = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "GroupPriority")
        }
        if !TaxValue.rate.isRemoved {
            TaxValue.rate = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "RATE")
        }
        if !TaxValue.rateValueUnit.isRemoved {
            TaxValue.rateValueUnit = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "RateValueUnit")
        }
        if !TaxValue.field01.isRemoved {
            TaxValue.field01 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field01")
        }
        if !TaxValue.field02.isRemoved {
            TaxValue.field02 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field02")
        }
        if !TaxValue.field03.isRemoved {
            TaxValue.field03 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field03")
        }
        if !TaxValue.field04.isRemoved {
            TaxValue.field04 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field04")
        }
        if !TaxValue.field05.isRemoved {
            TaxValue.field05 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field05")
        }
        if !TaxValue.field06.isRemoved {
            TaxValue.field06 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field06")
        }
        if !TaxValue.field07.isRemoved {
            TaxValue.field07 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field07")
        }
        if !TaxValue.field08.isRemoved {
            TaxValue.field08 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field08")
        }
        if !TaxValue.field09.isRemoved {
            TaxValue.field09 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field09")
        }
        if !TaxValue.field10.isRemoved {
            TaxValue.field10 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field10")
        }
        if !TaxValue.field11.isRemoved {
            TaxValue.field11 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field11")
        }
        if !TaxValue.field12.isRemoved {
            TaxValue.field12 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field12")
        }
        if !TaxValue.field13.isRemoved {
            TaxValue.field13 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field13")
        }
        if !TaxValue.field14.isRemoved {
            TaxValue.field14 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field14")
        }
        if !TaxValue.field15.isRemoved {
            TaxValue.field15 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field15")
        }
        if !TaxValue.field16.isRemoved {
            TaxValue.field16 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field16")
        }
        if !TaxValue.field17.isRemoved {
            TaxValue.field17 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field17")
        }
        if !TaxValue.field18.isRemoved {
            TaxValue.field18 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field18")
        }
        if !TaxValue.field19.isRemoved {
            TaxValue.field19 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field19")
        }
        if !TaxValue.field20.isRemoved {
            TaxValue.field20 = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field20")
        }
        if !ACustSalesPartnerFuncType.customer.isRemoved {
            ACustSalesPartnerFuncType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "Customer")
        }
        if !ACustSalesPartnerFuncType.salesOrganization.isRemoved {
            ACustSalesPartnerFuncType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "SalesOrganization")
        }
        if !ACustSalesPartnerFuncType.distributionChannel.isRemoved {
            ACustSalesPartnerFuncType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "DistributionChannel")
        }
        if !ACustSalesPartnerFuncType.division.isRemoved {
            ACustSalesPartnerFuncType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "Division")
        }
        if !ACustSalesPartnerFuncType.partnerCounter.isRemoved {
            ACustSalesPartnerFuncType.partnerCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "PartnerCounter")
        }
        if !ACustSalesPartnerFuncType.partnerFunction.isRemoved {
            ACustSalesPartnerFuncType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "PartnerFunction")
        }
        if !ACustSalesPartnerFuncType.bpCustomerNumber.isRemoved {
            ACustSalesPartnerFuncType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "BPCustomerNumber")
        }
        if !ACustSalesPartnerFuncType.customerPartnerDescription.isRemoved {
            ACustSalesPartnerFuncType.customerPartnerDescription = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "CustomerPartnerDescription")
        }
        if !ACustSalesPartnerFuncType.defaultPartner.isRemoved {
            ACustSalesPartnerFuncType.defaultPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "DefaultPartner")
        }
        if !ACustSalesPartnerFuncType.authorizationGroup.isRemoved {
            ACustSalesPartnerFuncType.authorizationGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "AuthorizationGroup")
        }
        if !ACustSalesPartnerFuncType.isFinal.isRemoved {
            ACustSalesPartnerFuncType.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "IsFinal")
        }
        if !AttachmentContent.documentInfoRecordDocType.isRemoved {
            AttachmentContent.documentInfoRecordDocType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocType")
        }
        if !AttachmentContent.documentInfoRecordDocNumber.isRemoved {
            AttachmentContent.documentInfoRecordDocNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocNumber")
        }
        if !AttachmentContent.documentInfoRecordDocVersion.isRemoved {
            AttachmentContent.documentInfoRecordDocVersion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocVersion")
        }
        if !AttachmentContent.documentInfoRecordDocPart.isRemoved {
            AttachmentContent.documentInfoRecordDocPart = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocPart")
        }
        if !AttachmentContent.logicalDocument.isRemoved {
            AttachmentContent.logicalDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LogicalDocument")
        }
        if !AttachmentContent.archiveDocumentID.isRemoved {
            AttachmentContent.archiveDocumentID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ArchiveDocumentID")
        }
        if !AttachmentContent.linkedSAPObjectKey.isRemoved {
            AttachmentContent.linkedSAPObjectKey = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LinkedSAPObjectKey")
        }
        if !AttachmentContent.businessObjectTypeName.isRemoved {
            AttachmentContent.businessObjectTypeName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "BusinessObjectTypeName")
        }
        if !AttachmentContent.semanticObject.isRemoved {
            AttachmentContent.semanticObject = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "SemanticObject")
        }
        if !AttachmentContent.workstationApplication.isRemoved {
            AttachmentContent.workstationApplication = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "WorkstationApplication")
        }
        if !AttachmentContent.fileSize.isRemoved {
            AttachmentContent.fileSize = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "FileSize")
        }
        if !AttachmentContent.fileName.isRemoved {
            AttachmentContent.fileName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "FileName")
        }
        if !AttachmentContent.documentURL.isRemoved {
            AttachmentContent.documentURL = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentURL")
        }
        if !AttachmentContent.mimeType.isRemoved {
            AttachmentContent.mimeType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "MimeType")
        }
        if !AttachmentContent.content.isRemoved {
            AttachmentContent.content = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "Content")
        }
        if !AttachmentContent.createdByUser.isRemoved {
            AttachmentContent.createdByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreatedByUser")
        }
        if !AttachmentContent.createdByUserFullName.isRemoved {
            AttachmentContent.createdByUserFullName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreatedByUserFullName")
        }
        if !AttachmentContent.creationDateTime.isRemoved {
            AttachmentContent.creationDateTime = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreationDateTime")
        }
        if !AttachmentContent.businessObjectType.isRemoved {
            AttachmentContent.businessObjectType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "BusinessObjectType")
        }
        if !AttachmentContent.lastChangedByUser.isRemoved {
            AttachmentContent.lastChangedByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LastChangedByUser")
        }
        if !AttachmentContent.lastChangedByUserFullName.isRemoved {
            AttachmentContent.lastChangedByUserFullName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LastChangedByUserFullName")
        }
        if !AttachmentContent.changedDateTime.isRemoved {
            AttachmentContent.changedDateTime = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ChangedDateTime")
        }
        if !AttachmentContent.storageCategory.isRemoved {
            AttachmentContent.storageCategory = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "StorageCategory")
        }
        if !AttachmentContent.archiveLinkRepository.isRemoved {
            AttachmentContent.archiveLinkRepository = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ArchiveLinkRepository")
        }
        if !IBillingDocumentBasicStdVHType.billingDocument.isRemoved {
            IBillingDocumentBasicStdVHType.billingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType.property(withName: "BillingDocument")
        }
        if !ICompanyCodeStdVHType.companyCode.isRemoved {
            ICompanyCodeStdVHType.companyCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType.property(withName: "CompanyCode")
        }
        if !ICompanyCodeStdVHType.companyCodeName.isRemoved {
            ICompanyCodeStdVHType.companyCodeName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType.property(withName: "CompanyCodeName")
        }
        if !ICustomerVHType.customer.isRemoved {
            ICustomerVHType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "Customer")
        }
        if !ICustomerVHType.organizationBPName1.isRemoved {
            ICustomerVHType.organizationBPName1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "OrganizationBPName1")
        }
        if !ICustomerVHType.organizationBPName2.isRemoved {
            ICustomerVHType.organizationBPName2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "OrganizationBPName2")
        }
        if !ICustomerVHType.country.isRemoved {
            ICustomerVHType.country = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "Country")
        }
        if !ICustomerVHType.cityName.isRemoved {
            ICustomerVHType.cityName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CityName")
        }
        if !ICustomerVHType.streetName.isRemoved {
            ICustomerVHType.streetName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "StreetName")
        }
        if !ICustomerVHType.customerName.isRemoved {
            ICustomerVHType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CustomerName")
        }
        if !ICustomerVHType.customerAccountGroup.isRemoved {
            ICustomerVHType.customerAccountGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CustomerAccountGroup")
        }
        if !ICustomerVHType.authorizationGroup.isRemoved {
            ICustomerVHType.authorizationGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "AuthorizationGroup")
        }
        if !ICustomerVHType.isBusinessPurposeCompleted.isRemoved {
            ICustomerVHType.isBusinessPurposeCompleted = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "IsBusinessPurposeCompleted")
        }
        if !SalesOrderWithoutChargeItemPartnerType.contactPerson.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.contactPerson = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "ContactPerson")
        }
        if !SalesOrderWithoutChargeItemPartnerType.customer.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Customer")
        }
        if !SalesOrderWithoutChargeItemPartnerType.partnerFunction.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "PartnerFunction")
        }
        if !SalesOrderWithoutChargeItemPartnerType.personnel.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.personnel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Personnel")
        }
        if !SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "SalesOrderWithoutChargeItem")
        }
        if !SalesOrderWithoutChargeItemPartnerType.supplier.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.supplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Supplier")
        }
        if !SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem.isRemoved {
            SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItem")
        }
        if !SalesOrderWithoutChargeItemPricing.conditionCurrency.isRemoved {
            SalesOrderWithoutChargeItemPricing.conditionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionCurrency")
        }
        if !SalesOrderWithoutChargeItemPricing.conditionQuantity.isRemoved {
            SalesOrderWithoutChargeItemPricing.conditionQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionQuantity")
        }
        if !SalesOrderWithoutChargeItemPricing.conditionQuantityUnit.isRemoved {
            SalesOrderWithoutChargeItemPricing.conditionQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionQuantityUnit")
        }
        if !SalesOrderWithoutChargeItemPricing.conditionRateValue.isRemoved {
            SalesOrderWithoutChargeItemPricing.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionRateValue")
        }
        if !SalesOrderWithoutChargeItemPricing.conditionType.isRemoved {
            SalesOrderWithoutChargeItemPricing.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionType")
        }
        if !SalesOrderWithoutChargeItemPricing.pricingProcedureCounter.isRemoved {
            SalesOrderWithoutChargeItemPricing.pricingProcedureCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "PricingProcedureCounter")
        }
        if !SalesOrderWithoutChargeItemPricing.pricingProcedureStep.isRemoved {
            SalesOrderWithoutChargeItemPricing.pricingProcedureStep = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "PricingProcedureStep")
        }
        if !SalesOrderWithoutChargeItemPricing.salesBonification.isRemoved {
            SalesOrderWithoutChargeItemPricing.salesBonification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "SalesBonification")
        }
        if !SalesOrderWithoutChargeItemPricing.salesBonificationItem.isRemoved {
            SalesOrderWithoutChargeItemPricing.salesBonificationItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "SalesBonificationItem")
        }
        if !SalesOrderWithoutChargeItemPricing.transactionCurrency.isRemoved {
            SalesOrderWithoutChargeItemPricing.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "TransactionCurrency")
        }
        if !SalesOrderWithoutChargeItemType.alternativeToItem.isRemoved {
            SalesOrderWithoutChargeItemType.alternativeToItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "AlternativeToItem")
        }
        if !SalesOrderWithoutChargeItemType.customerPaymentTerms.isRemoved {
            SalesOrderWithoutChargeItemType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "CustomerPaymentTerms")
        }
        if !SalesOrderWithoutChargeItemType.higherLevelItem.isRemoved {
            SalesOrderWithoutChargeItemType.higherLevelItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "HigherLevelItem")
        }
        if !SalesOrderWithoutChargeItemType.incotermsClassification.isRemoved {
            SalesOrderWithoutChargeItemType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsClassification")
        }
        if !SalesOrderWithoutChargeItemType.incotermsLocation1.isRemoved {
            SalesOrderWithoutChargeItemType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsLocation1")
        }
        if !SalesOrderWithoutChargeItemType.incotermsLocation2.isRemoved {
            SalesOrderWithoutChargeItemType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsLocation2")
        }
        if !SalesOrderWithoutChargeItemType.incotermsTransferLocation.isRemoved {
            SalesOrderWithoutChargeItemType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsTransferLocation")
        }
        if !SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent.isRemoved {
            SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ItemOrderProbabilityInPercent")
        }
        if !SalesOrderWithoutChargeItemType.material.isRemoved {
            SalesOrderWithoutChargeItemType.material = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "Material")
        }
        if !SalesOrderWithoutChargeItemType.materialByCustomer.isRemoved {
            SalesOrderWithoutChargeItemType.materialByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialByCustomer")
        }
        if !SalesOrderWithoutChargeItemType.materialGroup.isRemoved {
            SalesOrderWithoutChargeItemType.materialGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialGroup")
        }
        if !SalesOrderWithoutChargeItemType.materialPricingGroup.isRemoved {
            SalesOrderWithoutChargeItemType.materialPricingGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialPricingGroup")
        }
        if !SalesOrderWithoutChargeItemType.plant.isRemoved {
            SalesOrderWithoutChargeItemType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "Plant")
        }
        if !SalesOrderWithoutChargeItemType.profitCenter.isRemoved {
            SalesOrderWithoutChargeItemType.profitCenter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ProfitCenter")
        }
        if !SalesOrderWithoutChargeItemType.purchaseOrderByCustomer.isRemoved {
            SalesOrderWithoutChargeItemType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "PurchaseOrderByCustomer")
        }
        if !SalesOrderWithoutChargeItemType.referenceSDDocument.isRemoved {
            SalesOrderWithoutChargeItemType.referenceSDDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ReferenceSDDocument")
        }
        if !SalesOrderWithoutChargeItemType.referenceSDDocumentItem.isRemoved {
            SalesOrderWithoutChargeItemType.referenceSDDocumentItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ReferenceSDDocumentItem")
        }
        if !SalesOrderWithoutChargeItemType.requestedQuantity.isRemoved {
            SalesOrderWithoutChargeItemType.requestedQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "RequestedQuantity")
        }
        if !SalesOrderWithoutChargeItemType.requestedQuantityUnit.isRemoved {
            SalesOrderWithoutChargeItemType.requestedQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "RequestedQuantityUnit")
        }
        if !SalesOrderWithoutChargeItemType.salesBonificationItemCategory.isRemoved {
            SalesOrderWithoutChargeItemType.salesBonificationItemCategory = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationItemCategory")
        }
        if !SalesOrderWithoutChargeItemType.salesBonificationItemText.isRemoved {
            SalesOrderWithoutChargeItemType.salesBonificationItemText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationItemText")
        }
        if !SalesOrderWithoutChargeItemType.salesBonificationType.isRemoved {
            SalesOrderWithoutChargeItemType.salesBonificationType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationType")
        }
        if !SalesOrderWithoutChargeItemType.salesDocumentRjcnReason.isRemoved {
            SalesOrderWithoutChargeItemType.salesDocumentRjcnReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesDocumentRjcnReason")
        }
        if !SalesOrderWithoutChargeItemType.salesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargeItemType.salesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem.isRemoved {
            SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesOrderWithoutChargeItem")
        }
        if !SalesOrderWithoutChargeItemType.wbsElement.isRemoved {
            SalesOrderWithoutChargeItemType.wbsElement = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "WBSElement")
        }
        if !SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner.isRemoved {
            SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItemPartner")
        }
        if !SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine.isRemoved {
            SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeScheduleLine")
        }
        if !SalesOrderWithoutChargePartnerType.contactPerson.isRemoved {
            SalesOrderWithoutChargePartnerType.contactPerson = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "ContactPerson")
        }
        if !SalesOrderWithoutChargePartnerType.customer.isRemoved {
            SalesOrderWithoutChargePartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Customer")
        }
        if !SalesOrderWithoutChargePartnerType.partnerFunction.isRemoved {
            SalesOrderWithoutChargePartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "PartnerFunction")
        }
        if !SalesOrderWithoutChargePartnerType.personnel.isRemoved {
            SalesOrderWithoutChargePartnerType.personnel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Personnel")
        }
        if !SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargePartnerType.supplier.isRemoved {
            SalesOrderWithoutChargePartnerType.supplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Supplier")
        }
        if !SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner.isRemoved {
            SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargePartner")
        }
        if !SalesOrderWithoutChargePricing.conditionCurrency.isRemoved {
            SalesOrderWithoutChargePricing.conditionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionCurrency")
        }
        if !SalesOrderWithoutChargePricing.conditionQuantity.isRemoved {
            SalesOrderWithoutChargePricing.conditionQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionQuantity")
        }
        if !SalesOrderWithoutChargePricing.conditionQuantityUnit.isRemoved {
            SalesOrderWithoutChargePricing.conditionQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionQuantityUnit")
        }
        if !SalesOrderWithoutChargePricing.conditionRateValue.isRemoved {
            SalesOrderWithoutChargePricing.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionRateValue")
        }
        if !SalesOrderWithoutChargePricing.conditionType.isRemoved {
            SalesOrderWithoutChargePricing.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionType")
        }
        if !SalesOrderWithoutChargePricing.pricingProcedureCounter.isRemoved {
            SalesOrderWithoutChargePricing.pricingProcedureCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "PricingProcedureCounter")
        }
        if !SalesOrderWithoutChargePricing.pricingProcedureStep.isRemoved {
            SalesOrderWithoutChargePricing.pricingProcedureStep = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "PricingProcedureStep")
        }
        if !SalesOrderWithoutChargePricing.salesQuotation.isRemoved {
            SalesOrderWithoutChargePricing.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "SalesQuotation")
        }
        if !SalesOrderWithoutChargePricing.transactionCurrency.isRemoved {
            SalesOrderWithoutChargePricing.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "TransactionCurrency")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "ConfdOrderQtyByMatlAvailCheck")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "DelivBlockReasonForSchedLine")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "DeliveredQtyInOrderQtyUnit")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OpenConfdDelivQtyInOrdQtyUnit")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OrderQuantityUnit")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.organizationDivision.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OrganizationDivision")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "SalesOrderWithoutChargeItem")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.scheduleLine.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.scheduleLine = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "ScheduleLine")
        }
        if !SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine.isRemoved {
            SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "to_xITSPDSFAxA_to_SalesOrderWithoutChargeScheduleLine")
        }
        if !SalesOrderWithoutChargeType.bindingPeriodValidityEndDate.isRemoved {
            SalesOrderWithoutChargeType.bindingPeriodValidityEndDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "BindingPeriodValidityEndDate")
        }
        if !SalesOrderWithoutChargeType.bindingPeriodValidityStartDate.isRemoved {
            SalesOrderWithoutChargeType.bindingPeriodValidityStartDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "BindingPeriodValidityStartDate")
        }
        if !SalesOrderWithoutChargeType.completeDeliveryIsDefined.isRemoved {
            SalesOrderWithoutChargeType.completeDeliveryIsDefined = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !SalesOrderWithoutChargeType.createdByUser.isRemoved {
            SalesOrderWithoutChargeType.createdByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CreatedByUser")
        }
        if !SalesOrderWithoutChargeType.creationDate.isRemoved {
            SalesOrderWithoutChargeType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CreationDate")
        }
        if !SalesOrderWithoutChargeType.customerPaymentTerms.isRemoved {
            SalesOrderWithoutChargeType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPaymentTerms")
        }
        if !SalesOrderWithoutChargeType.customerPurchaseOrderDate.isRemoved {
            SalesOrderWithoutChargeType.customerPurchaseOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPurchaseOrderDate")
        }
        if !SalesOrderWithoutChargeType.customerPurchaseOrderType.isRemoved {
            SalesOrderWithoutChargeType.customerPurchaseOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPurchaseOrderType")
        }
        if !SalesOrderWithoutChargeType.deliveryBlockReason.isRemoved {
            SalesOrderWithoutChargeType.deliveryBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "DeliveryBlockReason")
        }
        if !SalesOrderWithoutChargeType.distributionChannel.isRemoved {
            SalesOrderWithoutChargeType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "DistributionChannel")
        }
        if !SalesOrderWithoutChargeType.expectedOrderNetAmount.isRemoved {
            SalesOrderWithoutChargeType.expectedOrderNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ExpectedOrderNetAmount")
        }
        if !SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent.isRemoved {
            SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "HdrOrderProbabilityInPercent")
        }
        if !SalesOrderWithoutChargeType.headerBillingBlockReason.isRemoved {
            SalesOrderWithoutChargeType.headerBillingBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "HeaderBillingBlockReason")
        }
        if !SalesOrderWithoutChargeType.incotermsClassification.isRemoved {
            SalesOrderWithoutChargeType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsClassification")
        }
        if !SalesOrderWithoutChargeType.incotermsLocation1.isRemoved {
            SalesOrderWithoutChargeType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsLocation1")
        }
        if !SalesOrderWithoutChargeType.incotermsLocation2.isRemoved {
            SalesOrderWithoutChargeType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsLocation2")
        }
        if !SalesOrderWithoutChargeType.incotermsTransferLocation.isRemoved {
            SalesOrderWithoutChargeType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsTransferLocation")
        }
        if !SalesOrderWithoutChargeType.incotermsVersion.isRemoved {
            SalesOrderWithoutChargeType.incotermsVersion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsVersion")
        }
        if !SalesOrderWithoutChargeType.lastChangeDate.isRemoved {
            SalesOrderWithoutChargeType.lastChangeDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "LastChangeDate")
        }
        if !SalesOrderWithoutChargeType.lastChangeDateTime.isRemoved {
            SalesOrderWithoutChargeType.lastChangeDateTime = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "LastChangeDateTime")
        }
        if !SalesOrderWithoutChargeType.organizationDivision.isRemoved {
            SalesOrderWithoutChargeType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OrganizationDivision")
        }
        if !SalesOrderWithoutChargeType.overallSDDocumentRejectionSts.isRemoved {
            SalesOrderWithoutChargeType.overallSDDocumentRejectionSts = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OverallSDDocumentRejectionSts")
        }
        if !SalesOrderWithoutChargeType.overallSDProcessStatus.isRemoved {
            SalesOrderWithoutChargeType.overallSDProcessStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OverallSDProcessStatus")
        }
        if !SalesOrderWithoutChargeType.paymentMethod.isRemoved {
            SalesOrderWithoutChargeType.paymentMethod = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PaymentMethod")
        }
        if !SalesOrderWithoutChargeType.pricingDate.isRemoved {
            SalesOrderWithoutChargeType.pricingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PricingDate")
        }
        if !SalesOrderWithoutChargeType.purchaseOrderByCustomer.isRemoved {
            SalesOrderWithoutChargeType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PurchaseOrderByCustomer")
        }
        if !SalesOrderWithoutChargeType.requestedDeliveryDate.isRemoved {
            SalesOrderWithoutChargeType.requestedDeliveryDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "RequestedDeliveryDate")
        }
        if !SalesOrderWithoutChargeType.sdDocumentReason.isRemoved {
            SalesOrderWithoutChargeType.sdDocumentReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SDDocumentReason")
        }
        if !SalesOrderWithoutChargeType.salesDistrict.isRemoved {
            SalesOrderWithoutChargeType.salesDistrict = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesDistrict")
        }
        if !SalesOrderWithoutChargeType.salesGroup.isRemoved {
            SalesOrderWithoutChargeType.salesGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesGroup")
        }
        if !SalesOrderWithoutChargeType.salesOffice.isRemoved {
            SalesOrderWithoutChargeType.salesOffice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOffice")
        }
        if !SalesOrderWithoutChargeType.salesOrderWithoutCharge.isRemoved {
            SalesOrderWithoutChargeType.salesOrderWithoutCharge = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrderWithoutCharge")
        }
        if !SalesOrderWithoutChargeType.salesOrderWithoutChargeType.isRemoved {
            SalesOrderWithoutChargeType.salesOrderWithoutChargeType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrderWithoutChargeType")
        }
        if !SalesOrderWithoutChargeType.salesOrganization.isRemoved {
            SalesOrderWithoutChargeType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrganization")
        }
        if !SalesOrderWithoutChargeType.salesQuotationDate.isRemoved {
            SalesOrderWithoutChargeType.salesQuotationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesQuotationDate")
        }
        if !SalesOrderWithoutChargeType.shippingCondition.isRemoved {
            SalesOrderWithoutChargeType.shippingCondition = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ShippingCondition")
        }
        if !SalesOrderWithoutChargeType.shippingType.isRemoved {
            SalesOrderWithoutChargeType.shippingType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ShippingType")
        }
        if !SalesOrderWithoutChargeType.soldToParty.isRemoved {
            SalesOrderWithoutChargeType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SoldToParty")
        }
        if !SalesOrderWithoutChargeType.totalCreditCheckStatus.isRemoved {
            SalesOrderWithoutChargeType.totalCreditCheckStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TotalCreditCheckStatus")
        }
        if !SalesOrderWithoutChargeType.totalNetAmount.isRemoved {
            SalesOrderWithoutChargeType.totalNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TotalNetAmount")
        }
        if !SalesOrderWithoutChargeType.transactionCurrency.isRemoved {
            SalesOrderWithoutChargeType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TransactionCurrency")
        }
        if !SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH.isRemoved {
            SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "xITSPDSFAxMobileID_SDH")
        }
        if !SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem.isRemoved {
            SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItem")
        }
        if !SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner.isRemoved {
            SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargePartner")
        }
        if !SalesQuotationItemPartnerType.contactPerson.isRemoved {
            SalesQuotationItemPartnerType.contactPerson = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "ContactPerson")
        }
        if !SalesQuotationItemPartnerType.customer.isRemoved {
            SalesQuotationItemPartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Customer")
        }
        if !SalesQuotationItemPartnerType.partnerFunction.isRemoved {
            SalesQuotationItemPartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "PartnerFunction")
        }
        if !SalesQuotationItemPartnerType.personnel.isRemoved {
            SalesQuotationItemPartnerType.personnel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Personnel")
        }
        if !SalesQuotationItemPartnerType.salesQuotation.isRemoved {
            SalesQuotationItemPartnerType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationItemPartnerType.supplier.isRemoved {
            SalesQuotationItemPartnerType.supplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Supplier")
        }
        if !SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner.isRemoved {
            SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPartner")
        }
        if !SalesQuotationItemPricingType.conditionCurrency.isRemoved {
            SalesQuotationItemPricingType.conditionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionCurrency")
        }
        if !SalesQuotationItemPricingType.conditionQuantity.isRemoved {
            SalesQuotationItemPricingType.conditionQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionQuantity")
        }
        if !SalesQuotationItemPricingType.conditionQuantityUnit.isRemoved {
            SalesQuotationItemPricingType.conditionQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionQuantityUnit")
        }
        if !SalesQuotationItemPricingType.conditionRateValue.isRemoved {
            SalesQuotationItemPricingType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionRateValue")
        }
        if !SalesQuotationItemPricingType.conditionType.isRemoved {
            SalesQuotationItemPricingType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionType")
        }
        if !SalesQuotationItemPricingType.pricingProcedureCounter.isRemoved {
            SalesQuotationItemPricingType.pricingProcedureCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "PricingProcedureCounter")
        }
        if !SalesQuotationItemPricingType.pricingProcedureStep.isRemoved {
            SalesQuotationItemPricingType.pricingProcedureStep = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "PricingProcedureStep")
        }
        if !SalesQuotationItemPricingType.salesQuotation.isRemoved {
            SalesQuotationItemPricingType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationItemPricingType.transactionCurrency.isRemoved {
            SalesQuotationItemPricingType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "TransactionCurrency")
        }
        if !SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing.isRemoved {
            SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPricing")
        }
        if !SalesQuotationItensType.alternativeToItem.isRemoved {
            SalesQuotationItensType.alternativeToItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "AlternativeToItem")
        }
        if !SalesQuotationItensType.customerPaymentTerms.isRemoved {
            SalesQuotationItensType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "CustomerPaymentTerms")
        }
        if !SalesQuotationItensType.higherLevelItem.isRemoved {
            SalesQuotationItensType.higherLevelItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "HigherLevelItem")
        }
        if !SalesQuotationItensType.incotermsClassification.isRemoved {
            SalesQuotationItensType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsClassification")
        }
        if !SalesQuotationItensType.incotermsLocation1.isRemoved {
            SalesQuotationItensType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsLocation1")
        }
        if !SalesQuotationItensType.incotermsLocation2.isRemoved {
            SalesQuotationItensType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsLocation2")
        }
        if !SalesQuotationItensType.incotermsTransferLocation.isRemoved {
            SalesQuotationItensType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsTransferLocation")
        }
        if !SalesQuotationItensType.itemOrderProbabilityInPercent.isRemoved {
            SalesQuotationItensType.itemOrderProbabilityInPercent = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ItemOrderProbabilityInPercent")
        }
        if !SalesQuotationItensType.material.isRemoved {
            SalesQuotationItensType.material = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "Material")
        }
        if !SalesQuotationItensType.materialByCustomer.isRemoved {
            SalesQuotationItensType.materialByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialByCustomer")
        }
        if !SalesQuotationItensType.materialGroup.isRemoved {
            SalesQuotationItensType.materialGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialGroup")
        }
        if !SalesQuotationItensType.materialPricingGroup.isRemoved {
            SalesQuotationItensType.materialPricingGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialPricingGroup")
        }
        if !SalesQuotationItensType.plant.isRemoved {
            SalesQuotationItensType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "Plant")
        }
        if !SalesQuotationItensType.profitCenter.isRemoved {
            SalesQuotationItensType.profitCenter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ProfitCenter")
        }
        if !SalesQuotationItensType.purchaseOrderByCustomer.isRemoved {
            SalesQuotationItensType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "PurchaseOrderByCustomer")
        }
        if !SalesQuotationItensType.referenceSDDocument.isRemoved {
            SalesQuotationItensType.referenceSDDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ReferenceSDDocument")
        }
        if !SalesQuotationItensType.referenceSDDocumentItem.isRemoved {
            SalesQuotationItensType.referenceSDDocumentItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ReferenceSDDocumentItem")
        }
        if !SalesQuotationItensType.requestedQuantity.isRemoved {
            SalesQuotationItensType.requestedQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "RequestedQuantity")
        }
        if !SalesQuotationItensType.requestedQuantityUnit.isRemoved {
            SalesQuotationItensType.requestedQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "RequestedQuantityUnit")
        }
        if !SalesQuotationItensType.salesDocumentRjcnReason.isRemoved {
            SalesQuotationItensType.salesDocumentRjcnReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesDocumentRjcnReason")
        }
        if !SalesQuotationItensType.salesQuotation.isRemoved {
            SalesQuotationItensType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationItensType.salesQuotationItemCategory.isRemoved {
            SalesQuotationItensType.salesQuotationItemCategory = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationItemCategory")
        }
        if !SalesQuotationItensType.salesQuotationItemText.isRemoved {
            SalesQuotationItensType.salesQuotationItemText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationItemText")
        }
        if !SalesQuotationItensType.salesQuotationType.isRemoved {
            SalesQuotationItensType.salesQuotationType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationType")
        }
        if !SalesQuotationItensType.wbsElement.isRemoved {
            SalesQuotationItensType.wbsElement = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "WBSElement")
        }
        if !SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner.isRemoved {
            SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPartner")
        }
        if !SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing.isRemoved {
            SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPricing")
        }
        if !SalesQuotationItensType.toXITSPDSFAxASalesQuotations.isRemoved {
            SalesQuotationItensType.toXITSPDSFAxASalesQuotations = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotations")
        }
        if !SalesQuotationPartnerType.contactPerson.isRemoved {
            SalesQuotationPartnerType.contactPerson = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "ContactPerson")
        }
        if !SalesQuotationPartnerType.customer.isRemoved {
            SalesQuotationPartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Customer")
        }
        if !SalesQuotationPartnerType.partnerFunction.isRemoved {
            SalesQuotationPartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "PartnerFunction")
        }
        if !SalesQuotationPartnerType.personnel.isRemoved {
            SalesQuotationPartnerType.personnel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Personnel")
        }
        if !SalesQuotationPartnerType.salesQuotation.isRemoved {
            SalesQuotationPartnerType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationPartnerType.supplier.isRemoved {
            SalesQuotationPartnerType.supplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Supplier")
        }
        if !SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner.isRemoved {
            SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "to_xITSPDSFAxA_SalesQuotationsPartner")
        }
        if !SalesQuotationPricingType.conditionCurrency.isRemoved {
            SalesQuotationPricingType.conditionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionCurrency")
        }
        if !SalesQuotationPricingType.conditionQuantity.isRemoved {
            SalesQuotationPricingType.conditionQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionQuantity")
        }
        if !SalesQuotationPricingType.conditionQuantityUnit.isRemoved {
            SalesQuotationPricingType.conditionQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionQuantityUnit")
        }
        if !SalesQuotationPricingType.conditionRateValue.isRemoved {
            SalesQuotationPricingType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionRateValue")
        }
        if !SalesQuotationPricingType.conditionType.isRemoved {
            SalesQuotationPricingType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionType")
        }
        if !SalesQuotationPricingType.pricingProcedureCounter.isRemoved {
            SalesQuotationPricingType.pricingProcedureCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "PricingProcedureCounter")
        }
        if !SalesQuotationPricingType.pricingProcedureStep.isRemoved {
            SalesQuotationPricingType.pricingProcedureStep = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "PricingProcedureStep")
        }
        if !SalesQuotationPricingType.salesQuotation.isRemoved {
            SalesQuotationPricingType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationPricingType.transactionCurrency.isRemoved {
            SalesQuotationPricingType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "TransactionCurrency")
        }
        if !SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing.isRemoved {
            SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "to_xITSPDSFAxA_to_SalesQuotationPricing")
        }
        if !SalesQuotationsType.bindingPeriodValidityEndDate.isRemoved {
            SalesQuotationsType.bindingPeriodValidityEndDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "BindingPeriodValidityEndDate")
        }
        if !SalesQuotationsType.bindingPeriodValidityStartDate.isRemoved {
            SalesQuotationsType.bindingPeriodValidityStartDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "BindingPeriodValidityStartDate")
        }
        if !SalesQuotationsType.completeDeliveryIsDefined.isRemoved {
            SalesQuotationsType.completeDeliveryIsDefined = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !SalesQuotationsType.createdByUser.isRemoved {
            SalesQuotationsType.createdByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CreatedByUser")
        }
        if !SalesQuotationsType.creationDate.isRemoved {
            SalesQuotationsType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CreationDate")
        }
        if !SalesQuotationsType.customerPaymentTerms.isRemoved {
            SalesQuotationsType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPaymentTerms")
        }
        if !SalesQuotationsType.customerPurchaseOrderDate.isRemoved {
            SalesQuotationsType.customerPurchaseOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPurchaseOrderDate")
        }
        if !SalesQuotationsType.customerPurchaseOrderType.isRemoved {
            SalesQuotationsType.customerPurchaseOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPurchaseOrderType")
        }
        if !SalesQuotationsType.deliveryBlockReason.isRemoved {
            SalesQuotationsType.deliveryBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "DeliveryBlockReason")
        }
        if !SalesQuotationsType.distributionChannel.isRemoved {
            SalesQuotationsType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "DistributionChannel")
        }
        if !SalesQuotationsType.expectedOrderNetAmount.isRemoved {
            SalesQuotationsType.expectedOrderNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ExpectedOrderNetAmount")
        }
        if !SalesQuotationsType.hdrOrderProbabilityInPercent.isRemoved {
            SalesQuotationsType.hdrOrderProbabilityInPercent = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "HdrOrderProbabilityInPercent")
        }
        if !SalesQuotationsType.headerBillingBlockReason.isRemoved {
            SalesQuotationsType.headerBillingBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "HeaderBillingBlockReason")
        }
        if !SalesQuotationsType.incotermsClassification.isRemoved {
            SalesQuotationsType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsClassification")
        }
        if !SalesQuotationsType.incotermsLocation1.isRemoved {
            SalesQuotationsType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsLocation1")
        }
        if !SalesQuotationsType.incotermsLocation2.isRemoved {
            SalesQuotationsType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsLocation2")
        }
        if !SalesQuotationsType.incotermsTransferLocation.isRemoved {
            SalesQuotationsType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsTransferLocation")
        }
        if !SalesQuotationsType.incotermsVersion.isRemoved {
            SalesQuotationsType.incotermsVersion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsVersion")
        }
        if !SalesQuotationsType.lastChangeDate.isRemoved {
            SalesQuotationsType.lastChangeDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "LastChangeDate")
        }
        if !SalesQuotationsType.lastChangeDateTime.isRemoved {
            SalesQuotationsType.lastChangeDateTime = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "LastChangeDateTime")
        }
        if !SalesQuotationsType.organizationDivision.isRemoved {
            SalesQuotationsType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OrganizationDivision")
        }
        if !SalesQuotationsType.overallSDDocumentRejectionSts.isRemoved {
            SalesQuotationsType.overallSDDocumentRejectionSts = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OverallSDDocumentRejectionSts")
        }
        if !SalesQuotationsType.overallSDProcessStatus.isRemoved {
            SalesQuotationsType.overallSDProcessStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OverallSDProcessStatus")
        }
        if !SalesQuotationsType.paymentMethod.isRemoved {
            SalesQuotationsType.paymentMethod = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PaymentMethod")
        }
        if !SalesQuotationsType.pricingDate.isRemoved {
            SalesQuotationsType.pricingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PricingDate")
        }
        if !SalesQuotationsType.purchaseOrderByCustomer.isRemoved {
            SalesQuotationsType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PurchaseOrderByCustomer")
        }
        if !SalesQuotationsType.requestedDeliveryDate.isRemoved {
            SalesQuotationsType.requestedDeliveryDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "RequestedDeliveryDate")
        }
        if !SalesQuotationsType.sdDocumentReason.isRemoved {
            SalesQuotationsType.sdDocumentReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SDDocumentReason")
        }
        if !SalesQuotationsType.salesDistrict.isRemoved {
            SalesQuotationsType.salesDistrict = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesDistrict")
        }
        if !SalesQuotationsType.salesGroup.isRemoved {
            SalesQuotationsType.salesGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesGroup")
        }
        if !SalesQuotationsType.salesOffice.isRemoved {
            SalesQuotationsType.salesOffice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesOffice")
        }
        if !SalesQuotationsType.salesOrganization.isRemoved {
            SalesQuotationsType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesOrganization")
        }
        if !SalesQuotationsType.salesQuotation.isRemoved {
            SalesQuotationsType.salesQuotation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotation")
        }
        if !SalesQuotationsType.salesQuotationDate.isRemoved {
            SalesQuotationsType.salesQuotationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotationDate")
        }
        if !SalesQuotationsType.salesQuotationType.isRemoved {
            SalesQuotationsType.salesQuotationType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotationType")
        }
        if !SalesQuotationsType.shippingCondition.isRemoved {
            SalesQuotationsType.shippingCondition = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ShippingCondition")
        }
        if !SalesQuotationsType.shippingType.isRemoved {
            SalesQuotationsType.shippingType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ShippingType")
        }
        if !SalesQuotationsType.soldToParty.isRemoved {
            SalesQuotationsType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SoldToParty")
        }
        if !SalesQuotationsType.totalCreditCheckStatus.isRemoved {
            SalesQuotationsType.totalCreditCheckStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TotalCreditCheckStatus")
        }
        if !SalesQuotationsType.totalNetAmount.isRemoved {
            SalesQuotationsType.totalNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TotalNetAmount")
        }
        if !SalesQuotationsType.transactionCurrency.isRemoved {
            SalesQuotationsType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TransactionCurrency")
        }
        if !SalesQuotationsType.xITSPDSFAxMobileIDSDH.isRemoved {
            SalesQuotationsType.xITSPDSFAxMobileIDSDH = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "xITSPDSFAxMobileID_SDH")
        }
        if !SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner.isRemoved {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationPartner")
        }
        if !SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing.isRemoved {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationPricing")
        }
        if !SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens.isRemoved {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationsItens")
        }
        if !SyncEventType.eventDate.isRemoved {
            SyncEventType.eventDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventDate")
        }
        if !SyncEventType.eventIdentifier.isRemoved {
            SyncEventType.eventIdentifier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventIdentifier")
        }
        if !SyncEventType.eventMsg.isRemoved {
            SyncEventType.eventMsg = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventMsg")
        }
        if !SyncEventType.eventObject.isRemoved {
            SyncEventType.eventObject = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventObject")
        }
        if !SyncEventType.id.isRemoved {
            SyncEventType.id = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "Id")
        }
        if !SyncEventType.mobileID.isRemoved {
            SyncEventType.mobileID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "MobileId")
        }
        if !SyncEventType.operationStatusDesc.isRemoved {
            SyncEventType.operationStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "OperationStatusDesc")
        }
        if !SyncEventType.operationStatusID.isRemoved {
            SyncEventType.operationStatusID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "OperationStatusId")
        }
        if !SyncEventType.toSyncOperationStatus.isRemoved {
            SyncEventType.toSyncOperationStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "to_SyncOperationStatus")
        }
        if !SyncOperationStatusType.id.isRemoved {
            SyncOperationStatusType.id = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "Id")
        }
        if !SyncOperationStatusType.statusColor.isRemoved {
            SyncOperationStatusType.statusColor = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "StatusColor")
        }
        if !SyncOperationStatusType.statusDesc.isRemoved {
            SyncOperationStatusType.statusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "StatusDesc")
        }
        if !XITSPDSFAxABillingDocumentType.billingDocument.isRemoved {
            XITSPDSFAxABillingDocumentType.billingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BillingDocument")
        }
        if !XITSPDSFAxABillingDocumentType.salesOrganization.isRemoved {
            XITSPDSFAxABillingDocumentType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxABillingDocumentType.distributionChannel.isRemoved {
            XITSPDSFAxABillingDocumentType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxABillingDocumentType.division.isRemoved {
            XITSPDSFAxABillingDocumentType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "Division")
        }
        if !XITSPDSFAxABillingDocumentType.soldToParty.isRemoved {
            XITSPDSFAxABillingDocumentType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxABillingDocumentType.bpCustomerNumber.isRemoved {
            XITSPDSFAxABillingDocumentType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxABillingDocumentType.portion.isRemoved {
            XITSPDSFAxABillingDocumentType.portion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "Portion")
        }
        if !XITSPDSFAxABillingDocumentType.customerName.isRemoved {
            XITSPDSFAxABillingDocumentType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "CustomerName")
        }
    }

    private static func merge2(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !XITSPDSFAxABillingDocumentType.additionalValueDays.isRemoved {
            XITSPDSFAxABillingDocumentType.additionalValueDays = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "AdditionalValueDays")
        }
        if !XITSPDSFAxABillingDocumentType.companyCode.isRemoved {
            XITSPDSFAxABillingDocumentType.companyCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "CompanyCode")
        }
        if !XITSPDSFAxABillingDocumentType.fiscalYear.isRemoved {
            XITSPDSFAxABillingDocumentType.fiscalYear = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "FiscalYear")
        }
        if !XITSPDSFAxABillingDocumentType.docNum.isRemoved {
            XITSPDSFAxABillingDocumentType.docNum = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DocNum")
        }
        if !XITSPDSFAxABillingDocumentType.docNumDate.isRemoved {
            XITSPDSFAxABillingDocumentType.docNumDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DocNumDate")
        }
        if !XITSPDSFAxABillingDocumentType.netDueDate.isRemoved {
            XITSPDSFAxABillingDocumentType.netDueDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "NetDueDate")
        }
        if !XITSPDSFAxABillingDocumentType.clearingDate.isRemoved {
            XITSPDSFAxABillingDocumentType.clearingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "ClearingDate")
        }
        if !XITSPDSFAxABillingDocumentType.zInternalDatePeriod.isRemoved {
            XITSPDSFAxABillingDocumentType.zInternalDatePeriod = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "ZInternalDatePeriod")
        }
        if !XITSPDSFAxABillingDocumentType.portionTotal.isRemoved {
            XITSPDSFAxABillingDocumentType.portionTotal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "PortionTotal")
        }
        if !XITSPDSFAxABillingDocumentType.netValue.isRemoved {
            XITSPDSFAxABillingDocumentType.netValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "NetValue")
        }
        if !XITSPDSFAxABillingDocumentType.brNFeNumber.isRemoved {
            XITSPDSFAxABillingDocumentType.brNFeNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BR_NFeNumber")
        }
        if !XITSPDSFAxABillingDocumentType.statusColorDesc.isRemoved {
            XITSPDSFAxABillingDocumentType.statusColorDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "StatusColorDesc")
        }
        if !XITSPDSFAxABillingDocumentType.statusColorHex.isRemoved {
            XITSPDSFAxABillingDocumentType.statusColorHex = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "StatusColorHex")
        }
        if !XITSPDSFAxABusinessRulesType.tipo.isRemoved {
            XITSPDSFAxABusinessRulesType.tipo = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "tipo")
        }
        if !XITSPDSFAxABusinessRulesType.nome.isRemoved {
            XITSPDSFAxABusinessRulesType.nome = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "nome")
        }
        if !XITSPDSFAxABusinessRulesType.seq.isRemoved {
            XITSPDSFAxABusinessRulesType.seq = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "seq")
        }
        if !XITSPDSFAxABusinessRulesType.valor.isRemoved {
            XITSPDSFAxABusinessRulesType.valor = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "valor")
        }
        if !XITSPDSFAxABusinessRulesType.descricao.isRemoved {
            XITSPDSFAxABusinessRulesType.descricao = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "descricao")
        }
        if !XITSPDSFAxACenterSupplierType.plant.isRemoved {
            XITSPDSFAxACenterSupplierType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "Plant")
        }
        if !XITSPDSFAxACenterSupplierType.salesOrganization.isRemoved {
            XITSPDSFAxACenterSupplierType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxACenterSupplierType.plantName.isRemoved {
            XITSPDSFAxACenterSupplierType.plantName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "PlantName")
        }
        if !XITSPDSFAxACenterSupplierType.region.isRemoved {
            XITSPDSFAxACenterSupplierType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "Region")
        }
        if !XITSPDSFAxAConditionListType.conditionType.isRemoved {
            XITSPDSFAxAConditionListType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxAConditionListType.accessSequence.isRemoved {
            XITSPDSFAxAConditionListType.accessSequence = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "AccessSequence")
        }
        if !XITSPDSFAxAConditionListType.conditionTable.isRemoved {
            XITSPDSFAxAConditionListType.conditionTable = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "ConditionTable")
        }
        if !XITSPDSFAxAConditionListType.tableName.isRemoved {
            XITSPDSFAxAConditionListType.tableName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "TableName")
        }
        if !XITSPDSFAxAConditionListType.hasValidity.isRemoved {
            XITSPDSFAxAConditionListType.hasValidity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "HasValidity")
        }
        if !XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet.isRemoved {
            XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "to_xITSPDSFAxA_NameSet")
        }
        if !XITSPDSFAxAConditionScaleType.conditionType.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxAConditionScaleType.accessSequence.isRemoved {
            XITSPDSFAxAConditionScaleType.accessSequence = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "AccessSequence")
        }
        if !XITSPDSFAxAConditionScaleType.conditionRecord.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionRecord = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionRecord")
        }
        if !XITSPDSFAxAConditionScaleType.conditionSequentialNumber.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionSequentialNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionSequentialNumber")
        }
        if !XITSPDSFAxAConditionScaleType.conditionScaleLine.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionScaleLine = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionScaleLine")
        }
        if !XITSPDSFAxAConditionScaleType.conditionScaleQuantity.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionScaleQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionScaleQuantity")
        }
        if !XITSPDSFAxAConditionScaleType.conditionRateValue.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionRateValue")
        }
        if !XITSPDSFAxAConditionScaleType.conditionTable.isRemoved {
            XITSPDSFAxAConditionScaleType.conditionTable = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionTable")
        }
        if !XITSPDSFAxAConditionScaleType.tableName.isRemoved {
            XITSPDSFAxAConditionScaleType.tableName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "TableName")
        }
        if !XITSPDSFAxAConditionScaleType.hasValidity.isRemoved {
            XITSPDSFAxAConditionScaleType.hasValidity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "HasValidity")
        }
        if !XITSPDSFAxAConditionValue.conditionRecord.isRemoved {
            XITSPDSFAxAConditionValue.conditionRecord = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRecord")
        }
        if !XITSPDSFAxAConditionValue.conditionType.isRemoved {
            XITSPDSFAxAConditionValue.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionType")
        }
        if !XITSPDSFAxAConditionValue.accessSequence.isRemoved {
            XITSPDSFAxAConditionValue.accessSequence = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "AccessSequence")
        }
        if !XITSPDSFAxAConditionValue.conditionTable.isRemoved {
            XITSPDSFAxAConditionValue.conditionTable = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionTable")
        }
        if !XITSPDSFAxAConditionValue.field01.isRemoved {
            XITSPDSFAxAConditionValue.field01 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field01")
        }
        if !XITSPDSFAxAConditionValue.field02.isRemoved {
            XITSPDSFAxAConditionValue.field02 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field02")
        }
        if !XITSPDSFAxAConditionValue.field03.isRemoved {
            XITSPDSFAxAConditionValue.field03 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field03")
        }
        if !XITSPDSFAxAConditionValue.field04.isRemoved {
            XITSPDSFAxAConditionValue.field04 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field04")
        }
        if !XITSPDSFAxAConditionValue.field05.isRemoved {
            XITSPDSFAxAConditionValue.field05 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field05")
        }
        if !XITSPDSFAxAConditionValue.field06.isRemoved {
            XITSPDSFAxAConditionValue.field06 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field06")
        }
        if !XITSPDSFAxAConditionValue.field07.isRemoved {
            XITSPDSFAxAConditionValue.field07 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field07")
        }
        if !XITSPDSFAxAConditionValue.field08.isRemoved {
            XITSPDSFAxAConditionValue.field08 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field08")
        }
        if !XITSPDSFAxAConditionValue.field09.isRemoved {
            XITSPDSFAxAConditionValue.field09 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field09")
        }
        if !XITSPDSFAxAConditionValue.field10.isRemoved {
            XITSPDSFAxAConditionValue.field10 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field10")
        }
        if !XITSPDSFAxAConditionValue.field11.isRemoved {
            XITSPDSFAxAConditionValue.field11 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field11")
        }
        if !XITSPDSFAxAConditionValue.field12.isRemoved {
            XITSPDSFAxAConditionValue.field12 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field12")
        }
        if !XITSPDSFAxAConditionValue.field13.isRemoved {
            XITSPDSFAxAConditionValue.field13 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field13")
        }
        if !XITSPDSFAxAConditionValue.field14.isRemoved {
            XITSPDSFAxAConditionValue.field14 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field14")
        }
        if !XITSPDSFAxAConditionValue.field15.isRemoved {
            XITSPDSFAxAConditionValue.field15 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field15")
        }
        if !XITSPDSFAxAConditionValue.field16.isRemoved {
            XITSPDSFAxAConditionValue.field16 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field16")
        }
        if !XITSPDSFAxAConditionValue.field17.isRemoved {
            XITSPDSFAxAConditionValue.field17 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field17")
        }
        if !XITSPDSFAxAConditionValue.field18.isRemoved {
            XITSPDSFAxAConditionValue.field18 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field18")
        }
        if !XITSPDSFAxAConditionValue.field19.isRemoved {
            XITSPDSFAxAConditionValue.field19 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field19")
        }
        if !XITSPDSFAxAConditionValue.field20.isRemoved {
            XITSPDSFAxAConditionValue.field20 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field20")
        }
        if !XITSPDSFAxAConditionValue.conditionRateValue.isRemoved {
            XITSPDSFAxAConditionValue.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRateValue")
        }
        if !XITSPDSFAxAConditionValue.conditionRateValueUnit.isRemoved {
            XITSPDSFAxAConditionValue.conditionRateValueUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRateValueUnit")
        }
        if !XITSPDSFAxACustomerEmailXMLType.customer.isRemoved {
            XITSPDSFAxACustomerEmailXMLType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerEmailXMLType.emailAddressXML.isRemoved {
            XITSPDSFAxACustomerEmailXMLType.emailAddressXML = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "EmailAddressXML")
        }
        if !XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster.isRemoved {
            XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "to_xITSPDSFAxA_CustomerMaster")
        }
        if !XITSPDSFAxACustomerGrpTextType.customerGroup.isRemoved {
            XITSPDSFAxACustomerGrpTextType.customerGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType.property(withName: "CustomerGroup")
        }
        if !XITSPDSFAxACustomerGrpTextType.customerGroupName.isRemoved {
            XITSPDSFAxACustomerGrpTextType.customerGroupName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType.property(withName: "CustomerGroupName")
        }
        if !XITSPDSFAxACustomerLatLong.customer.isRemoved {
            XITSPDSFAxACustomerLatLong.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerLatLong.salesorganization.isRemoved {
            XITSPDSFAxACustomerLatLong.salesorganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Salesorganization")
        }
        if !XITSPDSFAxACustomerLatLong.distributionchannel.isRemoved {
            XITSPDSFAxACustomerLatLong.distributionchannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Distributionchannel")
        }
        if !XITSPDSFAxACustomerLatLong.division.isRemoved {
            XITSPDSFAxACustomerLatLong.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Division")
        }
        if !XITSPDSFAxACustomerLatLong.customerobsid.isRemoved {
            XITSPDSFAxACustomerLatLong.customerobsid = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customerobsid")
        }
        if !XITSPDSFAxACustomerLatLong.longitude.isRemoved {
            XITSPDSFAxACustomerLatLong.longitude = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Longitude")
        }
        if !XITSPDSFAxACustomerLatLong.latitude.isRemoved {
            XITSPDSFAxACustomerLatLong.latitude = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Latitude")
        }
        if !XITSPDSFAxACustomerLatLong.customerobsseq.isRemoved {
            XITSPDSFAxACustomerLatLong.customerobsseq = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customerobsseq")
        }
        if !XITSPDSFAxACustomerLatLong.bpcustomernumber.isRemoved {
            XITSPDSFAxACustomerLatLong.bpcustomernumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Bpcustomernumber")
        }
        if !XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster.isRemoved {
            XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "to_xITSPDSFAxA_CustomerMaster")
        }
        if !XITSPDSFAxACustomerLstInvType.customer.isRemoved {
            XITSPDSFAxACustomerLstInvType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerLstInvType.salesOrganization.isRemoved {
            XITSPDSFAxACustomerLstInvType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxACustomerLstInvType.bpCustomerNumber.isRemoved {
            XITSPDSFAxACustomerLstInvType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxACustomerLstInvType.distributionChannel.isRemoved {
            XITSPDSFAxACustomerLstInvType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxACustomerLstInvType.division.isRemoved {
            XITSPDSFAxACustomerLstInvType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "Division")
        }
        if !XITSPDSFAxACustomerLstInvType.brNotaFiscal.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNotaFiscal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NotaFiscal")
        }
        if !XITSPDSFAxACustomerLstInvType.salesDocument.isRemoved {
            XITSPDSFAxACustomerLstInvType.salesDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesDocument")
        }
        if !XITSPDSFAxACustomerLstInvType.soldToParty.isRemoved {
            XITSPDSFAxACustomerLstInvType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxACustomerLstInvType.billingDocument.isRemoved {
            XITSPDSFAxACustomerLstInvType.billingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BillingDocument")
        }
        if !XITSPDSFAxACustomerLstInvType.brNFeNumber.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNFeNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFeNumber")
        }
        if !XITSPDSFAxACustomerLstInvType.customerName.isRemoved {
            XITSPDSFAxACustomerLstInvType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxACustomerLstInvType.salesOrderType.isRemoved {
            XITSPDSFAxACustomerLstInvType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesOrderType")
        }
        if !XITSPDSFAxACustomerLstInvType.brNFType.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNFType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFType")
        }
        if !XITSPDSFAxACustomerLstInvType.creationDate.isRemoved {
            XITSPDSFAxACustomerLstInvType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "CreationDate")
        }
        if !XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFArrivalOrDepartureDate")
        }
        if !XITSPDSFAxACustomerLstInvType.brNFPostingDate.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNFPostingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFPostingDate")
        }
        if !XITSPDSFAxACustomerLstInvType.brNFTotalAmount.isRemoved {
            XITSPDSFAxACustomerLstInvType.brNFTotalAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFTotalAmount")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.customer.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.salesOrganization.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.distributionChannel.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.division.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Division")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.salesOrder.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.salesOrderDate.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.salesOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrderDate")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.totalNetAmount.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.totalNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "TotalNetAmount")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.conditionAmount.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.conditionAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "ConditionAmount")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.greenDate.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.greenDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "GreenDate")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.yellowDate.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.yellowDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "YellowDate")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.status.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.status = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Status")
        }
        if !XITSPDSFAxACustomerLstSalOrdType.colorHex.isRemoved {
            XITSPDSFAxACustomerLstSalOrdType.colorHex = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "ColorHex")
        }
        if !XITSPDSFAxACustomerMasterType.customer.isRemoved {
            XITSPDSFAxACustomerMasterType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerMasterType.organizationBPName1.isRemoved {
            XITSPDSFAxACustomerMasterType.organizationBPName1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName1")
        }
        if !XITSPDSFAxACustomerMasterType.organizationBPName2.isRemoved {
            XITSPDSFAxACustomerMasterType.organizationBPName2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName2")
        }
        if !XITSPDSFAxACustomerMasterType.organizationBPName3.isRemoved {
            XITSPDSFAxACustomerMasterType.organizationBPName3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName3")
        }
        if !XITSPDSFAxACustomerMasterType.organizationBPName4.isRemoved {
            XITSPDSFAxACustomerMasterType.organizationBPName4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName4")
        }
        if !XITSPDSFAxACustomerMasterType.searchTerm1.isRemoved {
            XITSPDSFAxACustomerMasterType.searchTerm1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "SearchTerm1")
        }
        if !XITSPDSFAxACustomerMasterType.cityName.isRemoved {
            XITSPDSFAxACustomerMasterType.cityName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CityName")
        }
        if !XITSPDSFAxACustomerMasterType.country.isRemoved {
            XITSPDSFAxACustomerMasterType.country = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Country")
        }
        if !XITSPDSFAxACustomerMasterType.district.isRemoved {
            XITSPDSFAxACustomerMasterType.district = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "District")
        }
        if !XITSPDSFAxACustomerMasterType.region.isRemoved {
            XITSPDSFAxACustomerMasterType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Region")
        }
        if !XITSPDSFAxACustomerMasterType.streetName.isRemoved {
            XITSPDSFAxACustomerMasterType.streetName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "StreetName")
        }
        if !XITSPDSFAxACustomerMasterType.houseNumber.isRemoved {
            XITSPDSFAxACustomerMasterType.houseNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "HouseNumber")
        }
        if !XITSPDSFAxACustomerMasterType.postalCode.isRemoved {
            XITSPDSFAxACustomerMasterType.postalCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "PostalCode")
        }
        if !XITSPDSFAxACustomerMasterType.customerCreditLimitAmount.isRemoved {
            XITSPDSFAxACustomerMasterType.customerCreditLimitAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CustomerCreditLimitAmount")
        }
        if !XITSPDSFAxACustomerMasterType.creditExposure.isRemoved {
            XITSPDSFAxACustomerMasterType.creditExposure = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CreditExposure")
        }
        if !XITSPDSFAxACustomerMasterType.xBlocked.isRemoved {
            XITSPDSFAxACustomerMasterType.xBlocked = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "XBlocked")
        }
        if !XITSPDSFAxACustomerMasterType.taxJurisdiction.isRemoved {
            XITSPDSFAxACustomerMasterType.taxJurisdiction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxJurisdiction")
        }
        if !XITSPDSFAxACustomerMasterType.emailAddress.isRemoved {
            XITSPDSFAxACustomerMasterType.emailAddress = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "EmailAddress")
        }
        if !XITSPDSFAxACustomerMasterType.phoneNumber.isRemoved {
            XITSPDSFAxACustomerMasterType.phoneNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "PhoneNumber")
        }
        if !XITSPDSFAxACustomerMasterType.taxNumber1.isRemoved {
            XITSPDSFAxACustomerMasterType.taxNumber1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxNumber1")
        }
        if !XITSPDSFAxACustomerMasterType.taxNumber3.isRemoved {
            XITSPDSFAxACustomerMasterType.taxNumber3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxNumber3")
        }
        if !XITSPDSFAxACustomerMasterType.fieldAux1.isRemoved {
            XITSPDSFAxACustomerMasterType.fieldAux1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "FIELD_AUX_1")
        }
        if !XITSPDSFAxACustomerMasterType.fieldAux2.isRemoved {
            XITSPDSFAxACustomerMasterType.fieldAux2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "FIELD_AUX_2")
        }
        if !XITSPDSFAxACustomerMasterType.isFinal.isRemoved {
            XITSPDSFAxACustomerMasterType.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "IsFinal")
        }
        if !XITSPDSFAxACustomerMasterType.industrialSector.isRemoved {
            XITSPDSFAxACustomerMasterType.industrialSector = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "IndustrialSector")
        }
        if !XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML.isRemoved {
            XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerEmailXML")
        }
        if !XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea.isRemoved {
            XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")
        }
        if !XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong.isRemoved {
            XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerLatLong")
        }
        if !XITSPDSFAxACustomerSalesAreaType.customer.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerSalesAreaType.salesOrganization.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxACustomerSalesAreaType.distributionChannel.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxACustomerSalesAreaType.division.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Division")
        }
        if !XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxACustomerSalesAreaType.partnerFunction.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PartnerFunction")
        }
        if !XITSPDSFAxACustomerSalesAreaType.partnerFunctionName.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.partnerFunctionName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PartnerFunctionName")
        }
        if !XITSPDSFAxACustomerSalesAreaType.customerName.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxACustomerSalesAreaType.customerPriceGroup.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.customerPriceGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerPriceGroup")
        }
        if !XITSPDSFAxACustomerSalesAreaType.customerGroup.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.customerGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerGroup")
        }
        if !XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerPaymentTerms")
        }
        if !XITSPDSFAxACustomerSalesAreaType.priceListType.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.priceListType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PriceListType")
        }
        if !XITSPDSFAxACustomerSalesAreaType.salesOffice.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.salesOffice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesOffice")
        }
        if !XITSPDSFAxACustomerSalesAreaType.salesDistrict.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.salesDistrict = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesDistrict")
        }
        if !XITSPDSFAxACustomerSalesAreaType.supplyingPlant.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.supplyingPlant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SupplyingPlant")
        }
        if !XITSPDSFAxACustomerSalesAreaType.currency.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.currency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Currency")
        }
        if !XITSPDSFAxACustomerSalesAreaType.incotermsClassification.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "IncotermsClassification")
        }
        if !XITSPDSFAxACustomerSalesAreaType.status.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.status = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Status")
        }
        if !XITSPDSFAxACustomerSalesAreaType.colorHex.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.colorHex = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "ColorHex")
        }
        if !XITSPDSFAxACustomerSalesAreaType.isFinal.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "IsFinal")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_BillingDocument")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerGrpText")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerLstInv")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerLstSalOrd")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerMaster")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerShipTo")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Notifications")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Partner")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_PaymentTerms")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Pricelists")
        }
        if !XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders.isRemoved {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_SalesOrders")
        }
        if !XITSPDSFAxACustomerShipToType.customer.isRemoved {
            XITSPDSFAxACustomerShipToType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Customer")
        }
        if !XITSPDSFAxACustomerShipToType.bpCustomerNumber.isRemoved {
            XITSPDSFAxACustomerShipToType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxACustomerShipToType.salesOrganization.isRemoved {
            XITSPDSFAxACustomerShipToType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxACustomerShipToType.distributionChannel.isRemoved {
            XITSPDSFAxACustomerShipToType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxACustomerShipToType.division.isRemoved {
            XITSPDSFAxACustomerShipToType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Division")
        }
        if !XITSPDSFAxACustomerShipToType.partnerCounter.isRemoved {
            XITSPDSFAxACustomerShipToType.partnerCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PartnerCounter")
        }
        if !XITSPDSFAxACustomerShipToType.partnerFunction.isRemoved {
            XITSPDSFAxACustomerShipToType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PartnerFunction")
        }
        if !XITSPDSFAxACustomerShipToType.customerShipNumber.isRemoved {
            XITSPDSFAxACustomerShipToType.customerShipNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CustomerShipNumber")
        }
        if !XITSPDSFAxACustomerShipToType.customerShipName.isRemoved {
            XITSPDSFAxACustomerShipToType.customerShipName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CustomerShipName")
        }
        if !XITSPDSFAxACustomerShipToType.taxNumber1.isRemoved {
            XITSPDSFAxACustomerShipToType.taxNumber1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "TaxNumber1")
        }
        if !XITSPDSFAxACustomerShipToType.region.isRemoved {
            XITSPDSFAxACustomerShipToType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Region")
        }
        if !XITSPDSFAxACustomerShipToType.cityName.isRemoved {
            XITSPDSFAxACustomerShipToType.cityName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CityName")
        }
        if !XITSPDSFAxACustomerShipToType.streetName.isRemoved {
            XITSPDSFAxACustomerShipToType.streetName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "StreetName")
        }
        if !XITSPDSFAxACustomerShipToType.country.isRemoved {
            XITSPDSFAxACustomerShipToType.country = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Country")
        }
        if !XITSPDSFAxACustomerShipToType.postalCode.isRemoved {
            XITSPDSFAxACustomerShipToType.postalCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PostalCode")
        }
        if !XITSPDSFAxADocumentsType.linkedSAPObjectKey.isRemoved {
            XITSPDSFAxADocumentsType.linkedSAPObjectKey = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LinkedSAPObjectKey")
        }
        if !XITSPDSFAxADocumentsType.documentInfoRecordDocType.isRemoved {
            XITSPDSFAxADocumentsType.documentInfoRecordDocType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocType")
        }
        if !XITSPDSFAxADocumentsType.documentInfoRecordDocNumber.isRemoved {
            XITSPDSFAxADocumentsType.documentInfoRecordDocNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocNumber")
        }
        if !XITSPDSFAxADocumentsType.documentInfoRecordDocVersion.isRemoved {
            XITSPDSFAxADocumentsType.documentInfoRecordDocVersion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocVersion")
        }
        if !XITSPDSFAxADocumentsType.documentInfoRecordDocPart.isRemoved {
            XITSPDSFAxADocumentsType.documentInfoRecordDocPart = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocPart")
        }
        if !XITSPDSFAxADocumentsType.archiveDocumentID.isRemoved {
            XITSPDSFAxADocumentsType.archiveDocumentID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "ArchiveDocumentID")
        }
        if !XITSPDSFAxADocumentsType.salesOrganization.isRemoved {
            XITSPDSFAxADocumentsType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxADocumentsType.distributionChannel.isRemoved {
            XITSPDSFAxADocumentsType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxADocumentsType.plant.isRemoved {
            XITSPDSFAxADocumentsType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "Plant")
        }
        if !XITSPDSFAxADocumentsType.linkedSAPObject.isRemoved {
            XITSPDSFAxADocumentsType.linkedSAPObject = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LinkedSAPObject")
        }
        if !XITSPDSFAxADocumentsType.fileName.isRemoved {
            XITSPDSFAxADocumentsType.fileName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "FileName")
        }
        if !XITSPDSFAxADocumentsType.fileSize.isRemoved {
            XITSPDSFAxADocumentsType.fileSize = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "FileSize")
        }
        if !XITSPDSFAxADocumentsType.workstationApplication.isRemoved {
            XITSPDSFAxADocumentsType.workstationApplication = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "WorkstationApplication")
        }
        if !XITSPDSFAxADocumentsType.mimeType.isRemoved {
            XITSPDSFAxADocumentsType.mimeType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "MimeType")
        }
        if !XITSPDSFAxADocumentsType.logicalDocument.isRemoved {
            XITSPDSFAxADocumentsType.logicalDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LogicalDocument")
        }
        if !XITSPDSFAxADocumentsType.reportContentMedia.isRemoved {
            XITSPDSFAxADocumentsType.reportContentMedia = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "ReportContentMedia")
        }
        if !XITSPDSFAxAHierarchyType.partner.isRemoved {
            XITSPDSFAxAHierarchyType.partner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "PARTNER")
        }
        if !XITSPDSFAxAHierarchyType.organization.isRemoved {
            XITSPDSFAxAHierarchyType.organization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "ORGANIZATION")
        }
        if !XITSPDSFAxAHierarchyType.parent.isRemoved {
            XITSPDSFAxAHierarchyType.parent = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "PARENT")
        }
        if !XITSPDSFAxAHierarchyType.child.isRemoved {
            XITSPDSFAxAHierarchyType.child = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "CHILD")
        }
        if !XITSPDSFAxAIncoTermsType.incotermsClassification.isRemoved {
            XITSPDSFAxAIncoTermsType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType.property(withName: "IncotermsClassification")
        }
        if !XITSPDSFAxAIncoTermsType.incotermsClassificationName.isRemoved {
            XITSPDSFAxAIncoTermsType.incotermsClassificationName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType.property(withName: "IncotermsClassificationName")
        }
        if !XITSPDSFAxAInvoiceType.salesDocument.isRemoved {
            XITSPDSFAxAInvoiceType.salesDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesDocument")
        }
        if !XITSPDSFAxAInvoiceType.soldToParty.isRemoved {
            XITSPDSFAxAInvoiceType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxAInvoiceType.bpCustomerNumber.isRemoved {
            XITSPDSFAxAInvoiceType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxAInvoiceType.billingDocument.isRemoved {
            XITSPDSFAxAInvoiceType.billingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BillingDocument")
        }
        if !XITSPDSFAxAInvoiceType.brNotaFiscal.isRemoved {
            XITSPDSFAxAInvoiceType.brNotaFiscal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NotaFiscal")
        }
        if !XITSPDSFAxAInvoiceType.brNFeNumber.isRemoved {
            XITSPDSFAxAInvoiceType.brNFeNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFeNumber")
        }
        if !XITSPDSFAxAInvoiceType.customerName.isRemoved {
            XITSPDSFAxAInvoiceType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxAInvoiceType.purchaseOrderByCustomer.isRemoved {
            XITSPDSFAxAInvoiceType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "PurchaseOrderByCustomer")
        }
        if !XITSPDSFAxAInvoiceType.salesOrganization.isRemoved {
            XITSPDSFAxAInvoiceType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAInvoiceType.distributionChannel.isRemoved {
            XITSPDSFAxAInvoiceType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxAInvoiceType.organizationDivision.isRemoved {
            XITSPDSFAxAInvoiceType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxAInvoiceType.salesOrderType.isRemoved {
            XITSPDSFAxAInvoiceType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesOrderType")
        }
        if !XITSPDSFAxAInvoiceType.brNFType.isRemoved {
            XITSPDSFAxAInvoiceType.brNFType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFType")
        }
        if !XITSPDSFAxAInvoiceType.creationDate.isRemoved {
            XITSPDSFAxAInvoiceType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "CreationDate")
        }
        if !XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate.isRemoved {
            XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFArrivalOrDepartureDate")
        }
        if !XITSPDSFAxAInvoiceType.brNFPostingDate.isRemoved {
            XITSPDSFAxAInvoiceType.brNFPostingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFPostingDate")
        }
        if !XITSPDSFAxAInvoiceType.brNFTotalAmount.isRemoved {
            XITSPDSFAxAInvoiceType.brNFTotalAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFTotalAmount")
        }
        if !XITSPDSFAxAMsgPrtnrType.messageID.isRemoved {
            XITSPDSFAxAMsgPrtnrType.messageID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "MessageID")
        }
        if !XITSPDSFAxAMsgPrtnrType.salesOrganization.isRemoved {
            XITSPDSFAxAMsgPrtnrType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAMsgPrtnrType.partnerID.isRemoved {
            XITSPDSFAxAMsgPrtnrType.partnerID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "PartnerID")
        }
        if !XITSPDSFAxAMsgPrtnrType.createdAt.isRemoved {
            XITSPDSFAxAMsgPrtnrType.createdAt = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "CreatedAt")
        }
        if !XITSPDSFAxAMsgPrtnrType.createdBy.isRemoved {
            XITSPDSFAxAMsgPrtnrType.createdBy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "CreatedBy")
        }
        if !XITSPDSFAxAMsgPrtnrType.lastChangedAt.isRemoved {
            XITSPDSFAxAMsgPrtnrType.lastChangedAt = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "LastChangedAt")
        }
        if !XITSPDSFAxAMsgPrtnrType.lastChangedBy.isRemoved {
            XITSPDSFAxAMsgPrtnrType.lastChangedBy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "LastChangedBy")
        }
        if !XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg.isRemoved {
            XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "to_xITSPDSFAxA_Msg")
        }
        if !XITSPDSFAxAMsgType.messageID.isRemoved {
            XITSPDSFAxAMsgType.messageID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageID")
        }
        if !XITSPDSFAxAMsgType.messageType.isRemoved {
            XITSPDSFAxAMsgType.messageType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageType")
        }
        if !XITSPDSFAxAMsgType.messageTitle.isRemoved {
            XITSPDSFAxAMsgType.messageTitle = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageTitle")
        }
        if !XITSPDSFAxAMsgType.documentVideo.isRemoved {
            XITSPDSFAxAMsgType.documentVideo = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "DocumentVideo")
        }
        if !XITSPDSFAxAMsgType.documentImage.isRemoved {
            XITSPDSFAxAMsgType.documentImage = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "DocumentImage")
        }
        if !XITSPDSFAxAMsgType.createdAt.isRemoved {
            XITSPDSFAxAMsgType.createdAt = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "CreatedAt")
        }
        if !XITSPDSFAxAMsgType.createdBy.isRemoved {
            XITSPDSFAxAMsgType.createdBy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "CreatedBy")
        }
        if !XITSPDSFAxAMsgType.lastChangedAt.isRemoved {
            XITSPDSFAxAMsgType.lastChangedAt = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "LastChangedAt")
        }
        if !XITSPDSFAxAMsgType.lastChangedBy.isRemoved {
            XITSPDSFAxAMsgType.lastChangedBy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "LastChangedBy")
        }
        if !XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr.isRemoved {
            XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "to_xITSPDSFAxA_MsgPrtnr")
        }
        if !XITSPDSFAxANameSetType.conditionType.isRemoved {
            XITSPDSFAxANameSetType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxANameSetType.accessSequence.isRemoved {
            XITSPDSFAxANameSetType.accessSequence = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "AccessSequence")
        }
        if !XITSPDSFAxANameSetType.sapField.isRemoved {
            XITSPDSFAxANameSetType.sapField = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "SAP_Field")
        }
        if !XITSPDSFAxANameSetType.oDataField.isRemoved {
            XITSPDSFAxANameSetType.oDataField = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "OData_Field")
        }
        if !XITSPDSFAxANameSetType.entity.isRemoved {
            XITSPDSFAxANameSetType.entity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Entity")
        }
        if !XITSPDSFAxANameSetType.atribute.isRemoved {
            XITSPDSFAxANameSetType.atribute = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Atribute")
        }
        if !XITSPDSFAxANameSetType.isOutPut.isRemoved {
            XITSPDSFAxANameSetType.isOutPut = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Is_OutPut")
        }
        if !XITSPDSFAxANotificationsStatType.notification.isRemoved {
            XITSPDSFAxANotificationsStatType.notification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Notification")
        }
        if !XITSPDSFAxANotificationsStatType.istat.isRemoved {
            XITSPDSFAxANotificationsStatType.istat = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "istat")
        }
        if !XITSPDSFAxANotificationsStatType.salesOrganization.isRemoved {
            XITSPDSFAxANotificationsStatType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxANotificationsStatType.distributionChannel.isRemoved {
            XITSPDSFAxANotificationsStatType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxANotificationsStatType.customer.isRemoved {
            XITSPDSFAxANotificationsStatType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Customer")
        }
        if !XITSPDSFAxANotificationsStatType.bpCustomerNumber.isRemoved {
            XITSPDSFAxANotificationsStatType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxANotificationsStatType.status.isRemoved {
            XITSPDSFAxANotificationsStatType.status = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Status")
        }
        if !XITSPDSFAxANotificationsStatType.statusDesc.isRemoved {
            XITSPDSFAxANotificationsStatType.statusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "StatusDesc")
        }
        if !XITSPDSFAxANotificationsType.notification.isRemoved {
            XITSPDSFAxANotificationsType.notification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Notification")
        }
        if !XITSPDSFAxANotificationsType.salesOrganization.isRemoved {
            XITSPDSFAxANotificationsType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxANotificationsType.distributionChannel.isRemoved {
            XITSPDSFAxANotificationsType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxANotificationsType.customer.isRemoved {
            XITSPDSFAxANotificationsType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Customer")
        }
        if !XITSPDSFAxANotificationsType.bpCustomerNumber.isRemoved {
            XITSPDSFAxANotificationsType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxANotificationsType.notificationType.isRemoved {
            XITSPDSFAxANotificationsType.notificationType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationType")
        }
        if !XITSPDSFAxANotificationsType.notificationText.isRemoved {
            XITSPDSFAxANotificationsType.notificationText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationText")
        }
        if !XITSPDSFAxANotificationsType.notificationPriorityType.isRemoved {
            XITSPDSFAxANotificationsType.notificationPriorityType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationPriorityType")
        }
        if !XITSPDSFAxANotificationsType.notificationPriority.isRemoved {
            XITSPDSFAxANotificationsType.notificationPriority = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationPriority")
        }
        if !XITSPDSFAxANotificationsType.maintPriorityDesc.isRemoved {
            XITSPDSFAxANotificationsType.maintPriorityDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "MaintPriorityDesc")
        }
        if !XITSPDSFAxANotificationsType.createdByUser.isRemoved {
            XITSPDSFAxANotificationsType.createdByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "CreatedByUser")
        }
        if !XITSPDSFAxANotificationsType.creationDate.isRemoved {
            XITSPDSFAxANotificationsType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "CreationDate")
        }
        if !XITSPDSFAxANotificationsType.lastChangedByUser.isRemoved {
            XITSPDSFAxANotificationsType.lastChangedByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "LastChangedByUser")
        }
        if !XITSPDSFAxANotificationsType.lastChangedDate.isRemoved {
            XITSPDSFAxANotificationsType.lastChangedDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "LastChangedDate")
        }
        if !XITSPDSFAxANotificationsType.notificationReportingDate.isRemoved {
            XITSPDSFAxANotificationsType.notificationReportingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationReportingDate")
        }
        if !XITSPDSFAxANotificationsType.orderID.isRemoved {
            XITSPDSFAxANotificationsType.orderID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "OrderID")
        }
        if !XITSPDSFAxANotificationsType.material.isRemoved {
            XITSPDSFAxANotificationsType.material = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Material")
        }
        if !XITSPDSFAxANotificationsType.notificationStatusObject.isRemoved {
            XITSPDSFAxANotificationsType.notificationStatusObject = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationStatusObject")
        }
        if !XITSPDSFAxANotificationsType.notificationCompletionDate.isRemoved {
            XITSPDSFAxANotificationsType.notificationCompletionDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationCompletionDate")
        }
        if !XITSPDSFAxANotificationsType.notificationOrigin.isRemoved {
            XITSPDSFAxANotificationsType.notificationOrigin = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationOrigin")
        }
        if !XITSPDSFAxANotificationsType.activeDivision.isRemoved {
            XITSPDSFAxANotificationsType.activeDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "ActiveDivision")
        }
        if !XITSPDSFAxANotificationsType.notificationQuantityUnit.isRemoved {
            XITSPDSFAxANotificationsType.notificationQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationQuantityUnit")
        }
        if !XITSPDSFAxANotificationsType.isDeleted_.isRemoved {
            XITSPDSFAxANotificationsType.isDeleted_ = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "IsDeleted")
        }
        if !XITSPDSFAxANotificationsType.notifProcessingPhase.isRemoved {
            XITSPDSFAxANotificationsType.notifProcessingPhase = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotifProcessingPhase")
        }
        if !XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat.isRemoved {
            XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "to_xITSPDSFAxA_NotificationsStat")
        }
        if !XITSPDSFAxAPartnerLastSalesType.salesOrder.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxAPartnerLastSalesType.salesOrganization.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAPartnerLastSalesType.soldToParty.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxAPartnerLastSalesType.customerName.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxAPartnerLastSalesType.distributionChannel.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxAPartnerLastSalesType.organizationDivision.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxAPartnerLastSalesType.salesOrderDate.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.salesOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrderDate")
        }
        if !XITSPDSFAxAPartnerLastSalesType.totalNetAmount.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.totalNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "TotalNetAmount")
        }
        if !XITSPDSFAxAPartnerLastSalesType.conditionAmount.isRemoved {
            XITSPDSFAxAPartnerLastSalesType.conditionAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "ConditionAmount")
        }
        if !XITSPDSFAxAPartnerType.customer.isRemoved {
            XITSPDSFAxAPartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "Customer")
        }
        if !XITSPDSFAxAPartnerType.bpCustomerNumber.isRemoved {
            XITSPDSFAxAPartnerType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxAPartnerType.partnerFunction.isRemoved {
            XITSPDSFAxAPartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "PartnerFunction")
        }
        if !XITSPDSFAxAPartnerType.businessPartnerAddressID.isRemoved {
            XITSPDSFAxAPartnerType.businessPartnerAddressID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BusinessPartnerAddressID")
        }
        if !XITSPDSFAxAPartnerType.salesOrganization.isRemoved {
            XITSPDSFAxAPartnerType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAPartnerType.distributionChannel.isRemoved {
            XITSPDSFAxAPartnerType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxAPartnerType.division.isRemoved {
            XITSPDSFAxAPartnerType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "Division")
        }
        if !XITSPDSFAxAPartnerType.partnerFunctionName.isRemoved {
            XITSPDSFAxAPartnerType.partnerFunctionName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "PartnerFunctionName")
        }
        if !XITSPDSFAxAPartnerType.businessPartnerFullName.isRemoved {
            XITSPDSFAxAPartnerType.businessPartnerFullName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BusinessPartnerFullName")
        }
        if !XITSPDSFAxAPartnerType.bpTaxNumber.isRemoved {
            XITSPDSFAxAPartnerType.bpTaxNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BPTaxNumber")
        }
        if !XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea.isRemoved {
            XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")
        }
        if !XITSPDSFAxAPartnerUserType.userLogin.isRemoved {
            XITSPDSFAxAPartnerUserType.userLogin = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "UserLogin")
        }
        if !XITSPDSFAxAPartnerUserType.bpCustomerNumber.isRemoved {
            XITSPDSFAxAPartnerUserType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners.isRemoved {
            XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "to_xITSPDSFAxA_Partners")
        }
        if !XITSPDSFAxAPartnersType.bpCustomerNumber.isRemoved {
            XITSPDSFAxAPartnersType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxAPartnersType.salesOrganization.isRemoved {
            XITSPDSFAxAPartnersType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAPartnersType.salesOrganizationName.isRemoved {
            XITSPDSFAxAPartnersType.salesOrganizationName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "SalesOrganizationName")
        }
        if !XITSPDSFAxAPartnersType.partnerFunction.isRemoved {
            XITSPDSFAxAPartnersType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "PartnerFunction")
        }
        if !XITSPDSFAxAPartnersType.businessPartnerAddressID.isRemoved {
            XITSPDSFAxAPartnersType.businessPartnerAddressID = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BusinessPartnerAddressID")
        }
        if !XITSPDSFAxAPartnersType.businessPartnerFullName.isRemoved {
            XITSPDSFAxAPartnersType.businessPartnerFullName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BusinessPartnerFullName")
        }
        if !XITSPDSFAxAPartnersType.addressPhoneNumber.isRemoved {
            XITSPDSFAxAPartnersType.addressPhoneNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "AddressPhoneNumber")
        }
        if !XITSPDSFAxAPartnersType.mobilePhoneNumber.isRemoved {
            XITSPDSFAxAPartnersType.mobilePhoneNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "MobilePhoneNumber")
        }
        if !XITSPDSFAxAPartnersType.emailAddress.isRemoved {
            XITSPDSFAxAPartnersType.emailAddress = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "EmailAddress")
        }
        if !XITSPDSFAxAPartnersType.bpTaxNumber.isRemoved {
            XITSPDSFAxAPartnersType.bpTaxNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BPTaxNumber")
        }
        if !XITSPDSFAxAPartnersType.cityName.isRemoved {
            XITSPDSFAxAPartnersType.cityName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "CityName")
        }
        if !XITSPDSFAxAPartnersType.postalCode.isRemoved {
            XITSPDSFAxAPartnersType.postalCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "PostalCode")
        }
        if !XITSPDSFAxAPartnersType.region.isRemoved {
            XITSPDSFAxAPartnersType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "Region")
        }
        if !XITSPDSFAxAPartnersType.organizationBPName1.isRemoved {
            XITSPDSFAxAPartnersType.organizationBPName1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "OrganizationBPName1")
        }
        if !XITSPDSFAxAPartnersType.companyCode.isRemoved {
            XITSPDSFAxAPartnersType.companyCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "CompanyCode")
        }
        if !XITSPDSFAxAPartnersType.isFinal.isRemoved {
            XITSPDSFAxAPartnersType.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "IsFinal")
        }
        if !XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy.isRemoved {
            XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "to_xITSPDSFAxA_Hierarchy")
        }
        if !XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales.isRemoved {
            XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "to_xITSPDSFAxA_PartnerLastSales")
        }
        if !XITSPDSFAxAPaymentTermsType.paymentTerms.isRemoved {
            XITSPDSFAxAPaymentTermsType.paymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType.property(withName: "PaymentTerms")
        }
        if !XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc.isRemoved {
            XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType.property(withName: "PaymentTermsConditionDesc")
        }
        if !XITSPDSFAxAPricelistsType.priceListType.isRemoved {
            XITSPDSFAxAPricelistsType.priceListType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType.property(withName: "PriceListType")
        }
        if !XITSPDSFAxAPricelistsType.priceListTypeName.isRemoved {
            XITSPDSFAxAPricelistsType.priceListTypeName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType.property(withName: "PriceListTypeName")
        }
        if !XITSPDSFAxAProductHieLevel1Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel1Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel2Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel2Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel2Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel2Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel2Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel3Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel3Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel4Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel5Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductHieLevel5Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel5Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel6Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel6.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.zProductHierarchyLevel6 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ZProductHierarchyLevel6")
        }
        if !XITSPDSFAxAProductHieLevel6Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel6Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel7Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel6.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel6 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel6")
        }
        if !XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel7.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.zProductHierarchyLevel7 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ZProductHierarchyLevel7")
        }
        if !XITSPDSFAxAProductHieLevel7Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel7Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel8Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel6")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel7")
        }
        if !XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel8")
        }
        if !XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductHieLevel9Type.productHierarchyNode.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.productHierarchyNode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ProductHierarchyNode")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel6.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel6 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel6")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel7.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel7 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel7")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel8.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel8 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel8")
        }
        if !XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel9.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.zProductHierarchyLevel9 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ZProductHierarchyLevel9")
        }
        if !XITSPDSFAxAProductHieLevel9Type.productHierarchyNodeText.isRemoved {
            XITSPDSFAxAProductHieLevel9Type.productHierarchyNodeText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type.property(withName: "ProductHierarchyNodeText")
        }
        if !XITSPDSFAxAProductMasterType.product.isRemoved {
            XITSPDSFAxAProductMasterType.product = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Product")
        }
        if !XITSPDSFAxAProductMasterType.salesOrganization.isRemoved {
            XITSPDSFAxAProductMasterType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxAProductMasterType.distributionChannel.isRemoved {
            XITSPDSFAxAProductMasterType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxAProductMasterType.plant.isRemoved {
            XITSPDSFAxAProductMasterType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Plant")
        }
        if !XITSPDSFAxAProductMasterType.mtpos.isRemoved {
            XITSPDSFAxAProductMasterType.mtpos = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "mtpos")
        }
        if !XITSPDSFAxAProductMasterType.salesUnit.isRemoved {
            XITSPDSFAxAProductMasterType.salesUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "SalesUnit")
        }
        if !XITSPDSFAxAProductMasterType.productName.isRemoved {
            XITSPDSFAxAProductMasterType.productName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductName")
        }
        if !XITSPDSFAxAProductMasterType.productType.isRemoved {
            XITSPDSFAxAProductMasterType.productType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductType")
        }
        if !XITSPDSFAxAProductMasterType.grossWeight.isRemoved {
            XITSPDSFAxAProductMasterType.grossWeight = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "GrossWeight")
        }
        if !XITSPDSFAxAProductMasterType.weightUnit.isRemoved {
            XITSPDSFAxAProductMasterType.weightUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "WeightUnit")
        }
        if !XITSPDSFAxAProductMasterType.productGroup.isRemoved {
            XITSPDSFAxAProductMasterType.productGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductGroup")
        }
        if !XITSPDSFAxAProductMasterType.baseUnit.isRemoved {
            XITSPDSFAxAProductMasterType.baseUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "BaseUnit")
        }
        if !XITSPDSFAxAProductMasterType.netWeight.isRemoved {
            XITSPDSFAxAProductMasterType.netWeight = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "NetWeight")
        }
        if !XITSPDSFAxAProductMasterType.productHierarchy.isRemoved {
            XITSPDSFAxAProductMasterType.productHierarchy = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductHierarchy")
        }
        if !XITSPDSFAxAProductMasterType.productHierarchyText.isRemoved {
            XITSPDSFAxAProductMasterType.productHierarchyText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductHierarchyText")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel1.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel1")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel2.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel2")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel3.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel3")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel4.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel4 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel4")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel5.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel5 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel5")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel6.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel6 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel6")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel7.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel7 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel7")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel8.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel8 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel8")
        }
        if !XITSPDSFAxAProductMasterType.zProductHierarchyLevel9.isRemoved {
            XITSPDSFAxAProductMasterType.zProductHierarchyLevel9 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel9")
        }
        if !XITSPDSFAxAProductMasterType.division.isRemoved {
            XITSPDSFAxAProductMasterType.division = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Division")
        }
        if !XITSPDSFAxAProductMasterType.volumeUnit.isRemoved {
            XITSPDSFAxAProductMasterType.volumeUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "VolumeUnit")
        }
        if !XITSPDSFAxAProductMasterType.materialVolume.isRemoved {
            XITSPDSFAxAProductMasterType.materialVolume = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialVolume")
        }
        if !XITSPDSFAxAProductMasterType.eanupc.isRemoved {
            XITSPDSFAxAProductMasterType.eanupc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "EANUPC")
        }
        if !XITSPDSFAxAProductMasterType.ean.isRemoved {
            XITSPDSFAxAProductMasterType.ean = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "EAN")
        }
        if !XITSPDSFAxAProductMasterType.industrySector.isRemoved {
            XITSPDSFAxAProductMasterType.industrySector = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "IndustrySector")
        }
        if !XITSPDSFAxAProductMasterType.length.isRemoved {
            XITSPDSFAxAProductMasterType.length = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Length")
        }
        if !XITSPDSFAxAProductMasterType.width.isRemoved {
            XITSPDSFAxAProductMasterType.width = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Width")
        }
        if !XITSPDSFAxAProductMasterType.nbmcode.isRemoved {
            XITSPDSFAxAProductMasterType.nbmcode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "NBMCODE")
        }
        if !XITSPDSFAxAProductMasterType.height.isRemoved {
            XITSPDSFAxAProductMasterType.height = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Height")
        }
        if !XITSPDSFAxAProductMasterType.cubage.isRemoved {
            XITSPDSFAxAProductMasterType.cubage = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Cubage")
        }
        if !XITSPDSFAxAProductMasterType.dun14.isRemoved {
            XITSPDSFAxAProductMasterType.dun14 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "DUN14")
        }
        if !XITSPDSFAxAProductMasterType.multiple.isRemoved {
            XITSPDSFAxAProductMasterType.multiple = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Multiple")
        }
        if !XITSPDSFAxAProductMasterType.productRelease.isRemoved {
            XITSPDSFAxAProductMasterType.productRelease = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductRelease")
        }
        if !XITSPDSFAxAProductMasterType.unrestrictedUseStock.isRemoved {
            XITSPDSFAxAProductMasterType.unrestrictedUseStock = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "UnrestrictedUseStock")
        }
        if !XITSPDSFAxAProductMasterType.materialGroup.isRemoved {
            XITSPDSFAxAProductMasterType.materialGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialGroup")
        }
        if !XITSPDSFAxAProductMasterType.materialGroupDesc.isRemoved {
            XITSPDSFAxAProductMasterType.materialGroupDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialGroupDesc")
        }
        if !XITSPDSFAxAProductMasterType.priceSpecificationProductGroup.isRemoved {
            XITSPDSFAxAProductMasterType.priceSpecificationProductGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "PriceSpecificationProductGroup")
        }
        if !XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc.isRemoved {
            XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "PriceSpecProdGrpDesc")
        }
        if !XITSPDSFAxAProductMasterType.mtorg.isRemoved {
            XITSPDSFAxAProductMasterType.mtorg = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "mtorg")
        }
        if !XITSPDSFAxAProductMasterType.oldMaterial.isRemoved {
            XITSPDSFAxAProductMasterType.oldMaterial = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "OldMaterial")
        }
        if !XITSPDSFAxAProductMasterType.comissionGroup.isRemoved {
            XITSPDSFAxAProductMasterType.comissionGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ComissionGroup")
        }
        if !XITSPDSFAxAProductMasterType.externalMaterialGroup.isRemoved {
            XITSPDSFAxAProductMasterType.externalMaterialGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ExternalMaterialGroup")
        }
        if !XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments.isRemoved {
            XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "to_xITSPDSFAxA_Documents")
        }
        if !XITSPDSFAxAProductReqMRPType.availablequnty.isRemoved {
            XITSPDSFAxAProductReqMRPType.availablequnty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Availablequnty")
        }
        if !XITSPDSFAxAProductReqMRPType.distributionchannel.isRemoved {
            XITSPDSFAxAProductReqMRPType.distributionchannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Distributionchannel")
        }
        if !XITSPDSFAxAProductReqMRPType.mrpelement.isRemoved {
            XITSPDSFAxAProductReqMRPType.mrpelement = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Mrpelement")
        }
        if !XITSPDSFAxAProductReqMRPType.plant.isRemoved {
            XITSPDSFAxAProductReqMRPType.plant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Plant")
        }
        if !XITSPDSFAxAProductReqMRPType.product.isRemoved {
            XITSPDSFAxAProductReqMRPType.product = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Product")
        }
        if !XITSPDSFAxAProductReqMRPType.requirementsdate.isRemoved {
            XITSPDSFAxAProductReqMRPType.requirementsdate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Requirementsdate")
        }
        if !XITSPDSFAxAProductReqMRPType.salesorganization.isRemoved {
            XITSPDSFAxAProductReqMRPType.salesorganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Salesorganization")
        }
        if !XITSPDSFAxAProductReqMRPType.storagelocation.isRemoved {
            XITSPDSFAxAProductReqMRPType.storagelocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Storagelocation")
        }
        if !XITSPDSFAxAQMNote.notification.isRemoved {
            XITSPDSFAxAQMNote.notification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notification")
        }
        if !XITSPDSFAxAQMNote.bpcustomernumber.isRemoved {
            XITSPDSFAxAQMNote.bpcustomernumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Bpcustomernumber")
        }
        if !XITSPDSFAxAQMNote.customer.isRemoved {
            XITSPDSFAxAQMNote.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Customer")
        }
        if !XITSPDSFAxAQMNote.salesorganization.isRemoved {
            XITSPDSFAxAQMNote.salesorganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Salesorganization")
        }
        if !XITSPDSFAxAQMNote.creationdate.isRemoved {
            XITSPDSFAxAQMNote.creationdate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Creationdate")
        }
        if !XITSPDSFAxAQMNote.notificationtype.isRemoved {
            XITSPDSFAxAQMNote.notificationtype = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationtype")
        }
        if !XITSPDSFAxAQMNote.notificationshorttext.isRemoved {
            XITSPDSFAxAQMNote.notificationshorttext = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationshorttext")
        }
        if !XITSPDSFAxAQMNote.status.isRemoved {
            XITSPDSFAxAQMNote.status = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Status")
        }
        if !XITSPDSFAxAQMNote.statusdesc.isRemoved {
            XITSPDSFAxAQMNote.statusdesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Statusdesc")
        }
        if !XITSPDSFAxAQMNote.distributionchannel.isRemoved {
            XITSPDSFAxAQMNote.distributionchannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Distributionchannel")
        }
        if !XITSPDSFAxAQMNote.activedivision.isRemoved {
            XITSPDSFAxAQMNote.activedivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Activedivision")
        }
        if !XITSPDSFAxAQMNote.material.isRemoved {
            XITSPDSFAxAQMNote.material = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Material")
        }
        if !XITSPDSFAxAQMNote.orderid.isRemoved {
            XITSPDSFAxAQMNote.orderid = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Orderid")
        }
        if !XITSPDSFAxAQMNote.notificationexternalqty.isRemoved {
            XITSPDSFAxAQMNote.notificationexternalqty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationexternalqty")
        }
        if !XITSPDSFAxAQMNote.notificationtext.isRemoved {
            XITSPDSFAxAQMNote.notificationtext = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationtext")
        }
        if !XITSPDSFAxAQMNote.isFinal.isRemoved {
            XITSPDSFAxAQMNote.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "IsFinal")
        }
        if !XITSPDSFAxAQMNote.documenttype.isRemoved {
            XITSPDSFAxAQMNote.documenttype = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documenttype")
        }
        if !XITSPDSFAxAQMNote.documentnumber.isRemoved {
            XITSPDSFAxAQMNote.documentnumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentnumber")
        }
        if !XITSPDSFAxAQMNote.documentpart.isRemoved {
            XITSPDSFAxAQMNote.documentpart = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentpart")
        }
        if !XITSPDSFAxAQMNote.documentversion.isRemoved {
            XITSPDSFAxAQMNote.documentversion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentversion")
        }
        if !XITSPDSFAxAQMNote.hasimage.isRemoved {
            XITSPDSFAxAQMNote.hasimage = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Hasimage")
        }
        if !XITSPDSFAxAQMNote.toXITSPDSFAxADocuments.isRemoved {
            XITSPDSFAxAQMNote.toXITSPDSFAxADocuments = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "to_xITSPDSFAxA_Documents")
        }
        if !XITSPDSFAxASalesAmountsType.salesDocument.isRemoved {
            XITSPDSFAxASalesAmountsType.salesDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SalesDocument")
        }
        if !XITSPDSFAxASalesAmountsType.salesOrganization.isRemoved {
            XITSPDSFAxASalesAmountsType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASalesAmountsType.distributionChannel.isRemoved {
            XITSPDSFAxASalesAmountsType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASalesAmountsType.organizationDivision.isRemoved {
            XITSPDSFAxASalesAmountsType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASalesAmountsType.soldToParty.isRemoved {
            XITSPDSFAxASalesAmountsType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesAmountsType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesAmountsType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesAmountsType.conditionType.isRemoved {
            XITSPDSFAxASalesAmountsType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxASalesAmountsType.conditionRateValue.isRemoved {
            XITSPDSFAxASalesAmountsType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionRateValue")
        }
        if !XITSPDSFAxASalesAmountsType.conditionAmount.isRemoved {
            XITSPDSFAxASalesAmountsType.conditionAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionAmount")
        }
        if !XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders.isRemoved {
            XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "to_xITSPDSFAxA_SalesOrders")
        }
        if !XITSPDSFAxASalesDocTypeType.salesDocumentType.isRemoved {
            XITSPDSFAxASalesDocTypeType.salesDocumentType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType.property(withName: "SalesDocumentType")
        }
        if !XITSPDSFAxASalesDocTypeType.salesDocumentTypeName.isRemoved {
            XITSPDSFAxASalesDocTypeType.salesDocumentTypeName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType.property(withName: "SalesDocumentTypeName")
        }
        if !XITSPDSFAxASalesItemAmountsType.salesOrder.isRemoved {
            XITSPDSFAxASalesItemAmountsType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASalesItemAmountsType.salesOrderItem.isRemoved {
            XITSPDSFAxASalesItemAmountsType.salesOrderItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "SalesOrderItem")
        }
        if !XITSPDSFAxASalesItemAmountsType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesItemAmountsType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesItemAmountsType.conditionType.isRemoved {
            XITSPDSFAxASalesItemAmountsType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxASalesItemAmountsType.conditionRateValue.isRemoved {
            XITSPDSFAxASalesItemAmountsType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionRateValue")
        }
        if !XITSPDSFAxASalesItemAmountsType.conditionAmount.isRemoved {
            XITSPDSFAxASalesItemAmountsType.conditionAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionAmount")
        }
        if !XITSPDSFAxASalesOrdItPriceType.salesOrder.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASalesOrdItPriceType.salesOrganization.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASalesOrdItPriceType.distributionChannel.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASalesOrdItPriceType.organizationDivision.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASalesOrdItPriceType.soldToParty.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesOrdItPriceType.salesOrderItem.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.salesOrderItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrderItem")
        }
        if !XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "PricingProcedureStep")
        }
        if !XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "PricingProcedureCounter")
        }
        if !XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesOrdItPriceType.conditionType.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.conditionType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionType")
        }
        if !XITSPDSFAxASalesOrdItPriceType.conditionBaseValue.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.conditionBaseValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionBaseValue")
        }
        if !XITSPDSFAxASalesOrdItPriceType.conditionRateValue.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.conditionRateValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionRateValue")
        }
        if !XITSPDSFAxASalesOrdItPriceType.conditionCurrency.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.conditionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionCurrency")
        }
        if !XITSPDSFAxASalesOrdItPriceType.conditionAmount.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.conditionAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionAmount")
        }
        if !XITSPDSFAxASalesOrdItPriceType.salesOrderType.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrderType")
        }
    }

    private static func merge3(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens.isRemoved {
            XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "to_xITSPDSFAxA_SalesOrdersItens")
        }
        if !XITSPDSFAxASalesOrderPartnerType.salesOrder.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASalesOrderPartnerType.salesOrganization.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASalesOrderPartnerType.distributionChannel.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASalesOrderPartnerType.organizationDivision.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASalesOrderPartnerType.soldToParty.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesOrderPartnerType.partnerFunction.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.partnerFunction = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "PartnerFunction")
        }
        if !XITSPDSFAxASalesOrderPartnerType.customer.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Customer")
        }
        if !XITSPDSFAxASalesOrderPartnerType.customerName.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxASalesOrderPartnerType.cityName.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.cityName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "CityName")
        }
        if !XITSPDSFAxASalesOrderPartnerType.country.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.country = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Country")
        }
        if !XITSPDSFAxASalesOrderPartnerType.district.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.district = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "District")
        }
        if !XITSPDSFAxASalesOrderPartnerType.region.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Region")
        }
        if !XITSPDSFAxASalesOrderPartnerType.streetName.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.streetName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "StreetName")
        }
        if !XITSPDSFAxASalesOrderPartnerType.houseNumber.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.houseNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "HouseNumber")
        }
        if !XITSPDSFAxASalesOrderPartnerType.postalCode.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.postalCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "PostalCode")
        }
        if !XITSPDSFAxASalesOrderPartnerType.salesOrderType.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrderType")
        }
        if !XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders.isRemoved {
            XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "to_xITSPDSFAxA_SalesOrders")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrder.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrderItem.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrderItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItem")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrganization.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASalesOrdersItensType.distributionChannel.isRemoved {
            XITSPDSFAxASalesOrdersItensType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASalesOrdersItensType.organizationDivision.isRemoved {
            XITSPDSFAxASalesOrdersItensType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASalesOrdersItensType.customer.isRemoved {
            XITSPDSFAxASalesOrdersItensType.customer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Customer")
        }
        if !XITSPDSFAxASalesOrdersItensType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesOrdersItensType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesOrdersItensType.material.isRemoved {
            XITSPDSFAxASalesOrdersItensType.material = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Material")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrderItemText.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrderItemText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItemText")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrderType.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderType")
        }
        if !XITSPDSFAxASalesOrdersItensType.customerName.isRemoved {
            XITSPDSFAxASalesOrdersItensType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxASalesOrdersItensType.netAmount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.netAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetAmount")
        }
        if !XITSPDSFAxASalesOrdersItensType.netPriceAmount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.netPriceAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetPriceAmount")
        }
        if !XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit.isRemoved {
            XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetPriceQuantityUnit")
        }
        if !XITSPDSFAxASalesOrdersItensType.billingBlockStatus.isRemoved {
            XITSPDSFAxASalesOrdersItensType.billingBlockStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingBlockStatus")
        }
        if !XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc.isRemoved {
            XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingBlockStatusDesc")
        }
        if !XITSPDSFAxASalesOrdersItensType.billingDocumentDate.isRemoved {
            XITSPDSFAxASalesOrdersItensType.billingDocumentDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingDocumentDate")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesDocumentRjcnReason")
        }
        if !XITSPDSFAxASalesOrdersItensType.baseUnit.isRemoved {
            XITSPDSFAxASalesOrdersItensType.baseUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BaseUnit")
        }
        if !XITSPDSFAxASalesOrdersItensType.orderQuantity.isRemoved {
            XITSPDSFAxASalesOrdersItensType.orderQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OrderQuantity")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemGrossWeight.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemGrossWeight = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemGrossWeight")
        }
        if !XITSPDSFAxASalesOrdersItensType.higherLevelItem.isRemoved {
            XITSPDSFAxASalesOrdersItensType.higherLevelItem = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "HigherLevelItem")
        }
        if !XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory.isRemoved {
            XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItemCategory")
        }
        if !XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer.isRemoved {
            XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "PurchaseOrderByCustomer")
        }
        if !XITSPDSFAxASalesOrdersItensType.materialByCustomer.isRemoved {
            XITSPDSFAxASalesOrdersItensType.materialByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialByCustomer")
        }
        if !XITSPDSFAxASalesOrdersItensType.requestedQuantity.isRemoved {
            XITSPDSFAxASalesOrdersItensType.requestedQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "RequestedQuantity")
        }
        if !XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit.isRemoved {
            XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "RequestedQuantityUnit")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemNetWeight.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemNetWeight = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemNetWeight")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemWeightUnit.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemWeightUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemWeightUnit")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemVolume.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemVolume = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemVolume")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemVolumeUnit.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemVolumeUnit = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemVolumeUnit")
        }
        if !XITSPDSFAxASalesOrdersItensType.transactionCurrency.isRemoved {
            XITSPDSFAxASalesOrdersItensType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "TransactionCurrency")
        }
        if !XITSPDSFAxASalesOrdersItensType.materialGroup.isRemoved {
            XITSPDSFAxASalesOrdersItensType.materialGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialGroup")
        }
        if !XITSPDSFAxASalesOrdersItensType.materialPricingGroup.isRemoved {
            XITSPDSFAxASalesOrdersItensType.materialPricingGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialPricingGroup")
        }
        if !XITSPDSFAxASalesOrdersItensType.productionPlant.isRemoved {
            XITSPDSFAxASalesOrdersItensType.productionPlant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ProductionPlant")
        }
        if !XITSPDSFAxASalesOrdersItensType.storageLocation.isRemoved {
            XITSPDSFAxASalesOrdersItensType.storageLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "StorageLocation")
        }
        if !XITSPDSFAxASalesOrdersItensType.deliveryGroup.isRemoved {
            XITSPDSFAxASalesOrdersItensType.deliveryGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryGroup")
        }
        if !XITSPDSFAxASalesOrdersItensType.deliveryPriority.isRemoved {
            XITSPDSFAxASalesOrdersItensType.deliveryPriority = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryPriority")
        }
        if !XITSPDSFAxASalesOrdersItensType.incotermsClassification.isRemoved {
            XITSPDSFAxASalesOrdersItensType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsClassification")
        }
        if !XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation.isRemoved {
            XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsTransferLocation")
        }
        if !XITSPDSFAxASalesOrdersItensType.incotermsLocation1.isRemoved {
            XITSPDSFAxASalesOrdersItensType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsLocation1")
        }
        if !XITSPDSFAxASalesOrdersItensType.incotermsLocation2.isRemoved {
            XITSPDSFAxASalesOrdersItensType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsLocation2")
        }
        if !XITSPDSFAxASalesOrdersItensType.customerPaymentTerms.isRemoved {
            XITSPDSFAxASalesOrdersItensType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CustomerPaymentTerms")
        }
        if !XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason.isRemoved {
            XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemBillingBlockReason")
        }
        if !XITSPDSFAxASalesOrdersItensType.sdProcessStatus.isRemoved {
            XITSPDSFAxASalesOrdersItensType.sdProcessStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SDProcessStatus")
        }
        if !XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc.isRemoved {
            XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SDProcessStatusDesc")
        }
        if !XITSPDSFAxASalesOrdersItensType.deliveryStatus.isRemoved {
            XITSPDSFAxASalesOrdersItensType.deliveryStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryStatus")
        }
        if !XITSPDSFAxASalesOrdersItensType.pricingDate.isRemoved {
            XITSPDSFAxASalesOrdersItensType.pricingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "PricingDate")
        }
        if !XITSPDSFAxASalesOrdersItensType.profitCenter.isRemoved {
            XITSPDSFAxASalesOrdersItensType.profitCenter = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ProfitCenter")
        }
        if !XITSPDSFAxASalesOrdersItensType.referenceSDDocument.isRemoved {
            XITSPDSFAxASalesOrdersItensType.referenceSDDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ReferenceSDDocument")
        }
        if !XITSPDSFAxASalesOrdersItensType.taxAmount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.taxAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "TaxAmount")
        }
        if !XITSPDSFAxASalesOrdersItensType.costAmount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.costAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CostAmount")
        }
        if !XITSPDSFAxASalesOrdersItensType.subtotal1Amount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.subtotal1Amount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Subtotal1Amount")
        }
        if !XITSPDSFAxASalesOrdersItensType.subtotal2Amount.isRemoved {
            XITSPDSFAxASalesOrdersItensType.subtotal2Amount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Subtotal2Amount")
        }
        if !XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex.isRemoved {
            XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OverallTotDelivStatusColorHex")
        }
        if !XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc.isRemoved {
            XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OverallTotDelivStatusColorDesc")
        }
        if !XITSPDSFAxASalesOrdersItensType.billingQuantity.isRemoved {
            XITSPDSFAxASalesOrdersItensType.billingQuantity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingQuantity")
        }
        if !XITSPDSFAxASalesOrdersItensType.fieldAux1.isRemoved {
            XITSPDSFAxASalesOrdersItensType.fieldAux1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_1")
        }
        if !XITSPDSFAxASalesOrdersItensType.fieldAux2.isRemoved {
            XITSPDSFAxASalesOrdersItensType.fieldAux2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_2")
        }
        if !XITSPDSFAxASalesOrdersItensType.fieldAux3.isRemoved {
            XITSPDSFAxASalesOrdersItensType.fieldAux3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_3")
        }
        if !XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription.isRemoved {
            XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryStatusDescription")
        }
        if !XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier.isRemoved {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_CenterSupplier")
        }
        if !XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster.isRemoved {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_ProductMaster")
        }
        if !XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts.isRemoved {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesItemAmounts")
        }
        if !XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders.isRemoved {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesOrders")
        }
        if !XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice.isRemoved {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesOrdItPrice")
        }
        if !XITSPDSFAxASalesOrdersObs.salesorder.isRemoved {
            XITSPDSFAxASalesOrdersObs.salesorder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorder")
        }
        if !XITSPDSFAxASalesOrdersObs.salesorganization.isRemoved {
            XITSPDSFAxASalesOrdersObs.salesorganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorganization")
        }
        if !XITSPDSFAxASalesOrdersObs.distributionchannel.isRemoved {
            XITSPDSFAxASalesOrdersObs.distributionchannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Distributionchannel")
        }
        if !XITSPDSFAxASalesOrdersObs.organizationdivision.isRemoved {
            XITSPDSFAxASalesOrdersObs.organizationdivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Organizationdivision")
        }
        if !XITSPDSFAxASalesOrdersObs.salesorderobsid.isRemoved {
            XITSPDSFAxASalesOrdersObs.salesorderobsid = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobsid")
        }
        if !XITSPDSFAxASalesOrdersObs.salesorderobstext.isRemoved {
            XITSPDSFAxASalesOrdersObs.salesorderobstext = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobstext")
        }
        if !XITSPDSFAxASalesOrdersObs.salesorderobsseq.isRemoved {
            XITSPDSFAxASalesOrdersObs.salesorderobsseq = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobsseq")
        }
        if !XITSPDSFAxASalesOrdersObs.bpcustomernumber.isRemoved {
            XITSPDSFAxASalesOrdersObs.bpcustomernumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Bpcustomernumber")
        }
        if !XITSPDSFAxASalesOrdersObs.soldToParty.isRemoved {
            XITSPDSFAxASalesOrdersObs.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesOrdersObs.nSalesOrdersObs.isRemoved {
            XITSPDSFAxASalesOrdersObs.nSalesOrdersObs = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "N_SalesOrdersObs")
        }
        if !XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders.isRemoved {
            XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "to_xITSPDSFAxA_SalesOrders")
        }
        if !XITSPDSFAxASalesOrdersObsN.salesorder.isRemoved {
            XITSPDSFAxASalesOrdersObsN.salesorder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorder")
        }
        if !XITSPDSFAxASalesOrdersObsN.salesorganization.isRemoved {
            XITSPDSFAxASalesOrdersObsN.salesorganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorganization")
        }
        if !XITSPDSFAxASalesOrdersObsN.distributionchannel.isRemoved {
            XITSPDSFAxASalesOrdersObsN.distributionchannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Distributionchannel")
        }
        if !XITSPDSFAxASalesOrdersObsN.organizationdivision.isRemoved {
            XITSPDSFAxASalesOrdersObsN.organizationdivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Organizationdivision")
        }
        if !XITSPDSFAxASalesOrdersObsN.salesorderobsid.isRemoved {
            XITSPDSFAxASalesOrdersObsN.salesorderobsid = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobsid")
        }
        if !XITSPDSFAxASalesOrdersObsN.salesorderobstext.isRemoved {
            XITSPDSFAxASalesOrdersObsN.salesorderobstext = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobstext")
        }
        if !XITSPDSFAxASalesOrdersObsN.salesorderobsseq.isRemoved {
            XITSPDSFAxASalesOrdersObsN.salesorderobsseq = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobsseq")
        }
        if !XITSPDSFAxASalesOrdersObsN.bpcustomernumber.isRemoved {
            XITSPDSFAxASalesOrdersObsN.bpcustomernumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Bpcustomernumber")
        }
        if !XITSPDSFAxASalesOrdersObsN.soldToParty.isRemoved {
            XITSPDSFAxASalesOrdersObsN.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs.isRemoved {
            XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "N_SalesOrdersObs")
        }
        if !XITSPDSFAxASalesOrdersResnsType.sdDocumentReason.isRemoved {
            XITSPDSFAxASalesOrdersResnsType.sdDocumentReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType.property(withName: "SDDocumentReason")
        }
        if !XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText.isRemoved {
            XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType.property(withName: "SDDocumentReasonText")
        }
        if !XITSPDSFAxASalesOrdersType.salesOrder.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASalesOrdersType.salesOrganization.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASalesOrdersType.distributionChannel.isRemoved {
            XITSPDSFAxASalesOrdersType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASalesOrdersType.organizationDivision.isRemoved {
            XITSPDSFAxASalesOrdersType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASalesOrdersType.soldToParty.isRemoved {
            XITSPDSFAxASalesOrdersType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASalesOrdersType.bpCustomerNumber.isRemoved {
            XITSPDSFAxASalesOrdersType.bpCustomerNumber = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BPCustomerNumber")
        }
        if !XITSPDSFAxASalesOrdersType.customerName.isRemoved {
            XITSPDSFAxASalesOrdersType.customerName = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerName")
        }
        if !XITSPDSFAxASalesOrdersType.salesOrderType.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrderType")
        }
        if !XITSPDSFAxASalesOrdersType.salesGroup.isRemoved {
            XITSPDSFAxASalesOrdersType.salesGroup = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesGroup")
        }
        if !XITSPDSFAxASalesOrdersType.salesOffice.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOffice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOffice")
        }
        if !XITSPDSFAxASalesOrdersType.salesDistrict.isRemoved {
            XITSPDSFAxASalesOrdersType.salesDistrict = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesDistrict")
        }
        if !XITSPDSFAxASalesOrdersType.creationDate.isRemoved {
            XITSPDSFAxASalesOrdersType.creationDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CreationDate")
        }
        if !XITSPDSFAxASalesOrdersType.createdByUser.isRemoved {
            XITSPDSFAxASalesOrdersType.createdByUser = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CreatedByUser")
        }
        if !XITSPDSFAxASalesOrdersType.lastChangeDate.isRemoved {
            XITSPDSFAxASalesOrdersType.lastChangeDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "LastChangeDate")
        }
        if !XITSPDSFAxASalesOrdersType.lastChangeDateTime.isRemoved {
            XITSPDSFAxASalesOrdersType.lastChangeDateTime = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "LastChangeDateTime")
        }
        if !XITSPDSFAxASalesOrdersType.supplier.isRemoved {
            XITSPDSFAxASalesOrdersType.supplier = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "Supplier")
        }
        if !XITSPDSFAxASalesOrdersType.businessPartnerName1.isRemoved {
            XITSPDSFAxASalesOrdersType.businessPartnerName1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BusinessPartnerName1")
        }
        if !XITSPDSFAxASalesOrdersType.businessPartnerName2.isRemoved {
            XITSPDSFAxASalesOrdersType.businessPartnerName2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BusinessPartnerName2")
        }
        if !XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer.isRemoved {
            XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PurchaseOrderByCustomer")
        }
        if !XITSPDSFAxASalesOrdersType.salesOrderDate.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrderDate")
        }
        if !XITSPDSFAxASalesOrdersType.transactionCurrency.isRemoved {
            XITSPDSFAxASalesOrdersType.transactionCurrency = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TransactionCurrency")
        }
        if !XITSPDSFAxASalesOrdersType.sdDocumentReason.isRemoved {
            XITSPDSFAxASalesOrdersType.sdDocumentReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SDDocumentReason")
        }
        if !XITSPDSFAxASalesOrdersType.pricingDate.isRemoved {
            XITSPDSFAxASalesOrdersType.pricingDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PricingDate")
        }
        if !XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined.isRemoved {
            XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CompleteDeliveryIsDefined")
        }
        if !XITSPDSFAxASalesOrdersType.headerBillingBlockReason.isRemoved {
            XITSPDSFAxASalesOrdersType.headerBillingBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "HeaderBillingBlockReason")
        }
        if !XITSPDSFAxASalesOrdersType.deliveryBlockReason.isRemoved {
            XITSPDSFAxASalesOrdersType.deliveryBlockReason = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "DeliveryBlockReason")
        }
        if !XITSPDSFAxASalesOrdersType.incotermsClassification.isRemoved {
            XITSPDSFAxASalesOrdersType.incotermsClassification = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsClassification")
        }
        if !XITSPDSFAxASalesOrdersType.incotermsTransferLocation.isRemoved {
            XITSPDSFAxASalesOrdersType.incotermsTransferLocation = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsTransferLocation")
        }
        if !XITSPDSFAxASalesOrdersType.incotermsLocation1.isRemoved {
            XITSPDSFAxASalesOrdersType.incotermsLocation1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsLocation1")
        }
        if !XITSPDSFAxASalesOrdersType.incotermsLocation2.isRemoved {
            XITSPDSFAxASalesOrdersType.incotermsLocation2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsLocation2")
        }
        if !XITSPDSFAxASalesOrdersType.incotermsVersion.isRemoved {
            XITSPDSFAxASalesOrdersType.incotermsVersion = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsVersion")
        }
        if !XITSPDSFAxASalesOrdersType.customerPaymentTerms.isRemoved {
            XITSPDSFAxASalesOrdersType.customerPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPaymentTerms")
        }
        if !XITSPDSFAxASalesOrdersType.paymentMethod.isRemoved {
            XITSPDSFAxASalesOrdersType.paymentMethod = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PaymentMethod")
        }
        if !XITSPDSFAxASalesOrdersType.assignmentReference.isRemoved {
            XITSPDSFAxASalesOrdersType.assignmentReference = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "AssignmentReference")
        }
        if !XITSPDSFAxASalesOrdersType.referenceSDDocument.isRemoved {
            XITSPDSFAxASalesOrdersType.referenceSDDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ReferenceSDDocument")
        }
        if !XITSPDSFAxASalesOrdersType.customerPurchaseOrderType.isRemoved {
            XITSPDSFAxASalesOrdersType.customerPurchaseOrderType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPurchaseOrderType")
        }
        if !XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate.isRemoved {
            XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPurchaseOrderDate")
        }
        if !XITSPDSFAxASalesOrdersType.totalNetAmount.isRemoved {
            XITSPDSFAxASalesOrdersType.totalNetAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalNetAmount")
        }
        if !XITSPDSFAxASalesOrdersType.requestedDeliveryDate.isRemoved {
            XITSPDSFAxASalesOrdersType.requestedDeliveryDate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "RequestedDeliveryDate")
        }
        if !XITSPDSFAxASalesOrdersType.overallSDProcessStatus.isRemoved {
            XITSPDSFAxASalesOrdersType.overallSDProcessStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDProcessStatus")
        }
        if !XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc.isRemoved {
            XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDProcessStatusDesc")
        }
        if !XITSPDSFAxASalesOrdersType.totalCreditCheckStatus.isRemoved {
            XITSPDSFAxASalesOrdersType.totalCreditCheckStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalCreditCheckStatus")
        }
        if !XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc.isRemoved {
            XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalCreditCheckStatusDesc")
        }
        if !XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus.isRemoved {
            XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotalDeliveryStatus")
        }
        if !XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc.isRemoved {
            XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotalDeliveryStatusDesc")
        }
        if !XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex.isRemoved {
            XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotDelivStatusColorHex")
        }
        if !XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc.isRemoved {
            XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotDelivStatusColorDesc")
        }
        if !XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts.isRemoved {
            XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDDocumentRejectionSts")
        }
        if !XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc.isRemoved {
            XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OvrlSDDocumentRejectionStsDesc")
        }
        if !XITSPDSFAxASalesOrdersType.zInternalDatePeriod.isRemoved {
            XITSPDSFAxASalesOrdersType.zInternalDatePeriod = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZInternalDatePeriod")
        }
        if !XITSPDSFAxASalesOrdersType.fieldAux1.isRemoved {
            XITSPDSFAxASalesOrdersType.fieldAux1 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_1")
        }
        if !XITSPDSFAxASalesOrdersType.fieldAux2.isRemoved {
            XITSPDSFAxASalesOrdersType.fieldAux2 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_2")
        }
        if !XITSPDSFAxASalesOrdersType.fieldAux3.isRemoved {
            XITSPDSFAxASalesOrdersType.fieldAux3 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_3")
        }
        if !XITSPDSFAxASalesOrdersType.zPlant.isRemoved {
            XITSPDSFAxASalesOrdersType.zPlant = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZPlant")
        }
        if !XITSPDSFAxASalesOrdersType.zPriceList.isRemoved {
            XITSPDSFAxASalesOrdersType.zPriceList = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZPriceList")
        }
        if !XITSPDSFAxASalesOrdersType.priceListType.isRemoved {
            XITSPDSFAxASalesOrdersType.priceListType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PriceListType")
        }
        if !XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH.isRemoved {
            XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "xITSPDSFAxMobileID_SDH")
        }
        if !XITSPDSFAxASalesOrdersType.isFinal.isRemoved {
            XITSPDSFAxASalesOrdersType.isFinal = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IsFinal")
        }
        if !XITSPDSFAxASalesOrdersType.salesOperationType.isRemoved {
            XITSPDSFAxASalesOrdersType.salesOperationType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOperationType")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_CustomerMaster")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_IncoTerms")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_Invoice")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_PaymentTerms")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_Pricelists")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesAmounts")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesDocType")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrderPartner")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrdersItens")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrders_Obs")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrdersResns")
        }
        if !XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea.isRemoved {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")
        }
        if !XITSPDSFAxASoldVolumeType.salesOrder.isRemoved {
            XITSPDSFAxASoldVolumeType.salesOrder = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesOrder")
        }
        if !XITSPDSFAxASoldVolumeType.salesOrganization.isRemoved {
            XITSPDSFAxASoldVolumeType.salesOrganization = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesOrganization")
        }
        if !XITSPDSFAxASoldVolumeType.distributionChannel.isRemoved {
            XITSPDSFAxASoldVolumeType.distributionChannel = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "DistributionChannel")
        }
        if !XITSPDSFAxASoldVolumeType.organizationDivision.isRemoved {
            XITSPDSFAxASoldVolumeType.organizationDivision = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "OrganizationDivision")
        }
        if !XITSPDSFAxASoldVolumeType.billingDocument.isRemoved {
            XITSPDSFAxASoldVolumeType.billingDocument = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "BillingDocument")
        }
        if !XITSPDSFAxASoldVolumeType.partnerActual.isRemoved {
            XITSPDSFAxASoldVolumeType.partnerActual = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "PartnerActual")
        }
        if !XITSPDSFAxASoldVolumeType.partnerHistorical.isRemoved {
            XITSPDSFAxASoldVolumeType.partnerHistorical = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "PartnerHistorical")
        }
        if !XITSPDSFAxASoldVolumeType.soldToParty.isRemoved {
            XITSPDSFAxASoldVolumeType.soldToParty = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SoldToParty")
        }
        if !XITSPDSFAxASoldVolumeType.yearMonth.isRemoved {
            XITSPDSFAxASoldVolumeType.yearMonth = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "YearMonth")
        }
        if !XITSPDSFAxASoldVolumeType.salesDocumentAmount.isRemoved {
            XITSPDSFAxASoldVolumeType.salesDocumentAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesDocumentAmount")
        }
        if !XITSPDSFAxASoldVolumeType.billingDocumentAmount.isRemoved {
            XITSPDSFAxASoldVolumeType.billingDocumentAmount = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "BillingDocumentAmount")
        }
        if !XITSPDSFAxATaxJurisdictionType.country.isRemoved {
            XITSPDSFAxATaxJurisdictionType.country = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "Country")
        }
        if !XITSPDSFAxATaxJurisdictionType.postalCodeFrom.isRemoved {
            XITSPDSFAxATaxJurisdictionType.postalCodeFrom = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "PostalCodeFrom")
        }
        if !XITSPDSFAxATaxJurisdictionType.postalCodeTo.isRemoved {
            XITSPDSFAxATaxJurisdictionType.postalCodeTo = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "PostalCodeTo")
        }
        if !XITSPDSFAxATaxJurisdictionType.region.isRemoved {
            XITSPDSFAxATaxJurisdictionType.region = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "Region")
        }
        if !XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode.isRemoved {
            XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "TaxJurisdictionCode")
        }
        if !XITSPDSFAxATaxListType.taxType.isRemoved {
            XITSPDSFAxATaxListType.taxType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "TaxType")
        }
        if !XITSPDSFAxATaxListType.groupPriority.isRemoved {
            XITSPDSFAxATaxListType.groupPriority = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "GroupPriority")
        }
        if !XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset.isRemoved {
            XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "to_xITSPDSFAxA_TaxNameset")
        }
        if !XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue.isRemoved {
            XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "to_xITSPDSFAxA_TaxValue")
        }
        if !XITSPDSFAxATaxNamesetType.taxType.isRemoved {
            XITSPDSFAxATaxNamesetType.taxType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "TaxType")
        }
        if !XITSPDSFAxATaxNamesetType.groupPriority.isRemoved {
            XITSPDSFAxATaxNamesetType.groupPriority = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "GroupPriority")
        }
        if !XITSPDSFAxATaxNamesetType.sapField.isRemoved {
            XITSPDSFAxATaxNamesetType.sapField = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "SAP_Field")
        }
        if !XITSPDSFAxATaxNamesetType.oDataField.isRemoved {
            XITSPDSFAxATaxNamesetType.oDataField = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "OData_Field")
        }
        if !XITSPDSFAxATaxNamesetType.entity.isRemoved {
            XITSPDSFAxATaxNamesetType.entity = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Entity")
        }
        if !XITSPDSFAxATaxNamesetType.atribute.isRemoved {
            XITSPDSFAxATaxNamesetType.atribute = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Atribute")
        }
        if !XITSPDSFAxATaxNamesetType.isOutPut.isRemoved {
            XITSPDSFAxATaxNamesetType.isOutPut = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Is_OutPut")
        }
        if !XITSPDSFAxATaxValueType.taxType.isRemoved {
            XITSPDSFAxATaxValueType.taxType = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "TaxType")
        }
        if !XITSPDSFAxATaxValueType.groupPriority.isRemoved {
            XITSPDSFAxATaxValueType.groupPriority = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "GroupPriority")
        }
        if !XITSPDSFAxATaxValueType.shipfrom.isRemoved {
            XITSPDSFAxATaxValueType.shipfrom = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SHIPFROM")
        }
        if !XITSPDSFAxATaxValueType.shipto.isRemoved {
            XITSPDSFAxATaxValueType.shipto = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SHIPTO")
        }
        if !XITSPDSFAxATaxValueType.matnr.isRemoved {
            XITSPDSFAxATaxValueType.matnr = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "MATNR")
        }
        if !XITSPDSFAxATaxValueType.nbmcode.isRemoved {
            XITSPDSFAxATaxValueType.nbmcode = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "NBMCODE")
        }
        if !XITSPDSFAxATaxValueType.stgrp.isRemoved {
            XITSPDSFAxATaxValueType.stgrp = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "STGRP")
        }
        if !XITSPDSFAxATaxValueType.subkey01.isRemoved {
            XITSPDSFAxATaxValueType.subkey01 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY01")
        }
        if !XITSPDSFAxATaxValueType.subkey02.isRemoved {
            XITSPDSFAxATaxValueType.subkey02 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY02")
        }
        if !XITSPDSFAxATaxValueType.subkey03.isRemoved {
            XITSPDSFAxATaxValueType.subkey03 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY03")
        }
        if !XITSPDSFAxATaxValueType.rate.isRemoved {
            XITSPDSFAxATaxValueType.rate = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "RATE")
        }
        if !XITSPDSFAxATaxValueType.base.isRemoved {
            XITSPDSFAxATaxValueType.base = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "BASE")
        }
        if !XITSPDSFAxATaxValueType.field01.isRemoved {
            XITSPDSFAxATaxValueType.field01 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field01")
        }
        if !XITSPDSFAxATaxValueType.field02.isRemoved {
            XITSPDSFAxATaxValueType.field02 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field02")
        }
        if !XITSPDSFAxATaxValueType.field03.isRemoved {
            XITSPDSFAxATaxValueType.field03 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field03")
        }
        if !XITSPDSFAxATaxValueType.field04.isRemoved {
            XITSPDSFAxATaxValueType.field04 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field04")
        }
        if !XITSPDSFAxATaxValueType.field05.isRemoved {
            XITSPDSFAxATaxValueType.field05 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field05")
        }
        if !XITSPDSFAxATaxValueType.field06.isRemoved {
            XITSPDSFAxATaxValueType.field06 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field06")
        }
        if !XITSPDSFAxATaxValueType.field07.isRemoved {
            XITSPDSFAxATaxValueType.field07 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field07")
        }
        if !XITSPDSFAxATaxValueType.field08.isRemoved {
            XITSPDSFAxATaxValueType.field08 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field08")
        }
        if !XITSPDSFAxATaxValueType.field09.isRemoved {
            XITSPDSFAxATaxValueType.field09 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field09")
        }
        if !XITSPDSFAxATaxValueType.field10.isRemoved {
            XITSPDSFAxATaxValueType.field10 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field10")
        }
        if !XITSPDSFAxATaxValueType.field11.isRemoved {
            XITSPDSFAxATaxValueType.field11 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field11")
        }
        if !XITSPDSFAxATaxValueType.field12.isRemoved {
            XITSPDSFAxATaxValueType.field12 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field12")
        }
        if !XITSPDSFAxATaxValueType.field13.isRemoved {
            XITSPDSFAxATaxValueType.field13 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field13")
        }
        if !XITSPDSFAxATaxValueType.field14.isRemoved {
            XITSPDSFAxATaxValueType.field14 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field14")
        }
        if !XITSPDSFAxATaxValueType.field15.isRemoved {
            XITSPDSFAxATaxValueType.field15 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field15")
        }
        if !XITSPDSFAxATaxValueType.field16.isRemoved {
            XITSPDSFAxATaxValueType.field16 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field16")
        }
        if !XITSPDSFAxATaxValueType.field17.isRemoved {
            XITSPDSFAxATaxValueType.field17 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field17")
        }
        if !XITSPDSFAxATaxValueType.field18.isRemoved {
            XITSPDSFAxATaxValueType.field18 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field18")
        }
        if !XITSPDSFAxATaxValueType.field19.isRemoved {
            XITSPDSFAxATaxValueType.field19 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field19")
        }
        if !XITSPDSFAxATaxValueType.field20.isRemoved {
            XITSPDSFAxATaxValueType.field20 = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field20")
        }
    }
}
