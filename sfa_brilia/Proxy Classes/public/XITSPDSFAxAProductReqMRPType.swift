// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductReqMRPType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var availablequnty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Availablequnty")

    private static var distributionchannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Distributionchannel")

    private static var mrpelement_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Mrpelement")

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Plant")

    private static var product_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Product")

    private static var requirementsdate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Requirementsdate")

    private static var salesorganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Salesorganization")

    private static var storagelocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType.property(withName: "Storagelocation")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductReqMRPType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductReqMRPType>())
    }

    open class var availablequnty: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.availablequnty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.availablequnty_ = value
            }
        }
    }

    open var availablequnty: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.availablequnty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.availablequnty, to: IntegerValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAProductReqMRPType {
        return CastRequired<XITSPDSFAxAProductReqMRPType>.from(self.copyEntity())
    }

    open class var distributionchannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.distributionchannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.distributionchannel_ = value
            }
        }
    }

    open var distributionchannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.distributionchannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.distributionchannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(distributionchannel: String?, mrpelement: String?, plant: String?, product: String?, requirementsdate: LocalDateTime?, salesorganization: String?) -> EntityKey {
        return EntityKey().with(name: "Distributionchannel", value: StringValue.of(optional: distributionchannel)).with(name: "Mrpelement", value: StringValue.of(optional: mrpelement)).with(name: "Plant", value: StringValue.of(optional: plant)).with(name: "Product", value: StringValue.of(optional: product)).with(name: "Requirementsdate", value: requirementsdate).with(name: "Salesorganization", value: StringValue.of(optional: salesorganization))
    }

    open class var mrpelement: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.mrpelement_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.mrpelement_ = value
            }
        }
    }

    open var mrpelement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.mrpelement))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.mrpelement, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAProductReqMRPType {
        return CastRequired<XITSPDSFAxAProductReqMRPType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.plant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.plant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.product_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.product))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.product, to: StringValue.of(optional: value))
        }
    }

    open class var requirementsdate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.requirementsdate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.requirementsdate_ = value
            }
        }
    }

    open var requirementsdate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.requirementsdate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.requirementsdate, to: value)
        }
    }

    open class var salesorganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.salesorganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.salesorganization_ = value
            }
        }
    }

    open var salesorganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.salesorganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.salesorganization, to: StringValue.of(optional: value))
        }
    }

    open class var storagelocation: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                return XITSPDSFAxAProductReqMRPType.storagelocation_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductReqMRPType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductReqMRPType.self) }
            do {
                XITSPDSFAxAProductReqMRPType.storagelocation_ = value
            }
        }
    }

    open var storagelocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductReqMRPType.storagelocation))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductReqMRPType.storagelocation, to: StringValue.of(optional: value))
        }
    }
}
