// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxANotificationsStatType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var notification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Notification")

    private static var istat_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "istat")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "DistributionChannel")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Customer")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "BPCustomerNumber")

    private static var status_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "Status")

    private static var statusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType.property(withName: "StatusDesc")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxANotificationsStatType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxANotificationsStatType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxANotificationsStatType {
        return CastRequired<XITSPDSFAxANotificationsStatType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var istat: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.istat_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.istat_ = value
            }
        }
    }

    open var istat: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.istat))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.istat, to: StringValue.of(optional: value))
        }
    }

    open class func key(notification: String?, istat: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "Notification", value: StringValue.of(optional: notification)).with(name: "istat", value: StringValue.of(optional: istat)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open class var notification: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.notification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.notification_ = value
            }
        }
    }

    open var notification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.notification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.notification, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxANotificationsStatType {
        return CastRequired<XITSPDSFAxANotificationsStatType>.from(self.oldEntity)
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var status: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.status_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.status_ = value
            }
        }
    }

    open var status: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.status))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.status, to: StringValue.of(optional: value))
        }
    }

    open class var statusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                return XITSPDSFAxANotificationsStatType.statusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsStatType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsStatType.self) }
            do {
                XITSPDSFAxANotificationsStatType.statusDesc_ = value
            }
        }
    }

    open var statusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsStatType.statusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsStatType.statusDesc, to: StringValue.of(optional: value))
        }
    }
}
