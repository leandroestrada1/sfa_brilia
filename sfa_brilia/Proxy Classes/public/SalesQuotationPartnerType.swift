// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var contactPerson_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Personnel")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "SalesQuotation")

    private static var supplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "Supplier")

    private static var toXITSPDSFAxASalesQuotationsPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType.property(withName: "to_xITSPDSFAxA_SalesQuotationsPartner")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType)
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationPartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationPartnerType>())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesQuotationPartnerType {
        return CastRequired<SalesQuotationPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesQuotation: String?, partnerFunction: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation)).with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction))
    }

    open var old: SalesQuotationPartnerType {
        return CastRequired<SalesQuotationPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesQuotationsPartner: Property {
        get {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                return SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPartnerType.self)
            defer { objc_sync_exit(SalesQuotationPartnerType.self) }
            do {
                SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationsPartner: SalesQuotationsType? {
        get {
            return CastOptional<SalesQuotationsType>.from(self.optionalValue(for: SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPartnerType.toXITSPDSFAxASalesQuotationsPartner, to: value)
        }
    }
}
