// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerGrpTextType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customerGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType.property(withName: "CustomerGroup")

    private static var customerGroupName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType.property(withName: "CustomerGroupName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerGrpTextType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerGrpTextType>())
    }

    open func copy() -> XITSPDSFAxACustomerGrpTextType {
        return CastRequired<XITSPDSFAxACustomerGrpTextType>.from(self.copyEntity())
    }

    open class var customerGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerGrpTextType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerGrpTextType.self) }
            do {
                return XITSPDSFAxACustomerGrpTextType.customerGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerGrpTextType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerGrpTextType.self) }
            do {
                XITSPDSFAxACustomerGrpTextType.customerGroup_ = value
            }
        }
    }

    open var customerGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerGrpTextType.customerGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerGrpTextType.customerGroup, to: StringValue.of(optional: value))
        }
    }

    open class var customerGroupName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerGrpTextType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerGrpTextType.self) }
            do {
                return XITSPDSFAxACustomerGrpTextType.customerGroupName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerGrpTextType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerGrpTextType.self) }
            do {
                XITSPDSFAxACustomerGrpTextType.customerGroupName_ = value
            }
        }
    }

    open var customerGroupName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerGrpTextType.customerGroupName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerGrpTextType.customerGroupName, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customerGroup: String?) -> EntityKey {
        return EntityKey().with(name: "CustomerGroup", value: StringValue.of(optional: customerGroup))
    }

    open var old: XITSPDSFAxACustomerGrpTextType {
        return CastRequired<XITSPDSFAxACustomerGrpTextType>.from(self.oldEntity)
    }
}
