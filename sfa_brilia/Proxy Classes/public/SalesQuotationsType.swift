// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var bindingPeriodValidityEndDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "BindingPeriodValidityEndDate")

    private static var bindingPeriodValidityStartDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "BindingPeriodValidityStartDate")

    private static var completeDeliveryIsDefined_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CompleteDeliveryIsDefined")

    private static var createdByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CreationDate")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPaymentTerms")

    private static var customerPurchaseOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPurchaseOrderDate")

    private static var customerPurchaseOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "CustomerPurchaseOrderType")

    private static var deliveryBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "DeliveryBlockReason")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "DistributionChannel")

    private static var expectedOrderNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ExpectedOrderNetAmount")

    private static var hdrOrderProbabilityInPercent_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "HdrOrderProbabilityInPercent")

    private static var headerBillingBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "HeaderBillingBlockReason")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsClassification")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsLocation2")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsTransferLocation")

    private static var incotermsVersion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "IncotermsVersion")

    private static var lastChangeDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "LastChangeDate")

    private static var lastChangeDateTime_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "LastChangeDateTime")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OrganizationDivision")

    private static var overallSDDocumentRejectionSts_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OverallSDDocumentRejectionSts")

    private static var overallSDProcessStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "OverallSDProcessStatus")

    private static var paymentMethod_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PaymentMethod")

    private static var pricingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PricingDate")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "PurchaseOrderByCustomer")

    private static var requestedDeliveryDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "RequestedDeliveryDate")

    private static var sdDocumentReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SDDocumentReason")

    private static var salesDistrict_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesDistrict")

    private static var salesGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesGroup")

    private static var salesOffice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesOffice")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesOrganization")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotation")

    private static var salesQuotationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotationDate")

    private static var salesQuotationType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SalesQuotationType")

    private static var shippingCondition_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ShippingCondition")

    private static var shippingType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "ShippingType")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "SoldToParty")

    private static var totalCreditCheckStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TotalCreditCheckStatus")

    private static var totalNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TotalNetAmount")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "TransactionCurrency")

    private static var xITSPDSFAxMobileIDSDH_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "xITSPDSFAxMobileID_SDH")

    private static var toXITSPDSFAxASalesQuotationPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationPartner")

    private static var toXITSPDSFAxASalesQuotationPricing_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationPricing")

    private static var toXITSPDSFAxASalesQuotationsItens_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType.property(withName: "to_xITSPDSFAxA_SalesQuotationsItens")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType)
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationsType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationsType>())
    }

    open class var bindingPeriodValidityEndDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.bindingPeriodValidityEndDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.bindingPeriodValidityEndDate_ = value
            }
        }
    }

    open var bindingPeriodValidityEndDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.bindingPeriodValidityEndDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.bindingPeriodValidityEndDate, to: value)
        }
    }

    open class var bindingPeriodValidityStartDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.bindingPeriodValidityStartDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.bindingPeriodValidityStartDate_ = value
            }
        }
    }

    open var bindingPeriodValidityStartDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.bindingPeriodValidityStartDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.bindingPeriodValidityStartDate, to: value)
        }
    }

    open class var completeDeliveryIsDefined: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.completeDeliveryIsDefined_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.completeDeliveryIsDefined_ = value
            }
        }
    }

    open var completeDeliveryIsDefined: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: SalesQuotationsType.completeDeliveryIsDefined))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.completeDeliveryIsDefined, to: BooleanValue.of(optional: value))
        }
    }

    open func copy() -> SalesQuotationsType {
        return CastRequired<SalesQuotationsType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.creationDate, to: value)
        }
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var customerPurchaseOrderDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.customerPurchaseOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.customerPurchaseOrderDate_ = value
            }
        }
    }

    open var customerPurchaseOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.customerPurchaseOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.customerPurchaseOrderDate, to: value)
        }
    }

    open class var customerPurchaseOrderType: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.customerPurchaseOrderType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.customerPurchaseOrderType_ = value
            }
        }
    }

    open var customerPurchaseOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.customerPurchaseOrderType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.customerPurchaseOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryBlockReason: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.deliveryBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.deliveryBlockReason_ = value
            }
        }
    }

    open var deliveryBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.deliveryBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.deliveryBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var expectedOrderNetAmount: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.expectedOrderNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.expectedOrderNetAmount_ = value
            }
        }
    }

    open var expectedOrderNetAmount: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesQuotationsType.expectedOrderNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.expectedOrderNetAmount, to: IntegerValue.of(optional: value))
        }
    }

    open class var hdrOrderProbabilityInPercent: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.hdrOrderProbabilityInPercent_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.hdrOrderProbabilityInPercent_ = value
            }
        }
    }

    open var hdrOrderProbabilityInPercent: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.hdrOrderProbabilityInPercent))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.hdrOrderProbabilityInPercent, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillingBlockReason: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.headerBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.headerBillingBlockReason_ = value
            }
        }
    }

    open var headerBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.headerBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.headerBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsVersion: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.incotermsVersion_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.incotermsVersion_ = value
            }
        }
    }

    open var incotermsVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.incotermsVersion))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.incotermsVersion, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesQuotation: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.lastChangeDate, to: value)
        }
    }

    open class var lastChangeDateTime: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.lastChangeDateTime_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.lastChangeDateTime_ = value
            }
        }
    }

    open var lastChangeDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.lastChangeDateTime))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.lastChangeDateTime, to: value)
        }
    }

    open var old: SalesQuotationsType {
        return CastRequired<SalesQuotationsType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDDocumentRejectionSts: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.overallSDDocumentRejectionSts_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.overallSDDocumentRejectionSts_ = value
            }
        }
    }

    open var overallSDDocumentRejectionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.overallSDDocumentRejectionSts))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.overallSDDocumentRejectionSts, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatus: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.overallSDProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.overallSDProcessStatus_ = value
            }
        }
    }

    open var overallSDProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.overallSDProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.overallSDProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var paymentMethod: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.paymentMethod_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.paymentMethod_ = value
            }
        }
    }

    open var paymentMethod: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.paymentMethod))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.paymentMethod, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.pricingDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.pricingDate_ = value
            }
        }
    }

    open var pricingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.pricingDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.pricingDate, to: value)
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var requestedDeliveryDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.requestedDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.requestedDeliveryDate_ = value
            }
        }
    }

    open var requestedDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.requestedDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.requestedDeliveryDate, to: value)
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesGroup: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesGroup_ = value
            }
        }
    }

    open var salesGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesGroup, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotationDate: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesQuotationDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesQuotationDate_ = value
            }
        }
    }

    open var salesQuotationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesQuotationsType.salesQuotationDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesQuotationDate, to: value)
        }
    }

    open class var salesQuotationType: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.salesQuotationType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.salesQuotationType_ = value
            }
        }
    }

    open var salesQuotationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.salesQuotationType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.salesQuotationType, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentReason: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.sdDocumentReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.sdDocumentReason_ = value
            }
        }
    }

    open var sdDocumentReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.sdDocumentReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.sdDocumentReason, to: StringValue.of(optional: value))
        }
    }

    open class var shippingCondition: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.shippingCondition_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.shippingCondition_ = value
            }
        }
    }

    open var shippingCondition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.shippingCondition))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.shippingCondition, to: StringValue.of(optional: value))
        }
    }

    open class var shippingType: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.shippingType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.shippingType_ = value
            }
        }
    }

    open var shippingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.shippingType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.shippingType, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesQuotationPartner: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationPartner: Array<SalesQuotationPartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner)).toArray(), Array<SalesQuotationPartnerType>())
        }
        set(value) {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesQuotationPricing: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationPricing: Array<SalesQuotationPricingType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing)).toArray(), Array<SalesQuotationPricingType>())
        }
        set(value) {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationPricing.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesQuotationsItens: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationsItens: Array<SalesQuotationItensType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens)).toArray(), Array<SalesQuotationItensType>())
        }
        set(value) {
            SalesQuotationsType.toXITSPDSFAxASalesQuotationsItens.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var totalCreditCheckStatus: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.totalCreditCheckStatus_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.totalCreditCheckStatus_ = value
            }
        }
    }

    open var totalCreditCheckStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.totalCreditCheckStatus))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.totalCreditCheckStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalNetAmount: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.totalNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.totalNetAmount_ = value
            }
        }
    }

    open var totalNetAmount: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesQuotationsType.totalNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.totalNetAmount, to: IntegerValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var xITSPDSFAxMobileIDSDH: Property {
        get {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                return SalesQuotationsType.xITSPDSFAxMobileIDSDH_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationsType.self)
            defer { objc_sync_exit(SalesQuotationsType.self) }
            do {
                SalesQuotationsType.xITSPDSFAxMobileIDSDH_ = value
            }
        }
    }

    open var xITSPDSFAxMobileIDSDH: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationsType.xITSPDSFAxMobileIDSDH))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationsType.xITSPDSFAxMobileIDSDH, to: StringValue.of(optional: value))
        }
    }
}
