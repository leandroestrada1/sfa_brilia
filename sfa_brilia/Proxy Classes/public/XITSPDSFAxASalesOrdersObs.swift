// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdersObs: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesorder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorder")

    private static var salesorganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorganization")

    private static var distributionchannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Distributionchannel")

    private static var organizationdivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Organizationdivision")

    private static var salesorderobsid_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobsid")

    private static var salesorderobstext_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobstext")

    private static var salesorderobsseq_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Salesorderobsseq")

    private static var bpcustomernumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "Bpcustomernumber")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "SoldToParty")

    private static var nSalesOrdersObs_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "N_SalesOrdersObs")

    private static var toXITSPDSFAxASalesOrders_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs.property(withName: "to_xITSPDSFAxA_SalesOrders")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdersObs> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdersObs>())
    }

    open class var bpcustomernumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.bpcustomernumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.bpcustomernumber_ = value
            }
        }
    }

    open var bpcustomernumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.bpcustomernumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.bpcustomernumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrdersObs {
        return CastRequired<XITSPDSFAxASalesOrdersObs>.from(self.copyEntity())
    }

    open class var distributionchannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.distributionchannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.distributionchannel_ = value
            }
        }
    }

    open var distributionchannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.distributionchannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.distributionchannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?) -> EntityKey {
        return EntityKey().with(name: "Salesorder", value: StringValue.of(optional: salesorder)).with(name: "Salesorganization", value: StringValue.of(optional: salesorganization)).with(name: "Distributionchannel", value: StringValue.of(optional: distributionchannel)).with(name: "Organizationdivision", value: StringValue.of(optional: organizationdivision)).with(name: "Salesorderobsid", value: StringValue.of(optional: salesorderobsid)).with(name: "Salesorderobsseq", value: StringValue.of(optional: salesorderobsseq)).with(name: "Bpcustomernumber", value: StringValue.of(optional: bpcustomernumber)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty))
    }

    open class var nSalesOrdersObs: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.nSalesOrdersObs_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.nSalesOrdersObs_ = value
            }
        }
    }

    open var nSalesOrdersObs: Array<XITSPDSFAxASalesOrdersObsN> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersObs.nSalesOrdersObs)).toArray(), Array<XITSPDSFAxASalesOrdersObsN>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersObs.nSalesOrdersObs.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open var old: XITSPDSFAxASalesOrdersObs {
        return CastRequired<XITSPDSFAxASalesOrdersObs>.from(self.oldEntity)
    }

    open class var organizationdivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.organizationdivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.organizationdivision_ = value
            }
        }
    }

    open var organizationdivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.organizationdivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.organizationdivision, to: StringValue.of(optional: value))
        }
    }

    open class var salesorder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.salesorder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.salesorder_ = value
            }
        }
    }

    open var salesorder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.salesorder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.salesorder, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobsid: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.salesorderobsid_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.salesorderobsid_ = value
            }
        }
    }

    open var salesorderobsid: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobsid))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobsid, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobsseq: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.salesorderobsseq_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.salesorderobsseq_ = value
            }
        }
    }

    open var salesorderobsseq: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobsseq))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobsseq, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobstext: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.salesorderobstext_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.salesorderobstext_ = value
            }
        }
    }

    open var salesorderobstext: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobstext))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.salesorderobstext, to: StringValue.of(optional: value))
        }
    }

    open class var salesorganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.salesorganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.salesorganization_ = value
            }
        }
    }

    open var salesorganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.salesorganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.salesorganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrders: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                return XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObs.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObs.self) }
            do {
                XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrders: XITSPDSFAxASalesOrdersType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObs.toXITSPDSFAxASalesOrders, to: value)
        }
    }
}
