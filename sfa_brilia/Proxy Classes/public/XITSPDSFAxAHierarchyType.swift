// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAHierarchyType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var partner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "PARTNER")

    private static var organization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "ORGANIZATION")

    private static var parent_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "PARENT")

    private static var child_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType.property(withName: "CHILD")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAHierarchyType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAHierarchyType>())
    }

    open class var child: Property {
        get {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                return XITSPDSFAxAHierarchyType.child_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                XITSPDSFAxAHierarchyType.child_ = value
            }
        }
    }

    open var child: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAHierarchyType.child))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAHierarchyType.child, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAHierarchyType {
        return CastRequired<XITSPDSFAxAHierarchyType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(partner: String?, organization: String?, parent: String?, child: String?) -> EntityKey {
        return EntityKey().with(name: "PARTNER", value: StringValue.of(optional: partner)).with(name: "ORGANIZATION", value: StringValue.of(optional: organization)).with(name: "PARENT", value: StringValue.of(optional: parent)).with(name: "CHILD", value: StringValue.of(optional: child))
    }

    open var old: XITSPDSFAxAHierarchyType {
        return CastRequired<XITSPDSFAxAHierarchyType>.from(self.oldEntity)
    }

    open class var organization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                return XITSPDSFAxAHierarchyType.organization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                XITSPDSFAxAHierarchyType.organization_ = value
            }
        }
    }

    open var organization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAHierarchyType.organization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAHierarchyType.organization, to: StringValue.of(optional: value))
        }
    }

    open class var parent: Property {
        get {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                return XITSPDSFAxAHierarchyType.parent_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                XITSPDSFAxAHierarchyType.parent_ = value
            }
        }
    }

    open var parent: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAHierarchyType.parent))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAHierarchyType.parent, to: StringValue.of(optional: value))
        }
    }

    open class var partner: Property {
        get {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                return XITSPDSFAxAHierarchyType.partner_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAHierarchyType.self)
            defer { objc_sync_exit(XITSPDSFAxAHierarchyType.self) }
            do {
                XITSPDSFAxAHierarchyType.partner_ = value
            }
        }
    }

    open var partner: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAHierarchyType.partner))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAHierarchyType.partner, to: StringValue.of(optional: value))
        }
    }
}
