// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerLstSalOrdType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Customer")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Division")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "BPCustomerNumber")

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrder")

    private static var salesOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "SalesOrderDate")

    private static var totalNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "TotalNetAmount")

    private static var conditionAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "ConditionAmount")

    private static var greenDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "GreenDate")

    private static var yellowDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "YellowDate")

    private static var status_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "Status")

    private static var colorHex_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType.property(withName: "ColorHex")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerLstSalOrdType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerLstSalOrdType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var colorHex: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.colorHex_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.colorHex_ = value
            }
        }
    }

    open var colorHex: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.colorHex))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.colorHex, to: StringValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerLstSalOrdType {
        return CastRequired<XITSPDSFAxACustomerLstSalOrdType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.division, to: StringValue.of(optional: value))
        }
    }

    open class var greenDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.greenDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.greenDate_ = value
            }
        }
    }

    open var greenDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.greenDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.greenDate, to: value)
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open var old: XITSPDSFAxACustomerLstSalOrdType {
        return CastRequired<XITSPDSFAxACustomerLstSalOrdType>.from(self.oldEntity)
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.salesOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.salesOrderDate_ = value
            }
        }
    }

    open var salesOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrderDate, to: value)
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var status: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.status_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.status_ = value
            }
        }
    }

    open var status: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.status))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.status, to: StringValue.of(optional: value))
        }
    }

    open class var totalNetAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.totalNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.totalNetAmount_ = value
            }
        }
    }

    open var totalNetAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.totalNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.totalNetAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var yellowDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                return XITSPDSFAxACustomerLstSalOrdType.yellowDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstSalOrdType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstSalOrdType.self) }
            do {
                XITSPDSFAxACustomerLstSalOrdType.yellowDate_ = value
            }
        }
    }

    open var yellowDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstSalOrdType.yellowDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstSalOrdType.yellowDate, to: value)
        }
    }
}
