// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationItemPricingType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionCurrency")

    private static var conditionQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionQuantity")

    private static var conditionQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionQuantityUnit")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionRateValue")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "ConditionType")

    private static var pricingProcedureCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "PricingProcedureCounter")

    private static var pricingProcedureStep_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "PricingProcedureStep")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "SalesQuotation")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "TransactionCurrency")

    private static var toXITSPDSFAxASalesQuotationItemPricing_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPricing")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType)
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationItemPricingType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationItemPricingType>())
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantity: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.conditionQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.conditionQuantity_ = value
            }
        }
    }

    open var conditionQuantity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.conditionQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.conditionQuantity, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantityUnit: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.conditionQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.conditionQuantityUnit_ = value
            }
        }
    }

    open var conditionQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.conditionQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.conditionQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.conditionRateValue, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesQuotationItemPricingType {
        return CastRequired<SalesQuotationItemPricingType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesQuotation: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation))
    }

    open var old: SalesQuotationItemPricingType {
        return CastRequired<SalesQuotationItemPricingType>.from(self.oldEntity)
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesQuotationItemPricing: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationItemPricing: SalesQuotationItensType? {
        get {
            return CastOptional<SalesQuotationItensType>.from(self.optionalValue(for: SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.toXITSPDSFAxASalesQuotationItemPricing, to: value)
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                return SalesQuotationItemPricingType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPricingType.self)
            defer { objc_sync_exit(SalesQuotationItemPricingType.self) }
            do {
                SalesQuotationItemPricingType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPricingType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPricingType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }
}
