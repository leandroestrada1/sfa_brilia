// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductHieLevel8Type: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var productHierarchyNode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ProductHierarchyNode")

    private static var zProductHierarchyLevel1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel1")

    private static var zProductHierarchyLevel2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel2")

    private static var zProductHierarchyLevel3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel3")

    private static var zProductHierarchyLevel4_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel4")

    private static var zProductHierarchyLevel5_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel5")

    private static var zProductHierarchyLevel6_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel6")

    private static var zProductHierarchyLevel7_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel7")

    private static var zProductHierarchyLevel8_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ZProductHierarchyLevel8")

    private static var productHierarchyNodeText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type.property(withName: "ProductHierarchyNodeText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductHieLevel8Type> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductHieLevel8Type>())
    }

    open func copy() -> XITSPDSFAxAProductHieLevel8Type {
        return CastRequired<XITSPDSFAxAProductHieLevel8Type>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(productHierarchyNode: String?) -> EntityKey {
        return EntityKey().with(name: "ProductHierarchyNode", value: StringValue.of(optional: productHierarchyNode))
    }

    open var old: XITSPDSFAxAProductHieLevel8Type {
        return CastRequired<XITSPDSFAxAProductHieLevel8Type>.from(self.oldEntity)
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNodeText: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText_ = value
            }
        }
    }

    open var productHierarchyNodeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.productHierarchyNodeText, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel1: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1_ = value
            }
        }
    }

    open var zProductHierarchyLevel1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel1, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel2: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2_ = value
            }
        }
    }

    open var zProductHierarchyLevel2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel2, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel3: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3_ = value
            }
        }
    }

    open var zProductHierarchyLevel3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel3, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel4: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4_ = value
            }
        }
    }

    open var zProductHierarchyLevel4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel4, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel5: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5_ = value
            }
        }
    }

    open var zProductHierarchyLevel5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel5, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel6: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6_ = value
            }
        }
    }

    open var zProductHierarchyLevel6: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel6, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel7: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7_ = value
            }
        }
    }

    open var zProductHierarchyLevel7: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel7, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel8: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel8Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel8Type.self) }
            do {
                XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8_ = value
            }
        }
    }

    open var zProductHierarchyLevel8: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel8Type.zProductHierarchyLevel8, to: StringValue.of(optional: value))
        }
    }
}
