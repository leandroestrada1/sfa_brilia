// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerMasterType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Customer")

    private static var organizationBPName1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName1")

    private static var organizationBPName2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName2")

    private static var organizationBPName3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName3")

    private static var organizationBPName4_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "OrganizationBPName4")

    private static var searchTerm1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "SearchTerm1")

    private static var cityName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CityName")

    private static var country_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Country")

    private static var district_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "District")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "Region")

    private static var streetName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "StreetName")

    private static var houseNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "HouseNumber")

    private static var postalCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "PostalCode")

    private static var customerCreditLimitAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CustomerCreditLimitAmount")

    private static var creditExposure_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "CreditExposure")

    private static var xBlocked_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "XBlocked")

    private static var taxJurisdiction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxJurisdiction")

    private static var emailAddress_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "EmailAddress")

    private static var phoneNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "PhoneNumber")

    private static var taxNumber1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxNumber1")

    private static var taxNumber3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "TaxNumber3")

    private static var fieldAux1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "FIELD_AUX_1")

    private static var fieldAux2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "FIELD_AUX_2")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "IsFinal")

    private static var industrialSector_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "IndustrialSector")

    private static var toXITSPDSFAxACustomerEmailXML_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerEmailXML")

    private static var toXITSPDSFAxACustomerSalesArea_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")

    private static var toXITSPDSFAxACustomerLatLong_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType.property(withName: "to_xITSPDSFAxA_CustomerLatLong")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerMasterType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerMasterType>())
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.cityName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerMasterType {
        return CastRequired<XITSPDSFAxACustomerMasterType>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.country_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.country))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.country, to: StringValue.of(optional: value))
        }
    }

    open class var creditExposure: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.creditExposure_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.creditExposure_ = value
            }
        }
    }

    open var creditExposure: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.creditExposure))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.creditExposure, to: DecimalValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerCreditLimitAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.customerCreditLimitAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.customerCreditLimitAmount_ = value
            }
        }
    }

    open var customerCreditLimitAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.customerCreditLimitAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.customerCreditLimitAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var district: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.district_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.district_ = value
            }
        }
    }

    open var district: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.district))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.district, to: StringValue.of(optional: value))
        }
    }

    open class var emailAddress: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.emailAddress_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.emailAddress_ = value
            }
        }
    }

    open var emailAddress: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.emailAddress))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.emailAddress, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux1: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.fieldAux1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.fieldAux1_ = value
            }
        }
    }

    open var fieldAux1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.fieldAux1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.fieldAux1, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux2: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.fieldAux2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.fieldAux2_ = value
            }
        }
    }

    open var fieldAux2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.fieldAux2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.fieldAux2, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.houseNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.houseNumber_ = value
            }
        }
    }

    open var houseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.houseNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.houseNumber, to: StringValue.of(optional: value))
        }
    }

    open class var industrialSector: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.industrialSector_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.industrialSector_ = value
            }
        }
    }

    open var industrialSector: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.industrialSector))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.industrialSector, to: StringValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer))
    }

    open var old: XITSPDSFAxACustomerMasterType {
        return CastRequired<XITSPDSFAxACustomerMasterType>.from(self.oldEntity)
    }

    open class var organizationBPName1: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.organizationBPName1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.organizationBPName1_ = value
            }
        }
    }

    open var organizationBPName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName1, to: StringValue.of(optional: value))
        }
    }

    open class var organizationBPName2: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.organizationBPName2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.organizationBPName2_ = value
            }
        }
    }

    open var organizationBPName2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName2, to: StringValue.of(optional: value))
        }
    }

    open class var organizationBPName3: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.organizationBPName3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.organizationBPName3_ = value
            }
        }
    }

    open var organizationBPName3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName3, to: StringValue.of(optional: value))
        }
    }

    open class var organizationBPName4: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.organizationBPName4_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.organizationBPName4_ = value
            }
        }
    }

    open var organizationBPName4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName4))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.organizationBPName4, to: StringValue.of(optional: value))
        }
    }

    open class var phoneNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.phoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.phoneNumber_ = value
            }
        }
    }

    open var phoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.phoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.phoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.region, to: StringValue.of(optional: value))
        }
    }

    open class var searchTerm1: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.searchTerm1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.searchTerm1_ = value
            }
        }
    }

    open var searchTerm1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.searchTerm1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.searchTerm1, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdiction: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.taxJurisdiction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.taxJurisdiction_ = value
            }
        }
    }

    open var taxJurisdiction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.taxJurisdiction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.taxJurisdiction, to: StringValue.of(optional: value))
        }
    }

    open class var taxNumber1: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.taxNumber1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.taxNumber1_ = value
            }
        }
    }

    open var taxNumber1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.taxNumber1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.taxNumber1, to: StringValue.of(optional: value))
        }
    }

    open class var taxNumber3: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.taxNumber3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.taxNumber3_ = value
            }
        }
    }

    open var taxNumber3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.taxNumber3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.taxNumber3, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxACustomerEmailXML: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerEmailXML: Array<XITSPDSFAxACustomerEmailXMLType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML)).toArray(), Array<XITSPDSFAxACustomerEmailXMLType>())
        }
        set(value) {
            XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerEmailXML.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxACustomerLatLong: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerLatLong: XITSPDSFAxACustomerLatLong? {
        get {
            return CastOptional<XITSPDSFAxACustomerLatLong>.from(self.optionalValue(for: XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerLatLong, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerSalesArea: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerSalesArea: Array<XITSPDSFAxACustomerSalesAreaType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea)).toArray(), Array<XITSPDSFAxACustomerSalesAreaType>())
        }
        set(value) {
            XITSPDSFAxACustomerMasterType.toXITSPDSFAxACustomerSalesArea.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var xBlocked: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                return XITSPDSFAxACustomerMasterType.xBlocked_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerMasterType.self) }
            do {
                XITSPDSFAxACustomerMasterType.xBlocked_ = value
            }
        }
    }

    open var xBlocked: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxACustomerMasterType.xBlocked))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerMasterType.xBlocked, to: BooleanValue.of(optional: value))
        }
    }
}
