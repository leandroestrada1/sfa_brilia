// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxABillingDocumentType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var billingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BillingDocument")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "Division")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BPCustomerNumber")

    private static var portion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "Portion")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "CustomerName")

    private static var additionalValueDays_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "AdditionalValueDays")

    private static var companyCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "CompanyCode")

    private static var fiscalYear_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "FiscalYear")

    private static var docNum_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DocNum")

    private static var docNumDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "DocNumDate")

    private static var netDueDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "NetDueDate")

    private static var clearingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "ClearingDate")

    private static var zInternalDatePeriod_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "ZInternalDatePeriod")

    private static var portionTotal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "PortionTotal")

    private static var netValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "NetValue")

    private static var brNFeNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "BR_NFeNumber")

    private static var statusColorDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "StatusColorDesc")

    private static var statusColorHex_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType.property(withName: "StatusColorHex")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType)
    }

    open class var additionalValueDays: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.additionalValueDays_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.additionalValueDays_ = value
            }
        }
    }

    open var additionalValueDays: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.additionalValueDays))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.additionalValueDays, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxABillingDocumentType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxABillingDocumentType>())
    }

    open class var billingDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.billingDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.billingDocument_ = value
            }
        }
    }

    open var billingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.billingDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.billingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var brNFeNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.brNFeNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.brNFeNumber_ = value
            }
        }
    }

    open var brNFeNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.brNFeNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.brNFeNumber, to: StringValue.of(optional: value))
        }
    }

    open class var clearingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.clearingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.clearingDate_ = value
            }
        }
    }

    open var clearingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.clearingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.clearingDate, to: value)
        }
    }

    open class var companyCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.companyCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.companyCode_ = value
            }
        }
    }

    open var companyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.companyCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.companyCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxABillingDocumentType {
        return CastRequired<XITSPDSFAxABillingDocumentType>.from(self.copyEntity())
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.division, to: StringValue.of(optional: value))
        }
    }

    open class var docNum: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.docNum_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.docNum_ = value
            }
        }
    }

    open var docNum: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.docNum))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.docNum, to: StringValue.of(optional: value))
        }
    }

    open class var docNumDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.docNumDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.docNumDate_ = value
            }
        }
    }

    open var docNumDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.docNumDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.docNumDate, to: value)
        }
    }

    open class var fiscalYear: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.fiscalYear_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.fiscalYear_ = value
            }
        }
    }

    open var fiscalYear: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.fiscalYear))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.fiscalYear, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(billingDocument: String?, salesOrganization: String?, distributionChannel: String?, division: String?, soldToParty: String?, bpCustomerNumber: String?, portion: String?) -> EntityKey {
        return EntityKey().with(name: "BillingDocument", value: StringValue.of(optional: billingDocument)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "Portion", value: StringValue.of(optional: portion))
    }

    open class var netDueDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.netDueDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.netDueDate_ = value
            }
        }
    }

    open var netDueDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.netDueDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.netDueDate, to: value)
        }
    }

    open class var netValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.netValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.netValue_ = value
            }
        }
    }

    open var netValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.netValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.netValue, to: DecimalValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxABillingDocumentType {
        return CastRequired<XITSPDSFAxABillingDocumentType>.from(self.oldEntity)
    }

    open class var portion: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.portion_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.portion_ = value
            }
        }
    }

    open var portion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.portion))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.portion, to: StringValue.of(optional: value))
        }
    }

    open class var portionTotal: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.portionTotal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.portionTotal_ = value
            }
        }
    }

    open var portionTotal: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.portionTotal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.portionTotal, to: IntValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var statusColorDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.statusColorDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.statusColorDesc_ = value
            }
        }
    }

    open var statusColorDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.statusColorDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.statusColorDesc, to: StringValue.of(optional: value))
        }
    }

    open class var statusColorHex: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.statusColorHex_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.statusColorHex_ = value
            }
        }
    }

    open var statusColorHex: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.statusColorHex))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.statusColorHex, to: StringValue.of(optional: value))
        }
    }

    open class var zInternalDatePeriod: Property {
        get {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                return XITSPDSFAxABillingDocumentType.zInternalDatePeriod_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABillingDocumentType.self)
            defer { objc_sync_exit(XITSPDSFAxABillingDocumentType.self) }
            do {
                XITSPDSFAxABillingDocumentType.zInternalDatePeriod_ = value
            }
        }
    }

    open var zInternalDatePeriod: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxABillingDocumentType.zInternalDatePeriod))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABillingDocumentType.zInternalDatePeriod, to: value)
        }
    }
}
