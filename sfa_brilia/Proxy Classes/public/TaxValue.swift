// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class TaxValue: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var taxType_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "TaxType")

    private static var groupPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "GroupPriority")

    private static var rate_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "RATE")

    private static var rateValueUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "RateValueUnit")

    private static var field01_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field01")

    private static var field02_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field02")

    private static var field03_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field03")

    private static var field04_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field04")

    private static var field05_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field05")

    private static var field06_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field06")

    private static var field07_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field07")

    private static var field08_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field08")

    private static var field09_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field09")

    private static var field10_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field10")

    private static var field11_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field11")

    private static var field12_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field12")

    private static var field13_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field13")

    private static var field14_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field14")

    private static var field15_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field15")

    private static var field16_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field16")

    private static var field17_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field17")

    private static var field18_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field18")

    private static var field19_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field19")

    private static var field20_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue.property(withName: "Field20")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue)
    }

    open func copy() -> TaxValue {
        return CastRequired<TaxValue>.from(self.copyComplex())
    }

    open class var field01: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field01_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field01_ = value
            }
        }
    }

    open var field01: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field01))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field01, to: StringValue.of(optional: value))
        }
    }

    open class var field02: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field02_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field02_ = value
            }
        }
    }

    open var field02: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field02))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field02, to: StringValue.of(optional: value))
        }
    }

    open class var field03: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field03_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field03_ = value
            }
        }
    }

    open var field03: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field03))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field03, to: StringValue.of(optional: value))
        }
    }

    open class var field04: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field04_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field04_ = value
            }
        }
    }

    open var field04: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field04))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field04, to: StringValue.of(optional: value))
        }
    }

    open class var field05: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field05_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field05_ = value
            }
        }
    }

    open var field05: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field05))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field05, to: StringValue.of(optional: value))
        }
    }

    open class var field06: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field06_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field06_ = value
            }
        }
    }

    open var field06: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field06))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field06, to: StringValue.of(optional: value))
        }
    }

    open class var field07: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field07_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field07_ = value
            }
        }
    }

    open var field07: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field07))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field07, to: StringValue.of(optional: value))
        }
    }

    open class var field08: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field08_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field08_ = value
            }
        }
    }

    open var field08: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field08))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field08, to: StringValue.of(optional: value))
        }
    }

    open class var field09: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field09_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field09_ = value
            }
        }
    }

    open var field09: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field09))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field09, to: StringValue.of(optional: value))
        }
    }

    open class var field10: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field10_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field10_ = value
            }
        }
    }

    open var field10: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field10))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field10, to: StringValue.of(optional: value))
        }
    }

    open class var field11: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field11_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field11_ = value
            }
        }
    }

    open var field11: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field11))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field11, to: StringValue.of(optional: value))
        }
    }

    open class var field12: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field12_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field12_ = value
            }
        }
    }

    open var field12: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field12))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field12, to: StringValue.of(optional: value))
        }
    }

    open class var field13: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field13_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field13_ = value
            }
        }
    }

    open var field13: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field13))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field13, to: StringValue.of(optional: value))
        }
    }

    open class var field14: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field14_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field14_ = value
            }
        }
    }

    open var field14: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field14))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field14, to: StringValue.of(optional: value))
        }
    }

    open class var field15: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field15_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field15_ = value
            }
        }
    }

    open var field15: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field15))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field15, to: StringValue.of(optional: value))
        }
    }

    open class var field16: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field16_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field16_ = value
            }
        }
    }

    open var field16: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field16))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field16, to: StringValue.of(optional: value))
        }
    }

    open class var field17: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field17_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field17_ = value
            }
        }
    }

    open var field17: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field17))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field17, to: StringValue.of(optional: value))
        }
    }

    open class var field18: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field18_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field18_ = value
            }
        }
    }

    open var field18: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field18))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field18, to: StringValue.of(optional: value))
        }
    }

    open class var field19: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field19_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field19_ = value
            }
        }
    }

    open var field19: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field19))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field19, to: StringValue.of(optional: value))
        }
    }

    open class var field20: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.field20_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.field20_ = value
            }
        }
    }

    open var field20: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.field20))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.field20, to: StringValue.of(optional: value))
        }
    }

    open class var groupPriority: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.groupPriority_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.groupPriority_ = value
            }
        }
    }

    open var groupPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.groupPriority))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.groupPriority, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: TaxValue {
        return CastRequired<TaxValue>.from(self.oldComplex)
    }

    open class var rate: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.rate_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.rate_ = value
            }
        }
    }

    open var rate: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.rate))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.rate, to: StringValue.of(optional: value))
        }
    }

    open class var rateValueUnit: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.rateValueUnit_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.rateValueUnit_ = value
            }
        }
    }

    open var rateValueUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.rateValueUnit))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.rateValueUnit, to: StringValue.of(optional: value))
        }
    }

    open class var taxType: Property {
        get {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                return TaxValue.taxType_
            }
        }
        set(value) {
            objc_sync_enter(TaxValue.self)
            defer { objc_sync_exit(TaxValue.self) }
            do {
                TaxValue.taxType_ = value
            }
        }
    }

    open var taxType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: TaxValue.taxType))
        }
        set(value) {
            self.setOptionalValue(for: TaxValue.taxType, to: StringValue.of(optional: value))
        }
    }
}
