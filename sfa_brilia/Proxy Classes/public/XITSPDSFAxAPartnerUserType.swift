// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPartnerUserType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var userLogin_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "UserLogin")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "BPCustomerNumber")

    private static var toXITSPDSFAxAPartners_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType.property(withName: "to_xITSPDSFAxA_Partners")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPartnerUserType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPartnerUserType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                return XITSPDSFAxAPartnerUserType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                XITSPDSFAxAPartnerUserType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerUserType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerUserType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAPartnerUserType {
        return CastRequired<XITSPDSFAxAPartnerUserType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(userLogin: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "UserLogin", value: StringValue.of(optional: userLogin)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open var old: XITSPDSFAxAPartnerUserType {
        return CastRequired<XITSPDSFAxAPartnerUserType>.from(self.oldEntity)
    }

    open class var toXITSPDSFAxAPartners: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                return XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners_ = value
            }
        }
    }

    open var toXITSPDSFAxAPartners: Array<XITSPDSFAxAPartnersType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners)).toArray(), Array<XITSPDSFAxAPartnersType>())
        }
        set(value) {
            XITSPDSFAxAPartnerUserType.toXITSPDSFAxAPartners.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var userLogin: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                return XITSPDSFAxAPartnerUserType.userLogin_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerUserType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerUserType.self) }
            do {
                XITSPDSFAxAPartnerUserType.userLogin_ = value
            }
        }
    }

    open var userLogin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerUserType.userLogin))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerUserType.userLogin, to: StringValue.of(optional: value))
        }
    }
}
