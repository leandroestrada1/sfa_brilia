// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAConditionValue: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionRecord_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRecord")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionType")

    private static var accessSequence_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "AccessSequence")

    private static var conditionTable_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionTable")

    private static var field01_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field01")

    private static var field02_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field02")

    private static var field03_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field03")

    private static var field04_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field04")

    private static var field05_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field05")

    private static var field06_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field06")

    private static var field07_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field07")

    private static var field08_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field08")

    private static var field09_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field09")

    private static var field10_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field10")

    private static var field11_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field11")

    private static var field12_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field12")

    private static var field13_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field13")

    private static var field14_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field14")

    private static var field15_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field15")

    private static var field16_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field16")

    private static var field17_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field17")

    private static var field18_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field18")

    private static var field19_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field19")

    private static var field20_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "Field20")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRateValue")

    private static var conditionRateValueUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue.property(withName: "ConditionRateValueUnit")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue)
    }

    open class var accessSequence: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.accessSequence_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.accessSequence_ = value
            }
        }
    }

    open var accessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.accessSequence))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.accessSequence, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAConditionValue> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAConditionValue>())
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionRateValueUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.conditionRateValueUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.conditionRateValueUnit_ = value
            }
        }
    }

    open var conditionRateValueUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.conditionRateValueUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.conditionRateValueUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRecord: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.conditionRecord_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.conditionRecord_ = value
            }
        }
    }

    open var conditionRecord: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.conditionRecord))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.conditionRecord, to: StringValue.of(optional: value))
        }
    }

    open class var conditionTable: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.conditionTable_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.conditionTable_ = value
            }
        }
    }

    open var conditionTable: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.conditionTable))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.conditionTable, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAConditionValue {
        return CastRequired<XITSPDSFAxAConditionValue>.from(self.copyEntity())
    }

    open class var field01: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field01_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field01_ = value
            }
        }
    }

    open var field01: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field01))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field01, to: StringValue.of(optional: value))
        }
    }

    open class var field02: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field02_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field02_ = value
            }
        }
    }

    open var field02: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field02))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field02, to: StringValue.of(optional: value))
        }
    }

    open class var field03: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field03_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field03_ = value
            }
        }
    }

    open var field03: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field03))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field03, to: StringValue.of(optional: value))
        }
    }

    open class var field04: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field04_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field04_ = value
            }
        }
    }

    open var field04: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field04))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field04, to: StringValue.of(optional: value))
        }
    }

    open class var field05: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field05_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field05_ = value
            }
        }
    }

    open var field05: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field05))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field05, to: StringValue.of(optional: value))
        }
    }

    open class var field06: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field06_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field06_ = value
            }
        }
    }

    open var field06: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field06))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field06, to: StringValue.of(optional: value))
        }
    }

    open class var field07: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field07_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field07_ = value
            }
        }
    }

    open var field07: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field07))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field07, to: StringValue.of(optional: value))
        }
    }

    open class var field08: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field08_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field08_ = value
            }
        }
    }

    open var field08: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field08))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field08, to: StringValue.of(optional: value))
        }
    }

    open class var field09: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field09_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field09_ = value
            }
        }
    }

    open var field09: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field09))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field09, to: StringValue.of(optional: value))
        }
    }

    open class var field10: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field10_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field10_ = value
            }
        }
    }

    open var field10: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field10))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field10, to: StringValue.of(optional: value))
        }
    }

    open class var field11: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field11_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field11_ = value
            }
        }
    }

    open var field11: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field11))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field11, to: StringValue.of(optional: value))
        }
    }

    open class var field12: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field12_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field12_ = value
            }
        }
    }

    open var field12: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field12))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field12, to: StringValue.of(optional: value))
        }
    }

    open class var field13: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field13_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field13_ = value
            }
        }
    }

    open var field13: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field13))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field13, to: StringValue.of(optional: value))
        }
    }

    open class var field14: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field14_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field14_ = value
            }
        }
    }

    open var field14: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field14))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field14, to: StringValue.of(optional: value))
        }
    }

    open class var field15: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field15_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field15_ = value
            }
        }
    }

    open var field15: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field15))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field15, to: StringValue.of(optional: value))
        }
    }

    open class var field16: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field16_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field16_ = value
            }
        }
    }

    open var field16: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field16))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field16, to: StringValue.of(optional: value))
        }
    }

    open class var field17: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field17_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field17_ = value
            }
        }
    }

    open var field17: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field17))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field17, to: StringValue.of(optional: value))
        }
    }

    open class var field18: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field18_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field18_ = value
            }
        }
    }

    open var field18: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field18))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field18, to: StringValue.of(optional: value))
        }
    }

    open class var field19: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field19_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field19_ = value
            }
        }
    }

    open var field19: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field19))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field19, to: StringValue.of(optional: value))
        }
    }

    open class var field20: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                return XITSPDSFAxAConditionValue.field20_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionValue.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionValue.self) }
            do {
                XITSPDSFAxAConditionValue.field20_ = value
            }
        }
    }

    open var field20: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionValue.field20))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionValue.field20, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionTable: String?) -> EntityKey {
        return EntityKey().with(name: "ConditionRecord", value: StringValue.of(optional: conditionRecord)).with(name: "ConditionType", value: StringValue.of(optional: conditionType)).with(name: "AccessSequence", value: StringValue.of(optional: accessSequence)).with(name: "ConditionTable", value: StringValue.of(optional: conditionTable))
    }

    open var old: XITSPDSFAxAConditionValue {
        return CastRequired<XITSPDSFAxAConditionValue>.from(self.oldEntity)
    }
}
