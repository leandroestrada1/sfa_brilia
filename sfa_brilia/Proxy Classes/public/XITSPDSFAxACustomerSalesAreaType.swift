// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerSalesAreaType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Customer")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Division")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "BPCustomerNumber")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PartnerFunction")

    private static var partnerFunctionName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PartnerFunctionName")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerName")

    private static var customerPriceGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerPriceGroup")

    private static var customerGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerGroup")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "CustomerPaymentTerms")

    private static var priceListType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "PriceListType")

    private static var salesOffice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesOffice")

    private static var salesDistrict_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SalesDistrict")

    private static var supplyingPlant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "SupplyingPlant")

    private static var currency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Currency")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "IncotermsClassification")

    private static var status_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "Status")

    private static var colorHex_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "ColorHex")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "IsFinal")

    private static var toXITSPDSFAxABillingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_BillingDocument")

    private static var toXITSPDSFAxACustomerGrpText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerGrpText")

    private static var toXITSPDSFAxACustomerLstInv_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerLstInv")

    private static var toXITSPDSFAxACustomerLstSalOrd_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerLstSalOrd")

    private static var toXITSPDSFAxACustomerMaster_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerMaster")

    private static var toXITSPDSFAxACustomerShipTo_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_CustomerShipTo")

    private static var toXITSPDSFAxANotifications_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Notifications")

    private static var toXITSPDSFAxAPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Partner")

    private static var toXITSPDSFAxAPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_PaymentTerms")

    private static var toXITSPDSFAxAPricelists_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_Pricelists")

    private static var toXITSPDSFAxASalesOrders_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType.property(withName: "to_xITSPDSFAxA_SalesOrders")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerSalesAreaType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerSalesAreaType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var colorHex: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.colorHex_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.colorHex_ = value
            }
        }
    }

    open var colorHex: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.colorHex))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.colorHex, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerSalesAreaType {
        return CastRequired<XITSPDSFAxACustomerSalesAreaType>.from(self.copyEntity())
    }

    open class var currency: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.currency_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.currency_ = value
            }
        }
    }

    open var currency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.currency))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.currency, to: StringValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.customerGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.customerGroup_ = value
            }
        }
    }

    open var customerGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerGroup, to: StringValue.of(optional: value))
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var customerPriceGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.customerPriceGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.customerPriceGroup_ = value
            }
        }
    }

    open var customerPriceGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerPriceGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.customerPriceGroup, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.division, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open var old: XITSPDSFAxACustomerSalesAreaType {
        return CastRequired<XITSPDSFAxACustomerSalesAreaType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunctionName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.partnerFunctionName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.partnerFunctionName_ = value
            }
        }
    }

    open var partnerFunctionName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.partnerFunctionName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.partnerFunctionName, to: StringValue.of(optional: value))
        }
    }

    open class var priceListType: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.priceListType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.priceListType_ = value
            }
        }
    }

    open var priceListType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.priceListType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.priceListType, to: StringValue.of(optional: value))
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var status: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.status_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.status_ = value
            }
        }
    }

    open var status: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.status))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.status, to: StringValue.of(optional: value))
        }
    }

    open class var supplyingPlant: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.supplyingPlant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.supplyingPlant_ = value
            }
        }
    }

    open var supplyingPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.supplyingPlant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.supplyingPlant, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxABillingDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument_ = value
            }
        }
    }

    open var toXITSPDSFAxABillingDocument: Array<XITSPDSFAxABillingDocumentType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument)).toArray(), Array<XITSPDSFAxABillingDocumentType>())
        }
        set(value) {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxABillingDocument.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxACustomerGrpText: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerGrpText: XITSPDSFAxACustomerGrpTextType? {
        get {
            return CastOptional<XITSPDSFAxACustomerGrpTextType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerGrpText, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerLstInv: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerLstInv: XITSPDSFAxACustomerLstInvType? {
        get {
            return CastOptional<XITSPDSFAxACustomerLstInvType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstInv, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerLstSalOrd: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerLstSalOrd: XITSPDSFAxACustomerLstSalOrdType? {
        get {
            return CastOptional<XITSPDSFAxACustomerLstSalOrdType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerLstSalOrd, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerMaster: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerMaster: XITSPDSFAxACustomerMasterType? {
        get {
            return CastOptional<XITSPDSFAxACustomerMasterType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerMaster, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerShipTo: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerShipTo: Array<XITSPDSFAxACustomerShipToType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo)).toArray(), Array<XITSPDSFAxACustomerShipToType>())
        }
        set(value) {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxACustomerShipTo.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxANotifications: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications_ = value
            }
        }
    }

    open var toXITSPDSFAxANotifications: Array<XITSPDSFAxANotificationsType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications)).toArray(), Array<XITSPDSFAxANotificationsType>())
        }
        set(value) {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxANotifications.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxAPartner: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxAPartner: Array<XITSPDSFAxAPartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner)).toArray(), Array<XITSPDSFAxAPartnerType>())
        }
        set(value) {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxAPaymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms_ = value
            }
        }
    }

    open var toXITSPDSFAxAPaymentTerms: XITSPDSFAxAPaymentTermsType? {
        get {
            return CastOptional<XITSPDSFAxAPaymentTermsType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPaymentTerms, to: value)
        }
    }

    open class var toXITSPDSFAxAPricelists: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists_ = value
            }
        }
    }

    open var toXITSPDSFAxAPricelists: XITSPDSFAxAPricelistsType? {
        get {
            return CastOptional<XITSPDSFAxAPricelistsType>.from(self.optionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxAPricelists, to: value)
        }
    }

    open class var toXITSPDSFAxASalesOrders: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                return XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerSalesAreaType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerSalesAreaType.self) }
            do {
                XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrders: Array<XITSPDSFAxASalesOrdersType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders)).toArray(), Array<XITSPDSFAxASalesOrdersType>())
        }
        set(value) {
            XITSPDSFAxACustomerSalesAreaType.toXITSPDSFAxASalesOrders.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
