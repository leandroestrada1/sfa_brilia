// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAInvoiceType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesDocument")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BPCustomerNumber")

    private static var billingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BillingDocument")

    private static var brNotaFiscal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NotaFiscal")

    private static var brNFeNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFeNumber")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "CustomerName")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "PurchaseOrderByCustomer")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "OrganizationDivision")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "SalesOrderType")

    private static var brNFType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFType")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "CreationDate")

    private static var brNFArrivalOrDepartureDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFArrivalOrDepartureDate")

    private static var brNFPostingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFPostingDate")

    private static var brNFTotalAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType.property(withName: "BR_NFTotalAmount")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAInvoiceType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAInvoiceType>())
    }

    open class var billingDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.billingDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.billingDocument_ = value
            }
        }
    }

    open var billingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.billingDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.billingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var brNFArrivalOrDepartureDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate_ = value
            }
        }
    }

    open var brNFArrivalOrDepartureDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNFArrivalOrDepartureDate, to: value)
        }
    }

    open class var brNFPostingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNFPostingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNFPostingDate_ = value
            }
        }
    }

    open var brNFPostingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNFPostingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNFPostingDate, to: value)
        }
    }

    open class var brNFTotalAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNFTotalAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNFTotalAmount_ = value
            }
        }
    }

    open var brNFTotalAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNFTotalAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNFTotalAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var brNFType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNFType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNFType_ = value
            }
        }
    }

    open var brNFType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNFType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNFType, to: StringValue.of(optional: value))
        }
    }

    open class var brNFeNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNFeNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNFeNumber_ = value
            }
        }
    }

    open var brNFeNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNFeNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNFeNumber, to: StringValue.of(optional: value))
        }
    }

    open class var brNotaFiscal: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.brNotaFiscal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.brNotaFiscal_ = value
            }
        }
    }

    open var brNotaFiscal: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.brNotaFiscal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.brNotaFiscal, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAInvoiceType {
        return CastRequired<XITSPDSFAxAInvoiceType>.from(self.copyEntity())
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAInvoiceType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.creationDate, to: value)
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesDocument: String?, soldToParty: String?, bpCustomerNumber: String?, billingDocument: String?, brNotaFiscal: String?, brNFeNumber: String?) -> EntityKey {
        return EntityKey().with(name: "SalesDocument", value: StringValue.of(optional: salesDocument)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "BillingDocument", value: StringValue.of(optional: billingDocument)).with(name: "BR_NotaFiscal", value: StringValue.of(optional: brNotaFiscal)).with(name: "BR_NFeNumber", value: StringValue.of(optional: brNFeNumber))
    }

    open var old: XITSPDSFAxAInvoiceType {
        return CastRequired<XITSPDSFAxAInvoiceType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.salesDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.salesDocument_ = value
            }
        }
    }

    open var salesDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.salesDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.salesDocument, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                return XITSPDSFAxAInvoiceType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAInvoiceType.self)
            defer { objc_sync_exit(XITSPDSFAxAInvoiceType.self) }
            do {
                XITSPDSFAxAInvoiceType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAInvoiceType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAInvoiceType.soldToParty, to: StringValue.of(optional: value))
        }
    }
}
