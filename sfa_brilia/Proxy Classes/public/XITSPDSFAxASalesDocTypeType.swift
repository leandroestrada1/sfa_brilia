// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesDocTypeType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesDocumentType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType.property(withName: "SalesDocumentType")

    private static var salesDocumentTypeName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType.property(withName: "SalesDocumentTypeName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesDocTypeType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesDocTypeType>())
    }

    open func copy() -> XITSPDSFAxASalesDocTypeType {
        return CastRequired<XITSPDSFAxASalesDocTypeType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesDocumentType: String?) -> EntityKey {
        return EntityKey().with(name: "SalesDocumentType", value: StringValue.of(optional: salesDocumentType))
    }

    open var old: XITSPDSFAxASalesDocTypeType {
        return CastRequired<XITSPDSFAxASalesDocTypeType>.from(self.oldEntity)
    }

    open class var salesDocumentType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesDocTypeType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesDocTypeType.self) }
            do {
                return XITSPDSFAxASalesDocTypeType.salesDocumentType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesDocTypeType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesDocTypeType.self) }
            do {
                XITSPDSFAxASalesDocTypeType.salesDocumentType_ = value
            }
        }
    }

    open var salesDocumentType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesDocTypeType.salesDocumentType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesDocTypeType.salesDocumentType, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentTypeName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesDocTypeType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesDocTypeType.self) }
            do {
                return XITSPDSFAxASalesDocTypeType.salesDocumentTypeName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesDocTypeType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesDocTypeType.self) }
            do {
                XITSPDSFAxASalesDocTypeType.salesDocumentTypeName_ = value
            }
        }
    }

    open var salesDocumentTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesDocTypeType.salesDocumentTypeName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesDocTypeType.salesDocumentTypeName, to: StringValue.of(optional: value))
        }
    }
}
