// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACenterSupplierType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "Plant")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "SalesOrganization")

    private static var plantName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "PlantName")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType.property(withName: "Region")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACenterSupplierType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACenterSupplierType>())
    }

    open func copy() -> XITSPDSFAxACenterSupplierType {
        return CastRequired<XITSPDSFAxACenterSupplierType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(plant: String?, salesOrganization: String?) -> EntityKey {
        return EntityKey().with(name: "Plant", value: StringValue.of(optional: plant)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization))
    }

    open var old: XITSPDSFAxACenterSupplierType {
        return CastRequired<XITSPDSFAxACenterSupplierType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                return XITSPDSFAxACenterSupplierType.plant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                XITSPDSFAxACenterSupplierType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACenterSupplierType.plant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACenterSupplierType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var plantName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                return XITSPDSFAxACenterSupplierType.plantName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                XITSPDSFAxACenterSupplierType.plantName_ = value
            }
        }
    }

    open var plantName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACenterSupplierType.plantName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACenterSupplierType.plantName, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                return XITSPDSFAxACenterSupplierType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                XITSPDSFAxACenterSupplierType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACenterSupplierType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACenterSupplierType.region, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                return XITSPDSFAxACenterSupplierType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACenterSupplierType.self)
            defer { objc_sync_exit(XITSPDSFAxACenterSupplierType.self) }
            do {
                XITSPDSFAxACenterSupplierType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACenterSupplierType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACenterSupplierType.salesOrganization, to: StringValue.of(optional: value))
        }
    }
}
