// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAConditionScaleType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionType")

    private static var accessSequence_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "AccessSequence")

    private static var conditionRecord_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionRecord")

    private static var conditionSequentialNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionSequentialNumber")

    private static var conditionScaleLine_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionScaleLine")

    private static var conditionScaleQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionScaleQuantity")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionRateValue")

    private static var conditionTable_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "ConditionTable")

    private static var tableName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "TableName")

    private static var hasValidity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType.property(withName: "HasValidity")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType)
    }

    open class var accessSequence: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.accessSequence_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.accessSequence_ = value
            }
        }
    }

    open var accessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.accessSequence))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.accessSequence, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAConditionScaleType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAConditionScaleType>())
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionRecord: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionRecord_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionRecord_ = value
            }
        }
    }

    open var conditionRecord: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionRecord))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionRecord, to: StringValue.of(optional: value))
        }
    }

    open class var conditionScaleLine: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionScaleLine_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionScaleLine_ = value
            }
        }
    }

    open var conditionScaleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionScaleLine))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionScaleLine, to: StringValue.of(optional: value))
        }
    }

    open class var conditionScaleQuantity: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionScaleQuantity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionScaleQuantity_ = value
            }
        }
    }

    open var conditionScaleQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionScaleQuantity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionScaleQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionSequentialNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionSequentialNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionSequentialNumber_ = value
            }
        }
    }

    open var conditionSequentialNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionSequentialNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionSequentialNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionTable: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionTable_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionTable_ = value
            }
        }
    }

    open var conditionTable: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionTable))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionTable, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAConditionScaleType {
        return CastRequired<XITSPDSFAxAConditionScaleType>.from(self.copyEntity())
    }

    open class var hasValidity: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.hasValidity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.hasValidity_ = value
            }
        }
    }

    open var hasValidity: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.hasValidity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.hasValidity, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionSequentialNumber: String?, conditionScaleLine: String?) -> EntityKey {
        return EntityKey().with(name: "ConditionRecord", value: StringValue.of(optional: conditionRecord)).with(name: "ConditionType", value: StringValue.of(optional: conditionType)).with(name: "AccessSequence", value: StringValue.of(optional: accessSequence)).with(name: "ConditionSequentialNumber", value: StringValue.of(optional: conditionSequentialNumber)).with(name: "ConditionScaleLine", value: StringValue.of(optional: conditionScaleLine))
    }

    open var old: XITSPDSFAxAConditionScaleType {
        return CastRequired<XITSPDSFAxAConditionScaleType>.from(self.oldEntity)
    }

    open class var tableName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                return XITSPDSFAxAConditionScaleType.tableName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionScaleType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionScaleType.self) }
            do {
                XITSPDSFAxAConditionScaleType.tableName_ = value
            }
        }
    }

    open var tableName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionScaleType.tableName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionScaleType.tableName, to: StringValue.of(optional: value))
        }
    }
}
