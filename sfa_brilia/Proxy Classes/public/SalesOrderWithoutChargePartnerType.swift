// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargePartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var contactPerson_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Personnel")

    private static var salesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "SalesOrderWithoutCharge")

    private static var supplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "Supplier")

    private static var toXITSPDSFAxASalesOrderWithoutChargePartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargePartner")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargePartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargePartnerType>())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargePartnerType {
        return CastRequired<SalesOrderWithoutChargePartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(partnerFunction: String?, salesOrderWithoutCharge: String?) -> EntityKey {
        return EntityKey().with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction)).with(name: "SalesOrderWithoutCharge", value: StringValue.of(optional: salesOrderWithoutCharge))
    }

    open var old: SalesOrderWithoutChargePartnerType {
        return CastRequired<SalesOrderWithoutChargePartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge_ = value
            }
        }
    }

    open var salesOrderWithoutCharge: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.salesOrderWithoutCharge, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargePartner: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                return SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePartnerType.self) }
            do {
                SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargePartner: SalesOrderWithoutChargeType? {
        get {
            return CastOptional<SalesOrderWithoutChargeType>.from(self.optionalValue(for: SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePartnerType.toXITSPDSFAxASalesOrderWithoutChargePartner, to: value)
        }
    }
}
