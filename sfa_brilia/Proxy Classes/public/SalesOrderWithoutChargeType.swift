// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargeType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var bindingPeriodValidityEndDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "BindingPeriodValidityEndDate")

    private static var bindingPeriodValidityStartDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "BindingPeriodValidityStartDate")

    private static var completeDeliveryIsDefined_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CompleteDeliveryIsDefined")

    private static var createdByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CreationDate")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPaymentTerms")

    private static var customerPurchaseOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPurchaseOrderDate")

    private static var customerPurchaseOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "CustomerPurchaseOrderType")

    private static var deliveryBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "DeliveryBlockReason")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "DistributionChannel")

    private static var expectedOrderNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ExpectedOrderNetAmount")

    private static var hdrOrderProbabilityInPercent_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "HdrOrderProbabilityInPercent")

    private static var headerBillingBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "HeaderBillingBlockReason")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsClassification")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsLocation2")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsTransferLocation")

    private static var incotermsVersion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "IncotermsVersion")

    private static var lastChangeDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "LastChangeDate")

    private static var lastChangeDateTime_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "LastChangeDateTime")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OrganizationDivision")

    private static var overallSDDocumentRejectionSts_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OverallSDDocumentRejectionSts")

    private static var overallSDProcessStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "OverallSDProcessStatus")

    private static var paymentMethod_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PaymentMethod")

    private static var pricingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PricingDate")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "PurchaseOrderByCustomer")

    private static var requestedDeliveryDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "RequestedDeliveryDate")

    private static var sdDocumentReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SDDocumentReason")

    private static var salesDistrict_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesDistrict")

    private static var salesGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesGroup")

    private static var salesOffice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOffice")

    private static var salesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrderWithoutCharge")

    private static var salesOrderWithoutChargeType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrderWithoutChargeType")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesOrganization")

    private static var salesQuotationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SalesQuotationDate")

    private static var shippingCondition_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ShippingCondition")

    private static var shippingType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "ShippingType")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "SoldToParty")

    private static var totalCreditCheckStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TotalCreditCheckStatus")

    private static var totalNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TotalNetAmount")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "TransactionCurrency")

    private static var xITSPDSFAxMobileIDSDH_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "xITSPDSFAxMobileID_SDH")

    private static var toXITSPDSFAxASalesOrderWithoutChargeItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItem")

    private static var toXITSPDSFAxASalesOrderWithoutChargePartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargePartner")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargeType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargeType>())
    }

    open class var bindingPeriodValidityEndDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.bindingPeriodValidityEndDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.bindingPeriodValidityEndDate_ = value
            }
        }
    }

    open var bindingPeriodValidityEndDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.bindingPeriodValidityEndDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.bindingPeriodValidityEndDate, to: value)
        }
    }

    open class var bindingPeriodValidityStartDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.bindingPeriodValidityStartDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.bindingPeriodValidityStartDate_ = value
            }
        }
    }

    open var bindingPeriodValidityStartDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.bindingPeriodValidityStartDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.bindingPeriodValidityStartDate, to: value)
        }
    }

    open class var completeDeliveryIsDefined: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.completeDeliveryIsDefined_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.completeDeliveryIsDefined_ = value
            }
        }
    }

    open var completeDeliveryIsDefined: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.completeDeliveryIsDefined))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.completeDeliveryIsDefined, to: BooleanValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargeType {
        return CastRequired<SalesOrderWithoutChargeType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.creationDate, to: value)
        }
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var customerPurchaseOrderDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.customerPurchaseOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.customerPurchaseOrderDate_ = value
            }
        }
    }

    open var customerPurchaseOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.customerPurchaseOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.customerPurchaseOrderDate, to: value)
        }
    }

    open class var customerPurchaseOrderType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.customerPurchaseOrderType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.customerPurchaseOrderType_ = value
            }
        }
    }

    open var customerPurchaseOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.customerPurchaseOrderType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.customerPurchaseOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryBlockReason: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.deliveryBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.deliveryBlockReason_ = value
            }
        }
    }

    open var deliveryBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.deliveryBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.deliveryBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var expectedOrderNetAmount: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.expectedOrderNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.expectedOrderNetAmount_ = value
            }
        }
    }

    open var expectedOrderNetAmount: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.expectedOrderNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.expectedOrderNetAmount, to: IntegerValue.of(optional: value))
        }
    }

    open class var hdrOrderProbabilityInPercent: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent_ = value
            }
        }
    }

    open var hdrOrderProbabilityInPercent: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.hdrOrderProbabilityInPercent, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillingBlockReason: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.headerBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.headerBillingBlockReason_ = value
            }
        }
    }

    open var headerBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.headerBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.headerBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsVersion: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.incotermsVersion_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.incotermsVersion_ = value
            }
        }
    }

    open var incotermsVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.incotermsVersion))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.incotermsVersion, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrderWithoutCharge: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrderWithoutCharge", value: StringValue.of(optional: salesOrderWithoutCharge))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.lastChangeDate, to: value)
        }
    }

    open class var lastChangeDateTime: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.lastChangeDateTime_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.lastChangeDateTime_ = value
            }
        }
    }

    open var lastChangeDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.lastChangeDateTime))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.lastChangeDateTime, to: value)
        }
    }

    open var old: SalesOrderWithoutChargeType {
        return CastRequired<SalesOrderWithoutChargeType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDDocumentRejectionSts: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.overallSDDocumentRejectionSts_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.overallSDDocumentRejectionSts_ = value
            }
        }
    }

    open var overallSDDocumentRejectionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.overallSDDocumentRejectionSts))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.overallSDDocumentRejectionSts, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatus: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.overallSDProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.overallSDProcessStatus_ = value
            }
        }
    }

    open var overallSDProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.overallSDProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.overallSDProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var paymentMethod: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.paymentMethod_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.paymentMethod_ = value
            }
        }
    }

    open var paymentMethod: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.paymentMethod))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.paymentMethod, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.pricingDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.pricingDate_ = value
            }
        }
    }

    open var pricingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.pricingDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.pricingDate, to: value)
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var requestedDeliveryDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.requestedDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.requestedDeliveryDate_ = value
            }
        }
    }

    open var requestedDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.requestedDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.requestedDeliveryDate, to: value)
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesGroup: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesGroup_ = value
            }
        }
    }

    open var salesGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesGroup, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesOrderWithoutCharge_ = value
            }
        }
    }

    open var salesOrderWithoutCharge: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesOrderWithoutCharge, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutChargeType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesOrderWithoutChargeType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesOrderWithoutChargeType_ = value
            }
        }
    }

    open var salesOrderWithoutChargeType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesOrderWithoutChargeType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesOrderWithoutChargeType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotationDate: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.salesQuotationDate_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.salesQuotationDate_ = value
            }
        }
    }

    open var salesQuotationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SalesOrderWithoutChargeType.salesQuotationDate))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.salesQuotationDate, to: value)
        }
    }

    open class var sdDocumentReason: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.sdDocumentReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.sdDocumentReason_ = value
            }
        }
    }

    open var sdDocumentReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.sdDocumentReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.sdDocumentReason, to: StringValue.of(optional: value))
        }
    }

    open class var shippingCondition: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.shippingCondition_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.shippingCondition_ = value
            }
        }
    }

    open var shippingCondition: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.shippingCondition))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.shippingCondition, to: StringValue.of(optional: value))
        }
    }

    open class var shippingType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.shippingType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.shippingType_ = value
            }
        }
    }

    open var shippingType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.shippingType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.shippingType, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargeItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargeItem: Array<SalesOrderWithoutChargeItemType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem)).toArray(), Array<SalesOrderWithoutChargeItemType>())
        }
        set(value) {
            SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargeItem.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargePartner: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargePartner: Array<SalesOrderWithoutChargePartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner)).toArray(), Array<SalesOrderWithoutChargePartnerType>())
        }
        set(value) {
            SalesOrderWithoutChargeType.toXITSPDSFAxASalesOrderWithoutChargePartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var totalCreditCheckStatus: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.totalCreditCheckStatus_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.totalCreditCheckStatus_ = value
            }
        }
    }

    open var totalCreditCheckStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.totalCreditCheckStatus))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.totalCreditCheckStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalNetAmount: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.totalNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.totalNetAmount_ = value
            }
        }
    }

    open var totalNetAmount: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.totalNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.totalNetAmount, to: IntegerValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var xITSPDSFAxMobileIDSDH: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                return SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeType.self) }
            do {
                SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH_ = value
            }
        }
    }

    open var xITSPDSFAxMobileIDSDH: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeType.xITSPDSFAxMobileIDSDH, to: StringValue.of(optional: value))
        }
    }
}
