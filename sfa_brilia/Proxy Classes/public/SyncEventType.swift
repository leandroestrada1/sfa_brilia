// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SyncEventType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var eventDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventDate")

    private static var eventIdentifier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventIdentifier")

    private static var eventMsg_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventMsg")

    private static var eventObject_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "EventObject")

    private static var id_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "Id")

    private static var mobileID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "MobileId")

    private static var operationStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "OperationStatusDesc")

    private static var operationStatusID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "OperationStatusId")

    private static var toSyncOperationStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType.property(withName: "to_SyncOperationStatus")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType)
    }

    open class func array(from: EntityValueList) -> Array<SyncEventType> {
        return ArrayConverter.convert(from.toArray(), Array<SyncEventType>())
    }

    open func copy() -> SyncEventType {
        return CastRequired<SyncEventType>.from(self.copyEntity())
    }

    open class var eventDate: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.eventDate_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.eventDate_ = value
            }
        }
    }

    open var eventDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: SyncEventType.eventDate))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.eventDate, to: value)
        }
    }

    open class var eventIdentifier: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.eventIdentifier_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.eventIdentifier_ = value
            }
        }
    }

    open var eventIdentifier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncEventType.eventIdentifier))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.eventIdentifier, to: StringValue.of(optional: value))
        }
    }

    open class var eventMsg: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.eventMsg_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.eventMsg_ = value
            }
        }
    }

    open var eventMsg: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncEventType.eventMsg))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.eventMsg, to: StringValue.of(optional: value))
        }
    }

    open class var eventObject: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.eventObject_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.eventObject_ = value
            }
        }
    }

    open var eventObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncEventType.eventObject))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.eventObject, to: StringValue.of(optional: value))
        }
    }

    open class var id: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.id_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.id_ = value
            }
        }
    }

    open var id: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: SyncEventType.id))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.id, to: LongValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(id: Int64?) -> EntityKey {
        return EntityKey().with(name: "Id", value: LongValue.of(optional: id))
    }

    open class var mobileID: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.mobileID_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.mobileID_ = value
            }
        }
    }

    open var mobileID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncEventType.mobileID))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.mobileID, to: StringValue.of(optional: value))
        }
    }

    open var old: SyncEventType {
        return CastRequired<SyncEventType>.from(self.oldEntity)
    }

    open class var operationStatusDesc: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.operationStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.operationStatusDesc_ = value
            }
        }
    }

    open var operationStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncEventType.operationStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.operationStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var operationStatusID: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.operationStatusID_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.operationStatusID_ = value
            }
        }
    }

    open var operationStatusID: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: SyncEventType.operationStatusID))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.operationStatusID, to: IntValue.of(optional: value))
        }
    }

    open class var toSyncOperationStatus: Property {
        get {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                return SyncEventType.toSyncOperationStatus_
            }
        }
        set(value) {
            objc_sync_enter(SyncEventType.self)
            defer { objc_sync_exit(SyncEventType.self) }
            do {
                SyncEventType.toSyncOperationStatus_ = value
            }
        }
    }

    open var toSyncOperationStatus: SyncOperationStatusType? {
        get {
            return CastOptional<SyncOperationStatusType>.from(self.optionalValue(for: SyncEventType.toSyncOperationStatus))
        }
        set(value) {
            self.setOptionalValue(for: SyncEventType.toSyncOperationStatus, to: value)
        }
    }
}
