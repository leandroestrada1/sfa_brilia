// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPricelistsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var priceListType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType.property(withName: "PriceListType")

    private static var priceListTypeName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType.property(withName: "PriceListTypeName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPricelistsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPricelistsType>())
    }

    open func copy() -> XITSPDSFAxAPricelistsType {
        return CastRequired<XITSPDSFAxAPricelistsType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(priceListType: String?) -> EntityKey {
        return EntityKey().with(name: "PriceListType", value: StringValue.of(optional: priceListType))
    }

    open var old: XITSPDSFAxAPricelistsType {
        return CastRequired<XITSPDSFAxAPricelistsType>.from(self.oldEntity)
    }

    open class var priceListType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPricelistsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPricelistsType.self) }
            do {
                return XITSPDSFAxAPricelistsType.priceListType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPricelistsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPricelistsType.self) }
            do {
                XITSPDSFAxAPricelistsType.priceListType_ = value
            }
        }
    }

    open var priceListType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPricelistsType.priceListType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPricelistsType.priceListType, to: StringValue.of(optional: value))
        }
    }

    open class var priceListTypeName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPricelistsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPricelistsType.self) }
            do {
                return XITSPDSFAxAPricelistsType.priceListTypeName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPricelistsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPricelistsType.self) }
            do {
                XITSPDSFAxAPricelistsType.priceListTypeName_ = value
            }
        }
    }

    open var priceListTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPricelistsType.priceListTypeName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPricelistsType.priceListTypeName, to: StringValue.of(optional: value))
        }
    }
}
