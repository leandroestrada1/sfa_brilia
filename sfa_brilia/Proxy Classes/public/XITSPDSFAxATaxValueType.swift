// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxATaxValueType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var taxType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "TaxType")

    private static var groupPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "GroupPriority")

    private static var shipfrom_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SHIPFROM")

    private static var shipto_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SHIPTO")

    private static var matnr_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "MATNR")

    private static var nbmcode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "NBMCODE")

    private static var stgrp_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "STGRP")

    private static var subkey01_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY01")

    private static var subkey02_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY02")

    private static var subkey03_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "SUBKEY03")

    private static var rate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "RATE")

    private static var base_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "BASE")

    private static var field01_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field01")

    private static var field02_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field02")

    private static var field03_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field03")

    private static var field04_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field04")

    private static var field05_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field05")

    private static var field06_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field06")

    private static var field07_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field07")

    private static var field08_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field08")

    private static var field09_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field09")

    private static var field10_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field10")

    private static var field11_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field11")

    private static var field12_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field12")

    private static var field13_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field13")

    private static var field14_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field14")

    private static var field15_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field15")

    private static var field16_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field16")

    private static var field17_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field17")

    private static var field18_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field18")

    private static var field19_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field19")

    private static var field20_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType.property(withName: "Field20")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxATaxValueType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxATaxValueType>())
    }

    open class var base: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.base_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.base_ = value
            }
        }
    }

    open var base: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.base))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.base, to: DecimalValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxATaxValueType {
        return CastRequired<XITSPDSFAxATaxValueType>.from(self.copyEntity())
    }

    open class var field01: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field01_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field01_ = value
            }
        }
    }

    open var field01: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field01))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field01, to: StringValue.of(optional: value))
        }
    }

    open class var field02: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field02_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field02_ = value
            }
        }
    }

    open var field02: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field02))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field02, to: StringValue.of(optional: value))
        }
    }

    open class var field03: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field03_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field03_ = value
            }
        }
    }

    open var field03: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field03))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field03, to: StringValue.of(optional: value))
        }
    }

    open class var field04: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field04_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field04_ = value
            }
        }
    }

    open var field04: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field04))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field04, to: StringValue.of(optional: value))
        }
    }

    open class var field05: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field05_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field05_ = value
            }
        }
    }

    open var field05: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field05))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field05, to: StringValue.of(optional: value))
        }
    }

    open class var field06: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field06_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field06_ = value
            }
        }
    }

    open var field06: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field06))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field06, to: StringValue.of(optional: value))
        }
    }

    open class var field07: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field07_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field07_ = value
            }
        }
    }

    open var field07: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field07))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field07, to: StringValue.of(optional: value))
        }
    }

    open class var field08: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field08_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field08_ = value
            }
        }
    }

    open var field08: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field08))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field08, to: StringValue.of(optional: value))
        }
    }

    open class var field09: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field09_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field09_ = value
            }
        }
    }

    open var field09: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field09))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field09, to: StringValue.of(optional: value))
        }
    }

    open class var field10: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field10_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field10_ = value
            }
        }
    }

    open var field10: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field10))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field10, to: StringValue.of(optional: value))
        }
    }

    open class var field11: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field11_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field11_ = value
            }
        }
    }

    open var field11: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field11))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field11, to: StringValue.of(optional: value))
        }
    }

    open class var field12: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field12_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field12_ = value
            }
        }
    }

    open var field12: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field12))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field12, to: StringValue.of(optional: value))
        }
    }

    open class var field13: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field13_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field13_ = value
            }
        }
    }

    open var field13: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field13))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field13, to: StringValue.of(optional: value))
        }
    }

    open class var field14: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field14_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field14_ = value
            }
        }
    }

    open var field14: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field14))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field14, to: StringValue.of(optional: value))
        }
    }

    open class var field15: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field15_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field15_ = value
            }
        }
    }

    open var field15: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field15))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field15, to: StringValue.of(optional: value))
        }
    }

    open class var field16: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field16_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field16_ = value
            }
        }
    }

    open var field16: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field16))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field16, to: StringValue.of(optional: value))
        }
    }

    open class var field17: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field17_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field17_ = value
            }
        }
    }

    open var field17: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field17))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field17, to: StringValue.of(optional: value))
        }
    }

    open class var field18: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field18_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field18_ = value
            }
        }
    }

    open var field18: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field18))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field18, to: StringValue.of(optional: value))
        }
    }

    open class var field19: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field19_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field19_ = value
            }
        }
    }

    open var field19: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field19))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field19, to: StringValue.of(optional: value))
        }
    }

    open class var field20: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.field20_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.field20_ = value
            }
        }
    }

    open var field20: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.field20))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.field20, to: StringValue.of(optional: value))
        }
    }

    open class var groupPriority: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.groupPriority_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.groupPriority_ = value
            }
        }
    }

    open var groupPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.groupPriority))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.groupPriority, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(taxType: String?, groupPriority: String?, shipfrom: String?, shipto: String?, matnr: String?, nbmcode: String?, stgrp: String?, subkey01: String?, subkey02: String?, subkey03: String?) -> EntityKey {
        return EntityKey().with(name: "TaxType", value: StringValue.of(optional: taxType)).with(name: "GroupPriority", value: StringValue.of(optional: groupPriority)).with(name: "SHIPFROM", value: StringValue.of(optional: shipfrom)).with(name: "SHIPTO", value: StringValue.of(optional: shipto)).with(name: "MATNR", value: StringValue.of(optional: matnr)).with(name: "NBMCODE", value: StringValue.of(optional: nbmcode)).with(name: "STGRP", value: StringValue.of(optional: stgrp)).with(name: "SUBKEY01", value: StringValue.of(optional: subkey01)).with(name: "SUBKEY02", value: StringValue.of(optional: subkey02)).with(name: "SUBKEY03", value: StringValue.of(optional: subkey03))
    }

    open class var matnr: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.matnr_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.matnr_ = value
            }
        }
    }

    open var matnr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.matnr))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.matnr, to: StringValue.of(optional: value))
        }
    }

    open class var nbmcode: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.nbmcode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.nbmcode_ = value
            }
        }
    }

    open var nbmcode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.nbmcode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.nbmcode, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxATaxValueType {
        return CastRequired<XITSPDSFAxATaxValueType>.from(self.oldEntity)
    }

    open class var rate: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.rate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.rate_ = value
            }
        }
    }

    open var rate: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.rate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.rate, to: DecimalValue.of(optional: value))
        }
    }

    open class var shipfrom: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.shipfrom_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.shipfrom_ = value
            }
        }
    }

    open var shipfrom: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.shipfrom))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.shipfrom, to: StringValue.of(optional: value))
        }
    }

    open class var shipto: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.shipto_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.shipto_ = value
            }
        }
    }

    open var shipto: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.shipto))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.shipto, to: StringValue.of(optional: value))
        }
    }

    open class var stgrp: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.stgrp_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.stgrp_ = value
            }
        }
    }

    open var stgrp: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.stgrp))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.stgrp, to: StringValue.of(optional: value))
        }
    }

    open class var subkey01: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.subkey01_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.subkey01_ = value
            }
        }
    }

    open var subkey01: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.subkey01))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.subkey01, to: StringValue.of(optional: value))
        }
    }

    open class var subkey02: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.subkey02_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.subkey02_ = value
            }
        }
    }

    open var subkey02: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.subkey02))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.subkey02, to: StringValue.of(optional: value))
        }
    }

    open class var subkey03: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.subkey03_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.subkey03_ = value
            }
        }
    }

    open var subkey03: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.subkey03))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.subkey03, to: StringValue.of(optional: value))
        }
    }

    open class var taxType: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                return XITSPDSFAxATaxValueType.taxType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxValueType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxValueType.self) }
            do {
                XITSPDSFAxATaxValueType.taxType_ = value
            }
        }
    }

    open var taxType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxValueType.taxType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxValueType.taxType, to: StringValue.of(optional: value))
        }
    }
}
