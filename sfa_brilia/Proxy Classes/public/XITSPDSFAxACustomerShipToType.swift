// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerShipToType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Customer")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "BPCustomerNumber")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Division")

    private static var partnerCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PartnerCounter")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PartnerFunction")

    private static var customerShipNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CustomerShipNumber")

    private static var customerShipName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CustomerShipName")

    private static var taxNumber1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "TaxNumber1")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Region")

    private static var cityName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "CityName")

    private static var streetName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "StreetName")

    private static var country_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "Country")

    private static var postalCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType.property(withName: "PostalCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerShipToType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerShipToType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.cityName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerShipToType {
        return CastRequired<XITSPDSFAxACustomerShipToType>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.country_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.country))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.country, to: StringValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerShipName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.customerShipName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.customerShipName_ = value
            }
        }
    }

    open var customerShipName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.customerShipName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.customerShipName, to: StringValue.of(optional: value))
        }
    }

    open class var customerShipNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.customerShipNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.customerShipNumber_ = value
            }
        }
    }

    open var customerShipNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.customerShipNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.customerShipNumber, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.division, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, bpCustomerNumber: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "PartnerCounter", value: StringValue.of(optional: partnerCounter)).with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction))
    }

    open var old: XITSPDSFAxACustomerShipToType {
        return CastRequired<XITSPDSFAxACustomerShipToType>.from(self.oldEntity)
    }

    open class var partnerCounter: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.partnerCounter_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.partnerCounter_ = value
            }
        }
    }

    open var partnerCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.partnerCounter))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.partnerCounter, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.region, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var taxNumber1: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                return XITSPDSFAxACustomerShipToType.taxNumber1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerShipToType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerShipToType.self) }
            do {
                XITSPDSFAxACustomerShipToType.taxNumber1_ = value
            }
        }
    }

    open var taxNumber1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerShipToType.taxNumber1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerShipToType.taxNumber1, to: StringValue.of(optional: value))
        }
    }
}
