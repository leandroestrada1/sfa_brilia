// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class PricingValue: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var accessSequence_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue.property(withName: "AccessSequence")

    private static var price_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue.property(withName: "Price")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue)
    }

    open class var accessSequence: Property {
        get {
            objc_sync_enter(PricingValue.self)
            defer { objc_sync_exit(PricingValue.self) }
            do {
                return PricingValue.accessSequence_
            }
        }
        set(value) {
            objc_sync_enter(PricingValue.self)
            defer { objc_sync_exit(PricingValue.self) }
            do {
                PricingValue.accessSequence_ = value
            }
        }
    }

    open var accessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PricingValue.accessSequence))
        }
        set(value) {
            self.setOptionalValue(for: PricingValue.accessSequence, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> PricingValue {
        return CastRequired<PricingValue>.from(self.copyComplex())
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: PricingValue {
        return CastRequired<PricingValue>.from(self.oldComplex)
    }

    open class var price: Property {
        get {
            objc_sync_enter(PricingValue.self)
            defer { objc_sync_exit(PricingValue.self) }
            do {
                return PricingValue.price_
            }
        }
        set(value) {
            objc_sync_enter(PricingValue.self)
            defer { objc_sync_exit(PricingValue.self) }
            do {
                PricingValue.price_ = value
            }
        }
    }

    open var price: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: PricingValue.price))
        }
        set(value) {
            self.setOptionalValue(for: PricingValue.price, to: IntegerValue.of(optional: value))
        }
    }
}
