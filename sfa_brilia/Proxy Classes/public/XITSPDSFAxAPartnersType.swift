// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPartnersType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BPCustomerNumber")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "SalesOrganization")

    private static var salesOrganizationName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "SalesOrganizationName")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "PartnerFunction")

    private static var businessPartnerAddressID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BusinessPartnerAddressID")

    private static var businessPartnerFullName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BusinessPartnerFullName")

    private static var addressPhoneNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "AddressPhoneNumber")

    private static var mobilePhoneNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "MobilePhoneNumber")

    private static var emailAddress_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "EmailAddress")

    private static var bpTaxNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "BPTaxNumber")

    private static var cityName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "CityName")

    private static var postalCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "PostalCode")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "Region")

    private static var organizationBPName1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "OrganizationBPName1")

    private static var companyCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "CompanyCode")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "IsFinal")

    private static var toXITSPDSFAxAHierarchy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "to_xITSPDSFAxA_Hierarchy")

    private static var toXITSPDSFAxAPartnerLastSales_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType.property(withName: "to_xITSPDSFAxA_PartnerLastSales")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType)
    }

    open class var addressPhoneNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.addressPhoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.addressPhoneNumber_ = value
            }
        }
    }

    open var addressPhoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.addressPhoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.addressPhoneNumber, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPartnersType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPartnersType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var bpTaxNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.bpTaxNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.bpTaxNumber_ = value
            }
        }
    }

    open var bpTaxNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.bpTaxNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.bpTaxNumber, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerAddressID: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.businessPartnerAddressID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.businessPartnerAddressID_ = value
            }
        }
    }

    open var businessPartnerAddressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.businessPartnerAddressID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.businessPartnerAddressID, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerFullName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.businessPartnerFullName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.businessPartnerFullName_ = value
            }
        }
    }

    open var businessPartnerFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.businessPartnerFullName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.businessPartnerFullName, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.cityName, to: StringValue.of(optional: value))
        }
    }

    open class var companyCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.companyCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.companyCode_ = value
            }
        }
    }

    open var companyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.companyCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.companyCode, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAPartnersType {
        return CastRequired<XITSPDSFAxAPartnersType>.from(self.copyEntity())
    }

    open class var emailAddress: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.emailAddress_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.emailAddress_ = value
            }
        }
    }

    open var emailAddress: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.emailAddress))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.emailAddress, to: StringValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(bpCustomerNumber: String?, salesOrganization: String?) -> EntityKey {
        return EntityKey().with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization))
    }

    open class var mobilePhoneNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.mobilePhoneNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.mobilePhoneNumber_ = value
            }
        }
    }

    open var mobilePhoneNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.mobilePhoneNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.mobilePhoneNumber, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAPartnersType {
        return CastRequired<XITSPDSFAxAPartnersType>.from(self.oldEntity)
    }

    open class var organizationBPName1: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.organizationBPName1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.organizationBPName1_ = value
            }
        }
    }

    open var organizationBPName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.organizationBPName1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.organizationBPName1, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.region, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganizationName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.salesOrganizationName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.salesOrganizationName_ = value
            }
        }
    }

    open var salesOrganizationName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnersType.salesOrganizationName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnersType.salesOrganizationName, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxAHierarchy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy_ = value
            }
        }
    }

    open var toXITSPDSFAxAHierarchy: Array<XITSPDSFAxAHierarchyType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy)).toArray(), Array<XITSPDSFAxAHierarchyType>())
        }
        set(value) {
            XITSPDSFAxAPartnersType.toXITSPDSFAxAHierarchy.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxAPartnerLastSales: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                return XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnersType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnersType.self) }
            do {
                XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales_ = value
            }
        }
    }

    open var toXITSPDSFAxAPartnerLastSales: Array<XITSPDSFAxAPartnerLastSalesType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales)).toArray(), Array<XITSPDSFAxAPartnerLastSalesType>())
        }
        set(value) {
            XITSPDSFAxAPartnersType.toXITSPDSFAxAPartnerLastSales.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
