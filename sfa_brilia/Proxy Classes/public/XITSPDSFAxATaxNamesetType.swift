// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxATaxNamesetType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var taxType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "TaxType")

    private static var groupPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "GroupPriority")

    private static var sapField_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "SAP_Field")

    private static var oDataField_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "OData_Field")

    private static var entity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Entity")

    private static var atribute_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Atribute")

    private static var isOutPut_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType.property(withName: "Is_OutPut")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxATaxNamesetType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxATaxNamesetType>())
    }

    open class var atribute: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.atribute_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.atribute_ = value
            }
        }
    }

    open var atribute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.atribute))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.atribute, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxATaxNamesetType {
        return CastRequired<XITSPDSFAxATaxNamesetType>.from(self.copyEntity())
    }

    open class var entity: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.entity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.entity_ = value
            }
        }
    }

    open var entity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.entity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.entity, to: StringValue.of(optional: value))
        }
    }

    open class var groupPriority: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.groupPriority_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.groupPriority_ = value
            }
        }
    }

    open var groupPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.groupPriority))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.groupPriority, to: StringValue.of(optional: value))
        }
    }

    open class var isOutPut: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.isOutPut_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.isOutPut_ = value
            }
        }
    }

    open var isOutPut: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.isOutPut))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.isOutPut, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(taxType: String?, groupPriority: String?, sapField: String?) -> EntityKey {
        return EntityKey().with(name: "TaxType", value: StringValue.of(optional: taxType)).with(name: "GroupPriority", value: StringValue.of(optional: groupPriority)).with(name: "SAP_Field", value: StringValue.of(optional: sapField))
    }

    open class var oDataField: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.oDataField_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.oDataField_ = value
            }
        }
    }

    open var oDataField: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.oDataField))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.oDataField, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxATaxNamesetType {
        return CastRequired<XITSPDSFAxATaxNamesetType>.from(self.oldEntity)
    }

    open class var sapField: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.sapField_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.sapField_ = value
            }
        }
    }

    open var sapField: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.sapField))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.sapField, to: StringValue.of(optional: value))
        }
    }

    open class var taxType: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                return XITSPDSFAxATaxNamesetType.taxType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxNamesetType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxNamesetType.self) }
            do {
                XITSPDSFAxATaxNamesetType.taxType_ = value
            }
        }
    }

    open var taxType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxNamesetType.taxType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxNamesetType.taxType, to: StringValue.of(optional: value))
        }
    }
}
