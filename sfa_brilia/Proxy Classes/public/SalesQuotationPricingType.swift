// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationPricingType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionCurrency")

    private static var conditionQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionQuantity")

    private static var conditionQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionQuantityUnit")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionRateValue")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "ConditionType")

    private static var pricingProcedureCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "PricingProcedureCounter")

    private static var pricingProcedureStep_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "PricingProcedureStep")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "SalesQuotation")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "TransactionCurrency")

    private static var toXITSPDSFAxAToSalesQuotationPricing_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType.property(withName: "to_xITSPDSFAxA_to_SalesQuotationPricing")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType)
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationPricingType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationPricingType>())
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantity: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.conditionQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.conditionQuantity_ = value
            }
        }
    }

    open var conditionQuantity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.conditionQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.conditionQuantity, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantityUnit: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.conditionQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.conditionQuantityUnit_ = value
            }
        }
    }

    open var conditionQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.conditionQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.conditionQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.conditionRateValue, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesQuotationPricingType {
        return CastRequired<SalesQuotationPricingType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesQuotation: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation)).with(name: "PricingProcedureStep", value: StringValue.of(optional: pricingProcedureStep)).with(name: "PricingProcedureCounter", value: StringValue.of(optional: pricingProcedureCounter))
    }

    open var old: SalesQuotationPricingType {
        return CastRequired<SalesQuotationPricingType>.from(self.oldEntity)
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxAToSalesQuotationPricing: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing_ = value
            }
        }
    }

    open var toXITSPDSFAxAToSalesQuotationPricing: SalesQuotationItensType? {
        get {
            return CastOptional<SalesQuotationItensType>.from(self.optionalValue(for: SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.toXITSPDSFAxAToSalesQuotationPricing, to: value)
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                return SalesQuotationPricingType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationPricingType.self)
            defer { objc_sync_exit(SalesQuotationPricingType.self) }
            do {
                SalesQuotationPricingType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationPricingType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationPricingType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }
}
