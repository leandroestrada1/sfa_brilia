// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductHieLevel1Type: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var productHierarchyNode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type.property(withName: "ProductHierarchyNode")

    private static var productHierarchyNodeText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type.property(withName: "ProductHierarchyNodeText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductHieLevel1Type> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductHieLevel1Type>())
    }

    open func copy() -> XITSPDSFAxAProductHieLevel1Type {
        return CastRequired<XITSPDSFAxAProductHieLevel1Type>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(productHierarchyNode: String?) -> EntityKey {
        return EntityKey().with(name: "ProductHierarchyNode", value: StringValue.of(optional: productHierarchyNode))
    }

    open var old: XITSPDSFAxAProductHieLevel1Type {
        return CastRequired<XITSPDSFAxAProductHieLevel1Type>.from(self.oldEntity)
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel1Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel1Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel1Type.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel1Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel1Type.self) }
            do {
                XITSPDSFAxAProductHieLevel1Type.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel1Type.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel1Type.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNodeText: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel1Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel1Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel1Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel1Type.self) }
            do {
                XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText_ = value
            }
        }
    }

    open var productHierarchyNodeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel1Type.productHierarchyNodeText, to: StringValue.of(optional: value))
        }
    }
}
