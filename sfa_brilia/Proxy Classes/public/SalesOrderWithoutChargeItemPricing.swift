// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargeItemPricing: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionCurrency")

    private static var conditionQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionQuantity")

    private static var conditionQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionQuantityUnit")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionRateValue")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "ConditionType")

    private static var pricingProcedureCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "PricingProcedureCounter")

    private static var pricingProcedureStep_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "PricingProcedureStep")

    private static var salesBonification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "SalesBonification")

    private static var salesBonificationItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "SalesBonificationItem")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing.property(withName: "TransactionCurrency")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargeItemPricing> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargeItemPricing>())
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantity: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.conditionQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.conditionQuantity_ = value
            }
        }
    }

    open var conditionQuantity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.conditionQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.conditionQuantity, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantityUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.conditionQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.conditionQuantityUnit_ = value
            }
        }
    }

    open var conditionQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.conditionQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.conditionQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.conditionRateValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargeItemPricing {
        return CastRequired<SalesOrderWithoutChargeItemPricing>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesBonification: String?, salesBonificationItem: String?) -> EntityKey {
        return EntityKey().with(name: "SalesBonification", value: StringValue.of(optional: salesBonification)).with(name: "SalesBonificationItem", value: StringValue.of(optional: salesBonificationItem))
    }

    open var old: SalesOrderWithoutChargeItemPricing {
        return CastRequired<SalesOrderWithoutChargeItemPricing>.from(self.oldEntity)
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var salesBonification: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.salesBonification_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.salesBonification_ = value
            }
        }
    }

    open var salesBonification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.salesBonification))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.salesBonification, to: StringValue.of(optional: value))
        }
    }

    open class var salesBonificationItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.salesBonificationItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.salesBonificationItem_ = value
            }
        }
    }

    open var salesBonificationItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.salesBonificationItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.salesBonificationItem, to: StringValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                return SalesOrderWithoutChargeItemPricing.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPricing.self) }
            do {
                SalesOrderWithoutChargeItemPricing.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPricing.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPricing.transactionCurrency, to: StringValue.of(optional: value))
        }
    }
}
