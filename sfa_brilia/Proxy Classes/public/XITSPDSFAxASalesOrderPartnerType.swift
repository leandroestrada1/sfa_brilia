// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrderPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrder")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "OrganizationDivision")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "BPCustomerNumber")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "PartnerFunction")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Customer")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "CustomerName")

    private static var cityName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "CityName")

    private static var country_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Country")

    private static var district_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "District")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "Region")

    private static var streetName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "StreetName")

    private static var houseNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "HouseNumber")

    private static var postalCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "PostalCode")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "SalesOrderType")

    private static var toXITSPDSFAxASalesOrders_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType.property(withName: "to_xITSPDSFAxA_SalesOrders")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrderPartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrderPartnerType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.cityName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrderPartnerType {
        return CastRequired<XITSPDSFAxASalesOrderPartnerType>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.country_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.country))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.country, to: StringValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var district: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.district_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.district_ = value
            }
        }
    }

    open var district: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.district))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.district, to: StringValue.of(optional: value))
        }
    }

    open class var houseNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.houseNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.houseNumber_ = value
            }
        }
    }

    open var houseNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.houseNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.houseNumber, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, partnerFunction: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction))
    }

    open var old: XITSPDSFAxASalesOrderPartnerType {
        return CastRequired<XITSPDSFAxASalesOrderPartnerType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.region, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.streetName, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrders: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                return XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrderPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrderPartnerType.self) }
            do {
                XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrders: XITSPDSFAxASalesOrdersType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersType>.from(self.optionalValue(for: XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrderPartnerType.toXITSPDSFAxASalesOrders, to: value)
        }
    }
}
