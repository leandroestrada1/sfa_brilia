// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAMsgPrtnrType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var messageID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "MessageID")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "SalesOrganization")

    private static var partnerID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "PartnerID")

    private static var createdAt_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "CreatedAt")

    private static var createdBy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "CreatedBy")

    private static var lastChangedAt_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "LastChangedAt")

    private static var lastChangedBy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "LastChangedBy")

    private static var toXITSPDSFAxAMsg_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType.property(withName: "to_xITSPDSFAxA_Msg")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAMsgPrtnrType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAMsgPrtnrType>())
    }

    open func copy() -> XITSPDSFAxAMsgPrtnrType {
        return CastRequired<XITSPDSFAxAMsgPrtnrType>.from(self.copyEntity())
    }

    open class var createdAt: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.createdAt_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.createdAt_ = value
            }
        }
    }

    open var createdAt: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.createdAt))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.createdAt, to: value)
        }
    }

    open class var createdBy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.createdBy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.createdBy_ = value
            }
        }
    }

    open var createdBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.createdBy))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.createdBy, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(messageID: String?, salesOrganization: String?, partnerID: String?) -> EntityKey {
        return EntityKey().with(name: "MessageID", value: StringValue.of(optional: messageID)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "PartnerID", value: StringValue.of(optional: partnerID))
    }

    open class var lastChangedAt: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.lastChangedAt_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.lastChangedAt_ = value
            }
        }
    }

    open var lastChangedAt: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.lastChangedAt))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.lastChangedAt, to: value)
        }
    }

    open class var lastChangedBy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.lastChangedBy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.lastChangedBy_ = value
            }
        }
    }

    open var lastChangedBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.lastChangedBy))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.lastChangedBy, to: StringValue.of(optional: value))
        }
    }

    open class var messageID: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.messageID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.messageID_ = value
            }
        }
    }

    open var messageID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.messageID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.messageID, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAMsgPrtnrType {
        return CastRequired<XITSPDSFAxAMsgPrtnrType>.from(self.oldEntity)
    }

    open class var partnerID: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.partnerID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.partnerID_ = value
            }
        }
    }

    open var partnerID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.partnerID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.partnerID, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgPrtnrType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgPrtnrType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxAMsg: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                return XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgPrtnrType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgPrtnrType.self) }
            do {
                XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg_ = value
            }
        }
    }

    open var toXITSPDSFAxAMsg: Array<XITSPDSFAxAMsgPrtnrType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg)).toArray(), Array<XITSPDSFAxAMsgPrtnrType>())
        }
        set(value) {
            XITSPDSFAxAMsgPrtnrType.toXITSPDSFAxAMsg.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
