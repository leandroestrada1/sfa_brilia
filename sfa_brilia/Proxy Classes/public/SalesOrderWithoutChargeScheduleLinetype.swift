// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargeScheduleLinetype: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var confdOrderQtyByMatlAvailCheck_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "ConfdOrderQtyByMatlAvailCheck")

    private static var delivBlockReasonForSchedLine_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "DelivBlockReasonForSchedLine")

    private static var deliveredQtyInOrderQtyUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "DeliveredQtyInOrderQtyUnit")

    private static var openConfdDelivQtyInOrdQtyUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OpenConfdDelivQtyInOrdQtyUnit")

    private static var orderQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OrderQuantityUnit")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "OrganizationDivision")

    private static var salesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "SalesOrderWithoutCharge")

    private static var salesOrderWithoutChargeItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "SalesOrderWithoutChargeItem")

    private static var scheduleLine_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "ScheduleLine")

    private static var toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype.property(withName: "to_xITSPDSFAxA_to_SalesOrderWithoutChargeScheduleLine")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargeScheduleLinetype> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargeScheduleLinetype>())
    }

    open class var confdOrderQtyByMatlAvailCheck: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck_ = value
            }
        }
    }

    open var confdOrderQtyByMatlAvailCheck: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.confdOrderQtyByMatlAvailCheck, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargeScheduleLinetype {
        return CastRequired<SalesOrderWithoutChargeScheduleLinetype>.from(self.copyEntity())
    }

    open class var delivBlockReasonForSchedLine: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine_ = value
            }
        }
    }

    open var delivBlockReasonForSchedLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.delivBlockReasonForSchedLine, to: StringValue.of(optional: value))
        }
    }

    open class var deliveredQtyInOrderQtyUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit_ = value
            }
        }
    }

    open var deliveredQtyInOrderQtyUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.deliveredQtyInOrderQtyUnit, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, scheduleLine: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrderWithoutCharge", value: StringValue.of(optional: salesOrderWithoutCharge)).with(name: "SalesOrderWithoutChargeItem", value: StringValue.of(optional: salesOrderWithoutChargeItem)).with(name: "ScheduleLine", value: StringValue.of(optional: scheduleLine))
    }

    open var old: SalesOrderWithoutChargeScheduleLinetype {
        return CastRequired<SalesOrderWithoutChargeScheduleLinetype>.from(self.oldEntity)
    }

    open class var openConfdDelivQtyInOrdQtyUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit_ = value
            }
        }
    }

    open var openConfdDelivQtyInOrdQtyUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.openConfdDelivQtyInOrdQtyUnit, to: StringValue.of(optional: value))
        }
    }

    open class var orderQuantityUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit_ = value
            }
        }
    }

    open var orderQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.orderQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge_ = value
            }
        }
    }

    open var salesOrderWithoutCharge: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutCharge, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutChargeItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem_ = value
            }
        }
    }

    open var salesOrderWithoutChargeItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.salesOrderWithoutChargeItem, to: StringValue.of(optional: value))
        }
    }

    open class var scheduleLine: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.scheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.scheduleLine_ = value
            }
        }
    }

    open var scheduleLine: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.scheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.scheduleLine, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                return SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeScheduleLinetype.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeScheduleLinetype.self) }
            do {
                SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine_ = value
            }
        }
    }

    open var toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine: SalesOrderWithoutChargeItemType? {
        get {
            return CastOptional<SalesOrderWithoutChargeItemType>.from(self.optionalValue(for: SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeScheduleLinetype.toXITSPDSFAxAToSalesOrderWithoutChargeScheduleLine, to: value)
        }
    }
}
