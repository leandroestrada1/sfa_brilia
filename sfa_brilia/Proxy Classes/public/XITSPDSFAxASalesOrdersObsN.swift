// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdersObsN: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesorder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorder")

    private static var salesorganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorganization")

    private static var distributionchannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Distributionchannel")

    private static var organizationdivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Organizationdivision")

    private static var salesorderobsid_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobsid")

    private static var salesorderobstext_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobstext")

    private static var salesorderobsseq_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Salesorderobsseq")

    private static var bpcustomernumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "Bpcustomernumber")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "SoldToParty")

    private static var nSalesOrdersObs_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN.property(withName: "N_SalesOrdersObs")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdersObsN> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdersObsN>())
    }

    open class var bpcustomernumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.bpcustomernumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.bpcustomernumber_ = value
            }
        }
    }

    open var bpcustomernumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.bpcustomernumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.bpcustomernumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrdersObsN {
        return CastRequired<XITSPDSFAxASalesOrdersObsN>.from(self.copyEntity())
    }

    open class var distributionchannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.distributionchannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.distributionchannel_ = value
            }
        }
    }

    open var distributionchannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.distributionchannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.distributionchannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?) -> EntityKey {
        return EntityKey().with(name: "Salesorder", value: StringValue.of(optional: salesorder)).with(name: "Salesorganization", value: StringValue.of(optional: salesorganization)).with(name: "Distributionchannel", value: StringValue.of(optional: distributionchannel)).with(name: "Organizationdivision", value: StringValue.of(optional: organizationdivision)).with(name: "Salesorderobsid", value: StringValue.of(optional: salesorderobsid)).with(name: "Salesorderobsseq", value: StringValue.of(optional: salesorderobsseq)).with(name: "Bpcustomernumber", value: StringValue.of(optional: bpcustomernumber)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty))
    }

    open class var nSalesOrdersObs: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs_ = value
            }
        }
    }

    open var nSalesOrdersObs: Array<XITSPDSFAxASalesOrdersObsN> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs)).toArray(), Array<XITSPDSFAxASalesOrdersObsN>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersObsN.nSalesOrdersObs.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open var old: XITSPDSFAxASalesOrdersObsN {
        return CastRequired<XITSPDSFAxASalesOrdersObsN>.from(self.oldEntity)
    }

    open class var organizationdivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.organizationdivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.organizationdivision_ = value
            }
        }
    }

    open var organizationdivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.organizationdivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.organizationdivision, to: StringValue.of(optional: value))
        }
    }

    open class var salesorder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.salesorder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.salesorder_ = value
            }
        }
    }

    open var salesorder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorder, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobsid: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.salesorderobsid_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.salesorderobsid_ = value
            }
        }
    }

    open var salesorderobsid: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobsid))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobsid, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobsseq: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.salesorderobsseq_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.salesorderobsseq_ = value
            }
        }
    }

    open var salesorderobsseq: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobsseq))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobsseq, to: StringValue.of(optional: value))
        }
    }

    open class var salesorderobstext: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.salesorderobstext_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.salesorderobstext_ = value
            }
        }
    }

    open var salesorderobstext: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobstext))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorderobstext, to: StringValue.of(optional: value))
        }
    }

    open class var salesorganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.salesorganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.salesorganization_ = value
            }
        }
    }

    open var salesorganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.salesorganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                return XITSPDSFAxASalesOrdersObsN.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersObsN.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersObsN.self) }
            do {
                XITSPDSFAxASalesOrdersObsN.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersObsN.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersObsN.soldToParty, to: StringValue.of(optional: value))
        }
    }
}
