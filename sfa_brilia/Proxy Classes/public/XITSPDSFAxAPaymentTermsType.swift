// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPaymentTermsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var paymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType.property(withName: "PaymentTerms")

    private static var paymentTermsConditionDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType.property(withName: "PaymentTermsConditionDesc")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPaymentTermsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPaymentTermsType>())
    }

    open func copy() -> XITSPDSFAxAPaymentTermsType {
        return CastRequired<XITSPDSFAxAPaymentTermsType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(paymentTerms: String?) -> EntityKey {
        return EntityKey().with(name: "PaymentTerms", value: StringValue.of(optional: paymentTerms))
    }

    open var old: XITSPDSFAxAPaymentTermsType {
        return CastRequired<XITSPDSFAxAPaymentTermsType>.from(self.oldEntity)
    }

    open class var paymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPaymentTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPaymentTermsType.self) }
            do {
                return XITSPDSFAxAPaymentTermsType.paymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPaymentTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPaymentTermsType.self) }
            do {
                XITSPDSFAxAPaymentTermsType.paymentTerms_ = value
            }
        }
    }

    open var paymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPaymentTermsType.paymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPaymentTermsType.paymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var paymentTermsConditionDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPaymentTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPaymentTermsType.self) }
            do {
                return XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPaymentTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAPaymentTermsType.self) }
            do {
                XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc_ = value
            }
        }
    }

    open var paymentTermsConditionDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPaymentTermsType.paymentTermsConditionDesc, to: StringValue.of(optional: value))
        }
    }
}
