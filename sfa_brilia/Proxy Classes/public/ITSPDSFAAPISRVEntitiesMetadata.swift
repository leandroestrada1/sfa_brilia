// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

public class ITSPDSFAAPISRVEntitiesMetadata {
    private static var document_: CSDLDocument = ITSPDSFAAPISRVEntitiesMetadata.resolve()

    public static var document: CSDLDocument {
        get {
            objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.self)
            defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.self) }
            do {
                return ITSPDSFAAPISRVEntitiesMetadata.document_
            }
        }
        set(value) {
            objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.self)
            defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.self) }
            do {
                ITSPDSFAAPISRVEntitiesMetadata.document_ = value
            }
        }
    }

    private static func resolve() -> CSDLDocument {
        try! ITSPDSFAAPISRVEntitiesFactory.registerAll()
        ITSPDSFAAPISRVEntitiesMetadataParser.parsed.hasGeneratedProxies = true
        return ITSPDSFAAPISRVEntitiesMetadataParser.parsed
    }

    public class ComplexTypes {
        private static var parameterValue_: ComplexType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.complexType(withName: "sfa.ParameterValue")

        private static var pricingValue_: ComplexType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.complexType(withName: "sfa.PricingValue")

        private static var taxValue_: ComplexType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.complexType(withName: "sfa.TaxValue")

        public static var parameterValue: ComplexType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue_ = value
                }
            }
        }

        public static var pricingValue: ComplexType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.pricingValue_ = value
                }
            }
        }

        public static var taxValue: ComplexType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.taxValue_ = value
                }
            }
        }
    }

    public class EntityTypes {
        private static var aCustSalesPartnerFuncType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.A_CustSalesPartnerFuncType")

        private static var attachmentContent_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.AttachmentContent")

        private static var iBillingDocumentBasicStdVHType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.I_BillingDocumentBasicStdVHType")

        private static var iCompanyCodeStdVHType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.I_CompanyCodeStdVHType")

        private static var iCustomerVHType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.I_Customer_VHType")

        private static var salesOrderWithoutChargeItemPartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargeItemPartnerType")

        private static var salesOrderWithoutChargeItemPricing_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargeItemPricing")

        private static var salesOrderWithoutChargeItemType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargeItemType")

        private static var salesOrderWithoutChargePartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargePartnerType")

        private static var salesOrderWithoutChargePricing_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargePricing")

        private static var salesOrderWithoutChargeScheduleLinetype_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargeScheduleLinetype")

        private static var salesOrderWithoutChargeType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesOrderWithoutChargeType")

        private static var salesQuotationItemPartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationItemPartnerType")

        private static var salesQuotationItemPricingType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationItemPricingType")

        private static var salesQuotationItensType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationItensType")

        private static var salesQuotationPartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationPartnerType")

        private static var salesQuotationPricingType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationPricingType")

        private static var salesQuotationsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SalesQuotationsType")

        private static var syncEventType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SyncEventType")

        private static var syncOperationStatusType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.SyncOperationStatusType")

        private static var xITSPDSFAxABillingDocumentType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_BillingDocumentType")

        private static var xITSPDSFAxABusinessRulesType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_BusinessRulesType")

        private static var xITSPDSFAxACenterSupplierType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CenterSupplierType")

        private static var xITSPDSFAxAConditionListType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ConditionListType")

        private static var xITSPDSFAxAConditionScaleType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ConditionScaleType")

        private static var xITSPDSFAxAConditionValue_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ConditionValue")

        private static var xITSPDSFAxACustomerEmailXMLType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerEmailXMLType")

        private static var xITSPDSFAxACustomerGrpTextType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerGrpTextType")

        private static var xITSPDSFAxACustomerLatLong_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerLatLong")

        private static var xITSPDSFAxACustomerLstInvType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerLstInvType")

        private static var xITSPDSFAxACustomerLstSalOrdType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerLstSalOrdType")

        private static var xITSPDSFAxACustomerMasterType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerMasterType")

        private static var xITSPDSFAxACustomerSalesAreaType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerSalesAreaType")

        private static var xITSPDSFAxACustomerShipToType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_CustomerShipToType")

        private static var xITSPDSFAxADocumentsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_DocumentsType")

        private static var xITSPDSFAxAHierarchyType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_HierarchyType")

        private static var xITSPDSFAxAIncoTermsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_IncoTermsType")

        private static var xITSPDSFAxAInvoiceType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_InvoiceType")

        private static var xITSPDSFAxAMsgPrtnrType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_MsgPrtnrType")

        private static var xITSPDSFAxAMsgType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_MsgType")

        private static var xITSPDSFAxANameSetType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_NameSetType")

        private static var xITSPDSFAxANotificationsStatType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_NotificationsStatType")

        private static var xITSPDSFAxANotificationsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_NotificationsType")

        private static var xITSPDSFAxAPartnerLastSalesType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PartnerLastSalesType")

        private static var xITSPDSFAxAPartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PartnerType")

        private static var xITSPDSFAxAPartnerUserType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PartnerUserType")

        private static var xITSPDSFAxAPartnersType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PartnersType")

        private static var xITSPDSFAxAPaymentTermsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PaymentTermsType")

        private static var xITSPDSFAxAPricelistsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_PricelistsType")

        private static var xITSPDSFAxAProductHieLevel1Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel1Type")

        private static var xITSPDSFAxAProductHieLevel2Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel2Type")

        private static var xITSPDSFAxAProductHieLevel3Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel3Type")

        private static var xITSPDSFAxAProductHieLevel4Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel4Type")

        private static var xITSPDSFAxAProductHieLevel5Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel5Type")

        private static var xITSPDSFAxAProductHieLevel6Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel6Type")

        private static var xITSPDSFAxAProductHieLevel7Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel7Type")

        private static var xITSPDSFAxAProductHieLevel8Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel8Type")

        private static var xITSPDSFAxAProductHieLevel9Type_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductHieLevel9Type")

        private static var xITSPDSFAxAProductMasterType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductMasterType")

        private static var xITSPDSFAxAProductReqMRPType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_ProductReqMRPType")

        private static var xITSPDSFAxAQMNote_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_QMNote")

        private static var xITSPDSFAxASalesAmountsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesAmountsType")

        private static var xITSPDSFAxASalesDocTypeType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesDocTypeType")

        private static var xITSPDSFAxASalesItemAmountsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesItemAmountsType")

        private static var xITSPDSFAxASalesOrdItPriceType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdItPriceType")

        private static var xITSPDSFAxASalesOrderPartnerType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrderPartnerType")

        private static var xITSPDSFAxASalesOrdersItensType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersItensType")

        private static var xITSPDSFAxASalesOrdersObs_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersObs")

        private static var xITSPDSFAxASalesOrdersObsN_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersObsN")

        private static var xITSPDSFAxASalesOrdersResnsType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersResnsType")

        private static var xITSPDSFAxASalesOrdersType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SalesOrdersType")

        private static var xITSPDSFAxASoldVolumeType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_SoldVolumeType")

        private static var xITSPDSFAxATaxJurisdictionType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_TaxJurisdictionType")

        private static var xITSPDSFAxATaxListType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_TaxListType")

        private static var xITSPDSFAxATaxNamesetType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_TaxNamesetType")

        private static var xITSPDSFAxATaxValueType_: EntityType = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entityType(withName: "sfa.xITSPDSFAxA_TaxValueType")

        public static var aCustSalesPartnerFuncType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType_ = value
                }
            }
        }

        public static var attachmentContent: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent_ = value
                }
            }
        }

        public static var iBillingDocumentBasicStdVHType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType_ = value
                }
            }
        }

        public static var iCompanyCodeStdVHType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType_ = value
                }
            }
        }

        public static var iCustomerVHType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItemPartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItemPricing: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPricing_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItemType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType_ = value
                }
            }
        }

        public static var salesOrderWithoutChargePartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePartnerType_ = value
                }
            }
        }

        public static var salesOrderWithoutChargePricing: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeScheduleLinetype: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeScheduleLinetype_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeType_ = value
                }
            }
        }

        public static var salesQuotationItemPartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType_ = value
                }
            }
        }

        public static var salesQuotationItemPricingType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPricingType_ = value
                }
            }
        }

        public static var salesQuotationItensType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType_ = value
                }
            }
        }

        public static var salesQuotationPartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPartnerType_ = value
                }
            }
        }

        public static var salesQuotationPricingType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationPricingType_ = value
                }
            }
        }

        public static var salesQuotationsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationsType_ = value
                }
            }
        }

        public static var syncEventType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncEventType_ = value
                }
            }
        }

        public static var syncOperationStatusType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType_ = value
                }
            }
        }

        public static var xITSPDSFAxABillingDocumentType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABillingDocumentType_ = value
                }
            }
        }

        public static var xITSPDSFAxABusinessRulesType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType_ = value
                }
            }
        }

        public static var xITSPDSFAxACenterSupplierType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACenterSupplierType_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionListType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionScaleType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionScaleType_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionValue: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionValue_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerEmailXMLType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerGrpTextType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerGrpTextType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLatLong: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLstInvType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLstSalOrdType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstSalOrdType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerMasterType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerMasterType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerSalesAreaType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerSalesAreaType_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerShipToType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerShipToType_ = value
                }
            }
        }

        public static var xITSPDSFAxADocumentsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType_ = value
                }
            }
        }

        public static var xITSPDSFAxAHierarchyType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAHierarchyType_ = value
                }
            }
        }

        public static var xITSPDSFAxAIncoTermsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType_ = value
                }
            }
        }

        public static var xITSPDSFAxAInvoiceType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAInvoiceType_ = value
                }
            }
        }

        public static var xITSPDSFAxAMsgPrtnrType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgPrtnrType_ = value
                }
            }
        }

        public static var xITSPDSFAxAMsgType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType_ = value
                }
            }
        }

        public static var xITSPDSFAxANameSetType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType_ = value
                }
            }
        }

        public static var xITSPDSFAxANotificationsStatType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsStatType_ = value
                }
            }
        }

        public static var xITSPDSFAxANotificationsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnerLastSalesType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnerUserType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerUserType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnersType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnersType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPaymentTermsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPaymentTermsType_ = value
                }
            }
        }

        public static var xITSPDSFAxAPricelistsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPricelistsType_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel1Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel1Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel2Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel2Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel3Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel4Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel5Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel5Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel6Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel6Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel7Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel7Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel8Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel8Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel9Type: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel9Type_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductMasterType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductReqMRPType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductReqMRPType_ = value
                }
            }
        }

        public static var xITSPDSFAxAQMNote: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesAmountsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesDocTypeType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesDocTypeType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesItemAmountsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdItPriceType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrderPartnerType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrderPartnerType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersItensType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersObs: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObs_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersObsN: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersObsN_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersResnsType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType_ = value
                }
            }
        }

        public static var xITSPDSFAxASoldVolumeType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxJurisdictionType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxListType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxNamesetType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxNamesetType_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxValueType: EntityType {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxValueType_ = value
                }
            }
        }
    }

    public class EntitySets {
        private static var aCustSalesPartnerFunc_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "A_CustSalesPartnerFunc")

        private static var attachmentContentSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "AttachmentContentSet")

        private static var iBillingDocumentBasicStdVH_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "I_BillingDocumentBasicStdVH")

        private static var iCompanyCodeStdVH_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "I_CompanyCodeStdVH")

        private static var iCustomerVH_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "I_Customer_VH")

        private static var salesOrderWithoutCharge_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutCharge")

        private static var salesOrderWithoutChargeItem_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargeItem")

        private static var salesOrderWithoutChargeItemPartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargeItemPartner")

        private static var salesOrderWithoutChargeItemPricings_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargeItemPricings")

        private static var salesOrderWithoutChargePartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargePartner")

        private static var salesOrderWithoutChargePricings_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargePricings")

        private static var salesOrderWithoutChargeScheduleLine_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesOrderWithoutChargeScheduleLine")

        private static var salesQuotationItemPartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotationItemPartner")

        private static var salesQuotationItemPricing_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotationItemPricing")

        private static var salesQuotationItens_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotationItens")

        private static var salesQuotationPartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotationPartner")

        private static var salesQuotationPricing_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotationPricing")

        private static var salesQuotations_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SalesQuotations")

        private static var syncEvent_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SyncEvent")

        private static var syncOperationStatus_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "SyncOperationStatus")

        private static var xITSPDSFAxABillingDocument_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_BillingDocument")

        private static var xITSPDSFAxABusinessRules_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_BusinessRules")

        private static var xITSPDSFAxACenterSupplier_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CenterSupplier")

        private static var xITSPDSFAxAConditionList_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ConditionList")

        private static var xITSPDSFAxAConditionScale_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ConditionScale")

        private static var xITSPDSFAxAConditionValueSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ConditionValueSet")

        private static var xITSPDSFAxACustomerEmailXML_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerEmailXML")

        private static var xITSPDSFAxACustomerGrpText_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerGrpText")

        private static var xITSPDSFAxACustomerLatLongSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerLatLongSet")

        private static var xITSPDSFAxACustomerLstInv_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerLstInv")

        private static var xITSPDSFAxACustomerLstSalOrd_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerLstSalOrd")

        private static var xITSPDSFAxACustomerMaster_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerMaster")

        private static var xITSPDSFAxACustomerSalesArea_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerSalesArea")

        private static var xITSPDSFAxACustomerShipTo_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_CustomerShipTo")

        private static var xITSPDSFAxADocuments_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Documents")

        private static var xITSPDSFAxAHierarchy_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Hierarchy")

        private static var xITSPDSFAxAIncoTerms_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_IncoTerms")

        private static var xITSPDSFAxAInvoice_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Invoice")

        private static var xITSPDSFAxAMsg_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Msg")

        private static var xITSPDSFAxAMsgPrtnr_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_MsgPrtnr")

        private static var xITSPDSFAxANameSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_NameSet")

        private static var xITSPDSFAxANotifications_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Notifications")

        private static var xITSPDSFAxANotificationsStat_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_NotificationsStat")

        private static var xITSPDSFAxAPartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Partner")

        private static var xITSPDSFAxAPartnerLastSales_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_PartnerLastSales")

        private static var xITSPDSFAxAPartnerUser_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_PartnerUser")

        private static var xITSPDSFAxAPartners_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Partners")

        private static var xITSPDSFAxAPaymentTerms_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_PaymentTerms")

        private static var xITSPDSFAxAPricelists_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_Pricelists")

        private static var xITSPDSFAxAProductHieLevel1_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel1")

        private static var xITSPDSFAxAProductHieLevel2_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel2")

        private static var xITSPDSFAxAProductHieLevel3_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel3")

        private static var xITSPDSFAxAProductHieLevel4_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel4")

        private static var xITSPDSFAxAProductHieLevel5_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel5")

        private static var xITSPDSFAxAProductHieLevel6_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel6")

        private static var xITSPDSFAxAProductHieLevel7_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel7")

        private static var xITSPDSFAxAProductHieLevel8_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel8")

        private static var xITSPDSFAxAProductHieLevel9_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductHieLevel9")

        private static var xITSPDSFAxAProductMaster_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductMaster")

        private static var xITSPDSFAxAProductReqMRPSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_ProductReqMRPSet")

        private static var xITSPDSFAxAQMNoteSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_QMNoteSet")

        private static var xITSPDSFAxASalesAmounts_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesAmounts")

        private static var xITSPDSFAxASalesDocType_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesDocType")

        private static var xITSPDSFAxASalesItemAmounts_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesItemAmounts")

        private static var xITSPDSFAxASalesOrdItPrice_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrdItPrice")

        private static var xITSPDSFAxASalesOrderPartner_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrderPartner")

        private static var xITSPDSFAxASalesOrders_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrders")

        private static var xITSPDSFAxASalesOrdersItens_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrdersItens")

        private static var xITSPDSFAxASalesOrdersObsSet_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrdersObsSet")

        private static var xITSPDSFAxASalesOrdersObsSetN_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrdersObsSetN")

        private static var xITSPDSFAxASalesOrdersResns_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SalesOrdersResns")

        private static var xITSPDSFAxASoldVolume_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_SoldVolume")

        private static var xITSPDSFAxATaxList_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_TaxList")

        private static var xITSPDSFAxATaxNameset_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_TaxNameset")

        private static var xITSPDSFAxATaxValue_: EntitySet = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.entitySet(withName: "xITSPDSFAxA_TaxValue")

        public static var aCustSalesPartnerFunc: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc_ = value
                }
            }
        }

        public static var attachmentContentSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet_ = value
                }
            }
        }

        public static var iBillingDocumentBasicStdVH: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH_ = value
                }
            }
        }

        public static var iCompanyCodeStdVH: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH_ = value
                }
            }
        }

        public static var iCustomerVH: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH_ = value
                }
            }
        }

        public static var salesOrderWithoutCharge: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItem: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItemPartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeItemPricings: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings_ = value
                }
            }
        }

        public static var salesOrderWithoutChargePartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner_ = value
                }
            }
        }

        public static var salesOrderWithoutChargePricings: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings_ = value
                }
            }
        }

        public static var salesOrderWithoutChargeScheduleLine: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine_ = value
                }
            }
        }

        public static var salesQuotationItemPartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner_ = value
                }
            }
        }

        public static var salesQuotationItemPricing: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing_ = value
                }
            }
        }

        public static var salesQuotationItens: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens_ = value
                }
            }
        }

        public static var salesQuotationPartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner_ = value
                }
            }
        }

        public static var salesQuotationPricing: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing_ = value
                }
            }
        }

        public static var salesQuotations: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations_ = value
                }
            }
        }

        public static var syncEvent: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent_ = value
                }
            }
        }

        public static var syncOperationStatus: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus_ = value
                }
            }
        }

        public static var xITSPDSFAxABillingDocument: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument_ = value
                }
            }
        }

        public static var xITSPDSFAxABusinessRules: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules_ = value
                }
            }
        }

        public static var xITSPDSFAxACenterSupplier: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionList: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionScale: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale_ = value
                }
            }
        }

        public static var xITSPDSFAxAConditionValueSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerEmailXML: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerGrpText: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLatLongSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLstInv: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerLstSalOrd: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerMaster: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerSalesArea: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea_ = value
                }
            }
        }

        public static var xITSPDSFAxACustomerShipTo: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo_ = value
                }
            }
        }

        public static var xITSPDSFAxADocuments: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments_ = value
                }
            }
        }

        public static var xITSPDSFAxAHierarchy: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy_ = value
                }
            }
        }

        public static var xITSPDSFAxAIncoTerms: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms_ = value
                }
            }
        }

        public static var xITSPDSFAxAInvoice: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice_ = value
                }
            }
        }

        public static var xITSPDSFAxAMsg: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg_ = value
                }
            }
        }

        public static var xITSPDSFAxAMsgPrtnr: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr_ = value
                }
            }
        }

        public static var xITSPDSFAxANameSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet_ = value
                }
            }
        }

        public static var xITSPDSFAxANotifications: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications_ = value
                }
            }
        }

        public static var xITSPDSFAxANotificationsStat: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnerLastSales: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartnerUser: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser_ = value
                }
            }
        }

        public static var xITSPDSFAxAPartners: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners_ = value
                }
            }
        }

        public static var xITSPDSFAxAPaymentTerms: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms_ = value
                }
            }
        }

        public static var xITSPDSFAxAPricelists: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel1: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel2: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel3: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel4: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel5: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel6: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel7: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel8: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductHieLevel9: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductMaster: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster_ = value
                }
            }
        }

        public static var xITSPDSFAxAProductReqMRPSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet_ = value
                }
            }
        }

        public static var xITSPDSFAxAQMNoteSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesAmounts: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesDocType: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesItemAmounts: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdItPrice: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrderPartner: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrders: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersItens: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersObsSet: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersObsSetN: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN_ = value
                }
            }
        }

        public static var xITSPDSFAxASalesOrdersResns: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns_ = value
                }
            }
        }

        public static var xITSPDSFAxASoldVolume: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxList: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxNameset: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset_ = value
                }
            }
        }

        public static var xITSPDSFAxATaxValue: EntitySet {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue_ = value
                }
            }
        }
    }

    public class FunctionImports {
        private static var getParameterValue_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "GetParameterValue")

        private static var getProductPrice_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "GetProductPrice")

        private static var getProductTaxValue_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "GetProductTaxValue")

        private static var getReadCustomerSalesAreas_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "GetReadCustomerSalesAreas")

        private static var getSalesOrderItensHistory_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "GetSalesOrderItensHistory")

        private static var sumBillingDocumentsNetValues_: DataMethod = ITSPDSFAAPISRVEntitiesMetadataParser.parsed.dataMethod(withName: "SumBillingDocumentsNetValues")

        public static var getParameterValue: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getParameterValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getParameterValue_ = value
                }
            }
        }

        public static var getProductPrice: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductPrice_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductPrice_ = value
                }
            }
        }

        public static var getProductTaxValue: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductTaxValue_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductTaxValue_ = value
                }
            }
        }

        public static var getReadCustomerSalesAreas: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getReadCustomerSalesAreas_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getReadCustomerSalesAreas_ = value
                }
            }
        }

        public static var getSalesOrderItensHistory: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getSalesOrderItensHistory_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getSalesOrderItensHistory_ = value
                }
            }
        }

        public static var sumBillingDocumentsNetValues: DataMethod {
            get {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    return ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.sumBillingDocumentsNetValues_
                }
            }
            set(value) {
                objc_sync_enter(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self)
                defer { objc_sync_exit(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.self) }
                do {
                    ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.sumBillingDocumentsNetValues_ = value
                }
            }
        }
    }
}
