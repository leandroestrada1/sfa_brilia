// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPartnerLastSalesType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrder")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrganization")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "BPCustomerNumber")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "CustomerName")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "OrganizationDivision")

    private static var salesOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "SalesOrderDate")

    private static var totalNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "TotalNetAmount")

    private static var conditionAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType.property(withName: "ConditionAmount")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerLastSalesType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPartnerLastSalesType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPartnerLastSalesType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAPartnerLastSalesType {
        return CastRequired<XITSPDSFAxAPartnerLastSalesType>.from(self.copyEntity())
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrganization: String?, soldToParty: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open var old: XITSPDSFAxAPartnerLastSalesType {
        return CastRequired<XITSPDSFAxAPartnerLastSalesType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.salesOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.salesOrderDate_ = value
            }
        }
    }

    open var salesOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrderDate, to: value)
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var totalNetAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                return XITSPDSFAxAPartnerLastSalesType.totalNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerLastSalesType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerLastSalesType.self) }
            do {
                XITSPDSFAxAPartnerLastSalesType.totalNetAmount_ = value
            }
        }
    }

    open var totalNetAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerLastSalesType.totalNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerLastSalesType.totalNetAmount, to: DecimalValue.of(optional: value))
        }
    }
}
