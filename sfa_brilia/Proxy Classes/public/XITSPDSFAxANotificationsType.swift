// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxANotificationsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var notification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Notification")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "DistributionChannel")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Customer")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "BPCustomerNumber")

    private static var notificationType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationType")

    private static var notificationText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationText")

    private static var notificationPriorityType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationPriorityType")

    private static var notificationPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationPriority")

    private static var maintPriorityDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "MaintPriorityDesc")

    private static var createdByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "CreatedByUser")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "CreationDate")

    private static var lastChangedByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "LastChangedByUser")

    private static var lastChangedDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "LastChangedDate")

    private static var notificationReportingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationReportingDate")

    private static var orderID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "OrderID")

    private static var material_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "Material")

    private static var notificationStatusObject_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationStatusObject")

    private static var notificationCompletionDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationCompletionDate")

    private static var notificationOrigin_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationOrigin")

    private static var activeDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "ActiveDivision")

    private static var notificationQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotificationQuantityUnit")

    private static var isDeleted__: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "IsDeleted")

    private static var notifProcessingPhase_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "NotifProcessingPhase")

    private static var toXITSPDSFAxANotificationsStat_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType.property(withName: "to_xITSPDSFAxA_NotificationsStat")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANotificationsType)
    }

    open class var activeDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.activeDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.activeDivision_ = value
            }
        }
    }

    open var activeDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.activeDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.activeDivision, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxANotificationsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxANotificationsType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxANotificationsType {
        return CastRequired<XITSPDSFAxANotificationsType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxANotificationsType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.creationDate, to: value)
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var isDeleted_: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.isDeleted__
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.isDeleted__ = value
            }
        }
    }

    open var isDeleted_: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.isDeleted_))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.isDeleted_, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(notification: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "Notification", value: StringValue.of(optional: notification)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open class var lastChangedByUser: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.lastChangedByUser_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.lastChangedByUser_ = value
            }
        }
    }

    open var lastChangedByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.lastChangedByUser))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.lastChangedByUser, to: StringValue.of(optional: value))
        }
    }

    open class var lastChangedDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.lastChangedDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.lastChangedDate_ = value
            }
        }
    }

    open var lastChangedDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxANotificationsType.lastChangedDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.lastChangedDate, to: value)
        }
    }

    open class var maintPriorityDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.maintPriorityDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.maintPriorityDesc_ = value
            }
        }
    }

    open var maintPriorityDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.maintPriorityDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.maintPriorityDesc, to: StringValue.of(optional: value))
        }
    }

    open class var material: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.material_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.material))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.material, to: StringValue.of(optional: value))
        }
    }

    open class var notifProcessingPhase: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notifProcessingPhase_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notifProcessingPhase_ = value
            }
        }
    }

    open var notifProcessingPhase: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notifProcessingPhase))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notifProcessingPhase, to: StringValue.of(optional: value))
        }
    }

    open class var notification: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notification_ = value
            }
        }
    }

    open var notification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notification, to: StringValue.of(optional: value))
        }
    }

    open class var notificationCompletionDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationCompletionDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationCompletionDate_ = value
            }
        }
    }

    open var notificationCompletionDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationCompletionDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationCompletionDate, to: value)
        }
    }

    open class var notificationOrigin: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationOrigin_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationOrigin_ = value
            }
        }
    }

    open var notificationOrigin: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationOrigin))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationOrigin, to: StringValue.of(optional: value))
        }
    }

    open class var notificationPriority: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationPriority_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationPriority_ = value
            }
        }
    }

    open var notificationPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationPriority))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationPriority, to: StringValue.of(optional: value))
        }
    }

    open class var notificationPriorityType: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationPriorityType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationPriorityType_ = value
            }
        }
    }

    open var notificationPriorityType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationPriorityType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationPriorityType, to: StringValue.of(optional: value))
        }
    }

    open class var notificationQuantityUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationQuantityUnit_ = value
            }
        }
    }

    open var notificationQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var notificationReportingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationReportingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationReportingDate_ = value
            }
        }
    }

    open var notificationReportingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationReportingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationReportingDate, to: value)
        }
    }

    open class var notificationStatusObject: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationStatusObject_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationStatusObject_ = value
            }
        }
    }

    open var notificationStatusObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationStatusObject))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationStatusObject, to: StringValue.of(optional: value))
        }
    }

    open class var notificationText: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationText_ = value
            }
        }
    }

    open var notificationText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationText, to: StringValue.of(optional: value))
        }
    }

    open class var notificationType: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.notificationType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.notificationType_ = value
            }
        }
    }

    open var notificationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.notificationType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.notificationType, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxANotificationsType {
        return CastRequired<XITSPDSFAxANotificationsType>.from(self.oldEntity)
    }

    open class var orderID: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.orderID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.orderID_ = value
            }
        }
    }

    open var orderID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.orderID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.orderID, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANotificationsType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANotificationsType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxANotificationsStat: Property {
        get {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                return XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANotificationsType.self)
            defer { objc_sync_exit(XITSPDSFAxANotificationsType.self) }
            do {
                XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat_ = value
            }
        }
    }

    open var toXITSPDSFAxANotificationsStat: Array<XITSPDSFAxANotificationsStatType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat)).toArray(), Array<XITSPDSFAxANotificationsStatType>())
        }
        set(value) {
            XITSPDSFAxANotificationsType.toXITSPDSFAxANotificationsStat.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
