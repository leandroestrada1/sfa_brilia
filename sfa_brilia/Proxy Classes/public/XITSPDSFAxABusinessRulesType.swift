// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxABusinessRulesType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var tipo_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "tipo")

    private static var nome_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "nome")

    private static var seq_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "seq")

    private static var valor_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "valor")

    private static var descricao_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType.property(withName: "descricao")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxABusinessRulesType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxABusinessRulesType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxABusinessRulesType>())
    }

    open func copy() -> XITSPDSFAxABusinessRulesType {
        return CastRequired<XITSPDSFAxABusinessRulesType>.from(self.copyEntity())
    }

    open class var descricao: Property {
        get {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                return XITSPDSFAxABusinessRulesType.descricao_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                XITSPDSFAxABusinessRulesType.descricao_ = value
            }
        }
    }

    open var descricao: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABusinessRulesType.descricao))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABusinessRulesType.descricao, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(tipo: String?, nome: String?, seq: String?) -> EntityKey {
        return EntityKey().with(name: "tipo", value: StringValue.of(optional: tipo)).with(name: "nome", value: StringValue.of(optional: nome)).with(name: "seq", value: StringValue.of(optional: seq))
    }

    open class var nome: Property {
        get {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                return XITSPDSFAxABusinessRulesType.nome_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                XITSPDSFAxABusinessRulesType.nome_ = value
            }
        }
    }

    open var nome: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABusinessRulesType.nome))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABusinessRulesType.nome, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxABusinessRulesType {
        return CastRequired<XITSPDSFAxABusinessRulesType>.from(self.oldEntity)
    }

    open class var seq: Property {
        get {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                return XITSPDSFAxABusinessRulesType.seq_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                XITSPDSFAxABusinessRulesType.seq_ = value
            }
        }
    }

    open var seq: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABusinessRulesType.seq))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABusinessRulesType.seq, to: StringValue.of(optional: value))
        }
    }

    open class var tipo: Property {
        get {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                return XITSPDSFAxABusinessRulesType.tipo_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                XITSPDSFAxABusinessRulesType.tipo_ = value
            }
        }
    }

    open var tipo: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABusinessRulesType.tipo))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABusinessRulesType.tipo, to: StringValue.of(optional: value))
        }
    }

    open class var valor: Property {
        get {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                return XITSPDSFAxABusinessRulesType.valor_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxABusinessRulesType.self)
            defer { objc_sync_exit(XITSPDSFAxABusinessRulesType.self) }
            do {
                XITSPDSFAxABusinessRulesType.valor_ = value
            }
        }
    }

    open var valor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxABusinessRulesType.valor))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxABusinessRulesType.valor, to: StringValue.of(optional: value))
        }
    }
}
