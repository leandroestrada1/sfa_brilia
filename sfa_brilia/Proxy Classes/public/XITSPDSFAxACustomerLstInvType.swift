// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerLstInvType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "Customer")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesOrganization")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BPCustomerNumber")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "Division")

    private static var brNotaFiscal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NotaFiscal")

    private static var salesDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesDocument")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SoldToParty")

    private static var billingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BillingDocument")

    private static var brNFeNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFeNumber")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "CustomerName")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "SalesOrderType")

    private static var brNFType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFType")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "CreationDate")

    private static var brNFArrivalOrDepartureDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFArrivalOrDepartureDate")

    private static var brNFPostingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFPostingDate")

    private static var brNFTotalAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType.property(withName: "BR_NFTotalAmount")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLstInvType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerLstInvType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerLstInvType>())
    }

    open class var billingDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.billingDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.billingDocument_ = value
            }
        }
    }

    open var billingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.billingDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.billingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var brNFArrivalOrDepartureDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate_ = value
            }
        }
    }

    open var brNFArrivalOrDepartureDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNFArrivalOrDepartureDate, to: value)
        }
    }

    open class var brNFPostingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNFPostingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNFPostingDate_ = value
            }
        }
    }

    open var brNFPostingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNFPostingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNFPostingDate, to: value)
        }
    }

    open class var brNFTotalAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNFTotalAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNFTotalAmount_ = value
            }
        }
    }

    open var brNFTotalAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNFTotalAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNFTotalAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var brNFType: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNFType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNFType_ = value
            }
        }
    }

    open var brNFType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNFType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNFType, to: StringValue.of(optional: value))
        }
    }

    open class var brNFeNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNFeNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNFeNumber_ = value
            }
        }
    }

    open var brNFeNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNFeNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNFeNumber, to: StringValue.of(optional: value))
        }
    }

    open class var brNotaFiscal: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.brNotaFiscal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.brNotaFiscal_ = value
            }
        }
    }

    open var brNotaFiscal: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.brNotaFiscal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.brNotaFiscal, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerLstInvType {
        return CastRequired<XITSPDSFAxACustomerLstInvType>.from(self.copyEntity())
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.creationDate, to: value)
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.division, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, salesOrganization: String?, bpCustomerNumber: String?, distributionChannel: String?, division: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division))
    }

    open var old: XITSPDSFAxACustomerLstInvType {
        return CastRequired<XITSPDSFAxACustomerLstInvType>.from(self.oldEntity)
    }

    open class var salesDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.salesDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.salesDocument_ = value
            }
        }
    }

    open var salesDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.salesDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.salesDocument, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                return XITSPDSFAxACustomerLstInvType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLstInvType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLstInvType.self) }
            do {
                XITSPDSFAxACustomerLstInvType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLstInvType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLstInvType.soldToParty, to: StringValue.of(optional: value))
        }
    }
}
