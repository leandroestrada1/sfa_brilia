// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class ITSPDSFAAPISRVEntities<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = ITSPDSFAAPISRVEntitiesMetadata.document
    }

    @available(swift, deprecated: 4.0, renamed: "fetchACustSalesPartnerFunc")
    open func aCustSalesPartnerFunc(query: DataQuery = DataQuery()) throws -> Array<ACustSalesPartnerFuncType> {
        return try self.fetchACustSalesPartnerFunc(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchACustSalesPartnerFunc")
    open func aCustSalesPartnerFunc(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<ACustSalesPartnerFuncType>?, Error?) -> Void) {
        self.fetchACustSalesPartnerFunc(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAttachmentContentSet")
    open func attachmentContentSet(query: DataQuery = DataQuery()) throws -> Array<AttachmentContent> {
        return try self.fetchAttachmentContentSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAttachmentContentSet")
    open func attachmentContentSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<AttachmentContent>?, Error?) -> Void) {
        self.fetchAttachmentContentSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open func fetchACustSalesPartnerFunc(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<ACustSalesPartnerFuncType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ACustSalesPartnerFuncType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc), headers: var_headers, options: var_options).entityList())
    }

    open func fetchACustSalesPartnerFunc(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<ACustSalesPartnerFuncType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchACustSalesPartnerFunc(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchACustSalesPartnerFuncType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ACustSalesPartnerFuncType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ACustSalesPartnerFuncType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.aCustSalesPartnerFunc), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchACustSalesPartnerFuncType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ACustSalesPartnerFuncType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchACustSalesPartnerFuncType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchACustSalesPartnerFuncTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ACustSalesPartnerFuncType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchACustSalesPartnerFuncType(matching: var_query.withKey(ACustSalesPartnerFuncType.key(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, partnerCounter: partnerCounter, partnerFunction: partnerFunction)), headers: headers, options: options)
    }

    open func fetchACustSalesPartnerFuncTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ACustSalesPartnerFuncType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchACustSalesPartnerFuncTypeWithKey(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, partnerCounter: partnerCounter, partnerFunction: partnerFunction, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAttachmentContent(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AttachmentContent {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<AttachmentContent>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAttachmentContent(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AttachmentContent?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachmentContent(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAttachmentContentSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<AttachmentContent> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try AttachmentContent.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.attachmentContentSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAttachmentContentSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<AttachmentContent>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachmentContentSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAttachmentContentWithKey(documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, logicalDocument: String?, archiveDocumentID: String?, linkedSAPObjectKey: String?, businessObjectTypeName: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> AttachmentContent {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAttachmentContent(matching: var_query.withKey(AttachmentContent.key(documentInfoRecordDocType: documentInfoRecordDocType, documentInfoRecordDocNumber: documentInfoRecordDocNumber, documentInfoRecordDocVersion: documentInfoRecordDocVersion, documentInfoRecordDocPart: documentInfoRecordDocPart, logicalDocument: logicalDocument, archiveDocumentID: archiveDocumentID, linkedSAPObjectKey: linkedSAPObjectKey, businessObjectTypeName: businessObjectTypeName)), headers: headers, options: options)
    }

    open func fetchAttachmentContentWithKey(documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, logicalDocument: String?, archiveDocumentID: String?, linkedSAPObjectKey: String?, businessObjectTypeName: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (AttachmentContent?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachmentContentWithKey(documentInfoRecordDocType: documentInfoRecordDocType, documentInfoRecordDocNumber: documentInfoRecordDocNumber, documentInfoRecordDocVersion: documentInfoRecordDocVersion, documentInfoRecordDocPart: documentInfoRecordDocPart, logicalDocument: logicalDocument, archiveDocumentID: archiveDocumentID, linkedSAPObjectKey: linkedSAPObjectKey, businessObjectTypeName: businessObjectTypeName, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchIBillingDocumentBasicStdVH(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<IBillingDocumentBasicStdVHType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try IBillingDocumentBasicStdVHType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH), headers: var_headers, options: var_options).entityList())
    }

    open func fetchIBillingDocumentBasicStdVH(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<IBillingDocumentBasicStdVHType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchIBillingDocumentBasicStdVH(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchIBillingDocumentBasicStdVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> IBillingDocumentBasicStdVHType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<IBillingDocumentBasicStdVHType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iBillingDocumentBasicStdVH), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchIBillingDocumentBasicStdVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (IBillingDocumentBasicStdVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchIBillingDocumentBasicStdVHType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchIBillingDocumentBasicStdVHTypeWithKey(billingDocument: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> IBillingDocumentBasicStdVHType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchIBillingDocumentBasicStdVHType(matching: var_query.withKey(IBillingDocumentBasicStdVHType.key(billingDocument: billingDocument)), headers: headers, options: options)
    }

    open func fetchIBillingDocumentBasicStdVHTypeWithKey(billingDocument: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (IBillingDocumentBasicStdVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchIBillingDocumentBasicStdVHTypeWithKey(billingDocument: billingDocument, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICompanyCodeStdVH(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<ICompanyCodeStdVHType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ICompanyCodeStdVHType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH), headers: var_headers, options: var_options).entityList())
    }

    open func fetchICompanyCodeStdVH(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<ICompanyCodeStdVHType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICompanyCodeStdVH(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICompanyCodeStdVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ICompanyCodeStdVHType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ICompanyCodeStdVHType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCompanyCodeStdVH), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchICompanyCodeStdVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ICompanyCodeStdVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICompanyCodeStdVHType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICompanyCodeStdVHTypeWithKey(companyCode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ICompanyCodeStdVHType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchICompanyCodeStdVHType(matching: var_query.withKey(ICompanyCodeStdVHType.key(companyCode: companyCode)), headers: headers, options: options)
    }

    open func fetchICompanyCodeStdVHTypeWithKey(companyCode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ICompanyCodeStdVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICompanyCodeStdVHTypeWithKey(companyCode: companyCode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICustomerVH(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<ICustomerVHType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ICustomerVHType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH), headers: var_headers, options: var_options).entityList())
    }

    open func fetchICustomerVH(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<ICustomerVHType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICustomerVH(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICustomerVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ICustomerVHType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ICustomerVHType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.iCustomerVH), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchICustomerVHType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ICustomerVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICustomerVHType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchICustomerVHTypeWithKey(customer: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ICustomerVHType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchICustomerVHType(matching: var_query.withKey(ICustomerVHType.key(customer: customer)), headers: headers, options: options)
    }

    open func fetchICustomerVHTypeWithKey(customer: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ICustomerVHType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchICustomerVHTypeWithKey(customer: customer, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutCharge(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargeType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargeType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutCharge(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargeType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutCharge(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItem(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargeItemType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargeItemType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargeItem(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargeItemType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItem(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargeItemPartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargeItemPartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargeItemPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargeItemPartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargeItemPartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargeItemPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPartnerTypeWithKey(partnerFunction: String?, salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargeItemPartnerType(matching: var_query.withKey(SalesOrderWithoutChargeItemPartnerType.key(partnerFunction: partnerFunction, salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargeItemPartnerTypeWithKey(partnerFunction: String?, salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPartnerTypeWithKey(partnerFunction: partnerFunction, salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPricing(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemPricing {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargeItemPricing>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargeItemPricing(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemPricing?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPricing(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPricingWithKey(salesBonification: String?, salesBonificationItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemPricing {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargeItemPricing(matching: var_query.withKey(SalesOrderWithoutChargeItemPricing.key(salesBonification: salesBonification, salesBonificationItem: salesBonificationItem)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargeItemPricingWithKey(salesBonification: String?, salesBonificationItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemPricing?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPricingWithKey(salesBonification: salesBonification, salesBonificationItem: salesBonificationItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemPricings(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargeItemPricing> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargeItemPricing.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItemPricings), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargeItemPricings(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargeItemPricing>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemPricings(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargeItemType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeItem), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargeItemType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeItemTypeWithKey(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeItemType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargeItemType(matching: var_query.withKey(SalesOrderWithoutChargeItemType.key(salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargeItemTypeWithKey(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeItemType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeItemTypeWithKey(salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargePartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargePartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargePartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargePartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargePartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargePartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargePartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargePartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePartnerTypeWithKey(partnerFunction: String?, salesOrderWithoutCharge: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargePartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargePartnerType(matching: var_query.withKey(SalesOrderWithoutChargePartnerType.key(partnerFunction: partnerFunction, salesOrderWithoutCharge: salesOrderWithoutCharge)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargePartnerTypeWithKey(partnerFunction: String?, salesOrderWithoutCharge: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargePartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePartnerTypeWithKey(partnerFunction: partnerFunction, salesOrderWithoutCharge: salesOrderWithoutCharge, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePricing(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargePricing {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargePricing>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargePricing(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargePricing?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePricing(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePricingWithKey(pricingProcedureCounter: String?, pricingProcedureStep: String?, salesQuotation: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargePricing {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargePricing(matching: var_query.withKey(SalesOrderWithoutChargePricing.key(pricingProcedureCounter: pricingProcedureCounter, pricingProcedureStep: pricingProcedureStep, salesQuotation: salesQuotation)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargePricingWithKey(pricingProcedureCounter: String?, pricingProcedureStep: String?, salesQuotation: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargePricing?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePricingWithKey(pricingProcedureCounter: pricingProcedureCounter, pricingProcedureStep: pricingProcedureStep, salesQuotation: salesQuotation, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargePricings(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargePricing> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargePricing.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargePricings), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargePricings(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargePricing>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargePricings(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeScheduleLine(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesOrderWithoutChargeScheduleLinetype> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesOrderWithoutChargeScheduleLinetype.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesOrderWithoutChargeScheduleLine(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesOrderWithoutChargeScheduleLinetype>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeScheduleLine(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeScheduleLinetype(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeScheduleLinetype {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargeScheduleLinetype>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutChargeScheduleLine), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargeScheduleLinetype(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeScheduleLinetype?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeScheduleLinetype(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeScheduleLinetypeWithKey(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, scheduleLine: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeScheduleLinetype {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargeScheduleLinetype(matching: var_query.withKey(SalesOrderWithoutChargeScheduleLinetype.key(salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem, scheduleLine: scheduleLine)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargeScheduleLinetypeWithKey(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?, scheduleLine: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeScheduleLinetype?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeScheduleLinetypeWithKey(salesOrderWithoutCharge: salesOrderWithoutCharge, salesOrderWithoutChargeItem: salesOrderWithoutChargeItem, scheduleLine: scheduleLine, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesOrderWithoutChargeType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesOrderWithoutCharge), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesOrderWithoutChargeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesOrderWithoutChargeTypeWithKey(salesOrderWithoutCharge: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesOrderWithoutChargeType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesOrderWithoutChargeType(matching: var_query.withKey(SalesOrderWithoutChargeType.key(salesOrderWithoutCharge: salesOrderWithoutCharge)), headers: headers, options: options)
    }

    open func fetchSalesOrderWithoutChargeTypeWithKey(salesOrderWithoutCharge: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesOrderWithoutChargeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesOrderWithoutChargeTypeWithKey(salesOrderWithoutCharge: salesOrderWithoutCharge, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationItemPartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationItemPartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotationItemPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationItemPartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItemPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationItemPartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationItemPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItemPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPartnerTypeWithKey(salesQuotation: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItemPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationItemPartnerType(matching: var_query.withKey(SalesQuotationItemPartnerType.key(salesQuotation: salesQuotation)), headers: headers, options: options)
    }

    open func fetchSalesQuotationItemPartnerTypeWithKey(salesQuotation: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItemPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPartnerTypeWithKey(salesQuotation: salesQuotation, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPricing(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationItemPricingType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationItemPricingType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotationItemPricing(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationItemPricingType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPricing(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPricingType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItemPricingType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationItemPricingType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItemPricing), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationItemPricingType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItemPricingType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPricingType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItemPricingTypeWithKey(salesQuotation: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItemPricingType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationItemPricingType(matching: var_query.withKey(SalesQuotationItemPricingType.key(salesQuotation: salesQuotation)), headers: headers, options: options)
    }

    open func fetchSalesQuotationItemPricingTypeWithKey(salesQuotation: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItemPricingType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItemPricingTypeWithKey(salesQuotation: salesQuotation, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItens(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationItensType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationItensType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotationItens(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationItensType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItens(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItensType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItensType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationItensType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationItens), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationItensType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItensType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItensType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationItensTypeWithKey(salesQuotation: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationItensType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationItensType(matching: var_query.withKey(SalesQuotationItensType.key(salesQuotation: salesQuotation)), headers: headers, options: options)
    }

    open func fetchSalesQuotationItensTypeWithKey(salesQuotation: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationItensType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationItensTypeWithKey(salesQuotation: salesQuotation, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationPartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationPartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotationPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationPartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationPartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPartnerTypeWithKey(salesQuotation: String?, partnerFunction: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationPartnerType(matching: var_query.withKey(SalesQuotationPartnerType.key(salesQuotation: salesQuotation, partnerFunction: partnerFunction)), headers: headers, options: options)
    }

    open func fetchSalesQuotationPartnerTypeWithKey(salesQuotation: String?, partnerFunction: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPartnerTypeWithKey(salesQuotation: salesQuotation, partnerFunction: partnerFunction, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPricing(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationPricingType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationPricingType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotationPricing(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationPricingType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPricing(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPricingType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationPricingType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationPricingType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotationPricing), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationPricingType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationPricingType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPricingType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationPricingTypeWithKey(salesQuotation: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationPricingType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationPricingType(matching: var_query.withKey(SalesQuotationPricingType.key(salesQuotation: salesQuotation, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter)), headers: headers, options: options)
    }

    open func fetchSalesQuotationPricingTypeWithKey(salesQuotation: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationPricingType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationPricingTypeWithKey(salesQuotation: salesQuotation, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotations(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SalesQuotationsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SalesQuotationsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSalesQuotations(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SalesQuotationsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotations(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SalesQuotationsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.salesQuotations), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSalesQuotationsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSalesQuotationsTypeWithKey(salesQuotation: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SalesQuotationsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSalesQuotationsType(matching: var_query.withKey(SalesQuotationsType.key(salesQuotation: salesQuotation)), headers: headers, options: options)
    }

    open func fetchSalesQuotationsTypeWithKey(salesQuotation: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SalesQuotationsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSalesQuotationsTypeWithKey(salesQuotation: salesQuotation, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncEvent(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SyncEventType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SyncEventType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSyncEvent(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SyncEventType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncEvent(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncEventType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SyncEventType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SyncEventType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncEvent), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSyncEventType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SyncEventType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncEventType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncEventTypeWithKey(id: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SyncEventType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSyncEventType(matching: var_query.withKey(SyncEventType.key(id: id)), headers: headers, options: options)
    }

    open func fetchSyncEventTypeWithKey(id: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SyncEventType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncEventTypeWithKey(id: id, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncOperationStatus(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<SyncOperationStatusType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try SyncOperationStatusType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus), headers: var_headers, options: var_options).entityList())
    }

    open func fetchSyncOperationStatus(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<SyncOperationStatusType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncOperationStatus(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncOperationStatusType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SyncOperationStatusType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<SyncOperationStatusType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.syncOperationStatus), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchSyncOperationStatusType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SyncOperationStatusType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncOperationStatusType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchSyncOperationStatusTypeWithKey(id: Int?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> SyncOperationStatusType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchSyncOperationStatusType(matching: var_query.withKey(SyncOperationStatusType.key(id: id)), headers: headers, options: options)
    }

    open func fetchSyncOperationStatusTypeWithKey(id: Int?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (SyncOperationStatusType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchSyncOperationStatusTypeWithKey(id: id, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABillingDocument(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxABillingDocumentType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxABillingDocumentType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxABillingDocument(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxABillingDocumentType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABillingDocument(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABillingDocumentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxABillingDocumentType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxABillingDocumentType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABillingDocument), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxABillingDocumentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxABillingDocumentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABillingDocumentType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABillingDocumentTypeWithKey(billingDocument: String?, salesOrganization: String?, distributionChannel: String?, division: String?, soldToParty: String?, bpCustomerNumber: String?, portion: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxABillingDocumentType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxABillingDocumentType(matching: var_query.withKey(XITSPDSFAxABillingDocumentType.key(billingDocument: billingDocument, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, portion: portion)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxABillingDocumentTypeWithKey(billingDocument: String?, salesOrganization: String?, distributionChannel: String?, division: String?, soldToParty: String?, bpCustomerNumber: String?, portion: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxABillingDocumentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABillingDocumentTypeWithKey(billingDocument: billingDocument, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, portion: portion, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABusinessRules(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxABusinessRulesType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxABusinessRulesType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxABusinessRules(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxABusinessRulesType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABusinessRules(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABusinessRulesType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxABusinessRulesType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxABusinessRulesType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxABusinessRules), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxABusinessRulesType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxABusinessRulesType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABusinessRulesType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxABusinessRulesTypeWithKey(tipo: String?, nome: String?, seq: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxABusinessRulesType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxABusinessRulesType(matching: var_query.withKey(XITSPDSFAxABusinessRulesType.key(tipo: tipo, nome: nome, seq: seq)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxABusinessRulesTypeWithKey(tipo: String?, nome: String?, seq: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxABusinessRulesType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxABusinessRulesTypeWithKey(tipo: tipo, nome: nome, seq: seq, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACenterSupplier(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACenterSupplierType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACenterSupplierType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACenterSupplier(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACenterSupplierType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACenterSupplier(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACenterSupplierType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACenterSupplierType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACenterSupplierType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACenterSupplier), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACenterSupplierType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACenterSupplierType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACenterSupplierType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACenterSupplierTypeWithKey(plant: String?, salesOrganization: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACenterSupplierType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACenterSupplierType(matching: var_query.withKey(XITSPDSFAxACenterSupplierType.key(plant: plant, salesOrganization: salesOrganization)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACenterSupplierTypeWithKey(plant: String?, salesOrganization: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACenterSupplierType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACenterSupplierTypeWithKey(plant: plant, salesOrganization: salesOrganization, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionList(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAConditionListType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAConditionListType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAConditionList(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAConditionListType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionList(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionListType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAConditionListType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionList), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAConditionListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionListType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionListTypeWithKey(conditionType: String?, accessSequence: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionListType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAConditionListType(matching: var_query.withKey(XITSPDSFAxAConditionListType.key(conditionType: conditionType, accessSequence: accessSequence)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAConditionListTypeWithKey(conditionType: String?, accessSequence: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionListTypeWithKey(conditionType: conditionType, accessSequence: accessSequence, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionScale(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAConditionScaleType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAConditionScaleType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAConditionScale(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAConditionScaleType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionScale(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionScaleType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionScaleType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAConditionScaleType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionScale), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAConditionScaleType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionScaleType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionScaleType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionScaleTypeWithKey(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionSequentialNumber: String?, conditionScaleLine: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionScaleType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAConditionScaleType(matching: var_query.withKey(XITSPDSFAxAConditionScaleType.key(conditionRecord: conditionRecord, conditionType: conditionType, accessSequence: accessSequence, conditionSequentialNumber: conditionSequentialNumber, conditionScaleLine: conditionScaleLine)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAConditionScaleTypeWithKey(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionSequentialNumber: String?, conditionScaleLine: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionScaleType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionScaleTypeWithKey(conditionRecord: conditionRecord, conditionType: conditionType, accessSequence: accessSequence, conditionSequentialNumber: conditionSequentialNumber, conditionScaleLine: conditionScaleLine, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionValue(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionValue {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAConditionValue>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAConditionValue(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionValue?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionValue(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionValueSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAConditionValue> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAConditionValue.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAConditionValueSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAConditionValueSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAConditionValue>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionValueSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAConditionValueWithKey(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionTable: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAConditionValue {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAConditionValue(matching: var_query.withKey(XITSPDSFAxAConditionValue.key(conditionRecord: conditionRecord, conditionType: conditionType, accessSequence: accessSequence, conditionTable: conditionTable)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAConditionValueWithKey(conditionRecord: String?, conditionType: String?, accessSequence: String?, conditionTable: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAConditionValue?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAConditionValueWithKey(conditionRecord: conditionRecord, conditionType: conditionType, accessSequence: accessSequence, conditionTable: conditionTable, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerEmailXML(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerEmailXMLType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerEmailXMLType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerEmailXML(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerEmailXMLType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerEmailXML(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerEmailXMLType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerEmailXMLType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerEmailXMLType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerEmailXML), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerEmailXMLType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerEmailXMLType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerEmailXMLType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerEmailXMLTypeWithKey(customer: String?, emailAddressXML: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerEmailXMLType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerEmailXMLType(matching: var_query.withKey(XITSPDSFAxACustomerEmailXMLType.key(customer: customer, emailAddressXML: emailAddressXML)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerEmailXMLTypeWithKey(customer: String?, emailAddressXML: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerEmailXMLType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerEmailXMLTypeWithKey(customer: customer, emailAddressXML: emailAddressXML, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerGrpText(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerGrpTextType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerGrpTextType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerGrpText(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerGrpTextType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerGrpText(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerGrpTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerGrpTextType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerGrpTextType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerGrpText), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerGrpTextType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerGrpTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerGrpTextType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerGrpTextTypeWithKey(customerGroup: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerGrpTextType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerGrpTextType(matching: var_query.withKey(XITSPDSFAxACustomerGrpTextType.key(customerGroup: customerGroup)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerGrpTextTypeWithKey(customerGroup: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerGrpTextType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerGrpTextTypeWithKey(customerGroup: customerGroup, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLatLong(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLatLong {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerLatLong>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerLatLong(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLatLong?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLatLong(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLatLongSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerLatLong> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerLatLong.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLatLongSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerLatLongSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerLatLong>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLatLongSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLatLongWithKey(customer: String?, salesorganization: String?, distributionchannel: String?, division: String?, customerobsid: String?, customerobsseq: String?, bpcustomernumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLatLong {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerLatLong(matching: var_query.withKey(XITSPDSFAxACustomerLatLong.key(customer: customer, salesorganization: salesorganization, distributionchannel: distributionchannel, division: division, customerobsid: customerobsid, customerobsseq: customerobsseq, bpcustomernumber: bpcustomernumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerLatLongWithKey(customer: String?, salesorganization: String?, distributionchannel: String?, division: String?, customerobsid: String?, customerobsseq: String?, bpcustomernumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLatLong?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLatLongWithKey(customer: customer, salesorganization: salesorganization, distributionchannel: distributionchannel, division: division, customerobsid: customerobsid, customerobsseq: customerobsseq, bpcustomernumber: bpcustomernumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstInv(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerLstInvType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerLstInvType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerLstInv(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerLstInvType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstInv(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstInvType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLstInvType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerLstInvType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstInv), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerLstInvType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLstInvType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstInvType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstInvTypeWithKey(customer: String?, salesOrganization: String?, bpCustomerNumber: String?, distributionChannel: String?, division: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLstInvType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerLstInvType(matching: var_query.withKey(XITSPDSFAxACustomerLstInvType.key(customer: customer, salesOrganization: salesOrganization, bpCustomerNumber: bpCustomerNumber, distributionChannel: distributionChannel, division: division)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerLstInvTypeWithKey(customer: String?, salesOrganization: String?, bpCustomerNumber: String?, distributionChannel: String?, division: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLstInvType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstInvTypeWithKey(customer: customer, salesOrganization: salesOrganization, bpCustomerNumber: bpCustomerNumber, distributionChannel: distributionChannel, division: division, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstSalOrd(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerLstSalOrdType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerLstSalOrdType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerLstSalOrd(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerLstSalOrdType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstSalOrd(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstSalOrdType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLstSalOrdType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerLstSalOrdType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerLstSalOrd), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerLstSalOrdType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLstSalOrdType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstSalOrdType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerLstSalOrdTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerLstSalOrdType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerLstSalOrdType(matching: var_query.withKey(XITSPDSFAxACustomerLstSalOrdType.key(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerLstSalOrdTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerLstSalOrdType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerLstSalOrdTypeWithKey(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerMaster(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerMasterType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerMasterType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerMaster(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerMasterType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerMaster(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerMasterType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerMasterType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerMasterType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerMaster), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerMasterType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerMasterType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerMasterType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerMasterTypeWithKey(customer: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerMasterType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerMasterType(matching: var_query.withKey(XITSPDSFAxACustomerMasterType.key(customer: customer)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerMasterTypeWithKey(customer: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerMasterType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerMasterTypeWithKey(customer: customer, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerSalesArea(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerSalesAreaType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerSalesAreaType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerSalesArea(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerSalesAreaType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerSalesArea(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerSalesAreaType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerSalesAreaType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerSalesAreaType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerSalesArea), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerSalesAreaType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerSalesAreaType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerSalesAreaType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerSalesAreaTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerSalesAreaType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerSalesAreaType(matching: var_query.withKey(XITSPDSFAxACustomerSalesAreaType.key(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerSalesAreaTypeWithKey(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerSalesAreaType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerSalesAreaTypeWithKey(customer: customer, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerShipTo(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerShipToType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxACustomerShipToType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxACustomerShipTo(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerShipToType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerShipTo(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerShipToType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerShipToType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxACustomerShipToType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxACustomerShipTo), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxACustomerShipToType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerShipToType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerShipToType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxACustomerShipToTypeWithKey(customer: String?, bpCustomerNumber: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxACustomerShipToType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxACustomerShipToType(matching: var_query.withKey(XITSPDSFAxACustomerShipToType.key(customer: customer, bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, partnerCounter: partnerCounter, partnerFunction: partnerFunction)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxACustomerShipToTypeWithKey(customer: String?, bpCustomerNumber: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxACustomerShipToType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxACustomerShipToTypeWithKey(customer: customer, bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, partnerCounter: partnerCounter, partnerFunction: partnerFunction, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxADocuments(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxADocumentsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxADocumentsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxADocuments(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxADocumentsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxADocuments(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxADocumentsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxADocumentsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxADocumentsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxADocuments), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxADocumentsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxADocumentsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxADocumentsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxADocumentsTypeWithKey(linkedSAPObjectKey: String?, documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, archiveDocumentID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxADocumentsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxADocumentsType(matching: var_query.withKey(XITSPDSFAxADocumentsType.key(linkedSAPObjectKey: linkedSAPObjectKey, documentInfoRecordDocType: documentInfoRecordDocType, documentInfoRecordDocNumber: documentInfoRecordDocNumber, documentInfoRecordDocVersion: documentInfoRecordDocVersion, documentInfoRecordDocPart: documentInfoRecordDocPart, archiveDocumentID: archiveDocumentID)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxADocumentsTypeWithKey(linkedSAPObjectKey: String?, documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, archiveDocumentID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxADocumentsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxADocumentsTypeWithKey(linkedSAPObjectKey: linkedSAPObjectKey, documentInfoRecordDocType: documentInfoRecordDocType, documentInfoRecordDocNumber: documentInfoRecordDocNumber, documentInfoRecordDocVersion: documentInfoRecordDocVersion, documentInfoRecordDocPart: documentInfoRecordDocPart, archiveDocumentID: archiveDocumentID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAHierarchy(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAHierarchyType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAHierarchyType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAHierarchy(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAHierarchyType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAHierarchy(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAHierarchyType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAHierarchyType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAHierarchyType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAHierarchy), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAHierarchyType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAHierarchyType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAHierarchyType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAHierarchyTypeWithKey(partner: String?, organization: String?, parent: String?, child: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAHierarchyType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAHierarchyType(matching: var_query.withKey(XITSPDSFAxAHierarchyType.key(partner: partner, organization: organization, parent: parent, child: child)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAHierarchyTypeWithKey(partner: String?, organization: String?, parent: String?, child: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAHierarchyType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAHierarchyTypeWithKey(partner: partner, organization: organization, parent: parent, child: child, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAIncoTerms(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAIncoTermsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAIncoTermsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAIncoTerms(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAIncoTermsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAIncoTerms(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAIncoTermsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAIncoTermsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAIncoTermsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAIncoTerms), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAIncoTermsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAIncoTermsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAIncoTermsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAIncoTermsTypeWithKey(incotermsClassification: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAIncoTermsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAIncoTermsType(matching: var_query.withKey(XITSPDSFAxAIncoTermsType.key(incotermsClassification: incotermsClassification)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAIncoTermsTypeWithKey(incotermsClassification: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAIncoTermsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAIncoTermsTypeWithKey(incotermsClassification: incotermsClassification, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAInvoice(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAInvoiceType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAInvoiceType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAInvoice(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAInvoiceType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAInvoice(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAInvoiceType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAInvoiceType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAInvoiceType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAInvoice), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAInvoiceType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAInvoiceType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAInvoiceType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAInvoiceTypeWithKey(salesDocument: String?, soldToParty: String?, bpCustomerNumber: String?, billingDocument: String?, brNotaFiscal: String?, brNFeNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAInvoiceType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAInvoiceType(matching: var_query.withKey(XITSPDSFAxAInvoiceType.key(salesDocument: salesDocument, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, billingDocument: billingDocument, brNotaFiscal: brNotaFiscal, brNFeNumber: brNFeNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAInvoiceTypeWithKey(salesDocument: String?, soldToParty: String?, bpCustomerNumber: String?, billingDocument: String?, brNotaFiscal: String?, brNFeNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAInvoiceType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAInvoiceTypeWithKey(salesDocument: salesDocument, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, billingDocument: billingDocument, brNotaFiscal: brNotaFiscal, brNFeNumber: brNFeNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsg(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAMsgType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAMsgType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAMsg(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAMsgType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsg(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsgPrtnr(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAMsgPrtnrType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAMsgPrtnrType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAMsgPrtnr(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAMsgPrtnrType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsgPrtnr(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsgPrtnrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAMsgPrtnrType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAMsgPrtnrType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsgPrtnr), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAMsgPrtnrType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAMsgPrtnrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsgPrtnrType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsgPrtnrTypeWithKey(messageID: String?, salesOrganization: String?, partnerID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAMsgPrtnrType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAMsgPrtnrType(matching: var_query.withKey(XITSPDSFAxAMsgPrtnrType.key(messageID: messageID, salesOrganization: salesOrganization, partnerID: partnerID)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAMsgPrtnrTypeWithKey(messageID: String?, salesOrganization: String?, partnerID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAMsgPrtnrType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsgPrtnrTypeWithKey(messageID: messageID, salesOrganization: salesOrganization, partnerID: partnerID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsgType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAMsgType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAMsgType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAMsg), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAMsgType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAMsgType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsgType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAMsgTypeWithKey(messageID: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAMsgType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAMsgType(matching: var_query.withKey(XITSPDSFAxAMsgType.key(messageID: messageID)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAMsgTypeWithKey(messageID: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAMsgType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAMsgTypeWithKey(messageID: messageID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANameSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxANameSetType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxANameSetType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxANameSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxANameSetType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANameSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANameSetType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANameSetType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxANameSetType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANameSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxANameSetType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANameSetType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANameSetType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANameSetTypeWithKey(conditionType: String?, accessSequence: String?, sapField: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANameSetType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxANameSetType(matching: var_query.withKey(XITSPDSFAxANameSetType.key(conditionType: conditionType, accessSequence: accessSequence, sapField: sapField)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxANameSetTypeWithKey(conditionType: String?, accessSequence: String?, sapField: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANameSetType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANameSetTypeWithKey(conditionType: conditionType, accessSequence: accessSequence, sapField: sapField, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotifications(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxANotificationsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxANotificationsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxANotifications(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxANotificationsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotifications(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotificationsStat(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxANotificationsStatType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxANotificationsStatType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxANotificationsStat(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxANotificationsStatType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotificationsStat(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotificationsStatType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANotificationsStatType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxANotificationsStatType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotificationsStat), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxANotificationsStatType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANotificationsStatType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotificationsStatType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotificationsStatTypeWithKey(notification: String?, istat: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANotificationsStatType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxANotificationsStatType(matching: var_query.withKey(XITSPDSFAxANotificationsStatType.key(notification: notification, istat: istat, salesOrganization: salesOrganization, distributionChannel: distributionChannel, customer: customer, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxANotificationsStatTypeWithKey(notification: String?, istat: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANotificationsStatType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotificationsStatTypeWithKey(notification: notification, istat: istat, salesOrganization: salesOrganization, distributionChannel: distributionChannel, customer: customer, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotificationsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANotificationsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxANotificationsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxANotifications), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxANotificationsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANotificationsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotificationsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxANotificationsTypeWithKey(notification: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxANotificationsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxANotificationsType(matching: var_query.withKey(XITSPDSFAxANotificationsType.key(notification: notification, salesOrganization: salesOrganization, distributionChannel: distributionChannel, customer: customer, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxANotificationsTypeWithKey(notification: String?, salesOrganization: String?, distributionChannel: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxANotificationsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxANotificationsTypeWithKey(notification: notification, salesOrganization: salesOrganization, distributionChannel: distributionChannel, customer: customer, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerLastSales(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPartnerLastSalesType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPartnerLastSalesType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPartnerLastSales(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPartnerLastSalesType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerLastSales(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerLastSalesType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerLastSalesType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPartnerLastSalesType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerLastSales), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPartnerLastSalesType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerLastSalesType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerLastSalesType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerLastSalesTypeWithKey(salesOrder: String?, salesOrganization: String?, soldToParty: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerLastSalesType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPartnerLastSalesType(matching: var_query.withKey(XITSPDSFAxAPartnerLastSalesType.key(salesOrder: salesOrder, salesOrganization: salesOrganization, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPartnerLastSalesTypeWithKey(salesOrder: String?, salesOrganization: String?, soldToParty: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerLastSalesType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerLastSalesTypeWithKey(salesOrder: salesOrder, salesOrganization: salesOrganization, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerTypeWithKey(customer: String?, bpCustomerNumber: String?, partnerFunction: String?, businessPartnerAddressID: String?, salesOrganization: String?, distributionChannel: String?, division: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPartnerType(matching: var_query.withKey(XITSPDSFAxAPartnerType.key(customer: customer, bpCustomerNumber: bpCustomerNumber, partnerFunction: partnerFunction, businessPartnerAddressID: businessPartnerAddressID, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPartnerTypeWithKey(customer: String?, bpCustomerNumber: String?, partnerFunction: String?, businessPartnerAddressID: String?, salesOrganization: String?, distributionChannel: String?, division: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerTypeWithKey(customer: customer, bpCustomerNumber: bpCustomerNumber, partnerFunction: partnerFunction, businessPartnerAddressID: businessPartnerAddressID, salesOrganization: salesOrganization, distributionChannel: distributionChannel, division: division, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerUser(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPartnerUserType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPartnerUserType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPartnerUser(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPartnerUserType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerUser(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerUserType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerUserType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPartnerUserType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartnerUser), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPartnerUserType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerUserType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerUserType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnerUserTypeWithKey(userLogin: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnerUserType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPartnerUserType(matching: var_query.withKey(XITSPDSFAxAPartnerUserType.key(userLogin: userLogin, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPartnerUserTypeWithKey(userLogin: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnerUserType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnerUserTypeWithKey(userLogin: userLogin, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartners(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPartnersType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPartnersType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPartners(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPartnersType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartners(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnersType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnersType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPartnersType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPartners), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPartnersType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnersType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnersType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPartnersTypeWithKey(bpCustomerNumber: String?, salesOrganization: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPartnersType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPartnersType(matching: var_query.withKey(XITSPDSFAxAPartnersType.key(bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPartnersTypeWithKey(bpCustomerNumber: String?, salesOrganization: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPartnersType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPartnersTypeWithKey(bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPaymentTerms(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPaymentTermsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPaymentTermsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPaymentTerms(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPaymentTermsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPaymentTerms(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPaymentTermsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPaymentTermsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPaymentTermsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPaymentTerms), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPaymentTermsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPaymentTermsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPaymentTermsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPaymentTermsTypeWithKey(paymentTerms: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPaymentTermsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPaymentTermsType(matching: var_query.withKey(XITSPDSFAxAPaymentTermsType.key(paymentTerms: paymentTerms)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPaymentTermsTypeWithKey(paymentTerms: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPaymentTermsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPaymentTermsTypeWithKey(paymentTerms: paymentTerms, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPricelists(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAPricelistsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAPricelistsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAPricelists(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAPricelistsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPricelists(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPricelistsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPricelistsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAPricelistsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAPricelists), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAPricelistsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPricelistsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPricelistsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAPricelistsTypeWithKey(priceListType: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAPricelistsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAPricelistsType(matching: var_query.withKey(XITSPDSFAxAPricelistsType.key(priceListType: priceListType)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAPricelistsTypeWithKey(priceListType: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAPricelistsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAPricelistsTypeWithKey(priceListType: priceListType, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel1(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel1Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel1Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel1(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel1Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel1(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel1Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel1Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel1Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel1), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel1Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel1Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel1Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel1TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel1Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel1Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel1Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel1TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel1Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel1TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel2(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel2Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel2Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel2(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel2Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel2(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel2Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel2Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel2Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel2), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel2Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel2Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel2Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel2TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel2Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel2Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel2Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel2TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel2Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel2TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel3(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel3Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel3Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel3(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel3Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel3(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel3Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel3Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel3Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel3), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel3Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel3Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel3Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel3TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel3Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel3Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel3Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel3TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel3Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel3TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel4(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel4Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel4Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel4(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel4Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel4(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel4Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel4Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel4Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel4), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel4Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel4Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel4Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel4TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel4Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel4Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel4Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel4TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel4Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel4TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel5(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel5Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel5Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel5(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel5Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel5(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel5Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel5Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel5Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel5), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel5Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel5Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel5Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel5TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel5Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel5Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel5Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel5TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel5Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel5TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel6(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel6Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel6Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel6(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel6Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel6(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel6Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel6Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel6Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel6), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel6Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel6Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel6Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel6TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel6Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel6Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel6Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel6TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel6Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel6TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel7(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel7Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel7Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel7(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel7Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel7(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel7Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel7Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel7Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel7), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel7Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel7Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel7Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel7TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel7Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel7Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel7Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel7TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel7Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel7TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel8(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel8Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel8Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel8(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel8Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel8(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel8Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel8Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel8Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel8), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel8Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel8Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel8Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel8TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel8Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel8Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel8Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel8TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel8Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel8TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel9(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductHieLevel9Type> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductHieLevel9Type.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductHieLevel9(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel9Type>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel9(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel9Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel9Type {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductHieLevel9Type>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductHieLevel9), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductHieLevel9Type(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel9Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel9Type(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductHieLevel9TypeWithKey(productHierarchyNode: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductHieLevel9Type {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductHieLevel9Type(matching: var_query.withKey(XITSPDSFAxAProductHieLevel9Type.key(productHierarchyNode: productHierarchyNode)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductHieLevel9TypeWithKey(productHierarchyNode: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductHieLevel9Type?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductHieLevel9TypeWithKey(productHierarchyNode: productHierarchyNode, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductMaster(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductMasterType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductMasterType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductMaster(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductMasterType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductMaster(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductMasterType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductMasterType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductMasterType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductMaster), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductMasterType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductMasterType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductMasterType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductMasterTypeWithKey(product: String?, salesOrganization: String?, distributionChannel: String?, plant: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductMasterType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductMasterType(matching: var_query.withKey(XITSPDSFAxAProductMasterType.key(product: product, salesOrganization: salesOrganization, distributionChannel: distributionChannel, plant: plant)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductMasterTypeWithKey(product: String?, salesOrganization: String?, distributionChannel: String?, plant: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductMasterType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductMasterTypeWithKey(product: product, salesOrganization: salesOrganization, distributionChannel: distributionChannel, plant: plant, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductReqMRPSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAProductReqMRPType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAProductReqMRPType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAProductReqMRPSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAProductReqMRPType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductReqMRPSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductReqMRPType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductReqMRPType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAProductReqMRPType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAProductReqMRPSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAProductReqMRPType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductReqMRPType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductReqMRPType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAProductReqMRPTypeWithKey(distributionchannel: String?, mrpelement: String?, plant: String?, product: String?, requirementsdate: LocalDateTime?, salesorganization: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAProductReqMRPType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAProductReqMRPType(matching: var_query.withKey(XITSPDSFAxAProductReqMRPType.key(distributionchannel: distributionchannel, mrpelement: mrpelement, plant: plant, product: product, requirementsdate: requirementsdate, salesorganization: salesorganization)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAProductReqMRPTypeWithKey(distributionchannel: String?, mrpelement: String?, plant: String?, product: String?, requirementsdate: LocalDateTime?, salesorganization: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAProductReqMRPType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAProductReqMRPTypeWithKey(distributionchannel: distributionchannel, mrpelement: mrpelement, plant: plant, product: product, requirementsdate: requirementsdate, salesorganization: salesorganization, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAQMNote(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAQMNote {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxAQMNote>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxAQMNote(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAQMNote?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAQMNote(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAQMNoteSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxAQMNote> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxAQMNote.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxAQMNoteSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxAQMNoteSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxAQMNote>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAQMNoteSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxAQMNoteWithKey(notification: String?, bpcustomernumber: String?, customer: String?, salesorganization: String?, notificationtype: String?, distributionchannel: String?, activedivision: String?, material: String?, orderid: String?, notificationexternalqty: BigDecimal?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxAQMNote {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxAQMNote(matching: var_query.withKey(XITSPDSFAxAQMNote.key(notification: notification, bpcustomernumber: bpcustomernumber, customer: customer, salesorganization: salesorganization, notificationtype: notificationtype, distributionchannel: distributionchannel, activedivision: activedivision, material: material, orderid: orderid, notificationexternalqty: notificationexternalqty)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxAQMNoteWithKey(notification: String?, bpcustomernumber: String?, customer: String?, salesorganization: String?, notificationtype: String?, distributionchannel: String?, activedivision: String?, material: String?, orderid: String?, notificationexternalqty: BigDecimal?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxAQMNote?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxAQMNoteWithKey(notification: notification, bpcustomernumber: bpcustomernumber, customer: customer, salesorganization: salesorganization, notificationtype: notificationtype, distributionchannel: distributionchannel, activedivision: activedivision, material: material, orderid: orderid, notificationexternalqty: notificationexternalqty, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesAmounts(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesAmountsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesAmountsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesAmounts(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesAmountsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesAmounts(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesAmountsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesAmountsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesAmountsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesAmounts), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesAmountsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesAmountsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesAmountsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesAmountsTypeWithKey(salesDocument: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, conditionType: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesAmountsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesAmountsType(matching: var_query.withKey(XITSPDSFAxASalesAmountsType.key(salesDocument: salesDocument, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, conditionType: conditionType)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesAmountsTypeWithKey(salesDocument: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, conditionType: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesAmountsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesAmountsTypeWithKey(salesDocument: salesDocument, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, conditionType: conditionType, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesDocType(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesDocTypeType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesDocTypeType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesDocType(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesDocTypeType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesDocType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesDocTypeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesDocTypeType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesDocTypeType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesDocType), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesDocTypeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesDocTypeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesDocTypeType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesDocTypeTypeWithKey(salesDocumentType: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesDocTypeType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesDocTypeType(matching: var_query.withKey(XITSPDSFAxASalesDocTypeType.key(salesDocumentType: salesDocumentType)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesDocTypeTypeWithKey(salesDocumentType: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesDocTypeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesDocTypeTypeWithKey(salesDocumentType: salesDocumentType, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesItemAmounts(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesItemAmountsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesItemAmountsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesItemAmounts(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesItemAmountsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesItemAmounts(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesItemAmountsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesItemAmountsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesItemAmountsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesItemAmounts), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesItemAmountsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesItemAmountsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesItemAmountsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesItemAmountsTypeWithKey(salesOrder: String?, salesOrderItem: String?, bpCustomerNumber: String?, conditionType: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesItemAmountsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesItemAmountsType(matching: var_query.withKey(XITSPDSFAxASalesItemAmountsType.key(salesOrder: salesOrder, salesOrderItem: salesOrderItem, bpCustomerNumber: bpCustomerNumber, conditionType: conditionType)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesItemAmountsTypeWithKey(salesOrder: String?, salesOrderItem: String?, bpCustomerNumber: String?, conditionType: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesItemAmountsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesItemAmountsTypeWithKey(salesOrder: salesOrder, salesOrderItem: salesOrderItem, bpCustomerNumber: bpCustomerNumber, conditionType: conditionType, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdItPrice(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdItPriceType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdItPriceType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrdItPrice(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdItPriceType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdItPrice(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdItPriceType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdItPriceType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdItPriceType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdItPrice), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdItPriceType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdItPriceType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdItPriceType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdItPriceTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, salesOrderItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdItPriceType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdItPriceType(matching: var_query.withKey(XITSPDSFAxASalesOrdItPriceType.key(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, salesOrderItem: salesOrderItem, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdItPriceTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, salesOrderItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdItPriceType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdItPriceTypeWithKey(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, salesOrderItem: salesOrderItem, pricingProcedureStep: pricingProcedureStep, pricingProcedureCounter: pricingProcedureCounter, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrderPartner(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrderPartnerType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrderPartnerType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrderPartner(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrderPartnerType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrderPartner(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrderPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrderPartnerType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrderPartnerType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrderPartner), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrderPartnerType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrderPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrderPartnerType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrderPartnerTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, partnerFunction: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrderPartnerType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrderPartnerType(matching: var_query.withKey(XITSPDSFAxASalesOrderPartnerType.key(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, partnerFunction: partnerFunction)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrderPartnerTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, partnerFunction: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrderPartnerType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrderPartnerTypeWithKey(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, partnerFunction: partnerFunction, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrders(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdersType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrders(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrders(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersItens(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersItensType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdersItensType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrdersItens(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersItensType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersItens(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersItensType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersItensType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdersItensType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersItens), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdersItensType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersItensType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersItensType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersItensTypeWithKey(salesOrder: String?, salesOrderItem: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersItensType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdersItensType(matching: var_query.withKey(XITSPDSFAxASalesOrdersItensType.key(salesOrder: salesOrder, salesOrderItem: salesOrderItem, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, customer: customer, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdersItensTypeWithKey(salesOrder: String?, salesOrderItem: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, customer: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersItensType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersItensTypeWithKey(salesOrder: salesOrder, salesOrderItem: salesOrderItem, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, customer: customer, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObs(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersObs {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdersObs>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdersObs(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersObs?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObs(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObsN(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersObsN {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdersObsN>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdersObsN(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersObsN?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObsN(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObsNWithKey(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersObsN {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdersObsN(matching: var_query.withKey(XITSPDSFAxASalesOrdersObsN.key(salesorder: salesorder, salesorganization: salesorganization, distributionchannel: distributionchannel, organizationdivision: organizationdivision, salesorderobsid: salesorderobsid, salesorderobsseq: salesorderobsseq, bpcustomernumber: bpcustomernumber, soldToParty: soldToParty)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdersObsNWithKey(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersObsN?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObsNWithKey(salesorder: salesorder, salesorganization: salesorganization, distributionchannel: distributionchannel, organizationdivision: organizationdivision, salesorderobsid: salesorderobsid, salesorderobsseq: salesorderobsseq, bpcustomernumber: bpcustomernumber, soldToParty: soldToParty, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObsSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersObs> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdersObs.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrdersObsSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersObs>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObsSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObsSetN(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersObsN> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdersObsN.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersObsSetN), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrdersObsSetN(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersObsN>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObsSetN(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersObsWithKey(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersObs {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdersObs(matching: var_query.withKey(XITSPDSFAxASalesOrdersObs.key(salesorder: salesorder, salesorganization: salesorganization, distributionchannel: distributionchannel, organizationdivision: organizationdivision, salesorderobsid: salesorderobsid, salesorderobsseq: salesorderobsseq, bpcustomernumber: bpcustomernumber, soldToParty: soldToParty)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdersObsWithKey(salesorder: String?, salesorganization: String?, distributionchannel: String?, organizationdivision: String?, salesorderobsid: String?, salesorderobsseq: String?, bpcustomernumber: String?, soldToParty: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersObs?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersObsWithKey(salesorder: salesorder, salesorganization: salesorganization, distributionchannel: distributionchannel, organizationdivision: organizationdivision, salesorderobsid: salesorderobsid, salesorderobsseq: salesorderobsseq, bpcustomernumber: bpcustomernumber, soldToParty: soldToParty, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersResns(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersResnsType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASalesOrdersResnsType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASalesOrdersResns(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersResnsType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersResns(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersResnsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersResnsType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdersResnsType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrdersResns), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdersResnsType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersResnsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersResnsType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersResnsTypeWithKey(sdDocumentReason: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersResnsType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdersResnsType(matching: var_query.withKey(XITSPDSFAxASalesOrdersResnsType.key(sdDocumentReason: sdDocumentReason)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdersResnsTypeWithKey(sdDocumentReason: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersResnsType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersResnsTypeWithKey(sdDocumentReason: sdDocumentReason, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASalesOrdersType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASalesOrders), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASalesOrdersType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASalesOrdersTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASalesOrdersType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASalesOrdersType(matching: var_query.withKey(XITSPDSFAxASalesOrdersType.key(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASalesOrdersTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASalesOrdersType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASalesOrdersTypeWithKey(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, soldToParty: soldToParty, bpCustomerNumber: bpCustomerNumber, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASoldVolume(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASoldVolumeType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxASoldVolumeType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxASoldVolume(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASoldVolumeType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASoldVolume(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASoldVolumeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASoldVolumeType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxASoldVolumeType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxASoldVolume), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxASoldVolumeType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASoldVolumeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASoldVolumeType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxASoldVolumeTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, billingDocument: String?, partnerActual: String?, partnerHistorical: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxASoldVolumeType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxASoldVolumeType(matching: var_query.withKey(XITSPDSFAxASoldVolumeType.key(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, billingDocument: billingDocument, partnerActual: partnerActual, partnerHistorical: partnerHistorical)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxASoldVolumeTypeWithKey(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, billingDocument: String?, partnerActual: String?, partnerHistorical: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxASoldVolumeType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxASoldVolumeTypeWithKey(salesOrder: salesOrder, salesOrganization: salesOrganization, distributionChannel: distributionChannel, organizationDivision: organizationDivision, billingDocument: billingDocument, partnerActual: partnerActual, partnerHistorical: partnerHistorical, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxList(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxATaxListType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxATaxListType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxATaxList(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxATaxListType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxList(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxListType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxATaxListType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxList), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxATaxListType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxListType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxListTypeWithKey(taxType: String?, groupPriority: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxListType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxATaxListType(matching: var_query.withKey(XITSPDSFAxATaxListType.key(taxType: taxType, groupPriority: groupPriority)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxATaxListTypeWithKey(taxType: String?, groupPriority: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxListType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxListTypeWithKey(taxType: taxType, groupPriority: groupPriority, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxNameset(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxATaxNamesetType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxATaxNamesetType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxATaxNameset(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxATaxNamesetType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxNameset(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxNamesetType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxNamesetType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxATaxNamesetType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxNameset), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxATaxNamesetType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxNamesetType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxNamesetType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxNamesetTypeWithKey(taxType: String?, groupPriority: String?, sapField: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxNamesetType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxATaxNamesetType(matching: var_query.withKey(XITSPDSFAxATaxNamesetType.key(taxType: taxType, groupPriority: groupPriority, sapField: sapField)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxATaxNamesetTypeWithKey(taxType: String?, groupPriority: String?, sapField: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxNamesetType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxNamesetTypeWithKey(taxType: taxType, groupPriority: groupPriority, sapField: sapField, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxValue(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxATaxValueType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try XITSPDSFAxATaxValueType.array(from: self.executeQuery(var_query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue), headers: var_headers, options: var_options).entityList())
    }

    open func fetchXITSPDSFAxATaxValue(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxATaxValueType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxValue(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxValueType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxValueType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<XITSPDSFAxATaxValueType>.from(self.executeQuery(query.fromDefault(ITSPDSFAAPISRVEntitiesMetadata.EntitySets.xITSPDSFAxATaxValue), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchXITSPDSFAxATaxValueType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxValueType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxValueType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchXITSPDSFAxATaxValueTypeWithKey(taxType: String?, groupPriority: String?, shipfrom: String?, shipto: String?, matnr: String?, nbmcode: String?, stgrp: String?, subkey01: String?, subkey02: String?, subkey03: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> XITSPDSFAxATaxValueType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchXITSPDSFAxATaxValueType(matching: var_query.withKey(XITSPDSFAxATaxValueType.key(taxType: taxType, groupPriority: groupPriority, shipfrom: shipfrom, shipto: shipto, matnr: matnr, nbmcode: nbmcode, stgrp: stgrp, subkey01: subkey01, subkey02: subkey02, subkey03: subkey03)), headers: headers, options: options)
    }

    open func fetchXITSPDSFAxATaxValueTypeWithKey(taxType: String?, groupPriority: String?, shipfrom: String?, shipto: String?, matnr: String?, nbmcode: String?, stgrp: String?, subkey01: String?, subkey02: String?, subkey03: String?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (XITSPDSFAxATaxValueType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchXITSPDSFAxATaxValueTypeWithKey(taxType: taxType, groupPriority: groupPriority, shipfrom: shipfrom, shipto: shipto, matnr: matnr, nbmcode: nbmcode, stgrp: stgrp, subkey01: subkey01, subkey02: subkey02, subkey03: subkey03, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchIBillingDocumentBasicStdVH")
    open func iBillingDocumentBasicStdVH(query: DataQuery = DataQuery()) throws -> Array<IBillingDocumentBasicStdVHType> {
        return try self.fetchIBillingDocumentBasicStdVH(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchIBillingDocumentBasicStdVH")
    open func iBillingDocumentBasicStdVH(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<IBillingDocumentBasicStdVHType>?, Error?) -> Void) {
        self.fetchIBillingDocumentBasicStdVH(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchICompanyCodeStdVH")
    open func iCompanyCodeStdVH(query: DataQuery = DataQuery()) throws -> Array<ICompanyCodeStdVHType> {
        return try self.fetchICompanyCodeStdVH(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchICompanyCodeStdVH")
    open func iCompanyCodeStdVH(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<ICompanyCodeStdVHType>?, Error?) -> Void) {
        self.fetchICompanyCodeStdVH(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchICustomerVH")
    open func iCustomerVH(query: DataQuery = DataQuery()) throws -> Array<ICustomerVHType> {
        return try self.fetchICustomerVH(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchICustomerVH")
    open func iCustomerVH(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<ICustomerVHType>?, Error?) -> Void) {
        self.fetchICustomerVH(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open func parameterValue(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<ParameterValue> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ArrayConverter.convert(ComplexValueList.castRequired(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getParameterValue, ParameterList(capacity: (2 as Int)).with(name: "conditionType", value: StringValue.of(optional: conditionType)).with(name: "searchParameters", value: StringValue.of(optional: searchParameters))), headers: var_headers, options: var_options).result).toArray(), Array<ParameterValue>())
    }

    open func parameterValue(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<ParameterValue>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.parameterValue(conditionType: conditionType, searchParameters: searchParameters, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func productPrice(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<PricingValue> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ArrayConverter.convert(ComplexValueList.castRequired(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductPrice, ParameterList(capacity: (2 as Int)).with(name: "conditionType", value: StringValue.of(optional: conditionType)).with(name: "searchParameters", value: StringValue.of(optional: searchParameters))), headers: var_headers, options: var_options).result).toArray(), Array<PricingValue>())
    }

    open func productPrice(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<PricingValue>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.productPrice(conditionType: conditionType, searchParameters: searchParameters, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func productTaxValue(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<TaxValue> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ArrayConverter.convert(ComplexValueList.castRequired(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getProductTaxValue, ParameterList(capacity: (2 as Int)).with(name: "conditionType", value: StringValue.of(optional: conditionType)).with(name: "searchParameters", value: StringValue.of(optional: searchParameters))), headers: var_headers, options: var_options).result).toArray(), Array<TaxValue>())
    }

    open func productTaxValue(conditionType: String?, searchParameters: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<TaxValue>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.productTaxValue(conditionType: conditionType, searchParameters: searchParameters, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func readCustomerSalesAreas(bpCustomerNumber: String, salesOrganization: String, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxACustomerSalesAreaType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ArrayConverter.convert(EntityValueList.castRequired(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getReadCustomerSalesAreas, ParameterList(capacity: (2 as Int)).with(name: "bpCustomerNumber", value: StringValue.of(bpCustomerNumber)).with(name: "salesOrganization", value: StringValue.of(salesOrganization))), headers: var_headers, options: var_options).result).toArray(), Array<XITSPDSFAxACustomerSalesAreaType>())
    }

    open func readCustomerSalesAreas(bpCustomerNumber: String, salesOrganization: String, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxACustomerSalesAreaType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.readCustomerSalesAreas(bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadata(service: self, fetcher: nil, options: ITSPDSFAAPISRVEntitiesMetadataParser.options)
            ITSPDSFAAPISRVEntitiesMetadataChanges.merge(metadata: self.metadata)
        }
    }

    open func salesOrderItensHistory(materialCode: String, customerCode: String, organizationCode: String, bpCustomerCode: String, conditionTypes: String?, numResults: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Array<XITSPDSFAxASalesOrdersType> {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ArrayConverter.convert(EntityValueList.castRequired(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.getSalesOrderItensHistory, ParameterList(capacity: (6 as Int)).with(name: "materialCode", value: StringValue.of(materialCode)).with(name: "customerCode", value: StringValue.of(customerCode)).with(name: "organizationCode", value: StringValue.of(organizationCode)).with(name: "bpCustomerCode", value: StringValue.of(bpCustomerCode)).with(name: "conditionTypes", value: StringValue.of(optional: conditionTypes)).with(name: "numResults", value: StringValue.of(optional: numResults))), headers: var_headers, options: var_options).result).toArray(), Array<XITSPDSFAxASalesOrdersType>())
    }

    open func salesOrderItensHistory(materialCode: String, customerCode: String, organizationCode: String, bpCustomerCode: String, conditionTypes: String?, numResults: String?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersType>?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.salesOrderItensHistory(materialCode: materialCode, customerCode: customerCode, organizationCode: organizationCode, bpCustomerCode: bpCustomerCode, conditionTypes: conditionTypes, numResults: numResults, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutCharge")
    open func salesOrderWithoutCharge(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargeType> {
        return try self.fetchSalesOrderWithoutCharge(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutCharge")
    open func salesOrderWithoutCharge(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargeType>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutCharge(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItem")
    open func salesOrderWithoutChargeItem(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargeItemType> {
        return try self.fetchSalesOrderWithoutChargeItem(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItem")
    open func salesOrderWithoutChargeItem(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargeItemType>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargeItem(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItemPartner")
    open func salesOrderWithoutChargeItemPartner(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargeItemPartnerType> {
        return try self.fetchSalesOrderWithoutChargeItemPartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItemPartner")
    open func salesOrderWithoutChargeItemPartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargeItemPartnerType>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargeItemPartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItemPricings")
    open func salesOrderWithoutChargeItemPricings(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargeItemPricing> {
        return try self.fetchSalesOrderWithoutChargeItemPricings(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeItemPricings")
    open func salesOrderWithoutChargeItemPricings(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargeItemPricing>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargeItemPricings(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargePartner")
    open func salesOrderWithoutChargePartner(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargePartnerType> {
        return try self.fetchSalesOrderWithoutChargePartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargePartner")
    open func salesOrderWithoutChargePartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargePartnerType>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargePartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargePricings")
    open func salesOrderWithoutChargePricings(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargePricing> {
        return try self.fetchSalesOrderWithoutChargePricings(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargePricings")
    open func salesOrderWithoutChargePricings(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargePricing>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargePricings(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeScheduleLine")
    open func salesOrderWithoutChargeScheduleLine(query: DataQuery = DataQuery()) throws -> Array<SalesOrderWithoutChargeScheduleLinetype> {
        return try self.fetchSalesOrderWithoutChargeScheduleLine(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesOrderWithoutChargeScheduleLine")
    open func salesOrderWithoutChargeScheduleLine(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesOrderWithoutChargeScheduleLinetype>?, Error?) -> Void) {
        self.fetchSalesOrderWithoutChargeScheduleLine(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItemPartner")
    open func salesQuotationItemPartner(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationItemPartnerType> {
        return try self.fetchSalesQuotationItemPartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItemPartner")
    open func salesQuotationItemPartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationItemPartnerType>?, Error?) -> Void) {
        self.fetchSalesQuotationItemPartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItemPricing")
    open func salesQuotationItemPricing(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationItemPricingType> {
        return try self.fetchSalesQuotationItemPricing(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItemPricing")
    open func salesQuotationItemPricing(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationItemPricingType>?, Error?) -> Void) {
        self.fetchSalesQuotationItemPricing(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItens")
    open func salesQuotationItens(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationItensType> {
        return try self.fetchSalesQuotationItens(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationItens")
    open func salesQuotationItens(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationItensType>?, Error?) -> Void) {
        self.fetchSalesQuotationItens(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationPartner")
    open func salesQuotationPartner(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationPartnerType> {
        return try self.fetchSalesQuotationPartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationPartner")
    open func salesQuotationPartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationPartnerType>?, Error?) -> Void) {
        self.fetchSalesQuotationPartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationPricing")
    open func salesQuotationPricing(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationPricingType> {
        return try self.fetchSalesQuotationPricing(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotationPricing")
    open func salesQuotationPricing(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationPricingType>?, Error?) -> Void) {
        self.fetchSalesQuotationPricing(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotations")
    open func salesQuotations(query: DataQuery = DataQuery()) throws -> Array<SalesQuotationsType> {
        return try self.fetchSalesQuotations(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSalesQuotations")
    open func salesQuotations(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SalesQuotationsType>?, Error?) -> Void) {
        self.fetchSalesQuotations(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open func sumBillingDocumentsNetValues(bpCustomerNumber: String, salesOrganization: String, statusColorDesc: String, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> BigDecimal {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try DecimalValue.unwrap(self.executeQuery(var_query.invoke(ITSPDSFAAPISRVEntitiesMetadata.FunctionImports.sumBillingDocumentsNetValues, ParameterList(capacity: (3 as Int)).with(name: "bpCustomerNumber", value: StringValue.of(bpCustomerNumber)).with(name: "salesOrganization", value: StringValue.of(salesOrganization)).with(name: "statusColorDesc", value: StringValue.of(statusColorDesc))), headers: var_headers, options: var_options).result)
    }

    open func sumBillingDocumentsNetValues(bpCustomerNumber: String, salesOrganization: String, statusColorDesc: String, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (BigDecimal?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.sumBillingDocumentsNetValues(bpCustomerNumber: bpCustomerNumber, salesOrganization: salesOrganization, statusColorDesc: statusColorDesc, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSyncEvent")
    open func syncEvent(query: DataQuery = DataQuery()) throws -> Array<SyncEventType> {
        return try self.fetchSyncEvent(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSyncEvent")
    open func syncEvent(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SyncEventType>?, Error?) -> Void) {
        self.fetchSyncEvent(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSyncOperationStatus")
    open func syncOperationStatus(query: DataQuery = DataQuery()) throws -> Array<SyncOperationStatusType> {
        return try self.fetchSyncOperationStatus(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchSyncOperationStatus")
    open func syncOperationStatus(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<SyncOperationStatusType>?, Error?) -> Void) {
        self.fetchSyncOperationStatus(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxABillingDocument")
    open func xITSPDSFAxABillingDocument(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxABillingDocumentType> {
        return try self.fetchXITSPDSFAxABillingDocument(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxABillingDocument")
    open func xITSPDSFAxABillingDocument(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxABillingDocumentType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxABillingDocument(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxABusinessRules")
    open func xITSPDSFAxABusinessRules(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxABusinessRulesType> {
        return try self.fetchXITSPDSFAxABusinessRules(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxABusinessRules")
    open func xITSPDSFAxABusinessRules(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxABusinessRulesType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxABusinessRules(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACenterSupplier")
    open func xITSPDSFAxACenterSupplier(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACenterSupplierType> {
        return try self.fetchXITSPDSFAxACenterSupplier(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACenterSupplier")
    open func xITSPDSFAxACenterSupplier(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACenterSupplierType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACenterSupplier(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionList")
    open func xITSPDSFAxAConditionList(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAConditionListType> {
        return try self.fetchXITSPDSFAxAConditionList(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionList")
    open func xITSPDSFAxAConditionList(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAConditionListType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAConditionList(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionScale")
    open func xITSPDSFAxAConditionScale(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAConditionScaleType> {
        return try self.fetchXITSPDSFAxAConditionScale(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionScale")
    open func xITSPDSFAxAConditionScale(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAConditionScaleType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAConditionScale(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionValueSet")
    open func xITSPDSFAxAConditionValueSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAConditionValue> {
        return try self.fetchXITSPDSFAxAConditionValueSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAConditionValueSet")
    open func xITSPDSFAxAConditionValueSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAConditionValue>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAConditionValueSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerEmailXML")
    open func xITSPDSFAxACustomerEmailXML(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerEmailXMLType> {
        return try self.fetchXITSPDSFAxACustomerEmailXML(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerEmailXML")
    open func xITSPDSFAxACustomerEmailXML(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerEmailXMLType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerEmailXML(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerGrpText")
    open func xITSPDSFAxACustomerGrpText(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerGrpTextType> {
        return try self.fetchXITSPDSFAxACustomerGrpText(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerGrpText")
    open func xITSPDSFAxACustomerGrpText(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerGrpTextType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerGrpText(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLatLongSet")
    open func xITSPDSFAxACustomerLatLongSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerLatLong> {
        return try self.fetchXITSPDSFAxACustomerLatLongSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLatLongSet")
    open func xITSPDSFAxACustomerLatLongSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerLatLong>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerLatLongSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLstInv")
    open func xITSPDSFAxACustomerLstInv(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerLstInvType> {
        return try self.fetchXITSPDSFAxACustomerLstInv(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLstInv")
    open func xITSPDSFAxACustomerLstInv(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerLstInvType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerLstInv(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLstSalOrd")
    open func xITSPDSFAxACustomerLstSalOrd(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerLstSalOrdType> {
        return try self.fetchXITSPDSFAxACustomerLstSalOrd(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerLstSalOrd")
    open func xITSPDSFAxACustomerLstSalOrd(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerLstSalOrdType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerLstSalOrd(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerMaster")
    open func xITSPDSFAxACustomerMaster(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerMasterType> {
        return try self.fetchXITSPDSFAxACustomerMaster(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerMaster")
    open func xITSPDSFAxACustomerMaster(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerMasterType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerMaster(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerSalesArea")
    open func xITSPDSFAxACustomerSalesArea(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerSalesAreaType> {
        return try self.fetchXITSPDSFAxACustomerSalesArea(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerSalesArea")
    open func xITSPDSFAxACustomerSalesArea(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerSalesAreaType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerSalesArea(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerShipTo")
    open func xITSPDSFAxACustomerShipTo(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxACustomerShipToType> {
        return try self.fetchXITSPDSFAxACustomerShipTo(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxACustomerShipTo")
    open func xITSPDSFAxACustomerShipTo(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxACustomerShipToType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxACustomerShipTo(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxADocuments")
    open func xITSPDSFAxADocuments(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxADocumentsType> {
        return try self.fetchXITSPDSFAxADocuments(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxADocuments")
    open func xITSPDSFAxADocuments(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxADocumentsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxADocuments(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAHierarchy")
    open func xITSPDSFAxAHierarchy(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAHierarchyType> {
        return try self.fetchXITSPDSFAxAHierarchy(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAHierarchy")
    open func xITSPDSFAxAHierarchy(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAHierarchyType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAHierarchy(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAIncoTerms")
    open func xITSPDSFAxAIncoTerms(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAIncoTermsType> {
        return try self.fetchXITSPDSFAxAIncoTerms(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAIncoTerms")
    open func xITSPDSFAxAIncoTerms(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAIncoTermsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAIncoTerms(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAInvoice")
    open func xITSPDSFAxAInvoice(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAInvoiceType> {
        return try self.fetchXITSPDSFAxAInvoice(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAInvoice")
    open func xITSPDSFAxAInvoice(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAInvoiceType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAInvoice(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAMsg")
    open func xITSPDSFAxAMsg(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAMsgType> {
        return try self.fetchXITSPDSFAxAMsg(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAMsg")
    open func xITSPDSFAxAMsg(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAMsgType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAMsg(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAMsgPrtnr")
    open func xITSPDSFAxAMsgPrtnr(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAMsgPrtnrType> {
        return try self.fetchXITSPDSFAxAMsgPrtnr(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAMsgPrtnr")
    open func xITSPDSFAxAMsgPrtnr(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAMsgPrtnrType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAMsgPrtnr(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANameSet")
    open func xITSPDSFAxANameSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxANameSetType> {
        return try self.fetchXITSPDSFAxANameSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANameSet")
    open func xITSPDSFAxANameSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxANameSetType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxANameSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANotifications")
    open func xITSPDSFAxANotifications(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxANotificationsType> {
        return try self.fetchXITSPDSFAxANotifications(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANotifications")
    open func xITSPDSFAxANotifications(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxANotificationsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxANotifications(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANotificationsStat")
    open func xITSPDSFAxANotificationsStat(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxANotificationsStatType> {
        return try self.fetchXITSPDSFAxANotificationsStat(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxANotificationsStat")
    open func xITSPDSFAxANotificationsStat(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxANotificationsStatType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxANotificationsStat(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartner")
    open func xITSPDSFAxAPartner(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPartnerType> {
        return try self.fetchXITSPDSFAxAPartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartner")
    open func xITSPDSFAxAPartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPartnerType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartnerLastSales")
    open func xITSPDSFAxAPartnerLastSales(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPartnerLastSalesType> {
        return try self.fetchXITSPDSFAxAPartnerLastSales(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartnerLastSales")
    open func xITSPDSFAxAPartnerLastSales(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPartnerLastSalesType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPartnerLastSales(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartnerUser")
    open func xITSPDSFAxAPartnerUser(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPartnerUserType> {
        return try self.fetchXITSPDSFAxAPartnerUser(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartnerUser")
    open func xITSPDSFAxAPartnerUser(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPartnerUserType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPartnerUser(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartners")
    open func xITSPDSFAxAPartners(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPartnersType> {
        return try self.fetchXITSPDSFAxAPartners(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPartners")
    open func xITSPDSFAxAPartners(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPartnersType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPartners(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPaymentTerms")
    open func xITSPDSFAxAPaymentTerms(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPaymentTermsType> {
        return try self.fetchXITSPDSFAxAPaymentTerms(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPaymentTerms")
    open func xITSPDSFAxAPaymentTerms(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPaymentTermsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPaymentTerms(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPricelists")
    open func xITSPDSFAxAPricelists(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAPricelistsType> {
        return try self.fetchXITSPDSFAxAPricelists(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAPricelists")
    open func xITSPDSFAxAPricelists(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAPricelistsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAPricelists(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel1")
    open func xITSPDSFAxAProductHieLevel1(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel1Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel1(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel1")
    open func xITSPDSFAxAProductHieLevel1(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel1Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel1(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel2")
    open func xITSPDSFAxAProductHieLevel2(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel2Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel2(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel2")
    open func xITSPDSFAxAProductHieLevel2(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel2Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel2(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel3")
    open func xITSPDSFAxAProductHieLevel3(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel3Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel3(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel3")
    open func xITSPDSFAxAProductHieLevel3(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel3Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel3(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel4")
    open func xITSPDSFAxAProductHieLevel4(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel4Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel4(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel4")
    open func xITSPDSFAxAProductHieLevel4(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel4Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel4(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel5")
    open func xITSPDSFAxAProductHieLevel5(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel5Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel5(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel5")
    open func xITSPDSFAxAProductHieLevel5(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel5Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel5(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel6")
    open func xITSPDSFAxAProductHieLevel6(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel6Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel6(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel6")
    open func xITSPDSFAxAProductHieLevel6(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel6Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel6(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel7")
    open func xITSPDSFAxAProductHieLevel7(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel7Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel7(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel7")
    open func xITSPDSFAxAProductHieLevel7(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel7Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel7(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel8")
    open func xITSPDSFAxAProductHieLevel8(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel8Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel8(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel8")
    open func xITSPDSFAxAProductHieLevel8(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel8Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel8(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel9")
    open func xITSPDSFAxAProductHieLevel9(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductHieLevel9Type> {
        return try self.fetchXITSPDSFAxAProductHieLevel9(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductHieLevel9")
    open func xITSPDSFAxAProductHieLevel9(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductHieLevel9Type>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductHieLevel9(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductMaster")
    open func xITSPDSFAxAProductMaster(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductMasterType> {
        return try self.fetchXITSPDSFAxAProductMaster(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductMaster")
    open func xITSPDSFAxAProductMaster(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductMasterType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductMaster(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductReqMRPSet")
    open func xITSPDSFAxAProductReqMRPSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAProductReqMRPType> {
        return try self.fetchXITSPDSFAxAProductReqMRPSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAProductReqMRPSet")
    open func xITSPDSFAxAProductReqMRPSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAProductReqMRPType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAProductReqMRPSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAQMNoteSet")
    open func xITSPDSFAxAQMNoteSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxAQMNote> {
        return try self.fetchXITSPDSFAxAQMNoteSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxAQMNoteSet")
    open func xITSPDSFAxAQMNoteSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxAQMNote>?, Error?) -> Void) {
        self.fetchXITSPDSFAxAQMNoteSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesAmounts")
    open func xITSPDSFAxASalesAmounts(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesAmountsType> {
        return try self.fetchXITSPDSFAxASalesAmounts(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesAmounts")
    open func xITSPDSFAxASalesAmounts(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesAmountsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesAmounts(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesDocType")
    open func xITSPDSFAxASalesDocType(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesDocTypeType> {
        return try self.fetchXITSPDSFAxASalesDocType(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesDocType")
    open func xITSPDSFAxASalesDocType(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesDocTypeType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesDocType(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesItemAmounts")
    open func xITSPDSFAxASalesItemAmounts(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesItemAmountsType> {
        return try self.fetchXITSPDSFAxASalesItemAmounts(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesItemAmounts")
    open func xITSPDSFAxASalesItemAmounts(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesItemAmountsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesItemAmounts(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdItPrice")
    open func xITSPDSFAxASalesOrdItPrice(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdItPriceType> {
        return try self.fetchXITSPDSFAxASalesOrdItPrice(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdItPrice")
    open func xITSPDSFAxASalesOrdItPrice(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdItPriceType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrdItPrice(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrderPartner")
    open func xITSPDSFAxASalesOrderPartner(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrderPartnerType> {
        return try self.fetchXITSPDSFAxASalesOrderPartner(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrderPartner")
    open func xITSPDSFAxASalesOrderPartner(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrderPartnerType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrderPartner(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrders")
    open func xITSPDSFAxASalesOrders(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdersType> {
        return try self.fetchXITSPDSFAxASalesOrders(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrders")
    open func xITSPDSFAxASalesOrders(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrders(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersItens")
    open func xITSPDSFAxASalesOrdersItens(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdersItensType> {
        return try self.fetchXITSPDSFAxASalesOrdersItens(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersItens")
    open func xITSPDSFAxASalesOrdersItens(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersItensType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrdersItens(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersObsSet")
    open func xITSPDSFAxASalesOrdersObsSet(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdersObs> {
        return try self.fetchXITSPDSFAxASalesOrdersObsSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersObsSet")
    open func xITSPDSFAxASalesOrdersObsSet(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersObs>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrdersObsSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersObsSetN")
    open func xITSPDSFAxASalesOrdersObsSetN(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdersObsN> {
        return try self.fetchXITSPDSFAxASalesOrdersObsSetN(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersObsSetN")
    open func xITSPDSFAxASalesOrdersObsSetN(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersObsN>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrdersObsSetN(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersResns")
    open func xITSPDSFAxASalesOrdersResns(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASalesOrdersResnsType> {
        return try self.fetchXITSPDSFAxASalesOrdersResns(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASalesOrdersResns")
    open func xITSPDSFAxASalesOrdersResns(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASalesOrdersResnsType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASalesOrdersResns(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASoldVolume")
    open func xITSPDSFAxASoldVolume(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxASoldVolumeType> {
        return try self.fetchXITSPDSFAxASoldVolume(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxASoldVolume")
    open func xITSPDSFAxASoldVolume(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxASoldVolumeType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxASoldVolume(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxList")
    open func xITSPDSFAxATaxList(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxATaxListType> {
        return try self.fetchXITSPDSFAxATaxList(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxList")
    open func xITSPDSFAxATaxList(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxATaxListType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxATaxList(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxNameset")
    open func xITSPDSFAxATaxNameset(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxATaxNamesetType> {
        return try self.fetchXITSPDSFAxATaxNameset(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxNameset")
    open func xITSPDSFAxATaxNameset(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxATaxNamesetType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxATaxNameset(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxValue")
    open func xITSPDSFAxATaxValue(query: DataQuery = DataQuery()) throws -> Array<XITSPDSFAxATaxValueType> {
        return try self.fetchXITSPDSFAxATaxValue(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchXITSPDSFAxATaxValue")
    open func xITSPDSFAxATaxValue(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<XITSPDSFAxATaxValueType>?, Error?) -> Void) {
        self.fetchXITSPDSFAxATaxValue(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }
}
