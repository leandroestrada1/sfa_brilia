// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesAmountsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SalesDocument")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "OrganizationDivision")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "BPCustomerNumber")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionType")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionRateValue")

    private static var conditionAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "ConditionAmount")

    private static var toXITSPDSFAxASalesOrders_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType.property(withName: "to_xITSPDSFAxA_SalesOrders")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesAmountsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesAmountsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesAmountsType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesAmountsType {
        return CastRequired<XITSPDSFAxASalesAmountsType>.from(self.copyEntity())
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesDocument: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?, conditionType: String?) -> EntityKey {
        return EntityKey().with(name: "SalesDocument", value: StringValue.of(optional: salesDocument)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "ConditionType", value: StringValue.of(optional: conditionType))
    }

    open var old: XITSPDSFAxASalesAmountsType {
        return CastRequired<XITSPDSFAxASalesAmountsType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.salesDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.salesDocument_ = value
            }
        }
    }

    open var salesDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.salesDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.salesDocument, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesAmountsType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrders: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                return XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesAmountsType.self) }
            do {
                XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrders: XITSPDSFAxASalesOrdersType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersType>.from(self.optionalValue(for: XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesAmountsType.toXITSPDSFAxASalesOrders, to: value)
        }
    }
}
