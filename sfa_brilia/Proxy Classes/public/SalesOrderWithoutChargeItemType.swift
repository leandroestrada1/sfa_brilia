// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargeItemType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var alternativeToItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "AlternativeToItem")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "CustomerPaymentTerms")

    private static var higherLevelItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "HigherLevelItem")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsClassification")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsLocation2")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "IncotermsTransferLocation")

    private static var itemOrderProbabilityInPercent_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ItemOrderProbabilityInPercent")

    private static var material_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "Material")

    private static var materialByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialByCustomer")

    private static var materialGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialGroup")

    private static var materialPricingGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "MaterialPricingGroup")

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "Plant")

    private static var profitCenter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ProfitCenter")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "PurchaseOrderByCustomer")

    private static var referenceSDDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ReferenceSDDocument")

    private static var referenceSDDocumentItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "ReferenceSDDocumentItem")

    private static var requestedQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "RequestedQuantity")

    private static var requestedQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "RequestedQuantityUnit")

    private static var salesBonificationItemCategory_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationItemCategory")

    private static var salesBonificationItemText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationItemText")

    private static var salesBonificationType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesBonificationType")

    private static var salesDocumentRjcnReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesDocumentRjcnReason")

    private static var salesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesOrderWithoutCharge")

    private static var salesOrderWithoutChargeItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "SalesOrderWithoutChargeItem")

    private static var wbsElement_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "WBSElement")

    private static var toXITSPDSFAxASalesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutCharge")

    private static var toXITSPDSFAxASalesOrderWithoutChargeItemPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItemPartner")

    private static var toXITSPDSFAxASalesOrderWithoutChargeScheduleLine_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeScheduleLine")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemType)
    }

    open class var alternativeToItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.alternativeToItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.alternativeToItem_ = value
            }
        }
    }

    open var alternativeToItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.alternativeToItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.alternativeToItem, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargeItemType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargeItemType>())
    }

    open func copy() -> SalesOrderWithoutChargeItemType {
        return CastRequired<SalesOrderWithoutChargeItemType>.from(self.copyEntity())
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var higherLevelItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.higherLevelItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.higherLevelItem_ = value
            }
        }
    }

    open var higherLevelItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.higherLevelItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.higherLevelItem, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemOrderProbabilityInPercent: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent_ = value
            }
        }
    }

    open var itemOrderProbabilityInPercent: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.itemOrderProbabilityInPercent, to: StringValue.of(optional: value))
        }
    }

    open class func key(salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrderWithoutCharge", value: StringValue.of(optional: salesOrderWithoutCharge)).with(name: "SalesOrderWithoutChargeItem", value: StringValue.of(optional: salesOrderWithoutChargeItem))
    }

    open class var material: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.material_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.material))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialByCustomer: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.materialByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.materialByCustomer_ = value
            }
        }
    }

    open var materialByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.materialByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.materialByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialPricingGroup: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.materialPricingGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.materialPricingGroup_ = value
            }
        }
    }

    open var materialPricingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.materialPricingGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.materialPricingGroup, to: StringValue.of(optional: value))
        }
    }

    open var old: SalesOrderWithoutChargeItemType {
        return CastRequired<SalesOrderWithoutChargeItemType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.plant_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.plant))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.referenceSDDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.referenceSDDocumentItem_ = value
            }
        }
    }

    open var referenceSDDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.referenceSDDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.referenceSDDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var requestedQuantity: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.requestedQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.requestedQuantity_ = value
            }
        }
    }

    open var requestedQuantity: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.requestedQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.requestedQuantity, to: IntegerValue.of(optional: value))
        }
    }

    open class var requestedQuantityUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.requestedQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.requestedQuantityUnit_ = value
            }
        }
    }

    open var requestedQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.requestedQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.requestedQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var salesBonificationItemCategory: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesBonificationItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesBonificationItemCategory_ = value
            }
        }
    }

    open var salesBonificationItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var salesBonificationItemText: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesBonificationItemText_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesBonificationItemText_ = value
            }
        }
    }

    open var salesBonificationItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationItemText))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationItemText, to: StringValue.of(optional: value))
        }
    }

    open class var salesBonificationType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesBonificationType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesBonificationType_ = value
            }
        }
    }

    open var salesBonificationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesBonificationType, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentRjcnReason: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesDocumentRjcnReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesDocumentRjcnReason_ = value
            }
        }
    }

    open var salesDocumentRjcnReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesDocumentRjcnReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesDocumentRjcnReason, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesOrderWithoutCharge_ = value
            }
        }
    }

    open var salesOrderWithoutCharge: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesOrderWithoutCharge, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutChargeItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem_ = value
            }
        }
    }

    open var salesOrderWithoutChargeItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.salesOrderWithoutChargeItem, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutCharge: SalesOrderWithoutChargeType? {
        get {
            return CastOptional<SalesOrderWithoutChargeType>.from(self.optionalValue(for: SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutCharge, to: value)
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargeItemPartner: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargeItemPartner: Array<SalesOrderWithoutChargeItemPartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner)).toArray(), Array<SalesOrderWithoutChargeItemPartnerType>())
        }
        set(value) {
            SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeItemPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargeScheduleLine: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargeScheduleLine: Array<SalesOrderWithoutChargeScheduleLinetype> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine)).toArray(), Array<SalesOrderWithoutChargeScheduleLinetype>())
        }
        set(value) {
            SalesOrderWithoutChargeItemType.toXITSPDSFAxASalesOrderWithoutChargeScheduleLine.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var wbsElement: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                return SalesOrderWithoutChargeItemType.wbsElement_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemType.self) }
            do {
                SalesOrderWithoutChargeItemType.wbsElement_ = value
            }
        }
    }

    open var wbsElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemType.wbsElement))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemType.wbsElement, to: StringValue.of(optional: value))
        }
    }
}
