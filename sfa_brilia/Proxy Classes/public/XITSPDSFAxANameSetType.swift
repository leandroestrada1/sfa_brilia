// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxANameSetType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "ConditionType")

    private static var accessSequence_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "AccessSequence")

    private static var sapField_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "SAP_Field")

    private static var oDataField_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "OData_Field")

    private static var entity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Entity")

    private static var atribute_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Atribute")

    private static var isOutPut_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType.property(withName: "Is_OutPut")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxANameSetType)
    }

    open class var accessSequence: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.accessSequence_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.accessSequence_ = value
            }
        }
    }

    open var accessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.accessSequence))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.accessSequence, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxANameSetType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxANameSetType>())
    }

    open class var atribute: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.atribute_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.atribute_ = value
            }
        }
    }

    open var atribute: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.atribute))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.atribute, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxANameSetType {
        return CastRequired<XITSPDSFAxANameSetType>.from(self.copyEntity())
    }

    open class var entity: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.entity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.entity_ = value
            }
        }
    }

    open var entity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.entity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.entity, to: StringValue.of(optional: value))
        }
    }

    open class var isOutPut: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.isOutPut_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.isOutPut_ = value
            }
        }
    }

    open var isOutPut: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.isOutPut))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.isOutPut, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(conditionType: String?, accessSequence: String?, sapField: String?) -> EntityKey {
        return EntityKey().with(name: "ConditionType", value: StringValue.of(optional: conditionType)).with(name: "AccessSequence", value: StringValue.of(optional: accessSequence)).with(name: "SAP_Field", value: StringValue.of(optional: sapField))
    }

    open class var oDataField: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.oDataField_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.oDataField_ = value
            }
        }
    }

    open var oDataField: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.oDataField))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.oDataField, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxANameSetType {
        return CastRequired<XITSPDSFAxANameSetType>.from(self.oldEntity)
    }

    open class var sapField: Property {
        get {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                return XITSPDSFAxANameSetType.sapField_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxANameSetType.self)
            defer { objc_sync_exit(XITSPDSFAxANameSetType.self) }
            do {
                XITSPDSFAxANameSetType.sapField_ = value
            }
        }
    }

    open var sapField: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxANameSetType.sapField))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxANameSetType.sapField, to: StringValue.of(optional: value))
        }
    }
}
