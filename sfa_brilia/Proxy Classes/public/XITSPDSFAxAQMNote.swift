// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAQMNote: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var notification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notification")

    private static var bpcustomernumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Bpcustomernumber")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Customer")

    private static var salesorganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Salesorganization")

    private static var creationdate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Creationdate")

    private static var notificationtype_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationtype")

    private static var notificationshorttext_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationshorttext")

    private static var status_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Status")

    private static var statusdesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Statusdesc")

    private static var distributionchannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Distributionchannel")

    private static var activedivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Activedivision")

    private static var material_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Material")

    private static var orderid_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Orderid")

    private static var notificationexternalqty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationexternalqty")

    private static var notificationtext_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Notificationtext")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "IsFinal")

    private static var documenttype_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documenttype")

    private static var documentnumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentnumber")

    private static var documentpart_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentpart")

    private static var documentversion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Documentversion")

    private static var hasimage_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "Hasimage")

    private static var toXITSPDSFAxADocuments_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote.property(withName: "to_xITSPDSFAxA_Documents")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAQMNote)
    }

    open class var activedivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.activedivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.activedivision_ = value
            }
        }
    }

    open var activedivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.activedivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.activedivision, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAQMNote> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAQMNote>())
    }

    open class var bpcustomernumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.bpcustomernumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.bpcustomernumber_ = value
            }
        }
    }

    open var bpcustomernumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.bpcustomernumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.bpcustomernumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAQMNote {
        return CastRequired<XITSPDSFAxAQMNote>.from(self.copyEntity())
    }

    open class var creationdate: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.creationdate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.creationdate_ = value
            }
        }
    }

    open var creationdate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAQMNote.creationdate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.creationdate, to: value)
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.customer, to: StringValue.of(optional: value))
        }
    }

    open class var distributionchannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.distributionchannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.distributionchannel_ = value
            }
        }
    }

    open var distributionchannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.distributionchannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.distributionchannel, to: StringValue.of(optional: value))
        }
    }

    open class var documentnumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.documentnumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.documentnumber_ = value
            }
        }
    }

    open var documentnumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.documentnumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.documentnumber, to: StringValue.of(optional: value))
        }
    }

    open class var documentpart: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.documentpart_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.documentpart_ = value
            }
        }
    }

    open var documentpart: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.documentpart))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.documentpart, to: StringValue.of(optional: value))
        }
    }

    open class var documenttype: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.documenttype_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.documenttype_ = value
            }
        }
    }

    open var documenttype: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.documenttype))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.documenttype, to: StringValue.of(optional: value))
        }
    }

    open class var documentversion: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.documentversion_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.documentversion_ = value
            }
        }
    }

    open var documentversion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.documentversion))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.documentversion, to: StringValue.of(optional: value))
        }
    }

    open class var hasimage: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.hasimage_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.hasimage_ = value
            }
        }
    }

    open var hasimage: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.hasimage))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.hasimage, to: BooleanValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(notification: String?, bpcustomernumber: String?, customer: String?, salesorganization: String?, notificationtype: String?, distributionchannel: String?, activedivision: String?, material: String?, orderid: String?, notificationexternalqty: BigDecimal?) -> EntityKey {
        return EntityKey().with(name: "Notification", value: StringValue.of(optional: notification)).with(name: "Bpcustomernumber", value: StringValue.of(optional: bpcustomernumber)).with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "Salesorganization", value: StringValue.of(optional: salesorganization)).with(name: "Notificationtype", value: StringValue.of(optional: notificationtype)).with(name: "Distributionchannel", value: StringValue.of(optional: distributionchannel)).with(name: "Activedivision", value: StringValue.of(optional: activedivision)).with(name: "Material", value: StringValue.of(optional: material)).with(name: "Orderid", value: StringValue.of(optional: orderid)).with(name: "Notificationexternalqty", value: DecimalValue.of(optional: notificationexternalqty))
    }

    open class var material: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.material_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.material))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.material, to: StringValue.of(optional: value))
        }
    }

    open class var notification: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.notification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.notification_ = value
            }
        }
    }

    open var notification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.notification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.notification, to: StringValue.of(optional: value))
        }
    }

    open class var notificationexternalqty: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.notificationexternalqty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.notificationexternalqty_ = value
            }
        }
    }

    open var notificationexternalqty: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.notificationexternalqty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.notificationexternalqty, to: DecimalValue.of(optional: value))
        }
    }

    open class var notificationshorttext: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.notificationshorttext_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.notificationshorttext_ = value
            }
        }
    }

    open var notificationshorttext: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.notificationshorttext))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.notificationshorttext, to: StringValue.of(optional: value))
        }
    }

    open class var notificationtext: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.notificationtext_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.notificationtext_ = value
            }
        }
    }

    open var notificationtext: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.notificationtext))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.notificationtext, to: StringValue.of(optional: value))
        }
    }

    open class var notificationtype: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.notificationtype_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.notificationtype_ = value
            }
        }
    }

    open var notificationtype: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.notificationtype))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.notificationtype, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAQMNote {
        return CastRequired<XITSPDSFAxAQMNote>.from(self.oldEntity)
    }

    open class var orderid: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.orderid_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.orderid_ = value
            }
        }
    }

    open var orderid: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.orderid))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.orderid, to: StringValue.of(optional: value))
        }
    }

    open class var salesorganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.salesorganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.salesorganization_ = value
            }
        }
    }

    open var salesorganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.salesorganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.salesorganization, to: StringValue.of(optional: value))
        }
    }

    open class var status: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.status_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.status_ = value
            }
        }
    }

    open var status: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.status))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.status, to: StringValue.of(optional: value))
        }
    }

    open class var statusdesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.statusdesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.statusdesc_ = value
            }
        }
    }

    open var statusdesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAQMNote.statusdesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAQMNote.statusdesc, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxADocuments: Property {
        get {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                return XITSPDSFAxAQMNote.toXITSPDSFAxADocuments_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAQMNote.self)
            defer { objc_sync_exit(XITSPDSFAxAQMNote.self) }
            do {
                XITSPDSFAxAQMNote.toXITSPDSFAxADocuments_ = value
            }
        }
    }

    open var toXITSPDSFAxADocuments: Array<XITSPDSFAxADocumentsType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAQMNote.toXITSPDSFAxADocuments)).toArray(), Array<XITSPDSFAxADocumentsType>())
        }
        set(value) {
            XITSPDSFAxAQMNote.toXITSPDSFAxADocuments.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
