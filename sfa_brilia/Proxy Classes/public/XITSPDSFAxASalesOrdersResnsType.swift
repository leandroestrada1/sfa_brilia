// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdersResnsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var sdDocumentReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType.property(withName: "SDDocumentReason")

    private static var sdDocumentReasonText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType.property(withName: "SDDocumentReasonText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersResnsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdersResnsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdersResnsType>())
    }

    open func copy() -> XITSPDSFAxASalesOrdersResnsType {
        return CastRequired<XITSPDSFAxASalesOrdersResnsType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(sdDocumentReason: String?) -> EntityKey {
        return EntityKey().with(name: "SDDocumentReason", value: StringValue.of(optional: sdDocumentReason))
    }

    open var old: XITSPDSFAxASalesOrdersResnsType {
        return CastRequired<XITSPDSFAxASalesOrdersResnsType>.from(self.oldEntity)
    }

    open class var sdDocumentReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersResnsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersResnsType.self) }
            do {
                return XITSPDSFAxASalesOrdersResnsType.sdDocumentReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersResnsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersResnsType.self) }
            do {
                XITSPDSFAxASalesOrdersResnsType.sdDocumentReason_ = value
            }
        }
    }

    open var sdDocumentReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersResnsType.sdDocumentReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersResnsType.sdDocumentReason, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentReasonText: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersResnsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersResnsType.self) }
            do {
                return XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersResnsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersResnsType.self) }
            do {
                XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText_ = value
            }
        }
    }

    open var sdDocumentReasonText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersResnsType.sdDocumentReasonText, to: StringValue.of(optional: value))
        }
    }
}
