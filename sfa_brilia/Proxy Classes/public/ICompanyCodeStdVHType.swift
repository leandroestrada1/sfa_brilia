// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class ICompanyCodeStdVHType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var companyCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType.property(withName: "CompanyCode")

    private static var companyCodeName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType.property(withName: "CompanyCodeName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCompanyCodeStdVHType)
    }

    open class func array(from: EntityValueList) -> Array<ICompanyCodeStdVHType> {
        return ArrayConverter.convert(from.toArray(), Array<ICompanyCodeStdVHType>())
    }

    open class var companyCode: Property {
        get {
            objc_sync_enter(ICompanyCodeStdVHType.self)
            defer { objc_sync_exit(ICompanyCodeStdVHType.self) }
            do {
                return ICompanyCodeStdVHType.companyCode_
            }
        }
        set(value) {
            objc_sync_enter(ICompanyCodeStdVHType.self)
            defer { objc_sync_exit(ICompanyCodeStdVHType.self) }
            do {
                ICompanyCodeStdVHType.companyCode_ = value
            }
        }
    }

    open var companyCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICompanyCodeStdVHType.companyCode))
        }
        set(value) {
            self.setOptionalValue(for: ICompanyCodeStdVHType.companyCode, to: StringValue.of(optional: value))
        }
    }

    open class var companyCodeName: Property {
        get {
            objc_sync_enter(ICompanyCodeStdVHType.self)
            defer { objc_sync_exit(ICompanyCodeStdVHType.self) }
            do {
                return ICompanyCodeStdVHType.companyCodeName_
            }
        }
        set(value) {
            objc_sync_enter(ICompanyCodeStdVHType.self)
            defer { objc_sync_exit(ICompanyCodeStdVHType.self) }
            do {
                ICompanyCodeStdVHType.companyCodeName_ = value
            }
        }
    }

    open var companyCodeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICompanyCodeStdVHType.companyCodeName))
        }
        set(value) {
            self.setOptionalValue(for: ICompanyCodeStdVHType.companyCodeName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> ICompanyCodeStdVHType {
        return CastRequired<ICompanyCodeStdVHType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(companyCode: String?) -> EntityKey {
        return EntityKey().with(name: "CompanyCode", value: StringValue.of(optional: companyCode))
    }

    open var old: ICompanyCodeStdVHType {
        return CastRequired<ICompanyCodeStdVHType>.from(self.oldEntity)
    }
}
