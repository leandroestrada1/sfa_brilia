// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAMsgType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var messageID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageID")

    private static var messageType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageType")

    private static var messageTitle_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "MessageTitle")

    private static var documentVideo_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "DocumentVideo")

    private static var documentImage_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "DocumentImage")

    private static var createdAt_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "CreatedAt")

    private static var createdBy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "CreatedBy")

    private static var lastChangedAt_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "LastChangedAt")

    private static var lastChangedBy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "LastChangedBy")

    private static var toXITSPDSFAxAMsgPrtnr_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType.property(withName: "to_xITSPDSFAxA_MsgPrtnr")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAMsgType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAMsgType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAMsgType>())
    }

    open func copy() -> XITSPDSFAxAMsgType {
        return CastRequired<XITSPDSFAxAMsgType>.from(self.copyEntity())
    }

    open class var createdAt: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.createdAt_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.createdAt_ = value
            }
        }
    }

    open var createdAt: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAMsgType.createdAt))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.createdAt, to: value)
        }
    }

    open class var createdBy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.createdBy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.createdBy_ = value
            }
        }
    }

    open var createdBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.createdBy))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.createdBy, to: StringValue.of(optional: value))
        }
    }

    open class var documentImage: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.documentImage_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.documentImage_ = value
            }
        }
    }

    open var documentImage: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.documentImage))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.documentImage, to: StringValue.of(optional: value))
        }
    }

    open class var documentVideo: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.documentVideo_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.documentVideo_ = value
            }
        }
    }

    open var documentVideo: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.documentVideo))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.documentVideo, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(messageID: String?) -> EntityKey {
        return EntityKey().with(name: "MessageID", value: StringValue.of(optional: messageID))
    }

    open class var lastChangedAt: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.lastChangedAt_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.lastChangedAt_ = value
            }
        }
    }

    open var lastChangedAt: GlobalDateTime? {
        get {
            return GlobalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxAMsgType.lastChangedAt))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.lastChangedAt, to: value)
        }
    }

    open class var lastChangedBy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.lastChangedBy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.lastChangedBy_ = value
            }
        }
    }

    open var lastChangedBy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.lastChangedBy))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.lastChangedBy, to: StringValue.of(optional: value))
        }
    }

    open class var messageID: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.messageID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.messageID_ = value
            }
        }
    }

    open var messageID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.messageID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.messageID, to: StringValue.of(optional: value))
        }
    }

    open class var messageTitle: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.messageTitle_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.messageTitle_ = value
            }
        }
    }

    open var messageTitle: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.messageTitle))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.messageTitle, to: StringValue.of(optional: value))
        }
    }

    open class var messageType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.messageType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.messageType_ = value
            }
        }
    }

    open var messageType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAMsgType.messageType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAMsgType.messageType, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAMsgType {
        return CastRequired<XITSPDSFAxAMsgType>.from(self.oldEntity)
    }

    open class var toXITSPDSFAxAMsgPrtnr: Property {
        get {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                return XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAMsgType.self)
            defer { objc_sync_exit(XITSPDSFAxAMsgType.self) }
            do {
                XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr_ = value
            }
        }
    }

    open var toXITSPDSFAxAMsgPrtnr: Array<XITSPDSFAxAMsgPrtnrType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr)).toArray(), Array<XITSPDSFAxAMsgPrtnrType>())
        }
        set(value) {
            XITSPDSFAxAMsgType.toXITSPDSFAxAMsgPrtnr.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
