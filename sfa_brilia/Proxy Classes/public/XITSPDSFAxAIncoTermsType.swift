// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAIncoTermsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType.property(withName: "IncotermsClassification")

    private static var incotermsClassificationName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType.property(withName: "IncotermsClassificationName")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAIncoTermsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAIncoTermsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAIncoTermsType>())
    }

    open func copy() -> XITSPDSFAxAIncoTermsType {
        return CastRequired<XITSPDSFAxAIncoTermsType>.from(self.copyEntity())
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(XITSPDSFAxAIncoTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAIncoTermsType.self) }
            do {
                return XITSPDSFAxAIncoTermsType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAIncoTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAIncoTermsType.self) }
            do {
                XITSPDSFAxAIncoTermsType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAIncoTermsType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAIncoTermsType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassificationName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAIncoTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAIncoTermsType.self) }
            do {
                return XITSPDSFAxAIncoTermsType.incotermsClassificationName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAIncoTermsType.self)
            defer { objc_sync_exit(XITSPDSFAxAIncoTermsType.self) }
            do {
                XITSPDSFAxAIncoTermsType.incotermsClassificationName_ = value
            }
        }
    }

    open var incotermsClassificationName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAIncoTermsType.incotermsClassificationName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAIncoTermsType.incotermsClassificationName, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(incotermsClassification: String?) -> EntityKey {
        return EntityKey().with(name: "IncotermsClassification", value: StringValue.of(optional: incotermsClassification))
    }

    open var old: XITSPDSFAxAIncoTermsType {
        return CastRequired<XITSPDSFAxAIncoTermsType>.from(self.oldEntity)
    }
}
