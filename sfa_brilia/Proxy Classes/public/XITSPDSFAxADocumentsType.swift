// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxADocumentsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var linkedSAPObjectKey_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LinkedSAPObjectKey")

    private static var documentInfoRecordDocType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocType")

    private static var documentInfoRecordDocNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocNumber")

    private static var documentInfoRecordDocVersion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocVersion")

    private static var documentInfoRecordDocPart_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DocumentInfoRecordDocPart")

    private static var archiveDocumentID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "ArchiveDocumentID")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "DistributionChannel")

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "Plant")

    private static var linkedSAPObject_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LinkedSAPObject")

    private static var fileName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "FileName")

    private static var fileSize_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "FileSize")

    private static var workstationApplication_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "WorkstationApplication")

    private static var mimeType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "MimeType")

    private static var logicalDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "LogicalDocument")

    private static var reportContentMedia_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType.property(withName: "ReportContentMedia")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxADocumentsType)
    }

    open class var archiveDocumentID: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.archiveDocumentID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.archiveDocumentID_ = value
            }
        }
    }

    open var archiveDocumentID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.archiveDocumentID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.archiveDocumentID, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxADocumentsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxADocumentsType>())
    }

    open func copy() -> XITSPDSFAxADocumentsType {
        return CastRequired<XITSPDSFAxADocumentsType>.from(self.copyEntity())
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.documentInfoRecordDocNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.documentInfoRecordDocNumber_ = value
            }
        }
    }

    open var documentInfoRecordDocNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocNumber, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocPart: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.documentInfoRecordDocPart_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.documentInfoRecordDocPart_ = value
            }
        }
    }

    open var documentInfoRecordDocPart: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocPart))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocPart, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocType: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.documentInfoRecordDocType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.documentInfoRecordDocType_ = value
            }
        }
    }

    open var documentInfoRecordDocType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocType, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocVersion: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.documentInfoRecordDocVersion_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.documentInfoRecordDocVersion_ = value
            }
        }
    }

    open var documentInfoRecordDocVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocVersion))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.documentInfoRecordDocVersion, to: StringValue.of(optional: value))
        }
    }

    open class var fileName: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.fileName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.fileName_ = value
            }
        }
    }

    open var fileName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.fileName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.fileName, to: StringValue.of(optional: value))
        }
    }

    open class var fileSize: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.fileSize_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.fileSize_ = value
            }
        }
    }

    open var fileSize: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.fileSize))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.fileSize, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(linkedSAPObjectKey: String?, documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, archiveDocumentID: String?) -> EntityKey {
        return EntityKey().with(name: "LinkedSAPObjectKey", value: StringValue.of(optional: linkedSAPObjectKey)).with(name: "DocumentInfoRecordDocType", value: StringValue.of(optional: documentInfoRecordDocType)).with(name: "DocumentInfoRecordDocNumber", value: StringValue.of(optional: documentInfoRecordDocNumber)).with(name: "DocumentInfoRecordDocVersion", value: StringValue.of(optional: documentInfoRecordDocVersion)).with(name: "DocumentInfoRecordDocPart", value: StringValue.of(optional: documentInfoRecordDocPart)).with(name: "ArchiveDocumentID", value: StringValue.of(optional: archiveDocumentID))
    }

    open class var linkedSAPObject: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.linkedSAPObject_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.linkedSAPObject_ = value
            }
        }
    }

    open var linkedSAPObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.linkedSAPObject))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.linkedSAPObject, to: StringValue.of(optional: value))
        }
    }

    open class var linkedSAPObjectKey: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.linkedSAPObjectKey_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.linkedSAPObjectKey_ = value
            }
        }
    }

    open var linkedSAPObjectKey: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.linkedSAPObjectKey))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.linkedSAPObjectKey, to: StringValue.of(optional: value))
        }
    }

    open class var logicalDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.logicalDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.logicalDocument_ = value
            }
        }
    }

    open var logicalDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.logicalDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.logicalDocument, to: StringValue.of(optional: value))
        }
    }

    open class var mimeType: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.mimeType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.mimeType_ = value
            }
        }
    }

    open var mimeType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.mimeType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.mimeType, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxADocumentsType {
        return CastRequired<XITSPDSFAxADocumentsType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.plant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.plant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var reportContentMedia: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.reportContentMedia_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.reportContentMedia_ = value
            }
        }
    }

    open var reportContentMedia: Data? {
        get {
            return BinaryValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.reportContentMedia))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.reportContentMedia, to: BinaryValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var workstationApplication: Property {
        get {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                return XITSPDSFAxADocumentsType.workstationApplication_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxADocumentsType.self)
            defer { objc_sync_exit(XITSPDSFAxADocumentsType.self) }
            do {
                XITSPDSFAxADocumentsType.workstationApplication_ = value
            }
        }
    }

    open var workstationApplication: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxADocumentsType.workstationApplication))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxADocumentsType.workstationApplication, to: StringValue.of(optional: value))
        }
    }
}
