// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SyncOperationStatusType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var id_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "Id")

    private static var statusColor_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "StatusColor")

    private static var statusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType.property(withName: "StatusDesc")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.syncOperationStatusType)
    }

    open class func array(from: EntityValueList) -> Array<SyncOperationStatusType> {
        return ArrayConverter.convert(from.toArray(), Array<SyncOperationStatusType>())
    }

    open func copy() -> SyncOperationStatusType {
        return CastRequired<SyncOperationStatusType>.from(self.copyEntity())
    }

    open class var id: Property {
        get {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                return SyncOperationStatusType.id_
            }
        }
        set(value) {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                SyncOperationStatusType.id_ = value
            }
        }
    }

    open var id: Int? {
        get {
            return IntValue.optional(self.optionalValue(for: SyncOperationStatusType.id))
        }
        set(value) {
            self.setOptionalValue(for: SyncOperationStatusType.id, to: IntValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(id: Int?) -> EntityKey {
        return EntityKey().with(name: "Id", value: IntValue.of(optional: id))
    }

    open var old: SyncOperationStatusType {
        return CastRequired<SyncOperationStatusType>.from(self.oldEntity)
    }

    open class var statusColor: Property {
        get {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                return SyncOperationStatusType.statusColor_
            }
        }
        set(value) {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                SyncOperationStatusType.statusColor_ = value
            }
        }
    }

    open var statusColor: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncOperationStatusType.statusColor))
        }
        set(value) {
            self.setOptionalValue(for: SyncOperationStatusType.statusColor, to: StringValue.of(optional: value))
        }
    }

    open class var statusDesc: Property {
        get {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                return SyncOperationStatusType.statusDesc_
            }
        }
        set(value) {
            objc_sync_enter(SyncOperationStatusType.self)
            defer { objc_sync_exit(SyncOperationStatusType.self) }
            do {
                SyncOperationStatusType.statusDesc_ = value
            }
        }
    }

    open var statusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SyncOperationStatusType.statusDesc))
        }
        set(value) {
            self.setOptionalValue(for: SyncOperationStatusType.statusDesc, to: StringValue.of(optional: value))
        }
    }
}
