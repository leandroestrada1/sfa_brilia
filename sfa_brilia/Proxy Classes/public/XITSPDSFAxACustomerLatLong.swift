// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerLatLong: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customer")

    private static var salesorganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Salesorganization")

    private static var distributionchannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Distributionchannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Division")

    private static var customerobsid_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customerobsid")

    private static var longitude_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Longitude")

    private static var latitude_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Latitude")

    private static var customerobsseq_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Customerobsseq")

    private static var bpcustomernumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "Bpcustomernumber")

    private static var toXITSPDSFAxACustomerMaster_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong.property(withName: "to_xITSPDSFAxA_CustomerMaster")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerLatLong)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerLatLong> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerLatLong>())
    }

    open class var bpcustomernumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.bpcustomernumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.bpcustomernumber_ = value
            }
        }
    }

    open var bpcustomernumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.bpcustomernumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.bpcustomernumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxACustomerLatLong {
        return CastRequired<XITSPDSFAxACustomerLatLong>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerobsid: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.customerobsid_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.customerobsid_ = value
            }
        }
    }

    open var customerobsid: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.customerobsid))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.customerobsid, to: StringValue.of(optional: value))
        }
    }

    open class var customerobsseq: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.customerobsseq_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.customerobsseq_ = value
            }
        }
    }

    open var customerobsseq: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.customerobsseq))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.customerobsseq, to: StringValue.of(optional: value))
        }
    }

    open class var distributionchannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.distributionchannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.distributionchannel_ = value
            }
        }
    }

    open var distributionchannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.distributionchannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.distributionchannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.division, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, salesorganization: String?, distributionchannel: String?, division: String?, customerobsid: String?, customerobsseq: String?, bpcustomernumber: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "Salesorganization", value: StringValue.of(optional: salesorganization)).with(name: "Distributionchannel", value: StringValue.of(optional: distributionchannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "Customerobsid", value: StringValue.of(optional: customerobsid)).with(name: "Customerobsseq", value: StringValue.of(optional: customerobsseq)).with(name: "Bpcustomernumber", value: StringValue.of(optional: bpcustomernumber))
    }

    open class var latitude: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.latitude_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.latitude_ = value
            }
        }
    }

    open var latitude: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.latitude))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.latitude, to: StringValue.of(optional: value))
        }
    }

    open class var longitude: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.longitude_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.longitude_ = value
            }
        }
    }

    open var longitude: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.longitude))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.longitude, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxACustomerLatLong {
        return CastRequired<XITSPDSFAxACustomerLatLong>.from(self.oldEntity)
    }

    open class var salesorganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.salesorganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.salesorganization_ = value
            }
        }
    }

    open var salesorganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerLatLong.salesorganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.salesorganization, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxACustomerMaster: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                return XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerLatLong.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerLatLong.self) }
            do {
                XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerMaster: XITSPDSFAxACustomerMasterType? {
        get {
            return CastOptional<XITSPDSFAxACustomerMasterType>.from(self.optionalValue(for: XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerLatLong.toXITSPDSFAxACustomerMaster, to: value)
        }
    }
}
