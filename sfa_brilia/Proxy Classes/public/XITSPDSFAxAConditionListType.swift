// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAConditionListType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "ConditionType")

    private static var accessSequence_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "AccessSequence")

    private static var conditionTable_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "ConditionTable")

    private static var tableName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "TableName")

    private static var hasValidity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "HasValidity")

    private static var toXITSPDSFAxANameSet_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType.property(withName: "to_xITSPDSFAxA_NameSet")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAConditionListType)
    }

    open class var accessSequence: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.accessSequence_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.accessSequence_ = value
            }
        }
    }

    open var accessSequence: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionListType.accessSequence))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionListType.accessSequence, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAConditionListType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAConditionListType>())
    }

    open class var conditionTable: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.conditionTable_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.conditionTable_ = value
            }
        }
    }

    open var conditionTable: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionListType.conditionTable))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionListType.conditionTable, to: StringValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionListType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionListType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAConditionListType {
        return CastRequired<XITSPDSFAxAConditionListType>.from(self.copyEntity())
    }

    open class var hasValidity: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.hasValidity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.hasValidity_ = value
            }
        }
    }

    open var hasValidity: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxAConditionListType.hasValidity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionListType.hasValidity, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(conditionType: String?, accessSequence: String?) -> EntityKey {
        return EntityKey().with(name: "ConditionType", value: StringValue.of(optional: conditionType)).with(name: "AccessSequence", value: StringValue.of(optional: accessSequence))
    }

    open var old: XITSPDSFAxAConditionListType {
        return CastRequired<XITSPDSFAxAConditionListType>.from(self.oldEntity)
    }

    open class var tableName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.tableName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.tableName_ = value
            }
        }
    }

    open var tableName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAConditionListType.tableName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAConditionListType.tableName, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxANameSet: Property {
        get {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                return XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAConditionListType.self)
            defer { objc_sync_exit(XITSPDSFAxAConditionListType.self) }
            do {
                XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet_ = value
            }
        }
    }

    open var toXITSPDSFAxANameSet: Array<XITSPDSFAxANameSetType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet)).toArray(), Array<XITSPDSFAxANameSetType>())
        }
        set(value) {
            XITSPDSFAxAConditionListType.toXITSPDSFAxANameSet.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
