// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesItemAmountsType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "SalesOrder")

    private static var salesOrderItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "SalesOrderItem")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "BPCustomerNumber")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionType")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionRateValue")

    private static var conditionAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType.property(withName: "ConditionAmount")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesItemAmountsType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesItemAmountsType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesItemAmountsType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesItemAmountsType {
        return CastRequired<XITSPDSFAxASalesItemAmountsType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrderItem: String?, bpCustomerNumber: String?, conditionType: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrderItem", value: StringValue.of(optional: salesOrderItem)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "ConditionType", value: StringValue.of(optional: conditionType))
    }

    open var old: XITSPDSFAxASalesItemAmountsType {
        return CastRequired<XITSPDSFAxASalesItemAmountsType>.from(self.oldEntity)
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItem: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                return XITSPDSFAxASalesItemAmountsType.salesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesItemAmountsType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesItemAmountsType.self) }
            do {
                XITSPDSFAxASalesItemAmountsType.salesOrderItem_ = value
            }
        }
    }

    open var salesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesItemAmountsType.salesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesItemAmountsType.salesOrderItem, to: StringValue.of(optional: value))
        }
    }
}
