// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductMasterType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var product_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Product")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "DistributionChannel")

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Plant")

    private static var mtpos_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "mtpos")

    private static var salesUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "SalesUnit")

    private static var productName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductName")

    private static var productType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductType")

    private static var grossWeight_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "GrossWeight")

    private static var weightUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "WeightUnit")

    private static var productGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductGroup")

    private static var baseUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "BaseUnit")

    private static var netWeight_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "NetWeight")

    private static var productHierarchy_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductHierarchy")

    private static var productHierarchyText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductHierarchyText")

    private static var zProductHierarchyLevel1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel1")

    private static var zProductHierarchyLevel2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel2")

    private static var zProductHierarchyLevel3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel3")

    private static var zProductHierarchyLevel4_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel4")

    private static var zProductHierarchyLevel5_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel5")

    private static var zProductHierarchyLevel6_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel6")

    private static var zProductHierarchyLevel7_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel7")

    private static var zProductHierarchyLevel8_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel8")

    private static var zProductHierarchyLevel9_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ZProductHierarchyLevel9")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Division")

    private static var volumeUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "VolumeUnit")

    private static var materialVolume_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialVolume")

    private static var eanupc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "EANUPC")

    private static var ean_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "EAN")

    private static var industrySector_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "IndustrySector")

    private static var length_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Length")

    private static var width_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Width")

    private static var nbmcode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "NBMCODE")

    private static var height_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Height")

    private static var cubage_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Cubage")

    private static var dun14_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "DUN14")

    private static var multiple_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "Multiple")

    private static var productRelease_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ProductRelease")

    private static var unrestrictedUseStock_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "UnrestrictedUseStock")

    private static var materialGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialGroup")

    private static var materialGroupDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "MaterialGroupDesc")

    private static var priceSpecificationProductGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "PriceSpecificationProductGroup")

    private static var priceSpecProdGrpDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "PriceSpecProdGrpDesc")

    private static var mtorg_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "mtorg")

    private static var oldMaterial_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "OldMaterial")

    private static var comissionGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ComissionGroup")

    private static var externalMaterialGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "ExternalMaterialGroup")

    private static var toXITSPDSFAxADocuments_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType.property(withName: "to_xITSPDSFAxA_Documents")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductMasterType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductMasterType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductMasterType>())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var comissionGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.comissionGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.comissionGroup_ = value
            }
        }
    }

    open var comissionGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.comissionGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.comissionGroup, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAProductMasterType {
        return CastRequired<XITSPDSFAxAProductMasterType>.from(self.copyEntity())
    }

    open class var cubage: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.cubage_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.cubage_ = value
            }
        }
    }

    open var cubage: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.cubage))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.cubage, to: DecimalValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.division, to: StringValue.of(optional: value))
        }
    }

    open class var dun14: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.dun14_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.dun14_ = value
            }
        }
    }

    open var dun14: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.dun14))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.dun14, to: StringValue.of(optional: value))
        }
    }

    open class var ean: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.ean_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.ean_ = value
            }
        }
    }

    open var ean: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.ean))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.ean, to: StringValue.of(optional: value))
        }
    }

    open class var eanupc: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.eanupc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.eanupc_ = value
            }
        }
    }

    open var eanupc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.eanupc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.eanupc, to: StringValue.of(optional: value))
        }
    }

    open class var externalMaterialGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.externalMaterialGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.externalMaterialGroup_ = value
            }
        }
    }

    open var externalMaterialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.externalMaterialGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.externalMaterialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var grossWeight: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.grossWeight_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.grossWeight_ = value
            }
        }
    }

    open var grossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.grossWeight))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.grossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var height: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.height_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.height_ = value
            }
        }
    }

    open var height: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.height))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.height, to: DecimalValue.of(optional: value))
        }
    }

    open class var industrySector: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.industrySector_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.industrySector_ = value
            }
        }
    }

    open var industrySector: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.industrySector))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.industrySector, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(product: String?, salesOrganization: String?, distributionChannel: String?, plant: String?) -> EntityKey {
        return EntityKey().with(name: "Product", value: StringValue.of(optional: product)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Plant", value: StringValue.of(optional: plant))
    }

    open class var length: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.length_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.length_ = value
            }
        }
    }

    open var length: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.length))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.length, to: DecimalValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroupDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.materialGroupDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.materialGroupDesc_ = value
            }
        }
    }

    open var materialGroupDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.materialGroupDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.materialGroupDesc, to: StringValue.of(optional: value))
        }
    }

    open class var materialVolume: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.materialVolume_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.materialVolume_ = value
            }
        }
    }

    open var materialVolume: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.materialVolume))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.materialVolume, to: DecimalValue.of(optional: value))
        }
    }

    open class var mtorg: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.mtorg_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.mtorg_ = value
            }
        }
    }

    open var mtorg: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.mtorg))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.mtorg, to: StringValue.of(optional: value))
        }
    }

    open class var mtpos: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.mtpos_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.mtpos_ = value
            }
        }
    }

    open var mtpos: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.mtpos))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.mtpos, to: StringValue.of(optional: value))
        }
    }

    open class var multiple: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.multiple_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.multiple_ = value
            }
        }
    }

    open var multiple: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.multiple))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.multiple, to: DecimalValue.of(optional: value))
        }
    }

    open class var nbmcode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.nbmcode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.nbmcode_ = value
            }
        }
    }

    open var nbmcode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.nbmcode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.nbmcode, to: StringValue.of(optional: value))
        }
    }

    open class var netWeight: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.netWeight_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.netWeight_ = value
            }
        }
    }

    open var netWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.netWeight))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.netWeight, to: DecimalValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxAProductMasterType {
        return CastRequired<XITSPDSFAxAProductMasterType>.from(self.oldEntity)
    }

    open class var oldMaterial: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.oldMaterial_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.oldMaterial_ = value
            }
        }
    }

    open var oldMaterial: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.oldMaterial))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.oldMaterial, to: StringValue.of(optional: value))
        }
    }

    open class var plant: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.plant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.plant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var priceSpecProdGrpDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc_ = value
            }
        }
    }

    open var priceSpecProdGrpDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.priceSpecProdGrpDesc, to: StringValue.of(optional: value))
        }
    }

    open class var priceSpecificationProductGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.priceSpecificationProductGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.priceSpecificationProductGroup_ = value
            }
        }
    }

    open var priceSpecificationProductGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.priceSpecificationProductGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.priceSpecificationProductGroup, to: StringValue.of(optional: value))
        }
    }

    open class var product: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.product_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.product_ = value
            }
        }
    }

    open var product: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.product))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.product, to: StringValue.of(optional: value))
        }
    }

    open class var productGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productGroup_ = value
            }
        }
    }

    open var productGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productGroup, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchy: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productHierarchy_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productHierarchy_ = value
            }
        }
    }

    open var productHierarchy: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productHierarchy))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productHierarchy, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyText: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productHierarchyText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productHierarchyText_ = value
            }
        }
    }

    open var productHierarchyText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productHierarchyText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productHierarchyText, to: StringValue.of(optional: value))
        }
    }

    open class var productName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productName_ = value
            }
        }
    }

    open var productName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productName, to: StringValue.of(optional: value))
        }
    }

    open class var productRelease: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productRelease_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productRelease_ = value
            }
        }
    }

    open var productRelease: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productRelease))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productRelease, to: StringValue.of(optional: value))
        }
    }

    open class var productType: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.productType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.productType_ = value
            }
        }
    }

    open var productType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.productType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.productType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var salesUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.salesUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.salesUnit_ = value
            }
        }
    }

    open var salesUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.salesUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.salesUnit, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxADocuments: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments_ = value
            }
        }
    }

    open var toXITSPDSFAxADocuments: Array<XITSPDSFAxADocumentsType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments)).toArray(), Array<XITSPDSFAxADocumentsType>())
        }
        set(value) {
            XITSPDSFAxAProductMasterType.toXITSPDSFAxADocuments.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var unrestrictedUseStock: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.unrestrictedUseStock_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.unrestrictedUseStock_ = value
            }
        }
    }

    open var unrestrictedUseStock: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.unrestrictedUseStock))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.unrestrictedUseStock, to: DecimalValue.of(optional: value))
        }
    }

    open class var volumeUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.volumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.volumeUnit_ = value
            }
        }
    }

    open var volumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.volumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.volumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var weightUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.weightUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.weightUnit_ = value
            }
        }
    }

    open var weightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.weightUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.weightUnit, to: StringValue.of(optional: value))
        }
    }

    open class var width: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.width_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.width_ = value
            }
        }
    }

    open var width: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.width))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.width, to: DecimalValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel1: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel1_ = value
            }
        }
    }

    open var zProductHierarchyLevel1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel1, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel2: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel2_ = value
            }
        }
    }

    open var zProductHierarchyLevel2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel2, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel3: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel3_ = value
            }
        }
    }

    open var zProductHierarchyLevel3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel3, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel4: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel4_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel4_ = value
            }
        }
    }

    open var zProductHierarchyLevel4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel4))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel4, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel5: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel5_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel5_ = value
            }
        }
    }

    open var zProductHierarchyLevel5: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel5))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel5, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel6: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel6_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel6_ = value
            }
        }
    }

    open var zProductHierarchyLevel6: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel6))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel6, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel7: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel7_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel7_ = value
            }
        }
    }

    open var zProductHierarchyLevel7: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel7))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel7, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel8: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel8_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel8_ = value
            }
        }
    }

    open var zProductHierarchyLevel8: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel8))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel8, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel9: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                return XITSPDSFAxAProductMasterType.zProductHierarchyLevel9_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductMasterType.self)
            defer { objc_sync_exit(XITSPDSFAxAProductMasterType.self) }
            do {
                XITSPDSFAxAProductMasterType.zProductHierarchyLevel9_ = value
            }
        }
    }

    open var zProductHierarchyLevel9: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel9))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductMasterType.zProductHierarchyLevel9, to: StringValue.of(optional: value))
        }
    }
}
