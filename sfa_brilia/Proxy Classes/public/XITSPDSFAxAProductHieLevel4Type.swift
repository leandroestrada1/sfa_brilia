// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductHieLevel4Type: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var productHierarchyNode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ProductHierarchyNode")

    private static var zProductHierarchyLevel1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel1")

    private static var zProductHierarchyLevel2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel2")

    private static var zProductHierarchyLevel3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel3")

    private static var zProductHierarchyLevel4_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ZProductHierarchyLevel4")

    private static var productHierarchyNodeText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type.property(withName: "ProductHierarchyNodeText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel4Type)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductHieLevel4Type> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductHieLevel4Type>())
    }

    open func copy() -> XITSPDSFAxAProductHieLevel4Type {
        return CastRequired<XITSPDSFAxAProductHieLevel4Type>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(productHierarchyNode: String?) -> EntityKey {
        return EntityKey().with(name: "ProductHierarchyNode", value: StringValue.of(optional: productHierarchyNode))
    }

    open var old: XITSPDSFAxAProductHieLevel4Type {
        return CastRequired<XITSPDSFAxAProductHieLevel4Type>.from(self.oldEntity)
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNodeText: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText_ = value
            }
        }
    }

    open var productHierarchyNodeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.productHierarchyNodeText, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel1: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1_ = value
            }
        }
    }

    open var zProductHierarchyLevel1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel1, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel2: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2_ = value
            }
        }
    }

    open var zProductHierarchyLevel2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel2, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel3: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3_ = value
            }
        }
    }

    open var zProductHierarchyLevel3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel3, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel4: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel4Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel4Type.self) }
            do {
                XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4_ = value
            }
        }
    }

    open var zProductHierarchyLevel4: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel4Type.zProductHierarchyLevel4, to: StringValue.of(optional: value))
        }
    }
}
