// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class ICustomerVHType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "Customer")

    private static var organizationBPName1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "OrganizationBPName1")

    private static var organizationBPName2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "OrganizationBPName2")

    private static var country_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "Country")

    private static var cityName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CityName")

    private static var streetName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "StreetName")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CustomerName")

    private static var customerAccountGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "CustomerAccountGroup")

    private static var authorizationGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "AuthorizationGroup")

    private static var isBusinessPurposeCompleted_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType.property(withName: "IsBusinessPurposeCompleted")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iCustomerVHType)
    }

    open class func array(from: EntityValueList) -> Array<ICustomerVHType> {
        return ArrayConverter.convert(from.toArray(), Array<ICustomerVHType>())
    }

    open class var authorizationGroup: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.authorizationGroup_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.authorizationGroup_ = value
            }
        }
    }

    open var authorizationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.authorizationGroup))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.authorizationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var cityName: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.cityName_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.cityName_ = value
            }
        }
    }

    open var cityName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.cityName))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.cityName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> ICustomerVHType {
        return CastRequired<ICustomerVHType>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.country_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.country))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.country, to: StringValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.customer_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.customer))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerAccountGroup: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.customerAccountGroup_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.customerAccountGroup_ = value
            }
        }
    }

    open var customerAccountGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.customerAccountGroup))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.customerAccountGroup, to: StringValue.of(optional: value))
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var isBusinessPurposeCompleted: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.isBusinessPurposeCompleted_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.isBusinessPurposeCompleted_ = value
            }
        }
    }

    open var isBusinessPurposeCompleted: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.isBusinessPurposeCompleted))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.isBusinessPurposeCompleted, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer))
    }

    open var old: ICustomerVHType {
        return CastRequired<ICustomerVHType>.from(self.oldEntity)
    }

    open class var organizationBPName1: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.organizationBPName1_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.organizationBPName1_ = value
            }
        }
    }

    open var organizationBPName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.organizationBPName1))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.organizationBPName1, to: StringValue.of(optional: value))
        }
    }

    open class var organizationBPName2: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.organizationBPName2_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.organizationBPName2_ = value
            }
        }
    }

    open var organizationBPName2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.organizationBPName2))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.organizationBPName2, to: StringValue.of(optional: value))
        }
    }

    open class var streetName: Property {
        get {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                return ICustomerVHType.streetName_
            }
        }
        set(value) {
            objc_sync_enter(ICustomerVHType.self)
            defer { objc_sync_exit(ICustomerVHType.self) }
            do {
                ICustomerVHType.streetName_ = value
            }
        }
    }

    open var streetName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ICustomerVHType.streetName))
        }
        set(value) {
            self.setOptionalValue(for: ICustomerVHType.streetName, to: StringValue.of(optional: value))
        }
    }
}
