// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxATaxListType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var taxType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "TaxType")

    private static var groupPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "GroupPriority")

    private static var toXITSPDSFAxATaxNameset_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "to_xITSPDSFAxA_TaxNameset")

    private static var toXITSPDSFAxATaxValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType.property(withName: "to_xITSPDSFAxA_TaxValue")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxListType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxATaxListType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxATaxListType>())
    }

    open func copy() -> XITSPDSFAxATaxListType {
        return CastRequired<XITSPDSFAxATaxListType>.from(self.copyEntity())
    }

    open class var groupPriority: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                return XITSPDSFAxATaxListType.groupPriority_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                XITSPDSFAxATaxListType.groupPriority_ = value
            }
        }
    }

    open var groupPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxListType.groupPriority))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxListType.groupPriority, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(taxType: String?, groupPriority: String?) -> EntityKey {
        return EntityKey().with(name: "TaxType", value: StringValue.of(optional: taxType)).with(name: "GroupPriority", value: StringValue.of(optional: groupPriority))
    }

    open var old: XITSPDSFAxATaxListType {
        return CastRequired<XITSPDSFAxATaxListType>.from(self.oldEntity)
    }

    open class var taxType: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                return XITSPDSFAxATaxListType.taxType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                XITSPDSFAxATaxListType.taxType_ = value
            }
        }
    }

    open var taxType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxListType.taxType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxListType.taxType, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxATaxNameset: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                return XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset_ = value
            }
        }
    }

    open var toXITSPDSFAxATaxNameset: Array<XITSPDSFAxATaxNamesetType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset)).toArray(), Array<XITSPDSFAxATaxNamesetType>())
        }
        set(value) {
            XITSPDSFAxATaxListType.toXITSPDSFAxATaxNameset.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxATaxValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                return XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxListType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxListType.self) }
            do {
                XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue_ = value
            }
        }
    }

    open var toXITSPDSFAxATaxValue: Array<XITSPDSFAxATaxValueType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue)).toArray(), Array<XITSPDSFAxATaxValueType>())
        }
        set(value) {
            XITSPDSFAxATaxListType.toXITSPDSFAxATaxValue.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }
}
