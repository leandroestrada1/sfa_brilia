// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdItPriceType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrder")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "OrganizationDivision")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SoldToParty")

    private static var salesOrderItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrderItem")

    private static var pricingProcedureStep_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "PricingProcedureStep")

    private static var pricingProcedureCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "PricingProcedureCounter")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "BPCustomerNumber")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionType")

    private static var conditionBaseValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionBaseValue")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionRateValue")

    private static var conditionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionCurrency")

    private static var conditionAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "ConditionAmount")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "SalesOrderType")

    private static var toXITSPDSFAxASalesOrdersItens_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType.property(withName: "to_xITSPDSFAxA_SalesOrdersItens")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdItPriceType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdItPriceType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdItPriceType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var conditionAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.conditionAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.conditionAmount_ = value
            }
        }
    }

    open var conditionAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionBaseValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.conditionBaseValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.conditionBaseValue_ = value
            }
        }
    }

    open var conditionBaseValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionBaseValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionBaseValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionRateValue, to: DecimalValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrdItPriceType {
        return CastRequired<XITSPDSFAxASalesOrdItPriceType>.from(self.copyEntity())
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, salesOrderItem: String?, pricingProcedureStep: String?, pricingProcedureCounter: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "SalesOrderItem", value: StringValue.of(optional: salesOrderItem)).with(name: "PricingProcedureStep", value: StringValue.of(optional: pricingProcedureStep)).with(name: "PricingProcedureCounter", value: StringValue.of(optional: pricingProcedureCounter)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open var old: XITSPDSFAxASalesOrdItPriceType {
        return CastRequired<XITSPDSFAxASalesOrdItPriceType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItem: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.salesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.salesOrderItem_ = value
            }
        }
    }

    open var salesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrdersItens: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                return XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdItPriceType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdItPriceType.self) }
            do {
                XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrdersItens: XITSPDSFAxASalesOrdersItensType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersItensType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdItPriceType.toXITSPDSFAxASalesOrdersItens, to: value)
        }
    }
}
