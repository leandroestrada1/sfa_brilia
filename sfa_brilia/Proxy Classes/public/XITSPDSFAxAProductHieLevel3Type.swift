// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAProductHieLevel3Type: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var productHierarchyNode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ProductHierarchyNode")

    private static var zProductHierarchyLevel1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel1")

    private static var zProductHierarchyLevel2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel2")

    private static var zProductHierarchyLevel3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ZProductHierarchyLevel3")

    private static var productHierarchyNodeText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type.property(withName: "ProductHierarchyNodeText")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAProductHieLevel3Type)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAProductHieLevel3Type> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAProductHieLevel3Type>())
    }

    open func copy() -> XITSPDSFAxAProductHieLevel3Type {
        return CastRequired<XITSPDSFAxAProductHieLevel3Type>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(productHierarchyNode: String?) -> EntityKey {
        return EntityKey().with(name: "ProductHierarchyNode", value: StringValue.of(optional: productHierarchyNode))
    }

    open var old: XITSPDSFAxAProductHieLevel3Type {
        return CastRequired<XITSPDSFAxAProductHieLevel3Type>.from(self.oldEntity)
    }

    open class var productHierarchyNode: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel3Type.productHierarchyNode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                XITSPDSFAxAProductHieLevel3Type.productHierarchyNode_ = value
            }
        }
    }

    open var productHierarchyNode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel3Type.productHierarchyNode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel3Type.productHierarchyNode, to: StringValue.of(optional: value))
        }
    }

    open class var productHierarchyNodeText: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText_ = value
            }
        }
    }

    open var productHierarchyNodeText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel3Type.productHierarchyNodeText, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel1: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1_ = value
            }
        }
    }

    open var zProductHierarchyLevel1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel1, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel2: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2_ = value
            }
        }
    }

    open var zProductHierarchyLevel2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel2, to: StringValue.of(optional: value))
        }
    }

    open class var zProductHierarchyLevel3: Property {
        get {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                return XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAProductHieLevel3Type.self)
            defer { objc_sync_exit(XITSPDSFAxAProductHieLevel3Type.self) }
            do {
                XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3_ = value
            }
        }
    }

    open var zProductHierarchyLevel3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAProductHieLevel3Type.zProductHierarchyLevel3, to: StringValue.of(optional: value))
        }
    }
}
