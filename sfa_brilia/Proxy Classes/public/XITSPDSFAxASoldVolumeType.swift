// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASoldVolumeType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesOrder")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "OrganizationDivision")

    private static var billingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "BillingDocument")

    private static var partnerActual_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "PartnerActual")

    private static var partnerHistorical_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "PartnerHistorical")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SoldToParty")

    private static var yearMonth_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "YearMonth")

    private static var salesDocumentAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "SalesDocumentAmount")

    private static var billingDocumentAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType.property(withName: "BillingDocumentAmount")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASoldVolumeType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASoldVolumeType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASoldVolumeType>())
    }

    open class var billingDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.billingDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.billingDocument_ = value
            }
        }
    }

    open var billingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.billingDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.billingDocument, to: StringValue.of(optional: value))
        }
    }

    open class var billingDocumentAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.billingDocumentAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.billingDocumentAmount_ = value
            }
        }
    }

    open var billingDocumentAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.billingDocumentAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.billingDocumentAmount, to: DecimalValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASoldVolumeType {
        return CastRequired<XITSPDSFAxASoldVolumeType>.from(self.copyEntity())
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, billingDocument: String?, partnerActual: String?, partnerHistorical: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "BillingDocument", value: StringValue.of(optional: billingDocument)).with(name: "PartnerActual", value: StringValue.of(optional: partnerActual)).with(name: "PartnerHistorical", value: StringValue.of(optional: partnerHistorical))
    }

    open var old: XITSPDSFAxASoldVolumeType {
        return CastRequired<XITSPDSFAxASoldVolumeType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var partnerActual: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.partnerActual_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.partnerActual_ = value
            }
        }
    }

    open var partnerActual: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.partnerActual))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.partnerActual, to: StringValue.of(optional: value))
        }
    }

    open class var partnerHistorical: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.partnerHistorical_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.partnerHistorical_ = value
            }
        }
    }

    open var partnerHistorical: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.partnerHistorical))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.partnerHistorical, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.salesDocumentAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.salesDocumentAmount_ = value
            }
        }
    }

    open var salesDocumentAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.salesDocumentAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.salesDocumentAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var yearMonth: Property {
        get {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                return XITSPDSFAxASoldVolumeType.yearMonth_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASoldVolumeType.self)
            defer { objc_sync_exit(XITSPDSFAxASoldVolumeType.self) }
            do {
                XITSPDSFAxASoldVolumeType.yearMonth_ = value
            }
        }
    }

    open var yearMonth: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASoldVolumeType.yearMonth))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASoldVolumeType.yearMonth, to: StringValue.of(optional: value))
        }
    }
}
