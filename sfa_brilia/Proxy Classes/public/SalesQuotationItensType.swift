// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationItensType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var alternativeToItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "AlternativeToItem")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "CustomerPaymentTerms")

    private static var higherLevelItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "HigherLevelItem")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsClassification")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsLocation2")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "IncotermsTransferLocation")

    private static var itemOrderProbabilityInPercent_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ItemOrderProbabilityInPercent")

    private static var material_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "Material")

    private static var materialByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialByCustomer")

    private static var materialGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialGroup")

    private static var materialPricingGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "MaterialPricingGroup")

    private static var plant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "Plant")

    private static var profitCenter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ProfitCenter")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "PurchaseOrderByCustomer")

    private static var referenceSDDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ReferenceSDDocument")

    private static var referenceSDDocumentItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "ReferenceSDDocumentItem")

    private static var requestedQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "RequestedQuantity")

    private static var requestedQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "RequestedQuantityUnit")

    private static var salesDocumentRjcnReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesDocumentRjcnReason")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotation")

    private static var salesQuotationItemCategory_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationItemCategory")

    private static var salesQuotationItemText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationItemText")

    private static var salesQuotationType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "SalesQuotationType")

    private static var wbsElement_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "WBSElement")

    private static var toXITSPDSFAxASalesQuotationItemPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPartner")

    private static var toXITSPDSFAxASalesQuotationItemPricing_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPricing")

    private static var toXITSPDSFAxASalesQuotations_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType.property(withName: "to_xITSPDSFAxA_SalesQuotations")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItensType)
    }

    open class var alternativeToItem: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.alternativeToItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.alternativeToItem_ = value
            }
        }
    }

    open var alternativeToItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.alternativeToItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.alternativeToItem, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationItensType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationItensType>())
    }

    open func copy() -> SalesQuotationItensType {
        return CastRequired<SalesQuotationItensType>.from(self.copyEntity())
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var higherLevelItem: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.higherLevelItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.higherLevelItem_ = value
            }
        }
    }

    open var higherLevelItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.higherLevelItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.higherLevelItem, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemOrderProbabilityInPercent: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.itemOrderProbabilityInPercent_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.itemOrderProbabilityInPercent_ = value
            }
        }
    }

    open var itemOrderProbabilityInPercent: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.itemOrderProbabilityInPercent))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.itemOrderProbabilityInPercent, to: StringValue.of(optional: value))
        }
    }

    open class func key(salesQuotation: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation))
    }

    open class var material: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.material_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.material))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialByCustomer: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.materialByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.materialByCustomer_ = value
            }
        }
    }

    open var materialByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.materialByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.materialByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialPricingGroup: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.materialPricingGroup_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.materialPricingGroup_ = value
            }
        }
    }

    open var materialPricingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.materialPricingGroup))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.materialPricingGroup, to: StringValue.of(optional: value))
        }
    }

    open var old: SalesQuotationItensType {
        return CastRequired<SalesQuotationItensType>.from(self.oldEntity)
    }

    open class var plant: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.plant_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.plant_ = value
            }
        }
    }

    open var plant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.plant))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.plant, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocumentItem: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.referenceSDDocumentItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.referenceSDDocumentItem_ = value
            }
        }
    }

    open var referenceSDDocumentItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.referenceSDDocumentItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.referenceSDDocumentItem, to: StringValue.of(optional: value))
        }
    }

    open class var requestedQuantity: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.requestedQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.requestedQuantity_ = value
            }
        }
    }

    open var requestedQuantity: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesQuotationItensType.requestedQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.requestedQuantity, to: IntegerValue.of(optional: value))
        }
    }

    open class var requestedQuantityUnit: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.requestedQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.requestedQuantityUnit_ = value
            }
        }
    }

    open var requestedQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.requestedQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.requestedQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentRjcnReason: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.salesDocumentRjcnReason_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.salesDocumentRjcnReason_ = value
            }
        }
    }

    open var salesDocumentRjcnReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.salesDocumentRjcnReason))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.salesDocumentRjcnReason, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotationItemCategory: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.salesQuotationItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.salesQuotationItemCategory_ = value
            }
        }
    }

    open var salesQuotationItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.salesQuotationItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.salesQuotationItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotationItemText: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.salesQuotationItemText_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.salesQuotationItemText_ = value
            }
        }
    }

    open var salesQuotationItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.salesQuotationItemText))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.salesQuotationItemText, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotationType: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.salesQuotationType_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.salesQuotationType_ = value
            }
        }
    }

    open var salesQuotationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.salesQuotationType))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.salesQuotationType, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesQuotationItemPartner: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationItemPartner: Array<SalesQuotationItemPartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner)).toArray(), Array<SalesQuotationItemPartnerType>())
        }
        set(value) {
            SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesQuotationItemPricing: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationItemPricing: Array<SalesQuotationItemPricingType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing)).toArray(), Array<SalesQuotationItemPricingType>())
        }
        set(value) {
            SalesQuotationItensType.toXITSPDSFAxASalesQuotationItemPricing.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesQuotations: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.toXITSPDSFAxASalesQuotations_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.toXITSPDSFAxASalesQuotations_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotations: SalesQuotationsType? {
        get {
            return CastOptional<SalesQuotationsType>.from(self.optionalValue(for: SalesQuotationItensType.toXITSPDSFAxASalesQuotations))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.toXITSPDSFAxASalesQuotations, to: value)
        }
    }

    open class var wbsElement: Property {
        get {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                return SalesQuotationItensType.wbsElement_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItensType.self)
            defer { objc_sync_exit(SalesQuotationItensType.self) }
            do {
                SalesQuotationItensType.wbsElement_ = value
            }
        }
    }

    open var wbsElement: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItensType.wbsElement))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItensType.wbsElement, to: StringValue.of(optional: value))
        }
    }
}
