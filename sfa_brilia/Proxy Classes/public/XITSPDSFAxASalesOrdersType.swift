// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdersType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrder")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OrganizationDivision")

    private static var soldToParty_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SoldToParty")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BPCustomerNumber")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerName")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrderType")

    private static var salesGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesGroup")

    private static var salesOffice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOffice")

    private static var salesDistrict_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesDistrict")

    private static var creationDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CreationDate")

    private static var createdByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CreatedByUser")

    private static var lastChangeDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "LastChangeDate")

    private static var lastChangeDateTime_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "LastChangeDateTime")

    private static var supplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "Supplier")

    private static var businessPartnerName1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BusinessPartnerName1")

    private static var businessPartnerName2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "BusinessPartnerName2")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PurchaseOrderByCustomer")

    private static var salesOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOrderDate")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TransactionCurrency")

    private static var sdDocumentReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SDDocumentReason")

    private static var pricingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PricingDate")

    private static var completeDeliveryIsDefined_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CompleteDeliveryIsDefined")

    private static var headerBillingBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "HeaderBillingBlockReason")

    private static var deliveryBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "DeliveryBlockReason")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsClassification")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsTransferLocation")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsLocation2")

    private static var incotermsVersion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IncotermsVersion")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPaymentTerms")

    private static var paymentMethod_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PaymentMethod")

    private static var assignmentReference_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "AssignmentReference")

    private static var referenceSDDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ReferenceSDDocument")

    private static var customerPurchaseOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPurchaseOrderType")

    private static var customerPurchaseOrderDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "CustomerPurchaseOrderDate")

    private static var totalNetAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalNetAmount")

    private static var requestedDeliveryDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "RequestedDeliveryDate")

    private static var overallSDProcessStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDProcessStatus")

    private static var overallSDProcessStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDProcessStatusDesc")

    private static var totalCreditCheckStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalCreditCheckStatus")

    private static var totalCreditCheckStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "TotalCreditCheckStatusDesc")

    private static var overallTotalDeliveryStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotalDeliveryStatus")

    private static var overallTotalDeliveryStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotalDeliveryStatusDesc")

    private static var overallTotDelivStatusColorHex_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotDelivStatusColorHex")

    private static var overallTotDelivStatusColorDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallTotDelivStatusColorDesc")

    private static var overallSDDocumentRejectionSts_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OverallSDDocumentRejectionSts")

    private static var ovrlSDDocumentRejectionStsDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "OvrlSDDocumentRejectionStsDesc")

    private static var zInternalDatePeriod_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZInternalDatePeriod")

    private static var fieldAux1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_1")

    private static var fieldAux2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_2")

    private static var fieldAux3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "FIELD_AUX_3")

    private static var zPlant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZPlant")

    private static var zPriceList_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "ZPriceList")

    private static var priceListType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "PriceListType")

    private static var xITSPDSFAxMobileIDSDH_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "xITSPDSFAxMobileID_SDH")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "IsFinal")

    private static var salesOperationType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "SalesOperationType")

    private static var toXITSPDSFAxACustomerMaster_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_CustomerMaster")

    private static var toXITSPDSFAxAIncoTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_IncoTerms")

    private static var toXITSPDSFAxAInvoice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_Invoice")

    private static var toXITSPDSFAxAPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_PaymentTerms")

    private static var toXITSPDSFAxAPricelists_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_Pricelists")

    private static var toXITSPDSFAxASalesAmounts_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesAmounts")

    private static var toXITSPDSFAxASalesDocType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesDocType")

    private static var toXITSPDSFAxASalesOrderPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrderPartner")

    private static var toXITSPDSFAxASalesOrdersItens_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrdersItens")

    private static var toXITSPDSFAxASalesOrdersObs_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrders_Obs")

    private static var toXITSPDSFAxASalesOrdersResns_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_SalesOrdersResns")

    private static var toXITSPDSFAxACustomerSalesArea_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdersType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdersType>())
    }

    open class var assignmentReference: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.assignmentReference_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.assignmentReference_ = value
            }
        }
    }

    open var assignmentReference: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.assignmentReference))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.assignmentReference, to: StringValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName1: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.businessPartnerName1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.businessPartnerName1_ = value
            }
        }
    }

    open var businessPartnerName1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.businessPartnerName1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.businessPartnerName1, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerName2: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.businessPartnerName2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.businessPartnerName2_ = value
            }
        }
    }

    open var businessPartnerName2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.businessPartnerName2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.businessPartnerName2, to: StringValue.of(optional: value))
        }
    }

    open class var completeDeliveryIsDefined: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined_ = value
            }
        }
    }

    open var completeDeliveryIsDefined: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.completeDeliveryIsDefined, to: BooleanValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrdersType {
        return CastRequired<XITSPDSFAxASalesOrdersType>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var creationDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.creationDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.creationDate_ = value
            }
        }
    }

    open var creationDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.creationDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.creationDate, to: value)
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var customerPurchaseOrderDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate_ = value
            }
        }
    }

    open var customerPurchaseOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.customerPurchaseOrderDate, to: value)
        }
    }

    open class var customerPurchaseOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.customerPurchaseOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.customerPurchaseOrderType_ = value
            }
        }
    }

    open var customerPurchaseOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.customerPurchaseOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.customerPurchaseOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryBlockReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.deliveryBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.deliveryBlockReason_ = value
            }
        }
    }

    open var deliveryBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.deliveryBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.deliveryBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux1: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.fieldAux1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.fieldAux1_ = value
            }
        }
    }

    open var fieldAux1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux1, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux2: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.fieldAux2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.fieldAux2_ = value
            }
        }
    }

    open var fieldAux2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux2, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux3: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.fieldAux3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.fieldAux3_ = value
            }
        }
    }

    open var fieldAux3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.fieldAux3, to: StringValue.of(optional: value))
        }
    }

    open class var headerBillingBlockReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.headerBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.headerBillingBlockReason_ = value
            }
        }
    }

    open var headerBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.headerBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.headerBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsVersion: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.incotermsVersion_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.incotermsVersion_ = value
            }
        }
    }

    open var incotermsVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.incotermsVersion))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.incotermsVersion, to: StringValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesOrder: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, soldToParty: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "SoldToParty", value: StringValue.of(optional: soldToParty)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open class var lastChangeDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.lastChangeDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.lastChangeDate_ = value
            }
        }
    }

    open var lastChangeDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.lastChangeDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.lastChangeDate, to: value)
        }
    }

    open class var lastChangeDateTime: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.lastChangeDateTime_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.lastChangeDateTime_ = value
            }
        }
    }

    open var lastChangeDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.lastChangeDateTime))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.lastChangeDateTime, to: value)
        }
    }

    open var old: XITSPDSFAxASalesOrdersType {
        return CastRequired<XITSPDSFAxASalesOrdersType>.from(self.oldEntity)
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDDocumentRejectionSts: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts_ = value
            }
        }
    }

    open var overallSDDocumentRejectionSts: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallSDDocumentRejectionSts, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallSDProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallSDProcessStatus_ = value
            }
        }
    }

    open var overallSDProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallSDProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallSDProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallSDProcessStatusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc_ = value
            }
        }
    }

    open var overallSDProcessStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallSDProcessStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotDelivStatusColorDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc_ = value
            }
        }
    }

    open var overallTotDelivStatusColorDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorDesc, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotDelivStatusColorHex: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex_ = value
            }
        }
    }

    open var overallTotDelivStatusColorHex: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallTotDelivStatusColorHex, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotalDeliveryStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus_ = value
            }
        }
    }

    open var overallTotalDeliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotalDeliveryStatusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc_ = value
            }
        }
    }

    open var overallTotalDeliveryStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.overallTotalDeliveryStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var ovrlSDDocumentRejectionStsDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc_ = value
            }
        }
    }

    open var ovrlSDDocumentRejectionStsDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.ovrlSDDocumentRejectionStsDesc, to: StringValue.of(optional: value))
        }
    }

    open class var paymentMethod: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.paymentMethod_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.paymentMethod_ = value
            }
        }
    }

    open var paymentMethod: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.paymentMethod))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.paymentMethod, to: StringValue.of(optional: value))
        }
    }

    open class var priceListType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.priceListType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.priceListType_ = value
            }
        }
    }

    open var priceListType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.priceListType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.priceListType, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.pricingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.pricingDate_ = value
            }
        }
    }

    open var pricingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.pricingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.pricingDate, to: value)
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var requestedDeliveryDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.requestedDeliveryDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.requestedDeliveryDate_ = value
            }
        }
    }

    open var requestedDeliveryDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.requestedDeliveryDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.requestedDeliveryDate, to: value)
        }
    }

    open class var salesDistrict: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesDistrict_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesDistrict_ = value
            }
        }
    }

    open var salesDistrict: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesDistrict))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesDistrict, to: StringValue.of(optional: value))
        }
    }

    open class var salesGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesGroup_ = value
            }
        }
    }

    open var salesGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesGroup, to: StringValue.of(optional: value))
        }
    }

    open class var salesOffice: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOffice_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOffice_ = value
            }
        }
    }

    open var salesOffice: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOffice))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOffice, to: StringValue.of(optional: value))
        }
    }

    open class var salesOperationType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOperationType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOperationType_ = value
            }
        }
    }

    open var salesOperationType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOperationType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOperationType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOrderDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOrderDate_ = value
            }
        }
    }

    open var salesOrderDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOrderDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOrderDate, to: value)
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var sdDocumentReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.sdDocumentReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.sdDocumentReason_ = value
            }
        }
    }

    open var sdDocumentReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.sdDocumentReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.sdDocumentReason, to: StringValue.of(optional: value))
        }
    }

    open class var soldToParty: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.soldToParty_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.soldToParty_ = value
            }
        }
    }

    open var soldToParty: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.soldToParty))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.soldToParty, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxACustomerMaster: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerMaster: XITSPDSFAxACustomerMasterType? {
        get {
            return CastOptional<XITSPDSFAxACustomerMasterType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerMaster, to: value)
        }
    }

    open class var toXITSPDSFAxACustomerSalesArea: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerSalesArea: XITSPDSFAxACustomerSalesAreaType? {
        get {
            return CastOptional<XITSPDSFAxACustomerSalesAreaType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxACustomerSalesArea, to: value)
        }
    }

    open class var toXITSPDSFAxAIncoTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms_ = value
            }
        }
    }

    open var toXITSPDSFAxAIncoTerms: XITSPDSFAxAIncoTermsType? {
        get {
            return CastOptional<XITSPDSFAxAIncoTermsType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAIncoTerms, to: value)
        }
    }

    open class var toXITSPDSFAxAInvoice: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice_ = value
            }
        }
    }

    open var toXITSPDSFAxAInvoice: Array<XITSPDSFAxAInvoiceType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice)).toArray(), Array<XITSPDSFAxAInvoiceType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxAInvoice.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxAPaymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms_ = value
            }
        }
    }

    open var toXITSPDSFAxAPaymentTerms: XITSPDSFAxAPaymentTermsType? {
        get {
            return CastOptional<XITSPDSFAxAPaymentTermsType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPaymentTerms, to: value)
        }
    }

    open class var toXITSPDSFAxAPricelists: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists_ = value
            }
        }
    }

    open var toXITSPDSFAxAPricelists: XITSPDSFAxAPricelistsType? {
        get {
            return CastOptional<XITSPDSFAxAPricelistsType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxAPricelists, to: value)
        }
    }

    open class var toXITSPDSFAxASalesAmounts: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesAmounts: Array<XITSPDSFAxASalesAmountsType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts)).toArray(), Array<XITSPDSFAxASalesAmountsType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesAmounts.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesDocType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesDocType: XITSPDSFAxASalesDocTypeType? {
        get {
            return CastOptional<XITSPDSFAxASalesDocTypeType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesDocType, to: value)
        }
    }

    open class var toXITSPDSFAxASalesOrderPartner: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderPartner: Array<XITSPDSFAxASalesOrderPartnerType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner)).toArray(), Array<XITSPDSFAxASalesOrderPartnerType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrderPartner.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrdersItens: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrdersItens: Array<XITSPDSFAxASalesOrdersItensType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens)).toArray(), Array<XITSPDSFAxASalesOrdersItensType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersItens.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrdersObs: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrdersObs: Array<XITSPDSFAxASalesOrdersObs> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs)).toArray(), Array<XITSPDSFAxASalesOrdersObs>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersObs.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrdersResns: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrdersResns: XITSPDSFAxASalesOrdersResnsType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersResnsType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.toXITSPDSFAxASalesOrdersResns, to: value)
        }
    }

    open class var totalCreditCheckStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.totalCreditCheckStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.totalCreditCheckStatus_ = value
            }
        }
    }

    open var totalCreditCheckStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.totalCreditCheckStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.totalCreditCheckStatus, to: StringValue.of(optional: value))
        }
    }

    open class var totalCreditCheckStatusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc_ = value
            }
        }
    }

    open var totalCreditCheckStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.totalCreditCheckStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var totalNetAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.totalNetAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.totalNetAmount_ = value
            }
        }
    }

    open var totalNetAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.totalNetAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.totalNetAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var xITSPDSFAxMobileIDSDH: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH_ = value
            }
        }
    }

    open var xITSPDSFAxMobileIDSDH: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.xITSPDSFAxMobileIDSDH, to: StringValue.of(optional: value))
        }
    }

    open class var zInternalDatePeriod: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.zInternalDatePeriod_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.zInternalDatePeriod_ = value
            }
        }
    }

    open var zInternalDatePeriod: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.zInternalDatePeriod))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.zInternalDatePeriod, to: value)
        }
    }

    open class var zPlant: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.zPlant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.zPlant_ = value
            }
        }
    }

    open var zPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.zPlant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.zPlant, to: StringValue.of(optional: value))
        }
    }

    open class var zPriceList: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                return XITSPDSFAxASalesOrdersType.zPriceList_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersType.self) }
            do {
                XITSPDSFAxASalesOrdersType.zPriceList_ = value
            }
        }
    }

    open var zPriceList: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersType.zPriceList))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersType.zPriceList, to: StringValue.of(optional: value))
        }
    }
}
