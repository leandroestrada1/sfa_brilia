// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxACustomerEmailXMLType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "Customer")

    private static var emailAddressXML_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "EmailAddressXML")

    private static var toXITSPDSFAxACustomerMaster_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType.property(withName: "to_xITSPDSFAxA_CustomerMaster")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxACustomerEmailXMLType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxACustomerEmailXMLType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxACustomerEmailXMLType>())
    }

    open func copy() -> XITSPDSFAxACustomerEmailXMLType {
        return CastRequired<XITSPDSFAxACustomerEmailXMLType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                return XITSPDSFAxACustomerEmailXMLType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                XITSPDSFAxACustomerEmailXMLType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerEmailXMLType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerEmailXMLType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var emailAddressXML: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                return XITSPDSFAxACustomerEmailXMLType.emailAddressXML_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                XITSPDSFAxACustomerEmailXMLType.emailAddressXML_ = value
            }
        }
    }

    open var emailAddressXML: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxACustomerEmailXMLType.emailAddressXML))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerEmailXMLType.emailAddressXML, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, emailAddressXML: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "EmailAddressXML", value: StringValue.of(optional: emailAddressXML))
    }

    open var old: XITSPDSFAxACustomerEmailXMLType {
        return CastRequired<XITSPDSFAxACustomerEmailXMLType>.from(self.oldEntity)
    }

    open class var toXITSPDSFAxACustomerMaster: Property {
        get {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                return XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxACustomerEmailXMLType.self)
            defer { objc_sync_exit(XITSPDSFAxACustomerEmailXMLType.self) }
            do {
                XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerMaster: XITSPDSFAxACustomerMasterType? {
        get {
            return CastOptional<XITSPDSFAxACustomerMasterType>.from(self.optionalValue(for: XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxACustomerEmailXMLType.toXITSPDSFAxACustomerMaster, to: value)
        }
    }
}
