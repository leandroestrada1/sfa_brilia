// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxASalesOrdersItensType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var salesOrder_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrder")

    private static var salesOrderItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItem")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DistributionChannel")

    private static var organizationDivision_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OrganizationDivision")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Customer")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BPCustomerNumber")

    private static var material_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Material")

    private static var salesOrderItemText_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItemText")

    private static var salesOrderType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderType")

    private static var customerName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CustomerName")

    private static var netAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetAmount")

    private static var netPriceAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetPriceAmount")

    private static var netPriceQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "NetPriceQuantityUnit")

    private static var billingBlockStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingBlockStatus")

    private static var billingBlockStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingBlockStatusDesc")

    private static var billingDocumentDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingDocumentDate")

    private static var salesDocumentRjcnReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesDocumentRjcnReason")

    private static var baseUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BaseUnit")

    private static var orderQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OrderQuantity")

    private static var itemGrossWeight_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemGrossWeight")

    private static var higherLevelItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "HigherLevelItem")

    private static var salesOrderItemCategory_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SalesOrderItemCategory")

    private static var purchaseOrderByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "PurchaseOrderByCustomer")

    private static var materialByCustomer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialByCustomer")

    private static var requestedQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "RequestedQuantity")

    private static var requestedQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "RequestedQuantityUnit")

    private static var itemNetWeight_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemNetWeight")

    private static var itemWeightUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemWeightUnit")

    private static var itemVolume_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemVolume")

    private static var itemVolumeUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemVolumeUnit")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "TransactionCurrency")

    private static var materialGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialGroup")

    private static var materialPricingGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "MaterialPricingGroup")

    private static var productionPlant_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ProductionPlant")

    private static var storageLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "StorageLocation")

    private static var deliveryGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryGroup")

    private static var deliveryPriority_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryPriority")

    private static var incotermsClassification_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsClassification")

    private static var incotermsTransferLocation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsTransferLocation")

    private static var incotermsLocation1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsLocation1")

    private static var incotermsLocation2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "IncotermsLocation2")

    private static var customerPaymentTerms_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CustomerPaymentTerms")

    private static var itemBillingBlockReason_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ItemBillingBlockReason")

    private static var sdProcessStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SDProcessStatus")

    private static var sdProcessStatusDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "SDProcessStatusDesc")

    private static var deliveryStatus_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryStatus")

    private static var pricingDate_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "PricingDate")

    private static var profitCenter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ProfitCenter")

    private static var referenceSDDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "ReferenceSDDocument")

    private static var taxAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "TaxAmount")

    private static var costAmount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "CostAmount")

    private static var subtotal1Amount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Subtotal1Amount")

    private static var subtotal2Amount_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "Subtotal2Amount")

    private static var overallTotDelivStatusColorHex_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OverallTotDelivStatusColorHex")

    private static var overallTotDelivStatusColorDesc_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "OverallTotDelivStatusColorDesc")

    private static var billingQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "BillingQuantity")

    private static var fieldAux1_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_1")

    private static var fieldAux2_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_2")

    private static var fieldAux3_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "FIELD_AUX_3")

    private static var deliveryStatusDescription_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "DeliveryStatusDescription")

    private static var toXITSPDSFAxACenterSupplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_CenterSupplier")

    private static var toXITSPDSFAxAProductMaster_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_ProductMaster")

    private static var toXITSPDSFAxASalesItemAmounts_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesItemAmounts")

    private static var toXITSPDSFAxASalesOrders_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesOrders")

    private static var toXITSPDSFAxASalesOrdItPrice_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType.property(withName: "to_xITSPDSFAxA_SalesOrdItPrice")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxASalesOrdersItensType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxASalesOrdersItensType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxASalesOrdersItensType>())
    }

    open class var baseUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.baseUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.baseUnit_ = value
            }
        }
    }

    open var baseUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.baseUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.baseUnit, to: StringValue.of(optional: value))
        }
    }

    open class var billingBlockStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.billingBlockStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.billingBlockStatus_ = value
            }
        }
    }

    open var billingBlockStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.billingBlockStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.billingBlockStatus, to: StringValue.of(optional: value))
        }
    }

    open class var billingBlockStatusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc_ = value
            }
        }
    }

    open var billingBlockStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.billingBlockStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var billingDocumentDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.billingDocumentDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.billingDocumentDate_ = value
            }
        }
    }

    open var billingDocumentDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.billingDocumentDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.billingDocumentDate, to: value)
        }
    }

    open class var billingQuantity: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.billingQuantity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.billingQuantity_ = value
            }
        }
    }

    open var billingQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.billingQuantity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.billingQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxASalesOrdersItensType {
        return CastRequired<XITSPDSFAxASalesOrdersItensType>.from(self.copyEntity())
    }

    open class var costAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.costAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.costAmount_ = value
            }
        }
    }

    open var costAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.costAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.costAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerName: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.customerName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.customerName_ = value
            }
        }
    }

    open var customerName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.customerName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.customerName, to: StringValue.of(optional: value))
        }
    }

    open class var customerPaymentTerms: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.customerPaymentTerms_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.customerPaymentTerms_ = value
            }
        }
    }

    open var customerPaymentTerms: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.customerPaymentTerms))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.customerPaymentTerms, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.deliveryGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.deliveryGroup_ = value
            }
        }
    }

    open var deliveryGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryGroup, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryPriority: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.deliveryPriority_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.deliveryPriority_ = value
            }
        }
    }

    open var deliveryPriority: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryPriority))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryPriority, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.deliveryStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.deliveryStatus_ = value
            }
        }
    }

    open var deliveryStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryStatus, to: StringValue.of(optional: value))
        }
    }

    open class var deliveryStatusDescription: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription_ = value
            }
        }
    }

    open var deliveryStatusDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.deliveryStatusDescription, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux1: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.fieldAux1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.fieldAux1_ = value
            }
        }
    }

    open var fieldAux1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux1, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux2: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.fieldAux2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.fieldAux2_ = value
            }
        }
    }

    open var fieldAux2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux2, to: StringValue.of(optional: value))
        }
    }

    open class var fieldAux3: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.fieldAux3_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.fieldAux3_ = value
            }
        }
    }

    open var fieldAux3: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux3))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.fieldAux3, to: StringValue.of(optional: value))
        }
    }

    open class var higherLevelItem: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.higherLevelItem_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.higherLevelItem_ = value
            }
        }
    }

    open var higherLevelItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.higherLevelItem))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.higherLevelItem, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsClassification: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.incotermsClassification_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.incotermsClassification_ = value
            }
        }
    }

    open var incotermsClassification: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsClassification))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsClassification, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation1: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.incotermsLocation1_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.incotermsLocation1_ = value
            }
        }
    }

    open var incotermsLocation1: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsLocation1))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsLocation1, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsLocation2: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.incotermsLocation2_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.incotermsLocation2_ = value
            }
        }
    }

    open var incotermsLocation2: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsLocation2))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsLocation2, to: StringValue.of(optional: value))
        }
    }

    open class var incotermsTransferLocation: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation_ = value
            }
        }
    }

    open var incotermsTransferLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.incotermsTransferLocation, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class var itemBillingBlockReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason_ = value
            }
        }
    }

    open var itemBillingBlockReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemBillingBlockReason, to: StringValue.of(optional: value))
        }
    }

    open class var itemGrossWeight: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemGrossWeight_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemGrossWeight_ = value
            }
        }
    }

    open var itemGrossWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemGrossWeight))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemGrossWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemNetWeight: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemNetWeight_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemNetWeight_ = value
            }
        }
    }

    open var itemNetWeight: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemNetWeight))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemNetWeight, to: DecimalValue.of(optional: value))
        }
    }

    open class var itemVolume: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemVolume_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemVolume_ = value
            }
        }
    }

    open var itemVolume: Double? {
        get {
            return DoubleValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemVolume))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemVolume, to: DoubleValue.of(optional: value))
        }
    }

    open class var itemVolumeUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemVolumeUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemVolumeUnit_ = value
            }
        }
    }

    open var itemVolumeUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemVolumeUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemVolumeUnit, to: StringValue.of(optional: value))
        }
    }

    open class var itemWeightUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.itemWeightUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.itemWeightUnit_ = value
            }
        }
    }

    open var itemWeightUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.itemWeightUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.itemWeightUnit, to: StringValue.of(optional: value))
        }
    }

    open class func key(salesOrder: String?, salesOrderItem: String?, salesOrganization: String?, distributionChannel: String?, organizationDivision: String?, customer: String?, bpCustomerNumber: String?) -> EntityKey {
        return EntityKey().with(name: "SalesOrder", value: StringValue.of(optional: salesOrder)).with(name: "SalesOrderItem", value: StringValue.of(optional: salesOrderItem)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "OrganizationDivision", value: StringValue.of(optional: organizationDivision)).with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber))
    }

    open class var material: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.material_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.material_ = value
            }
        }
    }

    open var material: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.material))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.material, to: StringValue.of(optional: value))
        }
    }

    open class var materialByCustomer: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.materialByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.materialByCustomer_ = value
            }
        }
    }

    open var materialByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.materialByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.materialByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var materialGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.materialGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.materialGroup_ = value
            }
        }
    }

    open var materialGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.materialGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.materialGroup, to: StringValue.of(optional: value))
        }
    }

    open class var materialPricingGroup: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.materialPricingGroup_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.materialPricingGroup_ = value
            }
        }
    }

    open var materialPricingGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.materialPricingGroup))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.materialPricingGroup, to: StringValue.of(optional: value))
        }
    }

    open class var netAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.netAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.netAmount_ = value
            }
        }
    }

    open var netAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.netAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.netAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var netPriceAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.netPriceAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.netPriceAmount_ = value
            }
        }
    }

    open var netPriceAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.netPriceAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.netPriceAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var netPriceQuantityUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit_ = value
            }
        }
    }

    open var netPriceQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.netPriceQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open var old: XITSPDSFAxASalesOrdersItensType {
        return CastRequired<XITSPDSFAxASalesOrdersItensType>.from(self.oldEntity)
    }

    open class var orderQuantity: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.orderQuantity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.orderQuantity_ = value
            }
        }
    }

    open var orderQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.orderQuantity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.orderQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var organizationDivision: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.organizationDivision_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.organizationDivision_ = value
            }
        }
    }

    open var organizationDivision: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.organizationDivision))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.organizationDivision, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotDelivStatusColorDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc_ = value
            }
        }
    }

    open var overallTotDelivStatusColorDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorDesc, to: StringValue.of(optional: value))
        }
    }

    open class var overallTotDelivStatusColorHex: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex_ = value
            }
        }
    }

    open var overallTotDelivStatusColorHex: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.overallTotDelivStatusColorHex, to: StringValue.of(optional: value))
        }
    }

    open class var pricingDate: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.pricingDate_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.pricingDate_ = value
            }
        }
    }

    open var pricingDate: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.pricingDate))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.pricingDate, to: value)
        }
    }

    open class var productionPlant: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.productionPlant_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.productionPlant_ = value
            }
        }
    }

    open var productionPlant: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.productionPlant))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.productionPlant, to: StringValue.of(optional: value))
        }
    }

    open class var profitCenter: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.profitCenter_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.profitCenter_ = value
            }
        }
    }

    open var profitCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.profitCenter))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.profitCenter, to: StringValue.of(optional: value))
        }
    }

    open class var purchaseOrderByCustomer: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer_ = value
            }
        }
    }

    open var purchaseOrderByCustomer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.purchaseOrderByCustomer, to: StringValue.of(optional: value))
        }
    }

    open class var referenceSDDocument: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.referenceSDDocument_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.referenceSDDocument_ = value
            }
        }
    }

    open var referenceSDDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.referenceSDDocument))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.referenceSDDocument, to: StringValue.of(optional: value))
        }
    }

    open class var requestedQuantity: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.requestedQuantity_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.requestedQuantity_ = value
            }
        }
    }

    open var requestedQuantity: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.requestedQuantity))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.requestedQuantity, to: DecimalValue.of(optional: value))
        }
    }

    open class var requestedQuantityUnit: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit_ = value
            }
        }
    }

    open var requestedQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.requestedQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var salesDocumentRjcnReason: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason_ = value
            }
        }
    }

    open var salesDocumentRjcnReason: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesDocumentRjcnReason, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrder: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrder_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrder_ = value
            }
        }
    }

    open var salesOrder: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrder))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrder, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItem: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrderItem_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrderItem_ = value
            }
        }
    }

    open var salesOrderItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItem))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItem, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItemCategory: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory_ = value
            }
        }
    }

    open var salesOrderItemCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItemCategory, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderItemText: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrderItemText_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrderItemText_ = value
            }
        }
    }

    open var salesOrderItemText: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItemText))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderItemText, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderType: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrderType_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrderType_ = value
            }
        }
    }

    open var salesOrderType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderType))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrderType, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var sdProcessStatus: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.sdProcessStatus_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.sdProcessStatus_ = value
            }
        }
    }

    open var sdProcessStatus: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.sdProcessStatus))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.sdProcessStatus, to: StringValue.of(optional: value))
        }
    }

    open class var sdProcessStatusDesc: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc_ = value
            }
        }
    }

    open var sdProcessStatusDesc: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.sdProcessStatusDesc, to: StringValue.of(optional: value))
        }
    }

    open class var storageLocation: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.storageLocation_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.storageLocation_ = value
            }
        }
    }

    open var storageLocation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.storageLocation))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.storageLocation, to: StringValue.of(optional: value))
        }
    }

    open class var subtotal1Amount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.subtotal1Amount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.subtotal1Amount_ = value
            }
        }
    }

    open var subtotal1Amount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.subtotal1Amount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.subtotal1Amount, to: DecimalValue.of(optional: value))
        }
    }

    open class var subtotal2Amount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.subtotal2Amount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.subtotal2Amount_ = value
            }
        }
    }

    open var subtotal2Amount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.subtotal2Amount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.subtotal2Amount, to: DecimalValue.of(optional: value))
        }
    }

    open class var taxAmount: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.taxAmount_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.taxAmount_ = value
            }
        }
    }

    open var taxAmount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.taxAmount))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.taxAmount, to: DecimalValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxACenterSupplier: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier_ = value
            }
        }
    }

    open var toXITSPDSFAxACenterSupplier: XITSPDSFAxACenterSupplierType? {
        get {
            return CastOptional<XITSPDSFAxACenterSupplierType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxACenterSupplier, to: value)
        }
    }

    open class var toXITSPDSFAxAProductMaster: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster_ = value
            }
        }
    }

    open var toXITSPDSFAxAProductMaster: XITSPDSFAxAProductMasterType? {
        get {
            return CastOptional<XITSPDSFAxAProductMasterType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxAProductMaster, to: value)
        }
    }

    open class var toXITSPDSFAxASalesItemAmounts: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesItemAmounts: Array<XITSPDSFAxASalesItemAmountsType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts)).toArray(), Array<XITSPDSFAxASalesItemAmountsType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesItemAmounts.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrdItPrice: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrdItPrice: Array<XITSPDSFAxASalesOrdItPriceType> {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice)).toArray(), Array<XITSPDSFAxASalesOrdItPriceType>())
        }
        set(value) {
            XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrdItPrice.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, Array<EntityValue>())))
        }
    }

    open class var toXITSPDSFAxASalesOrders: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrders: XITSPDSFAxASalesOrdersType? {
        get {
            return CastOptional<XITSPDSFAxASalesOrdersType>.from(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.toXITSPDSFAxASalesOrders, to: value)
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                return XITSPDSFAxASalesOrdersItensType.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxASalesOrdersItensType.self)
            defer { objc_sync_exit(XITSPDSFAxASalesOrdersItensType.self) }
            do {
                XITSPDSFAxASalesOrdersItensType.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxASalesOrdersItensType.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxASalesOrdersItensType.transactionCurrency, to: StringValue.of(optional: value))
        }
    }
}
