// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxATaxJurisdictionType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var country_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "Country")

    private static var postalCodeFrom_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "PostalCodeFrom")

    private static var postalCodeTo_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "PostalCodeTo")

    private static var region_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "Region")

    private static var taxJurisdictionCode_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType.property(withName: "TaxJurisdictionCode")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxATaxJurisdictionType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxATaxJurisdictionType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxATaxJurisdictionType>())
    }

    open func copy() -> XITSPDSFAxATaxJurisdictionType {
        return CastRequired<XITSPDSFAxATaxJurisdictionType>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                return XITSPDSFAxATaxJurisdictionType.country_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                XITSPDSFAxATaxJurisdictionType.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxJurisdictionType.country))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxJurisdictionType.country, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(country: String?, postalCodeFrom: String?, postalCodeTo: String?, region: String?, taxJurisdictionCode: String?) -> EntityKey {
        return EntityKey().with(name: "Country", value: StringValue.of(optional: country)).with(name: "PostalCodeFrom", value: StringValue.of(optional: postalCodeFrom)).with(name: "PostalCodeTo", value: StringValue.of(optional: postalCodeTo)).with(name: "Region", value: StringValue.of(optional: region)).with(name: "TaxJurisdictionCode", value: StringValue.of(optional: taxJurisdictionCode))
    }

    open var old: XITSPDSFAxATaxJurisdictionType {
        return CastRequired<XITSPDSFAxATaxJurisdictionType>.from(self.oldEntity)
    }

    open class var postalCodeFrom: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                return XITSPDSFAxATaxJurisdictionType.postalCodeFrom_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                XITSPDSFAxATaxJurisdictionType.postalCodeFrom_ = value
            }
        }
    }

    open var postalCodeFrom: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxJurisdictionType.postalCodeFrom))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxJurisdictionType.postalCodeFrom, to: StringValue.of(optional: value))
        }
    }

    open class var postalCodeTo: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                return XITSPDSFAxATaxJurisdictionType.postalCodeTo_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                XITSPDSFAxATaxJurisdictionType.postalCodeTo_ = value
            }
        }
    }

    open var postalCodeTo: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxJurisdictionType.postalCodeTo))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxJurisdictionType.postalCodeTo, to: StringValue.of(optional: value))
        }
    }

    open class var region: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                return XITSPDSFAxATaxJurisdictionType.region_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                XITSPDSFAxATaxJurisdictionType.region_ = value
            }
        }
    }

    open var region: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxJurisdictionType.region))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxJurisdictionType.region, to: StringValue.of(optional: value))
        }
    }

    open class var taxJurisdictionCode: Property {
        get {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                return XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxATaxJurisdictionType.self)
            defer { objc_sync_exit(XITSPDSFAxATaxJurisdictionType.self) }
            do {
                XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode_ = value
            }
        }
    }

    open var taxJurisdictionCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxATaxJurisdictionType.taxJurisdictionCode, to: StringValue.of(optional: value))
        }
    }
}
