// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class AttachmentContent: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var documentInfoRecordDocType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocType")

    private static var documentInfoRecordDocNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocNumber")

    private static var documentInfoRecordDocVersion_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocVersion")

    private static var documentInfoRecordDocPart_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentInfoRecordDocPart")

    private static var logicalDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LogicalDocument")

    private static var archiveDocumentID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ArchiveDocumentID")

    private static var linkedSAPObjectKey_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LinkedSAPObjectKey")

    private static var businessObjectTypeName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "BusinessObjectTypeName")

    private static var semanticObject_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "SemanticObject")

    private static var workstationApplication_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "WorkstationApplication")

    private static var fileSize_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "FileSize")

    private static var fileName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "FileName")

    private static var documentURL_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "DocumentURL")

    private static var mimeType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "MimeType")

    private static var content_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "Content")

    private static var createdByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreatedByUser")

    private static var createdByUserFullName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreatedByUserFullName")

    private static var creationDateTime_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "CreationDateTime")

    private static var businessObjectType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "BusinessObjectType")

    private static var lastChangedByUser_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LastChangedByUser")

    private static var lastChangedByUserFullName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "LastChangedByUserFullName")

    private static var changedDateTime_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ChangedDateTime")

    private static var storageCategory_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "StorageCategory")

    private static var archiveLinkRepository_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent.property(withName: "ArchiveLinkRepository")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.attachmentContent)
    }

    open class var archiveDocumentID: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.archiveDocumentID_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.archiveDocumentID_ = value
            }
        }
    }

    open var archiveDocumentID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.archiveDocumentID))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.archiveDocumentID, to: StringValue.of(optional: value))
        }
    }

    open class var archiveLinkRepository: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.archiveLinkRepository_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.archiveLinkRepository_ = value
            }
        }
    }

    open var archiveLinkRepository: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.archiveLinkRepository))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.archiveLinkRepository, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<AttachmentContent> {
        return ArrayConverter.convert(from.toArray(), Array<AttachmentContent>())
    }

    open class var businessObjectType: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.businessObjectType_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.businessObjectType_ = value
            }
        }
    }

    open var businessObjectType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.businessObjectType))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.businessObjectType, to: StringValue.of(optional: value))
        }
    }

    open class var businessObjectTypeName: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.businessObjectTypeName_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.businessObjectTypeName_ = value
            }
        }
    }

    open var businessObjectTypeName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.businessObjectTypeName))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.businessObjectTypeName, to: StringValue.of(optional: value))
        }
    }

    open class var changedDateTime: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.changedDateTime_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.changedDateTime_ = value
            }
        }
    }

    open var changedDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AttachmentContent.changedDateTime))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.changedDateTime, to: value)
        }
    }

    open class var content: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.content_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.content_ = value
            }
        }
    }

    open var content: Data? {
        get {
            return BinaryValue.optional(self.optionalValue(for: AttachmentContent.content))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.content, to: BinaryValue.of(optional: value))
        }
    }

    open func copy() -> AttachmentContent {
        return CastRequired<AttachmentContent>.from(self.copyEntity())
    }

    open class var createdByUser: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.createdByUser_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.createdByUser_ = value
            }
        }
    }

    open var createdByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.createdByUser))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.createdByUser, to: StringValue.of(optional: value))
        }
    }

    open class var createdByUserFullName: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.createdByUserFullName_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.createdByUserFullName_ = value
            }
        }
    }

    open var createdByUserFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.createdByUserFullName))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.createdByUserFullName, to: StringValue.of(optional: value))
        }
    }

    open class var creationDateTime: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.creationDateTime_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.creationDateTime_ = value
            }
        }
    }

    open var creationDateTime: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: AttachmentContent.creationDateTime))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.creationDateTime, to: value)
        }
    }

    open class var documentInfoRecordDocNumber: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.documentInfoRecordDocNumber_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.documentInfoRecordDocNumber_ = value
            }
        }
    }

    open var documentInfoRecordDocNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.documentInfoRecordDocNumber))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.documentInfoRecordDocNumber, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocPart: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.documentInfoRecordDocPart_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.documentInfoRecordDocPart_ = value
            }
        }
    }

    open var documentInfoRecordDocPart: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.documentInfoRecordDocPart))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.documentInfoRecordDocPart, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocType: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.documentInfoRecordDocType_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.documentInfoRecordDocType_ = value
            }
        }
    }

    open var documentInfoRecordDocType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.documentInfoRecordDocType))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.documentInfoRecordDocType, to: StringValue.of(optional: value))
        }
    }

    open class var documentInfoRecordDocVersion: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.documentInfoRecordDocVersion_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.documentInfoRecordDocVersion_ = value
            }
        }
    }

    open var documentInfoRecordDocVersion: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.documentInfoRecordDocVersion))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.documentInfoRecordDocVersion, to: StringValue.of(optional: value))
        }
    }

    open class var documentURL: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.documentURL_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.documentURL_ = value
            }
        }
    }

    open var documentURL: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.documentURL))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.documentURL, to: StringValue.of(optional: value))
        }
    }

    open class var fileName: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.fileName_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.fileName_ = value
            }
        }
    }

    open var fileName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.fileName))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.fileName, to: StringValue.of(optional: value))
        }
    }

    open class var fileSize: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.fileSize_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.fileSize_ = value
            }
        }
    }

    open var fileSize: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.fileSize))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.fileSize, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(documentInfoRecordDocType: String?, documentInfoRecordDocNumber: String?, documentInfoRecordDocVersion: String?, documentInfoRecordDocPart: String?, logicalDocument: String?, archiveDocumentID: String?, linkedSAPObjectKey: String?, businessObjectTypeName: String?) -> EntityKey {
        return EntityKey().with(name: "DocumentInfoRecordDocType", value: StringValue.of(optional: documentInfoRecordDocType)).with(name: "DocumentInfoRecordDocNumber", value: StringValue.of(optional: documentInfoRecordDocNumber)).with(name: "DocumentInfoRecordDocVersion", value: StringValue.of(optional: documentInfoRecordDocVersion)).with(name: "DocumentInfoRecordDocPart", value: StringValue.of(optional: documentInfoRecordDocPart)).with(name: "LogicalDocument", value: StringValue.of(optional: logicalDocument)).with(name: "ArchiveDocumentID", value: StringValue.of(optional: archiveDocumentID)).with(name: "LinkedSAPObjectKey", value: StringValue.of(optional: linkedSAPObjectKey)).with(name: "BusinessObjectTypeName", value: StringValue.of(optional: businessObjectTypeName))
    }

    open class var lastChangedByUser: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.lastChangedByUser_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.lastChangedByUser_ = value
            }
        }
    }

    open var lastChangedByUser: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.lastChangedByUser))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.lastChangedByUser, to: StringValue.of(optional: value))
        }
    }

    open class var lastChangedByUserFullName: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.lastChangedByUserFullName_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.lastChangedByUserFullName_ = value
            }
        }
    }

    open var lastChangedByUserFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.lastChangedByUserFullName))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.lastChangedByUserFullName, to: StringValue.of(optional: value))
        }
    }

    open class var linkedSAPObjectKey: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.linkedSAPObjectKey_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.linkedSAPObjectKey_ = value
            }
        }
    }

    open var linkedSAPObjectKey: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.linkedSAPObjectKey))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.linkedSAPObjectKey, to: StringValue.of(optional: value))
        }
    }

    open class var logicalDocument: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.logicalDocument_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.logicalDocument_ = value
            }
        }
    }

    open var logicalDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.logicalDocument))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.logicalDocument, to: StringValue.of(optional: value))
        }
    }

    open class var mimeType: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.mimeType_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.mimeType_ = value
            }
        }
    }

    open var mimeType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.mimeType))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.mimeType, to: StringValue.of(optional: value))
        }
    }

    open var old: AttachmentContent {
        return CastRequired<AttachmentContent>.from(self.oldEntity)
    }

    open class var semanticObject: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.semanticObject_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.semanticObject_ = value
            }
        }
    }

    open var semanticObject: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.semanticObject))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.semanticObject, to: StringValue.of(optional: value))
        }
    }

    open class var storageCategory: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.storageCategory_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.storageCategory_ = value
            }
        }
    }

    open var storageCategory: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.storageCategory))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.storageCategory, to: StringValue.of(optional: value))
        }
    }

    open class var workstationApplication: Property {
        get {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                return AttachmentContent.workstationApplication_
            }
        }
        set(value) {
            objc_sync_enter(AttachmentContent.self)
            defer { objc_sync_exit(AttachmentContent.self) }
            do {
                AttachmentContent.workstationApplication_ = value
            }
        }
    }

    open var workstationApplication: String? {
        get {
            return StringValue.optional(self.optionalValue(for: AttachmentContent.workstationApplication))
        }
        set(value) {
            self.setOptionalValue(for: AttachmentContent.workstationApplication, to: StringValue.of(optional: value))
        }
    }
}
