// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class IBillingDocumentBasicStdVHType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var billingDocument_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType.property(withName: "BillingDocument")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.iBillingDocumentBasicStdVHType)
    }

    open class func array(from: EntityValueList) -> Array<IBillingDocumentBasicStdVHType> {
        return ArrayConverter.convert(from.toArray(), Array<IBillingDocumentBasicStdVHType>())
    }

    open class var billingDocument: Property {
        get {
            objc_sync_enter(IBillingDocumentBasicStdVHType.self)
            defer { objc_sync_exit(IBillingDocumentBasicStdVHType.self) }
            do {
                return IBillingDocumentBasicStdVHType.billingDocument_
            }
        }
        set(value) {
            objc_sync_enter(IBillingDocumentBasicStdVHType.self)
            defer { objc_sync_exit(IBillingDocumentBasicStdVHType.self) }
            do {
                IBillingDocumentBasicStdVHType.billingDocument_ = value
            }
        }
    }

    open var billingDocument: String? {
        get {
            return StringValue.optional(self.optionalValue(for: IBillingDocumentBasicStdVHType.billingDocument))
        }
        set(value) {
            self.setOptionalValue(for: IBillingDocumentBasicStdVHType.billingDocument, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> IBillingDocumentBasicStdVHType {
        return CastRequired<IBillingDocumentBasicStdVHType>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(billingDocument: String?) -> EntityKey {
        return EntityKey().with(name: "BillingDocument", value: StringValue.of(optional: billingDocument))
    }

    open var old: IBillingDocumentBasicStdVHType {
        return CastRequired<IBillingDocumentBasicStdVHType>.from(self.oldEntity)
    }
}
