// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargePricing: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionCurrency")

    private static var conditionQuantity_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionQuantity")

    private static var conditionQuantityUnit_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionQuantityUnit")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionRateValue")

    private static var conditionType_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "ConditionType")

    private static var pricingProcedureCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "PricingProcedureCounter")

    private static var pricingProcedureStep_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "PricingProcedureStep")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "SalesQuotation")

    private static var transactionCurrency_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing.property(withName: "TransactionCurrency")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargePricing)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargePricing> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargePricing>())
    }

    open class var conditionCurrency: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.conditionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.conditionCurrency_ = value
            }
        }
    }

    open var conditionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.conditionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.conditionCurrency, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantity: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.conditionQuantity_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.conditionQuantity_ = value
            }
        }
    }

    open var conditionQuantity: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.conditionQuantity))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.conditionQuantity, to: StringValue.of(optional: value))
        }
    }

    open class var conditionQuantityUnit: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.conditionQuantityUnit_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.conditionQuantityUnit_ = value
            }
        }
    }

    open var conditionQuantityUnit: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.conditionQuantityUnit))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.conditionQuantityUnit, to: StringValue.of(optional: value))
        }
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.conditionRateValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionType: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.conditionType_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.conditionType_ = value
            }
        }
    }

    open var conditionType: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.conditionType))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.conditionType, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargePricing {
        return CastRequired<SalesOrderWithoutChargePricing>.from(self.copyEntity())
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(pricingProcedureCounter: String?, pricingProcedureStep: String?, salesQuotation: String?) -> EntityKey {
        return EntityKey().with(name: "PricingProcedureCounter", value: StringValue.of(optional: pricingProcedureCounter)).with(name: "PricingProcedureStep", value: StringValue.of(optional: pricingProcedureStep)).with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation))
    }

    open var old: SalesOrderWithoutChargePricing {
        return CastRequired<SalesOrderWithoutChargePricing>.from(self.oldEntity)
    }

    open class var pricingProcedureCounter: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.pricingProcedureCounter_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.pricingProcedureCounter_ = value
            }
        }
    }

    open var pricingProcedureCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.pricingProcedureCounter))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.pricingProcedureCounter, to: StringValue.of(optional: value))
        }
    }

    open class var pricingProcedureStep: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.pricingProcedureStep_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.pricingProcedureStep_ = value
            }
        }
    }

    open var pricingProcedureStep: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.pricingProcedureStep))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.pricingProcedureStep, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var transactionCurrency: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                return SalesOrderWithoutChargePricing.transactionCurrency_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargePricing.self)
            defer { objc_sync_exit(SalesOrderWithoutChargePricing.self) }
            do {
                SalesOrderWithoutChargePricing.transactionCurrency_ = value
            }
        }
    }

    open var transactionCurrency: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargePricing.transactionCurrency))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargePricing.transactionCurrency, to: StringValue.of(optional: value))
        }
    }
}
