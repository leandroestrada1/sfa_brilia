// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesQuotationItemPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var contactPerson_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Personnel")

    private static var salesQuotation_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "SalesQuotation")

    private static var supplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "Supplier")

    private static var toXITSPDSFAxASalesQuotationItemPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType.property(withName: "to_xITSPDSFAxA_SalesQuotationItemPartner")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesQuotationItemPartnerType)
    }

    open class func array(from: EntityValueList) -> Array<SalesQuotationItemPartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesQuotationItemPartnerType>())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesQuotationItemPartnerType {
        return CastRequired<SalesQuotationItemPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(salesQuotation: String?) -> EntityKey {
        return EntityKey().with(name: "SalesQuotation", value: StringValue.of(optional: salesQuotation))
    }

    open var old: SalesQuotationItemPartnerType {
        return CastRequired<SalesQuotationItemPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var salesQuotation: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.salesQuotation_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.salesQuotation_ = value
            }
        }
    }

    open var salesQuotation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.salesQuotation))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.salesQuotation, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesQuotationItemPartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesQuotationItemPartner: Property {
        get {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                return SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner_
            }
        }
        set(value) {
            objc_sync_enter(SalesQuotationItemPartnerType.self)
            defer { objc_sync_exit(SalesQuotationItemPartnerType.self) }
            do {
                SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesQuotationItemPartner: SalesQuotationItensType? {
        get {
            return CastOptional<SalesQuotationItensType>.from(self.optionalValue(for: SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner))
        }
        set(value) {
            self.setOptionalValue(for: SalesQuotationItemPartnerType.toXITSPDSFAxASalesQuotationItemPartner, to: value)
        }
    }
}
