// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class ACustSalesPartnerFuncType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "Customer")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "Division")

    private static var partnerCounter_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "PartnerCounter")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "PartnerFunction")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "BPCustomerNumber")

    private static var customerPartnerDescription_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "CustomerPartnerDescription")

    private static var defaultPartner_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "DefaultPartner")

    private static var authorizationGroup_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "AuthorizationGroup")

    private static var isFinal_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType.property(withName: "IsFinal")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.aCustSalesPartnerFuncType)
    }

    open class func array(from: EntityValueList) -> Array<ACustSalesPartnerFuncType> {
        return ArrayConverter.convert(from.toArray(), Array<ACustSalesPartnerFuncType>())
    }

    open class var authorizationGroup: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.authorizationGroup_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.authorizationGroup_ = value
            }
        }
    }

    open var authorizationGroup: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.authorizationGroup))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.authorizationGroup, to: StringValue.of(optional: value))
        }
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> ACustSalesPartnerFuncType {
        return CastRequired<ACustSalesPartnerFuncType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.customer_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.customer))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var customerPartnerDescription: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.customerPartnerDescription_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.customerPartnerDescription_ = value
            }
        }
    }

    open var customerPartnerDescription: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.customerPartnerDescription))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.customerPartnerDescription, to: StringValue.of(optional: value))
        }
    }

    open class var defaultPartner: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.defaultPartner_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.defaultPartner_ = value
            }
        }
    }

    open var defaultPartner: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.defaultPartner))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.defaultPartner, to: BooleanValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.division_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.division))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.division, to: StringValue.of(optional: value))
        }
    }

    open class var isFinal: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.isFinal_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.isFinal_ = value
            }
        }
    }

    open var isFinal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.isFinal))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.isFinal, to: BooleanValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, salesOrganization: String?, distributionChannel: String?, division: String?, partnerCounter: String?, partnerFunction: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division)).with(name: "PartnerCounter", value: StringValue.of(optional: partnerCounter)).with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction))
    }

    open var old: ACustSalesPartnerFuncType {
        return CastRequired<ACustSalesPartnerFuncType>.from(self.oldEntity)
    }

    open class var partnerCounter: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.partnerCounter_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.partnerCounter_ = value
            }
        }
    }

    open var partnerCounter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.partnerCounter))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.partnerCounter, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                return ACustSalesPartnerFuncType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(ACustSalesPartnerFuncType.self)
            defer { objc_sync_exit(ACustSalesPartnerFuncType.self) }
            do {
                ACustSalesPartnerFuncType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ACustSalesPartnerFuncType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: ACustSalesPartnerFuncType.salesOrganization, to: StringValue.of(optional: value))
        }
    }
}
