// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class ParameterValue: ComplexValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var conditionRecord_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "ConditionRecord")

    private static var conditionRateValue_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "ConditionRateValue")

    private static var value01_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value01")

    private static var value02_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value02")

    private static var value03_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value03")

    private static var value04_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value04")

    private static var value05_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value05")

    private static var value06_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value06")

    private static var value07_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value07")

    private static var value08_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value08")

    private static var value09_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value09")

    private static var value10_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value10")

    private static var value11_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value11")

    private static var value12_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value12")

    private static var value13_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value13")

    private static var value14_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value14")

    private static var value15_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value15")

    private static var value16_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value16")

    private static var value17_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value17")

    private static var value18_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value18")

    private static var value19_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value19")

    private static var value20_: Property = ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue.property(withName: "Value20")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.ComplexTypes.parameterValue)
    }

    open class var conditionRateValue: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.conditionRateValue_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.conditionRateValue_ = value
            }
        }
    }

    open var conditionRateValue: BigInteger? {
        get {
            return IntegerValue.optional(self.optionalValue(for: ParameterValue.conditionRateValue))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.conditionRateValue, to: IntegerValue.of(optional: value))
        }
    }

    open class var conditionRecord: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.conditionRecord_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.conditionRecord_ = value
            }
        }
    }

    open var conditionRecord: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.conditionRecord))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.conditionRecord, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> ParameterValue {
        return CastRequired<ParameterValue>.from(self.copyComplex())
    }

    open override var isProxy: Bool {
        return true
    }

    open var old: ParameterValue {
        return CastRequired<ParameterValue>.from(self.oldComplex)
    }

    open class var value01: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value01_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value01_ = value
            }
        }
    }

    open var value01: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value01))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value01, to: StringValue.of(optional: value))
        }
    }

    open class var value02: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value02_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value02_ = value
            }
        }
    }

    open var value02: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value02))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value02, to: StringValue.of(optional: value))
        }
    }

    open class var value03: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value03_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value03_ = value
            }
        }
    }

    open var value03: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value03))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value03, to: StringValue.of(optional: value))
        }
    }

    open class var value04: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value04_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value04_ = value
            }
        }
    }

    open var value04: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value04))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value04, to: StringValue.of(optional: value))
        }
    }

    open class var value05: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value05_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value05_ = value
            }
        }
    }

    open var value05: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value05))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value05, to: StringValue.of(optional: value))
        }
    }

    open class var value06: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value06_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value06_ = value
            }
        }
    }

    open var value06: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value06))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value06, to: StringValue.of(optional: value))
        }
    }

    open class var value07: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value07_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value07_ = value
            }
        }
    }

    open var value07: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value07))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value07, to: StringValue.of(optional: value))
        }
    }

    open class var value08: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value08_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value08_ = value
            }
        }
    }

    open var value08: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value08))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value08, to: StringValue.of(optional: value))
        }
    }

    open class var value09: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value09_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value09_ = value
            }
        }
    }

    open var value09: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value09))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value09, to: StringValue.of(optional: value))
        }
    }

    open class var value10: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value10_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value10_ = value
            }
        }
    }

    open var value10: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value10))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value10, to: StringValue.of(optional: value))
        }
    }

    open class var value11: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value11_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value11_ = value
            }
        }
    }

    open var value11: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value11))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value11, to: StringValue.of(optional: value))
        }
    }

    open class var value12: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value12_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value12_ = value
            }
        }
    }

    open var value12: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value12))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value12, to: StringValue.of(optional: value))
        }
    }

    open class var value13: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value13_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value13_ = value
            }
        }
    }

    open var value13: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value13))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value13, to: StringValue.of(optional: value))
        }
    }

    open class var value14: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value14_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value14_ = value
            }
        }
    }

    open var value14: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value14))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value14, to: StringValue.of(optional: value))
        }
    }

    open class var value15: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value15_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value15_ = value
            }
        }
    }

    open var value15: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value15))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value15, to: StringValue.of(optional: value))
        }
    }

    open class var value16: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value16_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value16_ = value
            }
        }
    }

    open var value16: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value16))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value16, to: StringValue.of(optional: value))
        }
    }

    open class var value17: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value17_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value17_ = value
            }
        }
    }

    open var value17: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value17))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value17, to: StringValue.of(optional: value))
        }
    }

    open class var value18: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value18_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value18_ = value
            }
        }
    }

    open var value18: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value18))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value18, to: StringValue.of(optional: value))
        }
    }

    open class var value19: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value19_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value19_ = value
            }
        }
    }

    open var value19: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value19))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value19, to: StringValue.of(optional: value))
        }
    }

    open class var value20: Property {
        get {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                return ParameterValue.value20_
            }
        }
        set(value) {
            objc_sync_enter(ParameterValue.self)
            defer { objc_sync_exit(ParameterValue.self) }
            do {
                ParameterValue.value20_ = value
            }
        }
    }

    open var value20: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ParameterValue.value20))
        }
        set(value) {
            self.setOptionalValue(for: ParameterValue.value20, to: StringValue.of(optional: value))
        }
    }
}
