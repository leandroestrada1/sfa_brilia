// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class XITSPDSFAxAPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "Customer")

    private static var bpCustomerNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BPCustomerNumber")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "PartnerFunction")

    private static var businessPartnerAddressID_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BusinessPartnerAddressID")

    private static var salesOrganization_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "SalesOrganization")

    private static var distributionChannel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "DistributionChannel")

    private static var division_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "Division")

    private static var partnerFunctionName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "PartnerFunctionName")

    private static var businessPartnerFullName_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BusinessPartnerFullName")

    private static var bpTaxNumber_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "BPTaxNumber")

    private static var toXITSPDSFAxACustomerSalesArea_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType.property(withName: "to_xITSPDSFAxA_CustomerSalesArea")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.xITSPDSFAxAPartnerType)
    }

    open class func array(from: EntityValueList) -> Array<XITSPDSFAxAPartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<XITSPDSFAxAPartnerType>())
    }

    open class var bpCustomerNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.bpCustomerNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.bpCustomerNumber_ = value
            }
        }
    }

    open var bpCustomerNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.bpCustomerNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.bpCustomerNumber, to: StringValue.of(optional: value))
        }
    }

    open class var bpTaxNumber: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.bpTaxNumber_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.bpTaxNumber_ = value
            }
        }
    }

    open var bpTaxNumber: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.bpTaxNumber))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.bpTaxNumber, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerAddressID: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.businessPartnerAddressID_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.businessPartnerAddressID_ = value
            }
        }
    }

    open var businessPartnerAddressID: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.businessPartnerAddressID))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.businessPartnerAddressID, to: StringValue.of(optional: value))
        }
    }

    open class var businessPartnerFullName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.businessPartnerFullName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.businessPartnerFullName_ = value
            }
        }
    }

    open var businessPartnerFullName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.businessPartnerFullName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.businessPartnerFullName, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> XITSPDSFAxAPartnerType {
        return CastRequired<XITSPDSFAxAPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open class var distributionChannel: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.distributionChannel_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.distributionChannel_ = value
            }
        }
    }

    open var distributionChannel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.distributionChannel))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.distributionChannel, to: StringValue.of(optional: value))
        }
    }

    open class var division: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.division_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.division_ = value
            }
        }
    }

    open var division: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.division))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.division, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(customer: String?, bpCustomerNumber: String?, partnerFunction: String?, businessPartnerAddressID: String?, salesOrganization: String?, distributionChannel: String?, division: String?) -> EntityKey {
        return EntityKey().with(name: "Customer", value: StringValue.of(optional: customer)).with(name: "BPCustomerNumber", value: StringValue.of(optional: bpCustomerNumber)).with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction)).with(name: "BusinessPartnerAddressID", value: StringValue.of(optional: businessPartnerAddressID)).with(name: "SalesOrganization", value: StringValue.of(optional: salesOrganization)).with(name: "DistributionChannel", value: StringValue.of(optional: distributionChannel)).with(name: "Division", value: StringValue.of(optional: division))
    }

    open var old: XITSPDSFAxAPartnerType {
        return CastRequired<XITSPDSFAxAPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var partnerFunctionName: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.partnerFunctionName_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.partnerFunctionName_ = value
            }
        }
    }

    open var partnerFunctionName: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.partnerFunctionName))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.partnerFunctionName, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrganization: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.salesOrganization_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.salesOrganization_ = value
            }
        }
    }

    open var salesOrganization: String? {
        get {
            return StringValue.optional(self.optionalValue(for: XITSPDSFAxAPartnerType.salesOrganization))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.salesOrganization, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxACustomerSalesArea: Property {
        get {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                return XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea_
            }
        }
        set(value) {
            objc_sync_enter(XITSPDSFAxAPartnerType.self)
            defer { objc_sync_exit(XITSPDSFAxAPartnerType.self) }
            do {
                XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea_ = value
            }
        }
    }

    open var toXITSPDSFAxACustomerSalesArea: XITSPDSFAxACustomerSalesAreaType? {
        get {
            return CastOptional<XITSPDSFAxACustomerSalesAreaType>.from(self.optionalValue(for: XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea))
        }
        set(value) {
            self.setOptionalValue(for: XITSPDSFAxAPartnerType.toXITSPDSFAxACustomerSalesArea, to: value)
        }
    }
}
