// # Proxy Compiler 19.1.2-c24a64-20190318

import Foundation
import SAPOData

open class SalesOrderWithoutChargeItemPartnerType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var contactPerson_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "ContactPerson")

    private static var customer_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Customer")

    private static var partnerFunction_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "PartnerFunction")

    private static var personnel_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Personnel")

    private static var salesOrderWithoutCharge_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "SalesOrderWithoutCharge")

    private static var salesOrderWithoutChargeItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "SalesOrderWithoutChargeItem")

    private static var supplier_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "Supplier")

    private static var toXITSPDSFAxASalesOrderWithoutChargeItem_: Property = ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType.property(withName: "to_xITSPDSFAxA_SalesOrderWithoutChargeItem")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: ITSPDSFAAPISRVEntitiesMetadata.EntityTypes.salesOrderWithoutChargeItemPartnerType)
    }

    open class func array(from: EntityValueList) -> Array<SalesOrderWithoutChargeItemPartnerType> {
        return ArrayConverter.convert(from.toArray(), Array<SalesOrderWithoutChargeItemPartnerType>())
    }

    open class var contactPerson: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.contactPerson_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.contactPerson_ = value
            }
        }
    }

    open var contactPerson: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.contactPerson))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.contactPerson, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> SalesOrderWithoutChargeItemPartnerType {
        return CastRequired<SalesOrderWithoutChargeItemPartnerType>.from(self.copyEntity())
    }

    open class var customer: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.customer_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.customer_ = value
            }
        }
    }

    open var customer: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.customer))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.customer, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(partnerFunction: String?, salesOrderWithoutCharge: String?, salesOrderWithoutChargeItem: String?) -> EntityKey {
        return EntityKey().with(name: "PartnerFunction", value: StringValue.of(optional: partnerFunction)).with(name: "SalesOrderWithoutCharge", value: StringValue.of(optional: salesOrderWithoutCharge)).with(name: "SalesOrderWithoutChargeItem", value: StringValue.of(optional: salesOrderWithoutChargeItem))
    }

    open var old: SalesOrderWithoutChargeItemPartnerType {
        return CastRequired<SalesOrderWithoutChargeItemPartnerType>.from(self.oldEntity)
    }

    open class var partnerFunction: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.partnerFunction_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.partnerFunction_ = value
            }
        }
    }

    open var partnerFunction: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.partnerFunction))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.partnerFunction, to: StringValue.of(optional: value))
        }
    }

    open class var personnel: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.personnel_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.personnel_ = value
            }
        }
    }

    open var personnel: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.personnel))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.personnel, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutCharge: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge_ = value
            }
        }
    }

    open var salesOrderWithoutCharge: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutCharge, to: StringValue.of(optional: value))
        }
    }

    open class var salesOrderWithoutChargeItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem_ = value
            }
        }
    }

    open var salesOrderWithoutChargeItem: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.salesOrderWithoutChargeItem, to: StringValue.of(optional: value))
        }
    }

    open class var supplier: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.supplier_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.supplier_ = value
            }
        }
    }

    open var supplier: String? {
        get {
            return StringValue.optional(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.supplier))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.supplier, to: StringValue.of(optional: value))
        }
    }

    open class var toXITSPDSFAxASalesOrderWithoutChargeItem: Property {
        get {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                return SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem_
            }
        }
        set(value) {
            objc_sync_enter(SalesOrderWithoutChargeItemPartnerType.self)
            defer { objc_sync_exit(SalesOrderWithoutChargeItemPartnerType.self) }
            do {
                SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem_ = value
            }
        }
    }

    open var toXITSPDSFAxASalesOrderWithoutChargeItem: SalesOrderWithoutChargeItemType? {
        get {
            return CastOptional<SalesOrderWithoutChargeItemType>.from(self.optionalValue(for: SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem))
        }
        set(value) {
            self.setOptionalValue(for: SalesOrderWithoutChargeItemPartnerType.toXITSPDSFAxASalesOrderWithoutChargeItem, to: value)
        }
    }
}
