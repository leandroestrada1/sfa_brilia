//
// AppDelegate.swift
// sfa_produto
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 09/01/19
//

import GoogleMaps
import SAPCommon
import SAPFiori
import SAPFioriFlows
import SAPFoundation
import SAPOData
import SAPOfflineOData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate, OnboardingManagerDelegate, ConnectivityObserver, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    
    public let logger = Logger.shared(named: "AppDelegateLogger")
    var sfaEntities: ITSPDSFAAPISRVEntities<OfflineODataProvider>!
    private(set) var isOfflineStoreOpened = false
    
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Set a FUIInfoViewController as the rootViewController, since there it is none set in the Main.storyboard
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
        
        ConnectivityReceiver.registerObserver(self)
        OnboardingManager.shared.delegate = self
        OnboardingManager.shared.onboardOrRestore()
        
        GMSServices.provideAPIKey("AIzaSyB_Y5MSkVpRO9VCqFy5zirGcfF2SSPoDkA")
        
        return true
    }
    
    // To only support portrait orientation during onboarding
    func application(_: UIApplication, supportedInterfaceOrientationsFor _: UIWindow?) -> UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIInterfaceOrientationMask.landscape
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    // Delegate to OnboardingManager.
    func applicationDidEnterBackground(_: UIApplication) {
        OnboardingManager.shared.applicationDidEnterBackground()
        self.closeOfflineStore()
    }
    
    // Delegate to OnboardingManager.
    func applicationWillEnterForeground(_: UIApplication) {
        OnboardingManager.shared.applicationWillEnterForeground {
            self.openOfflineStore(onboarding: false)
        }
    }
    
    func onboarded(onboardingContext: OnboardingContext, onboarding: Bool) {
        let configurationURL = URL(string: URLBase.urlSession())!
        self.configureOData(onboardingContext.sapURLSession, configurationURL, onboarding)
        
        self.registerForRemoteNotification(onboardingContext.sapURLSession, onboardingContext.info[.sapcpmsSettingsParameters] as! SAPcpmsSettingsParameters)
        
        self.openOfflineStore(onboarding: onboarding)
    }
    
    private func setRootViewController() {
        DispatchQueue.main.async {
            var finalizeOnboarding: UIViewController?
            if UIDevice.current.userInterfaceIdiom == .pad {
                finalizeOnboarding = UIStoryboard(name: "iPad", bundle: Bundle.main).instantiateInitialViewController()
            } else if UIDevice.current.userInterfaceIdiom == .phone {
                finalizeOnboarding = UIStoryboard(name: "iPhone", bundle: Bundle.main).instantiateInitialViewController()
            }
            finalizeOnboarding?.modalPresentationStyle = .currentContext
            self.window?.rootViewController = finalizeOnboarding
        }
    }
    
    // MARK: - Split view
    
    func splitViewController(_: UISplitViewController, collapseSecondary _: UIViewController, onto _: UIViewController) -> Bool {
        // The first Collection will be selected automatically, so we never discard showing the secondary ViewController
        return false
    }
    
    // MARK: - Remote Notification handling
    
    private var deviceToken: Data?
    
    func application(_: UIApplication, willFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        UIApplication.shared.registerForRemoteNotifications()
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { _, _ in
            // Enable or disable features based on authorization.
        }
        center.delegate = self
        return true
    }
    
    // Called to let your app know which action was selected by the user for a given notification.
    func userNotificationCenter(_: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.logger.info("App opened via user selecting notification: \(response.notification.request.content.body)")
        // Here is where you want to take action to handle the notification, maybe navigate the user to a given screen.
        completionHandler()
    }
    
    // Called when a notification is delivered to a foreground app.
    func userNotificationCenter(_: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        self.logger.info("Remote Notification arrived while app was in foreground: \(notification.request.content.body)")
        // Currently we are presenting the notification alert as the application were in the background.
        // If you have handled the notification and do not want to display an alert, call the completionHandler with empty options: completionHandler([])
        completionHandler([.alert, .sound])
    }
    
    func registerForRemoteNotification(_ urlSession: SAPURLSession, _ settingsParameters: SAPcpmsSettingsParameters) {
        guard let deviceToken = self.deviceToken else {
            // Device token has not been acquired
            return
        }
        
        let remoteNotificationClient = SAPcpmsRemoteNotificationClient(sapURLSession: urlSession, settingsParameters: settingsParameters)
        remoteNotificationClient.registerDeviceToken(deviceToken) { error in
            if let error = error {
                self.logger.error("Register DeviceToken failed", error: error)
                return
            }
            self.logger.info("Register DeviceToken succeeded")
        }
    }
    
    func application(_: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        self.deviceToken = deviceToken
    }
    
    func application(_: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        self.logger.error("Failed to register for Remote Notification", error: error)
    }
    
    // MARK: - Configure Offline OData
    
    fileprivate func showOfflineODataError(_ error: OfflineODataError, message: String) {
        DispatchQueue.main.async(execute: {
            let errorOfflineInfoVC = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
            errorOfflineInfoVC.informationTextView.text = "\(message): \(error)"
            errorOfflineInfoVC.informationTextView.textColor = UIColor.black
            errorOfflineInfoVC.informationTextView.textAlignment = .center
            errorOfflineInfoVC.informationTextView.isHidden = false
            errorOfflineInfoVC.loadingIndicatorView.dismiss()
            self.window!.rootViewController = errorOfflineInfoVC
        })
    }
    
    public func performOfflineRefresh() {
        self.uploadOfflineStore()
        self.downloadOfflineStore()
    }
    
    private func openOfflineStore(onboarding: Bool) {

        DispatchQueue.main.async {
            let openingOfflineInfoVC = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
            openingOfflineInfoVC.informationTextView.text = "Opening and Syncing Offline Store."
            openingOfflineInfoVC.informationTextView.textAlignment = .center
            openingOfflineInfoVC.informationTextView.textColor = UIColor.black
            openingOfflineInfoVC.informationTextView.isHidden = false
            self.window!.rootViewController = openingOfflineInfoVC
        }
        if !self.isOfflineStoreOpened {
            // The OfflineODataProvider needs to be opened before performing any operations.
            self.sfaEntities.open { error in
                if let error = error {
                    self.logger.error("Could not open offline store.", error: error)
                    self.showOfflineODataError(error, message: "Could not open offline store")
                    return
                }
                self.isOfflineStoreOpened = true
                self.logger.info("Offline store opened.")
                if !onboarding {
                    // You might want to consider doing the synchronization based on an explicit user interaction instead of automatically synchronizing during startup
                    DispatchQueue.main.async {
                        let vc = self.window?.rootViewController
                        let storyboard = UIStoryboard(name: "iPad", bundle: nil)
                        let salesOrganizationVC = storyboard.instantiateViewController(withIdentifier: "salesOrganizationID") as! SalesOrganizationLoginVC
                        vc!.view.addSubview(salesOrganizationVC.view)
                        vc?.addChild(salesOrganizationVC)
                        salesOrganizationVC.didMove(toParent: vc)
                    }
                } else {
                    // chama organização de vendas aqui
                    DispatchQueue.main.async {
                        let vc = self.window?.rootViewController
                        let storyboard = UIStoryboard(name: "iPad", bundle: nil)
                        let salesOrganizationVC = storyboard.instantiateViewController(withIdentifier: "salesOrganizationID") as! SalesOrganizationLoginVC
                        vc!.view.addSubview(salesOrganizationVC.view)
                        vc?.addChild(salesOrganizationVC)
                        salesOrganizationVC.didMove(toParent: vc)
                    }
                }
            }
        } else if !onboarding {
            // You might want to consider doing the synchronization based on an explicit user interaction instead of automatically synchronizing during startup
            self.performOfflineRefresh()
        }
    }
    
    public func salesOrganizationSetRootViewController() {
        self.setRootViewController()
    }
    
    private func closeOfflineStore() {
        if self.isOfflineStoreOpened {
            do {
                // the Offline store should be closed when it is no longer used.
                try self.sfaEntities.close()
                self.isOfflineStoreOpened = false
            } catch {
                self.logger.error("Offline Store closing failed.")
            }
        }
        self.logger.info("Offline Store closed.")
    }
    
    private func downloadOfflineStore() {
        if !self.isOfflineStoreOpened {
            self.logger.error("Offline Store still closed")
            return
        }
        // the download function updates the client’s offline store from the backend.
        self.sfaEntities.download { error in
            if let error = error {
                self.logger.error("Offline Store download failed.", error: error)
            } else {
                self.logger.info("Offline Store is downloaded.")
            }
            // self.setRootViewController()
        }
    }
    
    private func uploadOfflineStore() {
        if !self.isOfflineStoreOpened {
            self.logger.error("Offline Store still closed")
            return
        }
        // the upload function updates the backend from the client’s offline store.
        self.sfaEntities.upload { error in
            if let error = error {
                self.logger.error("Offline Store upload failed.", error: error)
                return
            }
            self.logger.info("Offline Store is uploaded.")
        }
    }
    
    // MARK: - ConnectivityObserver implementation
    
    func connectionEstablished() {
        // connection established
    }
    
    func connectionChanged(_ previousReachabilityType: ReachabilityType, reachabilityType _: ReachabilityType) {
        // connection changed
        if case previousReachabilityType = ReachabilityType.offline {
            // connection established
        }
    }
    
    func connectionLost() {
        // connection lost
    }
}

class OfflineODataDelegateSample: OfflineODataDelegate {
    private let logger = Logger.shared(named: "AppDelegateLogger")
    
    public func offlineODataProvider(_: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        self.logger.debug("downloadProgress: \(progress.bytesSent)  \(progress.bytesReceived)")
    }
    
    public func offlineODataProvider(_: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        self.logger.debug("downloadProgress: \(progress.bytesReceived)  \(progress.fileSize)")
    }
    
    public func offlineODataProvider(_: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        self.logger.debug("downloadProgress: \(progress.bytesSent)  \(progress.bytesReceived)")
    }
    
    public func offlineODataProvider(_: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        self.logger.error("requestFailed: \(request.httpStatusCode)")
    }
    
    // The OfflineODataStoreState is a Swift OptionSet. Use the set operation to retrieve each setting.
    private func storeState2String(_ state: OfflineODataStoreState) -> String {
        var result = ""
        if state.contains(.opening) {
            result = result + ":opening"
        }
        if state.contains(.open) {
            result = result + ":open"
        }
        if state.contains(.closed) {
            result = result + ":closed"
        }
        if state.contains(.downloading) {
            result = result + ":downloading"
        }
        if state.contains(.uploading) {
            result = result + ":uploading"
        }
        if state.contains(.initializing) {
            result = result + ":initializing"
        }
        if state.contains(.fileDownloading) {
            result = result + ":fileDownloading"
        }
        if state.contains(.initialCommunication) {
            result = result + ":initialCommunication"
        }
        return result
    }
    
    public func offlineODataProvider(_: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        let stateString = storeState2String(newState)
        self.logger.debug("stateChanged: \(stateString)")
    }
}
