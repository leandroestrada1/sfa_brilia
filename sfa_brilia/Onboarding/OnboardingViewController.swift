//
//  OnboardingViewController.swift
//  sfa_produto
//
//  Created by Cristiano Salla Lunardi on 17/04/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

protocol OnboardingDelegate: NSObjectProtocol {
    func nextStep(step: OnboardingSteps)
}

enum OnboardingSteps: Int {
    case welcome
    case partners
    case passcode
    case databaseDownload
    case salesOrganization
}

class OnboardingViewController: UIViewController, OnboardingDelegate {
    @IBOutlet var contentView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        let view = LoginWelcome(frame: self.contentView.frame)
//        view.onboardingDelegate = self
//        self.replaceContentView(view: view)

        // self.replaceContentView(view: LoginSalesOrganizationView(frame: self.contentView.frame))
    }

    func replaceContentView(view: UIView) {
        self.contentView.removeFromSuperview()
        self.contentView = view
        self.view.addSubview(self.contentView)
    }

    func nextStep(step: OnboardingSteps) {
        switch step {
        case .welcome: break
//            let view = LoginWelcome(frame: self.contentView.frame)
//            view.onboardingDelegate = self
//            self.replaceContentView(view: view)
        case .partners: break
            DispatchQueue.main.async {
//                let view = LoginPartnerView(arrayTuple: OfflineODataAccess.shared.partners, frame: self.contentView.frame)
//                view.onboardingDelegate = self
//                self.replaceContentView(view: view)
            }
        case .passcode: break
            DispatchQueue.main.async {
                // TODO: mudar view
//                let view = LoginPartnerView(arrayTuple: OfflineODataAccess.shared.partners, frame: self.contentView.frame)
//                view.onboardingDelegate = self
//                self.replaceContentView(view: view)
            }
        case .databaseDownload: break
            DispatchQueue.main.async {
                // TODO: mudar view
//                let view = LoginPartnerView(arrayTuple: OfflineODataAccess.shared.partners, frame: self.contentView.frame)
//                view.onboardingDelegate = self
//                self.replaceContentView(view: view)
            }
        case .salesOrganization: break
            DispatchQueue.main.async {
                // TODO: mudar view
//                let view = LoginPartnerView(arrayTuple: OfflineODataAccess.shared.partners, frame: self.contentView.frame)
//                view.onboardingDelegate = self
//                self.replaceContentView(view: view)
            }
        }
    }
}
