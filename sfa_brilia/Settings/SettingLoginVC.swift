//
//  SettingsVC.swift
//  sfa_brilia
//
//  Created by Eduardo Tarallo on 11/09/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit

class SettingLoginVC: UIViewController {

    
    @IBOutlet var layerBtn: [UIButton]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        self.title = "Configurações"
        

        layerBtn[0].layer.cornerRadius = 10
        layerBtn[0].layer.borderWidth = 1
        layerBtn[0].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        layerBtn[1].layer.cornerRadius = 10
        layerBtn[1].layer.borderWidth = 1
        layerBtn[1].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        layerBtn[2].layer.cornerRadius = 10
        layerBtn[2].layer.borderWidth = 1
        layerBtn[2].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        layerBtn[3].layer.cornerRadius = 10
        layerBtn[3].layer.borderWidth = 1
        layerBtn[3].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        layerBtn[4].layer.cornerRadius = 10
        layerBtn[4].layer.borderWidth = 1
        layerBtn[4].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        

    }
    



}
